 <?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Excellib {
    public function __construct(){

    }

    public function __get($var){
        return get_instance()->$var;
    }

    public function read_excel($path = null){
        include APPPATH.'third_party/SimpleXLSX.php';
        $return_data = array(
                            "status"=>false,
                            "msg"=>"fail",
                            "data"=>""
                        );

        if($path != null){
            $path = 'assets/excel_upload/excel_example/coba.xlsx';
            if(file_exists($path)){
                if ( $xlsx = SimpleXLSX::parse($path) ) {
                    $return_data["status"] = true;
                    $return_data["msg"] = "get data success";
                    $return_data["data"] = $xlsx->rows();
                } else {
                    $return_data["msg"] = SimpleXLSX::parseError();
                }
            }else{
                $return_data["msg"] = "file_tidak tersedia";
            }
        }else{
            $return_data["msg"] = "param null";
        }
        
        return $return_data;
    }
    
}