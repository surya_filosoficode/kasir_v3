<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Updateitem extends CI_Controller {

	public function __construct(){
        parent::__construct(); 
        $this->load->model('admin/main_item', 'mi');
        $this->load->model('main/mainmodel', 'mm');
        $this->load->model('other/item_main', 'im');

        $this->load->library("response_message");
        $this->load->library("Auth_v0");
        
        $this->auth_v0->check_session_active_ad();

        $this->load->library('set_record_stok');
        $this->set_record_stok->insert_record();
    }


#===============================================================================
#-----------------------------------produk_main---------------------------------
#===============================================================================

	public function index(){
		$data["page"] = "produk_main_update";
        $data["list_data"]          = $this->im->get_data_produk_where(array("is_del_item"=>"0"));
        $data["list_data_brand"]    = $this->im->get_data_brand_where(array("is_del_brand"=>"0"));

        $distinct_nama = $this->im->get_distinct_nama_produk(array());
        $distinct_nama_str = "";
        foreach ($distinct_nama as $key => $value) {
            $distinct_nama_str .= "<option value=\"".$value->nama_item."\">";
        }

        // print_r($distinct_nama);
        $data["option_nama_item"] = $distinct_nama_str;
        // print_r($data);
		$this->load->view('index', $data);
	}

    public function val_form_insert_item(){
        $config_val_input = array(
                array(
                    'field'=>'stok',
                    'label'=>'stok',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'stok_opnam',
                    'label'=>'stok_opnam',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function get_data_item(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array();

        if(isset($_POST["id_item"])){
            $id_item = $this->input->post("id_item");

            // print_r($id_item);
            $data = $this->im->get_data_produk_where_each(array("id_item"=>$id_item));
            if($data){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
                $msg_detail["data"] = $data;
            }
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

    public function update_item(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "stok"=>"",
                    "stok_opnam"=>""
                );

        if($this->val_form_insert_item()){
            $stok        = $this->input->post("stok");
            $stok_opnam  = $this->input->post("stok_opnam");
            $id_item     = $this->input->post("id_item");

            $data_session = $this->auth_v0->get_session();
                $admin_create   = $data_session["id_admin"];
                $date_update    = date("Y-m-d H:i:s");
                $is_delete      = "0";

            #check kode_produksi
            
            $set = ["stok"=>$stok, "stok_opnam"=>$stok_opnam];
            $where = ["id_item"=>$id_item];

            $update = $this->mm->update_data("item", $set, $where);

            if($update){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
            }
            
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["stok"]         = strip_tags(form_error('stok'));
            $msg_detail["stok_opnam"]   = strip_tags(form_error('stok_opnam'));
        }

        // $msg_detail["list_data"] = $this->im->get_data_brand_where(array());
        $msg_detail["list_data"] = file_get_contents(base_url()."other/Itemmain/create_tbl_item_update");
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

#===============================================================================
#-----------------------------------produk_main---------------------------------
#===============================================================================

}
