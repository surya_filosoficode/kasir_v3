<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Itemerror extends CI_Controller {

	public function __construct(){
        parent::__construct();
        $this->load->model('main/mainmodel', 'mm');

        $this->load->library("response_message");
        $this->load->library("Auth_v0");
        
        $this->auth_v0->check_session_active_ad();

        $this->load->library('set_record_stok');
        // $this->set_record_stok->insert_record();
    }


#===============================================================================
#-----------------------------------produk_main---------------------------------
#===============================================================================

	public function index(){
		$data["page"] = "item_error";
        print_r("=========================================");
        print_r("double input item aktif dengan kode produksi yang sama");
        print_r("=========================================");

        // distict kode produksi

        $this->db->distinct("kode_produksi_item");
        $distinct_item_0 = $this->db->get_where("item", ["is_del_item"=>"0"])->result();

        $no = 1;
        foreach ($distinct_item_0 as $key => $value) {
            $kode_produksi_item = $value->kode_produksi_item;
            $data_double = $this->db->get_where("item", ["kode_produksi_item"=>$kode_produksi_item, "is_del_item"=>"0"])->result();

            if(count($data_double) == 2){
                print_r("<br>");
                print_r("----------tt------$no------------------------");
                print_r("<br>");

                // print_r($data_double);

                foreach ($data_double as $keyd => $valued) {
                    print_r("<br>");
                    print_r("<br>");
                    print_r("<br>");
                    print_r("===============================================================");

                    print_r("<br>");
                    print_r(">>>>> ". $valued->kode_produksi_item);
                    print_r(" - ". $valued->nama_item);
                    print_r(" -|| ". $valued->id_item);

                    print_r(" ===>> stok: ". $valued->stok);
                    print_r(" - stok_opnam: ". $valued->stok_opnam);
                    print_r("<br>");

                    print_r("<br>");
                    print_r("<pre>");
                    $this->check_item_di_pembelian_detail($valued->id_item);

                    print_r("===============================================================");
                    print_r("<br>");
                    print_r("<br>");
                    print_r("<br>");
                }
                
                print_r("<br>");
                print_r("---------tt-------------------------------");
                print_r("<br>");

                $no++;
            }
        }
	}


    public function index_all(){
        $data["page"] = "item_error";
        print_r("=========================================");
        print_r("double input item semua dengan kode produksi yang sama");
        print_r("=========================================");

        // distict kode produksi

        $this->db->distinct("kode_produksi_item");
        $distinct_item_0 = $this->db->get_where("item", [])->result();

        $no = 1;
        foreach ($distinct_item_0 as $key => $value) {
            $kode_produksi_item = $value->kode_produksi_item;
            $data_double = $this->db->get_where("item", ["kode_produksi_item"=>$kode_produksi_item])->result();

            if(count($data_double) == 2){
                print_r("<br>");
                print_r("--------tt----------$no---------------------");
                print_r("<br>");

                // print_r($data_double);

                foreach ($data_double as $keyd => $valued) {
                    print_r("<br>");
                    print_r("<br>");
                    print_r("<br>");
                    print_r("===============================================================");

                    print_r("<br>");
                    print_r(">>>>> ". $valued->kode_produksi_item);
                    print_r(" - ". $valued->nama_item);
                    print_r(" -|| ". $valued->id_item);

                    print_r(" ===>> stok: ". $valued->stok);
                    print_r(" - stok_opnam: ". $valued->stok_opnam);

                    print_r(" || >>>> status_delete: ". $valued->is_del_item);
                    print_r(" <<<<");
                    print_r("<br>");

                    print_r("<br>");
                    print_r("<pre>");
                    $this->check_item_di_pembelian_detail($valued->id_item);

                    print_r("===============================================================");
                    print_r("<br>");
                    print_r("<br>");
                    print_r("<br>");
                }
                
                print_r("<br>");
                print_r("---------tt-------------------------------");
                print_r("<br>");

                $no++;
            }
        }
    }



    public function item_no_active(){
        $data["page"] = "item_error";
        print_r("=========================================");
        print_r("double input item aktif dengan kode produksi yang sama");
        print_r("=========================================");

        // distict kode produksi

        $data_item = $this->db->get_where("item", ["is_del_item"=>"1"])->result();
        $no = 1;
        foreach ($data_item as $key => $valued) {
            print_r("<br>");
            print_r("<br>");
            print_r("<br>");
            print_r("===============================================================");

            print_r("<br>");
            print_r("---------tt----------$no---------------------");
            print_r("<br>");

            print_r("<br>");
            print_r(">>>>> ". $valued->kode_produksi_item);
            print_r(" - ". $valued->nama_item);
            print_r(" -|| ". $valued->id_item);

            print_r(" ===>> stok: ". $valued->stok);
            print_r(" - stok_opnam: ". $valued->stok_opnam);

            print_r(" || >>>> status_delete: ". $valued->is_del_item);
            print_r(" <<<<");
            print_r("<br>");
            
            print_r("<br>");
            print_r("<pre>");
            $this->check_item_di_pembelian_detail($valued->id_item);

            print_r("<br>");
            print_r("----------tt------------------------------");
            print_r("<br>");

            print_r("===============================================================");
            print_r("<br>");
            print_r("<br>");
            print_r("<br>");
            $no++;
        }

    }



    public function check_item_di_pembelian_detail($id_item){
        $data["page"] = "item_error";
        // print_r("=========================================");
        // print_r("check_item_di_pembelian_detail");
        // print_r("=========================================");

        //-----------pembelian_detail
            $data_d_pembelian = $this->db->get_where("tr_pb_detail", ["id_item"=>$id_item])->result();

            foreach ($data_d_pembelian as $key => $value) {
                print_r("<br>");
                print_r("-------------------pembelian_detail---------------------");
                print_r("<br>");

                print_r($value);


                print_r("<br>");
                print_r("----------------------------------------");
                print_r("<br>");
            }


        //-----------penjualan_detail
            $data_d_penjualan = $this->db->get_where("tr_detail", ["id_item"=>$id_item])->result();
            foreach ($data_d_penjualan as $key => $value) {
                print_r("<br>");
                print_r("-------------------penjualan_detail---------------------");
                print_r("<br>");

                print_r($value);


                print_r("<br>");
                print_r("----------------------------------------");
                print_r("<br>");
            }


        //-----------retur_penjualan_detail
            $data_retur_d_penjualan = $this->db->get_where("tr_rt_pj_detail", ["id_item"=>$id_item])->result();
            foreach ($data_retur_d_penjualan as $key => $value) {
                print_r("<br>");
                print_r("-------------------retur_penjualan_detail---------------------");
                print_r("<br>");

                print_r($value);


                print_r("<br>");
                print_r("----------------------------------------");
                print_r("<br>");
            }


        //-----------pengganti_retur_penjualan_detail
            $data_pengganti_retur_d_penjualan = $this->db->get_where("tr_rt_pj_detail_p", ["id_item"=>$id_item])->result();
            foreach ($data_pengganti_retur_d_penjualan as $key => $value) {
                print_r("<br>");
                print_r("-------------------pengganti_retur_penjualan_detail---------------------");
                print_r("<br>");

                print_r($value);


                print_r("<br>");
                print_r("----------------------------------------");
                print_r("<br>");
            }


        //-----------retur_pembelian_detail
            $data_retur_d_pembelian = $this->db->get_where("tr_rt_pb_detail", ["id_item"=>$id_item])->result();
            foreach ($data_retur_d_pembelian as $key => $value) {
                print_r("<br>");
                print_r("-------------------retur_pembelian_detail---------------------");
                print_r("<br>");

                print_r($value);


                print_r("<br>");
                print_r("----------------------------------------");
                print_r("<br>");
            }


        //-----------pengganti_retur_pembelian_detail
            $data_pengganti_retur_d_pembelian = $this->db->get_where("tr_rt_pb_detail_p", ["id_item"=>$id_item])->result();
            foreach ($data_pengganti_retur_d_pembelian as $key => $value) {
                print_r("<br>");
                print_r("-------------------pengganti_retur_pembelian_detail---------------------");
                print_r("<br>");

                print_r($value);


                print_r("<br>");
                print_r("----------------------------------------");
                print_r("<br>");
            }

        //-----------sample
            $data_sample = $this->db->get_where("tr_sm_detail", ["id_item"=>$id_item])->result();
            foreach ($data_sample as $key => $value) {
                print_r("<br>");
                print_r("-------------------sample---------------------");
                print_r("<br>");

                print_r($value);


                print_r("<br>");
                print_r("----------------------------------------");
                print_r("<br>");
            }


        //-----------pengganti_sample
            $data_pengganti_sample = $this->db->get_where("tr_sm_rt_detail_p", ["id_item"=>$id_item])->result();
            foreach ($data_pengganti_sample as $key => $value) {
                print_r("<br>");
                print_r("-------------------pengganti_sample---------------------");
                print_r("<br>");

                print_r($value);
 


                print_r("<br>");
                print_r("----------------------------------------");
                print_r("<br>");
            }
    }

    

#===============================================================================
#-----------------------------------produk_main---------------------------------
#===============================================================================

}
