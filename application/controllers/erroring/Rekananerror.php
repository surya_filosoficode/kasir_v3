<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekananerror extends CI_Controller {

	public function __construct(){
        parent::__construct();
        $this->load->model('main/mainmodel', 'mm');

        $this->load->library("response_message");
        $this->load->library("Auth_v0");
        
        $this->auth_v0->check_session_active_ad();

        $this->load->library('set_record_stok');
        // $this->set_record_stok->insert_record();
    }


#===============================================================================
#-----------------------------------produk_main---------------------------------
#===============================================================================

	public function index(){
		$data["page"] = "item_error";
        print_r("=========================================");
        print_r("double input item aktif dengan kode produksi yang sama");
        print_r("=========================================");

        // distict kode produksi

        $this->db->distinct("nama_rekanan");
        $distinct_nama = $this->db->get_where("rekanan")->result();

        $no = 1;
        foreach ($distinct_nama as $key => $value) {
            $data_rekanan = $this->db->get_where("rekanan", ["nama_rekanan"=>$value->nama_rekanan])->result();
            if(count($data_rekanan) == 2){
                print_r("<br>");
                print_r("----------------tt-------------------------$no---------------------");
                print_r("<br>");
                foreach ($data_rekanan as $keyd => $valued) {
                    print_r("<br>");
                    print_r("===============================================================");
                    print_r("<br>");
                    print_r(">>>>> ". $valued->nama_rekanan);
                    print_r(" -|| ". $valued->id_rekanan);

                    print_r(" || >>>> status_delete: ". $valued->is_delete);
                    print_r(" <<<<");
                    print_r("<br>");
                    print_r("<br>");

                    print_r("<pre>");

                    $this->check_rekanan_di_detail($valued->id_rekanan);
                    print_r("<br>");
                    print_r("===============================================================");
                    print_r("<br>");
                }
                print_r("<br>");
                print_r("-----------------tt------------------------------------------------");
                print_r("<br>");
                print_r("<br>");
                print_r("<br>");
                print_r("<br>");
                
                $no++;
            }
        }

	}


    public function index_is_delete(){
        $data["page"] = "item_error";
        print_r("=========================================");
        print_r("double input item aktif dengan kode produksi yang sama");
        print_r("=========================================");

        // distict kode produksi

        $this->db->distinct("nama_rekanan");
        $distinct_nama = $this->db->get_where("rekanan", ["is_delete"=>"1"])->result();

        $no = 1;
        foreach ($distinct_nama as $key => $value) {
            $data_rekanan = $this->db->get_where("rekanan", ["nama_rekanan"=>$value->nama_rekanan])->result();
            if(count($data_rekanan) == 2){
                print_r("<br>");
                print_r("----------------tt-------------------------$no---------------------");
                print_r("<br>");
                foreach ($data_rekanan as $keyd => $valued) {
                    print_r("<br>");
                    print_r("===============================================================");
                    print_r("<br>");
                    print_r(">>>>> ". $valued->nama_rekanan);
                    print_r(" -|| ". $valued->id_rekanan);

                    print_r(" || >>>> status_delete: ". $valued->is_delete);
                    print_r(" <<<<");
                    print_r("<br>");
                    print_r("<br>");

                    print_r("<pre>");

                    $this->check_rekanan_di_detail($valued->id_rekanan);
                    print_r("<br>");
                    print_r("===============================================================");
                    print_r("<br>");
                }
                print_r("<br>");
                print_r("-----------------tt------------------------------------------------");
                print_r("<br>");
                print_r("<br>");
                print_r("<br>");
                print_r("<br>");
                
                $no++;
            }
        }

    }

    public function check_rekanan_di_detail($id_customer){
        $data["page"] = "item_error";
        print_r("<pre>");
        // print_r("=========================================");
        // print_r("check_item_di_pembelian_detail");
        // print_r("=========================================");

        

        //-----------penjualan_detail
            $data_d_penjualan = $this->db->get_where("tr_header", ["id_customer"=>$id_customer])->result();
            foreach ($data_d_penjualan as $key => $value) {
                print_r("<br>");
                print_r("-------------------penjualan_detail---------------------");
                print_r("<br>");

                print_r($value);


                print_r("<br>");
                print_r("----------------------------------------");
                print_r("<br>");
            }


        //-----------retur_penjualan_detail
            $data_retur_d_penjualan = $this->db->get_where("tr_rt_pj_header", ["id_customer"=>$id_customer])->result();
            foreach ($data_retur_d_penjualan as $key => $value) {
                print_r("<br>");
                print_r("-------------------retur_penjualan_detail---------------------");
                print_r("<br>");

                print_r($value);


                print_r("<br>");
                print_r("----------------------------------------");
                print_r("<br>");
            }


        //-----------pengganti_retur_penjualan_detail
            $data_pengganti_retur_d_penjualan = $this->db->get_where("tr_rt_pj_header_p", ["id_customer"=>$id_customer])->result();
            foreach ($data_pengganti_retur_d_penjualan as $key => $value) {
                print_r("<br>");
                print_r("-------------------pengganti_retur_penjualan_detail---------------------");
                print_r("<br>");

                print_r($value);


                print_r("<br>");
                print_r("----------------------------------------");
                print_r("<br>");
            }


        //-----------sample
            $data_sample = $this->db->get_where("tr_sm_header", ["id_customer"=>$id_customer])->result();
            foreach ($data_sample as $key => $value) {
                print_r("<br>");
                print_r("-------------------sample---------------------");
                print_r("<br>");

                print_r($value);


                print_r("<br>");
                print_r("----------------------------------------");
                print_r("<br>");
            }

    }

    

#===============================================================================
#-----------------------------------produk_main---------------------------------
#===============================================================================

}
