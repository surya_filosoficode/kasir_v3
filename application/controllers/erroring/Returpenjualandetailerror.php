<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Returpenjualandetailerror extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('main/mainmodel', 'mm');

        $this->load->library("response_message");
        $this->load->library("Auth_v0");
        
        $this->auth_v0->check_session_active_ad();

        $this->load->library('set_record_stok');
        // $this->set_record_stok->insert_record();
    }


#==========================================================================
#-----------------------------------returpenjualan---------------------------------
#==========================================================================

    public function index_returpenjualan_detail_no_item(){
        $data["page"] = "item_error";
        print_r("=========================================");
        print_r("Check returpenjualan pengganti detail tanpa item");
        print_r("=========================================");

        // distict kode produksi

        $this->db->distinct("id_item");
        $distinct_0 = $this->db->get_where("tr_rt_pj_detail", [])->result();

        // print_r($distinct_0);
        $no = 1;
        foreach ($distinct_0 as $key => $value) {
            $id_item = $value->id_item;
            print_r("<pre>");
            print_r("<br>");
            print_r("<br>");
            print_r("<br>");
            print_r("================================$no===============================");
            print_r("<br>");

            print_r("---------------------------d_returpenjualan-------------------------");
            print_r("<br>");
            print_r($value);
            print_r("<br>");
            print_r("---------------------------d_returpenjualan-------------------------");
            print_r("<br>");
            $data_item = $this->db->get_where("item", ["id_item"=>$id_item])->row_array();

            if($data_item){
                // print_r("---------------------------item-------------------------");
                // print_r("<br>");
                // print_r($data_item);
                // print_r("<br>");
                // print_r("---------------------------item-------------------------");
                // print_r("<br>");

            }else{
                print_r("---------------------------item-------------------------");
                print_r("<br>");
                print_r("kosong");
                print_r("<br>");
                print_r("---------------------------item-------------------------");
                print_r("<br>");
            }
            
            
            print_r("===============================================================");
            print_r("<br>");
            print_r("<br>");
            print_r("<br>");

            $no++;
        }
    }


    public function index_returpenjualan_no_header(){
        $data["page"] = "item_error";
        print_r("=========================================");
        print_r("Check returpenjualan detail tanpa header");
        print_r("=========================================");

        // distict kode produksi

        $this->db->distinct("id_tr_header");
        $distinct_0 = $this->db->get_where("tr_rt_pj_detail", [])->result();

        // print_r($distinct_0);
        $no = 1;
        foreach ($distinct_0 as $key => $value) {
            $id_tr_header = $value->id_tr_header;
            print_r("<pre>");
            print_r("<br>");
            print_r("<br>");
            print_r("<br>");
            print_r("===============================$no================================");
            print_r("<br>");

            print_r("---------------------------d_returpenjualan-------------------------");
            print_r("<br>");
            print_r($value);
            print_r("<br>");
            print_r("---------------------------d_returpenjualan-------------------------");
            print_r("<br>");
            $data_header = $this->db->get_where("tr_rt_pj_header", ["id_tr_header"=>$id_tr_header])->row_array();

            if($data_header){
                print_r("---------------------------item-------------------------");
                print_r("<br>");
                print_r($data_header);
                print_r("<br>");
                print_r("---------------------------item-------------------------");
                print_r("<br>");

            }else{
                print_r("---------------------------item-------------------------");
                print_r("<br>");
                print_r("kosong");
                print_r("<br>");
                print_r("---------------------------item-------------------------");
                print_r("<br>");
            }
            
            
            print_r("===============================================================");
            print_r("<br>");
            print_r("<br>");
            print_r("<br>");

            $no++;
        }
    }


    public function index_returpenjualan_header_no_detail(){
        $data["page"] = "item_error";
        print_r("=========================================");
        print_r("Check returpenjualan header tanpa detail");
        print_r("=========================================");

        // distict kode produksi

        $this->db->distinct("id_tr_header");
        $distinct_0 = $this->db->get_where("tr_rt_pj_header", [])->result();

        // print_r($distinct_0);
        $no = 1;
        foreach ($distinct_0 as $key => $value) {
            $id_tr_header = $value->id_tr_header;
            print_r("<pre>");
            print_r("<br>");
            print_r("<br>");
            print_r("<br>");
            print_r("===============================$no================================");
            print_r("<br>");

            print_r("---------------------------d_returpenjualan-------------------------");
            print_r("<br>");
            print_r($value);
            print_r("<br>");
            print_r("---------------------------d_returpenjualan-------------------------");
            print_r("<br>");
            $data_header = $this->db->get_where("tr_rt_pj_detail", ["id_tr_header"=>$id_tr_header])->result();

            if($data_header){
                print_r("---------------------------item-------------------------");
                print_r("<br>");
                print_r($data_header);
                print_r("<br>");
                print_r("---------------------------item-------------------------");
                print_r("<br>");

            }else{
                print_r("---------------------------item-------------------------");
                print_r("<br>");
                print_r("kosong");
                print_r("<br>");
                print_r("---------------------------item-------------------------");
                print_r("<br>");
            }
            
            
            print_r("===============================================================");
            print_r("<br>");
            print_r("<br>");
            print_r("<br>");

            $no++;
        }
    }


    public function index_returpenjualan_no_customer(){
        $data["page"] = "item_error";
        print_r("=========================================");
        print_r("Check returpenjualan detail tanpa header");
        print_r("=========================================");

        // distict kode produksi

        $this->db->distinct("id_customer");
        $distinct_0 = $this->db->get_where("tr_rt_pj_header", [])->result();

        // print_r($distinct_0);
        $no = 1;
        foreach ($distinct_0 as $key => $value) {
            $id_customer = $value->id_customer;
            print_r("<pre>");
            print_r("<br>");
            print_r("<br>");
            print_r("<br>");
            print_r("===============================$no================================");
            print_r("<br>");

            print_r("---------------------------d_returpenjualan-------------------------");
            print_r("<br>");
            print_r($value);
            print_r("<br>");
            print_r("---------------------------d_returpenjualan-------------------------");
            print_r("<br>");
            $rekanan = $this->db->get_where("rekanan", ["id_rekanan"=>$id_customer])->row_array();

            if($rekanan){
                print_r("---------------------------customer-------------------------");
                print_r("<br>");
                print_r($rekanan);
                print_r("<br>");
                print_r("---------------------------customer-------------------------");
                print_r("<br>");

            }else{
                print_r("---------------------------customer-------------------------");
                print_r("<br>");
                print_r("kosong");
                print_r("<br>");
                print_r("---------------------------customer-------------------------");
                print_r("<br>");
            }
            
            
            print_r("===============================================================");
            print_r("<br>");
            print_r("<br>");
            print_r("<br>");

            $no++;
        }
    }
    

#==========================================================================
#-----------------------------------returpenjualan---------------------------------
#==========================================================================

#==========================================================================
#-----------------------------------returpenjualan_pengganti-----------------------
#==========================================================================


    public function index_returpenjualan_pengganti_detail_no_item(){
        $data["page"] = "item_error";
        print_r("=========================================");
        print_r("Check returpenjualan pengganti detail tanpa item");
        print_r("=========================================");

        // distict kode produksi

        $this->db->distinct("id_item");
        $distinct_0 = $this->db->get_where("tr_rt_pj_detail_p", [])->result();

        // print_r($distinct_0);
        $no = 1;
        foreach ($distinct_0 as $key => $value) {
            $id_item = $value->id_item;
            print_r("<pre>");
            print_r("<br>");
            print_r("<br>");
            print_r("<br>");
            print_r("================================$no===============================");
            print_r("<br>");

            print_r("---------------------------d_returpenjualan-------------------------");
            print_r("<br>");
            print_r($value);
            print_r("<br>");
            print_r("---------------------------d_returpenjualan-------------------------");
            print_r("<br>");
            $data_item = $this->db->get_where("item", ["id_item"=>$id_item])->row_array();

            if($data_item){
                // print_r("---------------------------item-------------------------");
                // print_r("<br>");
                // print_r($data_item);
                // print_r("<br>");
                // print_r("---------------------------item-------------------------");
                // print_r("<br>");

            }else{
                print_r("---------------------------item-------------------------");
                print_r("<br>");
                print_r("kosong");
                print_r("<br>");
                print_r("---------------------------item-------------------------");
                print_r("<br>");
            }
            
            
            print_r("===============================================================");
            print_r("<br>");
            print_r("<br>");
            print_r("<br>");

            $no++;
        }
    }


    public function index_returpenjualan_pengganti_no_header(){
        $data["page"] = "item_error";
        print_r("=========================================");
        print_r("Check returpenjualan detail tanpa header");
        print_r("=========================================");

        // distict kode produksi

        $this->db->distinct("id_tr_header_p");
        $distinct_0 = $this->db->get_where("tr_rt_pj_detail_p", [])->result();

        // print_r($distinct_0);
        $no = 1;
        foreach ($distinct_0 as $key => $value) {
            $id_tr_header = $value->id_tr_header_p;
            print_r("<pre>");
            print_r("<br>");
            print_r("<br>");
            print_r("<br>");
            print_r("===============================$no================================");
            print_r("<br>");

            print_r("---------------------------d_returpenjualan-------------------------");
            print_r("<br>");
            print_r($value);
            print_r("<br>");
            print_r("---------------------------d_returpenjualan-------------------------");
            print_r("<br>");
            $data_header = $this->db->get_where("tr_rt_pj_header_p", ["id_tr_header_p"=>$id_tr_header])->row_array();

            if($data_header){
                print_r("---------------------------item-------------------------");
                print_r("<br>");
                print_r($data_header);
                print_r("<br>");
                print_r("---------------------------item-------------------------");
                print_r("<br>");

            }else{
                print_r("---------------------------item-------------------------");
                print_r("<br>");
                print_r("kosong");
                print_r("<br>");
                print_r("---------------------------item-------------------------");
                print_r("<br>");
            }
            
            
            print_r("===============================================================");
            print_r("<br>");
            print_r("<br>");
            print_r("<br>");

            $no++;
        }
    }


    public function index_returpenjualan_pengganti_header_no_detail(){
        $data["page"] = "item_error";
        print_r("=========================================");
        print_r("Check returpenjualan header tanpa detail");
        print_r("=========================================");

        // distict kode produksi

        $this->db->distinct("id_tr_header_p");
        $distinct_0 = $this->db->get_where("tr_rt_pj_header_p", [])->result();

        // print_r($distinct_0);
        $no = 1;
        foreach ($distinct_0 as $key => $value) {
            $id_tr_header = $value->id_tr_header_p;
            print_r("<pre>");
            print_r("<br>");
            print_r("<br>");
            print_r("<br>");
            print_r("===============================$no================================");
            print_r("<br>");

            print_r("---------------------------d_returpenjualan-------------------------");
            print_r("<br>");
            print_r($value);
            print_r("<br>");
            print_r("---------------------------d_returpenjualan-------------------------");
            print_r("<br>");
            $data_header = $this->db->get_where("tr_rt_pj_detail_p", ["id_tr_header_p"=>$id_tr_header])->result();

            if($data_header){
                print_r("---------------------------item-------------------------");
                print_r("<br>");
                print_r($data_header);
                print_r("<br>");
                print_r("---------------------------item-------------------------");
                print_r("<br>");

            }else{
                print_r("---------------------------item-------------------------");
                print_r("<br>");
                print_r("kosong");
                print_r("<br>");
                print_r("---------------------------item-------------------------");
                print_r("<br>");
            }
            
            
            print_r("===============================================================");
            print_r("<br>");
            print_r("<br>");
            print_r("<br>");

            $no++;
        }
    }


    public function index_returpenjualan_pengganti_no_customer(){
        $data["page"] = "item_error";
        print_r("=========================================");
        print_r("Check returpenjualan detail tanpa header");
        print_r("=========================================");

        // distict kode produksi

        $this->db->distinct("id_customer");
        $distinct_0 = $this->db->get_where("tr_rt_pj_header_p", [])->result();

        // print_r($distinct_0);
        $no = 1;
        foreach ($distinct_0 as $key => $value) {
            $id_customer = $value->id_customer;
            print_r("<pre>");
            print_r("<br>");
            print_r("<br>");
            print_r("<br>");
            print_r("===============================$no================================");
            print_r("<br>");

            print_r("---------------------------d_returpenjualan-------------------------");
            print_r("<br>");
            print_r($value);
            print_r("<br>");
            print_r("---------------------------d_returpenjualan-------------------------");
            print_r("<br>");
            $rekanan = $this->db->get_where("rekanan", ["id_rekanan"=>$id_customer])->row_array();

            if($rekanan){
                print_r("---------------------------customer-------------------------");
                print_r("<br>");
                print_r($rekanan);
                print_r("<br>");
                print_r("---------------------------customer-------------------------");
                print_r("<br>");

            }else{
                print_r("---------------------------customer-------------------------");
                print_r("<br>");
                print_r("kosong");
                print_r("<br>");
                print_r("---------------------------customer-------------------------");
                print_r("<br>");
            }
            
            
            print_r("===============================================================");
            print_r("<br>");
            print_r("<br>");
            print_r("<br>");

            $no++;
        }
    }

#==========================================================================
#-----------------------------------returpenjualan_pengganti-----------------------
#==========================================================================


#==========================================================================
#-----------------------------------returpenjualan_pengganti_n_returpenjualan--------------
#==========================================================================
    public function index_returpenjualan_pengganti_header_no_returpenjualan_header(){
        $data["page"] = "item_error";
        print_r("=========================================");
        print_r("Check returpenjualan header tanpa detail");
        print_r("=========================================");

        // distict kode produksi

        $this->db->distinct("id_tr_header_p");
        $distinct_0 = $this->db->get_where("tr_rt_pj_header_p", [])->result();

        // print_r($distinct_0);
        $no = 1;
        foreach ($distinct_0 as $key => $value) {
            $id_tr_header = $value->id_tr_header_p;
            print_r("<pre>");
            print_r("<br>");
            print_r("<br>");
            print_r("<br>");
            print_r("===============================$no================================");
            print_r("<br>");

            print_r("---------------------------d_returpenjualan-------------------------");
            print_r("<br>");
            print_r($value);
            print_r("<br>");
            print_r("---------------------------d_returpenjualan-------------------------");
            print_r("<br>");
            $data_header = $this->db->get_where("tr_rt_pj_header", ["id_tr_header_p"=>$id_tr_header])->result();

            if($data_header){
                print_r("---------------------------item-------------------------");
                print_r("<br>");
                print_r($data_header);
                print_r("<br>");
                print_r("---------------------------item-------------------------");
                print_r("<br>");

            }else{
                print_r("---------------------------item-------------------------");
                print_r("<br>");
                print_r("kosong");
                print_r("<br>");
                print_r("---------------------------item-------------------------");
                print_r("<br>");
            }
            
            
            print_r("===============================================================");
            print_r("<br>");
            print_r("<br>");
            print_r("<br>");

            $no++;
        }
    }


    public function index_returpenjualan_header_no_returpenjualan_pengganti_header(){
        $data["page"] = "item_error";
        print_r("=========================================");
        print_r("Check index_returpenjualan_header_no_returpenjualan_pengganti_header");
        print_r("=========================================");

        // distict kode produksi

        $this->db->distinct("id_tr_header_p");
        $distinct_0 = $this->db->get_where("tr_rt_pj_header", [])->result();

        // print_r($distinct_0);
        $no = 1;
        foreach ($distinct_0 as $key => $value) {
            $id_tr_header = $value->id_tr_header_p;
            print_r("<pre>");
            print_r("<br>");
            print_r("<br>");
            print_r("<br>");
            print_r("===============================$no================================");
            print_r("<br>");

            print_r("---------------------------d_returpenjualan-------------------------");
            print_r("<br>");
            print_r($value);
            print_r("<br>");
            print_r("---------------------------d_returpenjualan-------------------------");
            print_r("<br>");
            $data_header = $this->db->get_where("tr_rt_pj_header_p", ["id_tr_header_p"=>$id_tr_header])->result();

            if($data_header){
                print_r("---------------------------item-------------------------");
                print_r("<br>");
                print_r($data_header);
                print_r("<br>");
                print_r("---------------------------item-------------------------");
                print_r("<br>");

            }else{
                print_r("---------------------------item-------------------------");
                print_r("<br>");
                print_r("kosong");
                print_r("<br>");
                print_r("---------------------------item-------------------------");
                print_r("<br>");
            }
            
            
            print_r("===============================================================");
            print_r("<br>");
            print_r("<br>");
            print_r("<br>");

            $no++;
        }
    }
#==========================================================================
#-----------------------------------returpenjualan_pengganti_n_returpenjualan--------------
#==========================================================================

}
