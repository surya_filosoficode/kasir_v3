<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Checksaveitem extends CI_Controller {

	public function __construct(){
        parent::__construct();
        $this->load->model('erroring/check_save_item', 'csi');
        $this->load->model('main/mainmodel', 'mm');
        $this->load->model('other/item_main', 'im');

        $this->load->library("response_message");
        $this->load->library("Auth_v0");
        
        $this->auth_v0->check_session_active_ad();

        $this->load->library('set_record_stok');
        // $this->set_record_stok->insert_record();
    }


#===============================================================================
#-----------------------------------produk_main---------------------------------
#===============================================================================

	public function index(){
		$data["page"] = "check_save_item";

        $data["list_data"]          = $this->csi->get_data_produk_where(array("is_del_item"=>"0"));
        $data["list_data_brand"]    = $this->csi->get_data_brand_where(array("is_del_brand"=>"0"));

        $distinct_nama = $this->csi->get_distinct_nama_produk(array("is_del_item"=>"0"));

        $data["distinct_nama_item"] = $distinct_nama;

        // print_r($data);
        $this->load->view("index", $data);
	}

    // public function val_form_insert_item(){
    //     $config_val_input = array(
    //             array(
    //                 'field'=>'stok',
    //                 'label'=>'stok',
    //                 'rules'=>'required',
    //                 'errors'=>array(
    //                     'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
    //                 )  
    //             ),array(
    //                 'field'=>'stok_opnam',
    //                 'label'=>'stok_opnam',
    //                 'rules'=>'required',
    //                 'errors'=>array(
    //                     'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
    //                 ) 
    //             )
    //         );
            
    //     $this->form_validation->set_rules($config_val_input); 
    //     return $this->form_validation->run();
    // }

    public function rekam_stok(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array();

        $date_now = date("Y-m-d");
        $array_status_rekam = [];
        // print_r($date_now);

        $item_real= $this->csi->get_data_produk_where(array("is_del_item"=>"0"));
        if($item_real){
            foreach ($item_real as $key => $value) {
                $status = false;

                $id_item = $value->id_item;
                $stok_record_item = $value->stok;
                $rusak_record_item = $value->stok_opnam;

                // print_r($value);
                $record_item = $this->mm->get_data_each("record_item", ["id_item"=>$value->id_item, "periode_record_item"=>$date_now]);
                // print_r($date_now);

                if($record_item){
                    $id_record_item = $record_item["id_record_item"];

                    $set = ["stok_record_item"=>$stok_record_item,
                            "rusak_record_item"=>$rusak_record_item,
                            ];
                    $where = ["id_record_item"=>$id_record_item];

                    $update = $this->mm->update_data("record_item", $set, $where);
                    if($update){
                        $status = true;
                        array_push($array_status_rekam, $status);
                    }
                }else{
                    $insert_record = $this->csi->insert_record_item($date_now, $id_item, $stok_record_item, $rusak_record_item);
                    if($insert_record){
                        $status = true;
                        array_push($array_status_rekam, $status);
                    }

                }
                // print_r("<br>");
                // print_r($record_item);
                // print_r("<br>");
                // print_r("------------------------------");
                // print_r("<br>");
            }
        }

        if(!in_array(false, $array_status_rekam)){
            $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
            // print_r("<script>alert('perekaman berhasil');</script>");
        }
        // else{
        //     print_r("<script>alert('perekaman gagal');</script>");
        // }
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
        // redirect(base_url()."checking/rekam_stok");
    }

    public function check_stok(){
        $data["page"] = "check_rekam_item";

        $date_now = date("Y-m-d");
        
        $item_real= $this->csi->get_data_produk_n_rekam(array("is_del_item"=>"0", "periode_record_item"=>$date_now));
        // print_r($item_real);
        $data["list_data"]          = $item_real;

        $this->load->view("index", $data);
    }

#===============================================================================
#-----------------------------------produk_main---------------------------------
#===============================================================================

}
