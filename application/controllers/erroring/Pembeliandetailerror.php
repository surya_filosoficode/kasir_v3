<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembeliandetailerror extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('main/mainmodel', 'mm');

        $this->load->library("response_message");
        $this->load->library("Auth_v0");
        
        $this->auth_v0->check_session_active_ad();

        $this->load->library('set_record_stok');
        // $this->set_record_stok->insert_record();
    }


#===============================================================================
#-----------------------------------produk_main---------------------------------
#===============================================================================

    public function index_pembelian_no_item(){
        $data["page"] = "item_error";
        print_r("=========================================");
        print_r("Check pembelian detail tanpa item");
        print_r("=========================================");

        // distict kode produksi

        $this->db->distinct("id_item");
        $distinct_0 = $this->db->get_where("tr_pb_detail", [])->result();

        // print_r($distinct_0);
        $no = 1;
        foreach ($distinct_0 as $key => $value) {
            $id_item = $value->id_item;
            print_r("<pre>");
            print_r("<br>");
            print_r("<br>");
            print_r("<br>");
            print_r("================================$no===============================");
            print_r("<br>");

            print_r("---------------------------d_pembelian-------------------------");
            print_r("<br>");
            print_r($value);
            print_r("<br>");
            print_r("---------------------------d_pembelian-------------------------");
            print_r("<br>");
            $data_item = $this->db->get_where("item", ["id_item"=>$id_item])->row_array();

            if($data_item){
                // print_r("---------------------------item-------------------------");
                // print_r("<br>");
                // print_r($data_item);
                // print_r("<br>");
                // print_r("---------------------------item-------------------------");
                // print_r("<br>");

            }else{
                print_r("---------------------------item-------------------------");
                print_r("<br>");
                print_r("kosong");
                print_r("<br>");
                print_r("---------------------------item-------------------------");
                print_r("<br>");
            }
            
            
            print_r("===============================================================");
            print_r("<br>");
            print_r("<br>");
            print_r("<br>");

            $no++;
        }
    }


    public function index_pembelian_no_header(){
        $data["page"] = "item_error";
        print_r("=========================================");
        print_r("Check pembelian detail tanpa header");
        print_r("=========================================");

        // distict kode produksi

        $this->db->distinct("id_tr_header");
        $distinct_0 = $this->db->get_where("tr_pb_detail", [])->result();

        // print_r($distinct_0);
        $no = 1;
        foreach ($distinct_0 as $key => $value) {
            $id_tr_header = $value->id_tr_header;
            print_r("<pre>");
            print_r("<br>");
            print_r("<br>");
            print_r("<br>");
            print_r("===============================$no================================");
            print_r("<br>");

            print_r("---------------------------d_pembelian-------------------------");
            print_r("<br>");
            print_r($value);
            print_r("<br>");
            print_r("---------------------------d_pembelian-------------------------");
            print_r("<br>");
            $data_header = $this->db->get_where("tr_pb_header", ["id_tr_header"=>$id_tr_header])->row_array();

            if($data_header){
                // print_r("---------------------------item-------------------------");
                // print_r("<br>");
                // print_r($data_item);
                // print_r("<br>");
                // print_r("---------------------------item-------------------------");
                // print_r("<br>");

            }else{
                print_r("---------------------------item-------------------------");
                print_r("<br>");
                print_r("kosong");
                print_r("<br>");
                print_r("---------------------------item-------------------------");
                print_r("<br>");
            }
            
            
            print_r("===============================================================");
            print_r("<br>");
            print_r("<br>");
            print_r("<br>");

            $no++;
        }
    }
    

#===============================================================================
#-----------------------------------produk_main---------------------------------
#===============================================================================

}
