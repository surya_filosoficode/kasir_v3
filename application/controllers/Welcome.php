<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct(){
        parent::__construct(); 
        $this->load->library('set_record_stok');
        
        
        date_default_timezone_set("Asia/Bangkok");
        // $this->auth_v0->check_session_active_ad();
        
        $this->set_record_stok->insert_record();
    }

	public function index(){
		redirect(base_url()."login");
	}

	public function cek(){
		$response = $this->set_record_stok->check_day();
		// if($response){
		// 	print_r("<script>alert(\"Update record stok berhasil. Terimakasih\");</script>");
  //       }else{
  //           print_r("<script>alert(\"program sedang dalam masalah, hubungi admin. Terimakasih\");</script>");
		// }
	}
}
