<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reportbpom_new extends CI_Controller {

    public $keterangan_record_stok = "report_bpom_main";
    public $array_of_month = ["", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];

    public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');
        // $this->load->model('report/report_bpom', 'rp');
        $this->load->model('report/report_penjualan_item', 'rp');

        $this->load->library("response_message");
        $this->load->library("Auth_v0");
        
        date_default_timezone_set("Asia/Bangkok");
        // $this->auth_v0->check_session_active_ad();
    }

    public function index(){
        $data["page"] = "report_bpom_main";
        $data["str_periode"] = "";
        $data["setting"] = $this->mm->get_data_all_where("setting", array("is_del_setting"=>"0"));

        // print_r($data);
        $this->load->view('index', $data);
    }

#------------------------------show----------------------------------#
    public function get_bpom_tgl(){
        $data["page"] = "report_bpom_main";
        $data["item"] = $this->rp->get_item_asc(array("is_del_item"=>"0"));
        $data["setting"] = $this->mm->get_data_all_where("setting", array("is_del_setting"=>"0"));

        $tgl_before = date('Y-m-d', strtotime('-1 day', strtotime(date("Y-m-d"))));

        $data["str_periode"] = "";

        $date_now = date("Y-m-d");

        $data["str_periode"] = $date_now;

        $tgl_start = $date_now;
        $tgl_finish = $date_now;
        // $data["item"] = $this->mm->get_data_all_where("item", array("is_del_item"=>"0"));

        $data["list_data"] = array();
        $data["item_v"] = [];
        $data["list_all_data"] = [];

        // print('Next Date ' . );

        $data["list_record_stok"] = [];
        $data["list_item_stok"] = [];

        if($tgl_start != "0" && $tgl_finish != "0"){
            foreach ($data["item"] as $key => $value) {
                $id_item = $value->id_item;
                $stok = $value->stok;

                $tmp_list = ["item_now"=>[],
                                "record_item_before"=>[],
                                "pemasukan"=>[
                                            "pabrik"=>0,
                                            "pbf"=>0,
                                            "retur"=>0,
                                            "lainnya"=>0
                                        ],
                                "pemasukan_all"=>0,
                                "pengeluaran"=>[
                                            "rs"=>0,
                                            "pbf"=>0,
                                            "apotik"=>0,
                                            "sp"=>0,
                                            "pm"=>0,
                                            "kl"=>0,
                                            "to"=>0,
                                            "ln"=>0,
                                            "retur"=>0,
                                            "lainnya"=>0
                                        ],
                                "pengeluaran_all"=>0,
                                "stok"=>0,
                                "stok_before"=>0,
                                "stok_move"=>0
                            ];


                $where_main_no_item_rusak = [
                            "th.tgl_transaksi_tr_header >="=>$tgl_start,
                            "th.tgl_transaksi_tr_header <="=>$tgl_finish,
                            "td.status_rt_tr_detail"=>"0",
                            "td.id_item"=>$id_item
                        ];

                $where_main = [
                            "th.tgl_transaksi_tr_header >="=>$tgl_start,
                            "th.tgl_transaksi_tr_header <="=>$tgl_finish,
                            "td.id_item"=>$id_item
                        ];

                // print_r("<pre>");
                // print_r("<br>");
                // print_r("----------------------------------------");
                // print_r("<br>");

                
                // get rekam stok
                $tmp_list["item_now"] = $value;
                $tmp_list["record_item_before"] = $this->mm->get_data_each("record_item", ["id_item"=>$value->id_item, "periode_record_item"=>$tgl_before]);

                $stok_before = $tmp_list["record_item_before"]["stok_record_item"];
                // $data["list_record_stok"][$id_item] = $this->mm->get_data_each("record_item", ["id_item"=>$value->id_item, "periode_record_item"=>$tgl_before]);

                // $data["list_item_stok"][$id_item] = $value;

            // --------------Pemasukan
                $pemasukan_all = 0;

                $pemasukan_pabrik   = 0;
                $pemasukan_pbf      = 0;
                $pemasukan_retur    = 0;
                $pemasukan_lainnya  = 0;

                // ---------------pembelian
                    // $stok_masuk = 0;
                    
                    $data_pembelian = $this->rp->get_pembelian_item_tgl($tgl_start, $tgl_finish, array("td.id_item"=>$id_item));

                    foreach ($data_pembelian as $key_p => $value_p) {
                        $jml_item = $value_p->jml_item_tr_detail;
                        $jenis_suplier = $value_p->jenis_suplier;

                        if($jenis_suplier == "pabrik"){
                            $pemasukan_pabrik   += $jml_item;

                        }elseif ($jenis_suplier == "pbf") {
                            $pemasukan_pbf      += $jml_item;
                        
                        }
                        $pemasukan_all += $jml_item;
                    }

                // ---------------retur_penjualan
                    // $stok_masuk = 0;
                    $data_retur_penjualan = $this->rp->get_retur_penjualan_detail($where_main_no_item_rusak);
                    
                    foreach ($data_retur_penjualan as $key_p => $value_p) {
                        $jml_item = $value_p->jml_item_tr_detail;
                        $jenis_rekanan = $value_p->jenis_rekanan;

                        $pemasukan_retur   += $jml_item;

                        $pemasukan_all += $jml_item;
                    }

                // ---------------pengganti_retur_pembelian
                    $data_retur_pembelian_p = $this->rp->get_retur_pembelian_detail_p($where_main);
                    
                    foreach ($data_retur_pembelian_p as $key_p => $value_p) {
                        $jml_item = $value_p->jml_item_tr_detail;
                        $jenis_suplier = $value_p->jenis_suplier;

                        $pemasukan_retur   += $jml_item;

                        $pemasukan_all += $jml_item;
                    }

                // ---------------Pengganti Retur Sample
                    $data_retur_sample_p = $this->rp->get_retur_sample_detail_p($where_main);
                    
                    foreach ($data_retur_sample_p as $key_p => $value_p) {
                        $jml_item = $value_p->jml_item_tr_detail;
                        // $jenis_rekanan = $value_p->jenis_rekanan;

                        // if($jenis_suplier == "pabrik"){
                        //     $pemasukan_pabrik   += $jml_item;

                        // }elseif ($jenis_suplier == "pbf") {
                        //     $pemasukan_pbf      += $jml_item;
                        
                        // }
                        $pemasukan_lainnya += $jml_item;
                        
                        $pemasukan_all += $jml_item;
                    }

                $tmp_list["pemasukan"]["pabrik"]    = $pemasukan_pabrik;
                $tmp_list["pemasukan"]["pbf"]       = $pemasukan_pbf;
                $tmp_list["pemasukan"]["retur"]     = $pemasukan_retur;
                $tmp_list["pemasukan"]["lainnya"]   = $pemasukan_lainnya;

                $tmp_list["pemasukan_all"] = $pemasukan_all;


            // --------------Pemasukan
                $pengeluaran_all = 0;

                $pengeluaran_rs     = 0;
                $pengeluaran_pbf    = 0;
                $pengeluaran_apotik = 0;
                $pengeluaran_sp     = 0;
                $pengeluaran_pm     = 0;
                $pengeluaran_kl     = 0;
                $pengeluaran_to     = 0;
                $pengeluaran_ln     = 0;
                $pengeluaran_retur  = 0;
                $pengeluaran_lainnya= 0;


                // ---------------Penjualan
                    $data_penjualan = $this->rp->get_penjualan_item_tgl($tgl_start, $tgl_finish, array("td.id_item"=>$id_item));

                    foreach ($data_penjualan as $key_p => $value_p) {
                        $jml_item = $value_p->jml_item_tr_detail;
                        $jenis_rekanan = $value_p->jenis_rekanan;

                        switch ($jenis_rekanan) {
                            case 'rs':
                                $pengeluaran_rs   += $jml_item;
                                break;

                            case 'pbf':
                                $pengeluaran_pbf   += $jml_item;
                                break;

                            case 'apotik':
                                $pengeluaran_apotik   += $jml_item;
                                break;

                            case 'sp':
                                $pengeluaran_sp   += $jml_item;
                                break;

                            case 'pm':
                                $pengeluaran_pm   += $jml_item;
                                break;

                            case 'kl':
                                $pengeluaran_kl   += $jml_item;
                                break;

                            case 'to':
                                $pengeluaran_to   += $jml_item;
                                break;

                            case 'ln':
                                $pengeluaran_ln   += $jml_item;
                                break;

                            case 'retur':
                                $pengeluaran_retur   += $jml_item;
                                break;

                            case 'lainnya':
                                $pengeluaran_lainnya   += $jml_item;
                                break;
                            
                            default:
                                $pengeluaran_rs   += $jml_item;
                                break;
                        }

                        $pengeluaran_all += $jml_item;
                    }

                // ---------------Pengganti retur penjualan
                    $data_retur_penjualan_p = $this->rp->get_retur_penjualan_detail_p($where_main);

                    foreach ($data_retur_penjualan_p as $key_p => $value_p) {
                        $jml_item = $value_p->jml_item_tr_detail;
                        $jenis_rekanan = $value_p->jenis_rekanan;

                        $pengeluaran_retur   += $jml_item;

                        $pengeluaran_all += $jml_item;
                    }

                // ---------------retur_pembelian
                    $data_retur_pembelian = $this->rp->get_retur_pembelian_detail($where_main_no_item_rusak);

                    foreach ($data_retur_pembelian as $key_p => $value_p) {
                        $jml_item = $value_p->jml_item_tr_detail;
                        $jenis_suplier = $value_p->jenis_suplier;

                        $pengeluaran_retur   += $jml_item;

                        $pengeluaran_all += $jml_item;
                    }

                // ---------------sample
                    $data_sample = $this->rp->get_sample_detail($where_main);

                    foreach ($data_sample as $key_p => $value_p) {
                        $jml_item = $value_p->jml_item_tr_detail;
                        $jenis_rekanan = $value_p->jenis_rekanan;

                        $pengeluaran_ln   += $jml_item;

                        $pengeluaran_all += $jml_item;
                    }

                $tmp_list["pengeluaran"]["rs"]     = $pengeluaran_rs;
                $tmp_list["pengeluaran"]["pbf"]    = $pengeluaran_pbf;
                $tmp_list["pengeluaran"]["apotik"] = $pengeluaran_apotik;
                $tmp_list["pengeluaran"]["sp"]     = $pengeluaran_sp;
                $tmp_list["pengeluaran"]["pm"]     = $pengeluaran_pm;
                $tmp_list["pengeluaran"]["kl"]     = $pengeluaran_kl;
                $tmp_list["pengeluaran"]["to"]     = $pengeluaran_to;
                $tmp_list["pengeluaran"]["ln"]     = $pengeluaran_ln;
                $tmp_list["pengeluaran"]["retur"]  = $pengeluaran_retur;
                $tmp_list["pengeluaran"]["lainnya"]= $pengeluaran_lainnya;

                $tmp_list["pengeluaran_all"] = $pengeluaran_all;

                $tmp_list["stok"] = $stok;
                $tmp_list["stok_move"] = $pemasukan_all - $pengeluaran_all;

                $tmp_list["stok_before"] = $stok - $pemasukan_all + $pengeluaran_all;

                
                // print_r("<br>");
                // print_r("----------------------------------------");
                // print_r("<br>");


                $data["list_data"][$id_item] = $tmp_list;
            }
        }
        // print_r($data["list_data"]);
        // print_r($data["list_data_retur_sample_p"]);
        // print_r($check_data);
        $this->load->view('index', $data);
    }

    public function get_bpom_triwulan(){
        $data["page"] = "report_bpom_main";
        $data["item"] = $this->rp->get_item_asc(array("is_del_item"=>"0"));
        $data["setting"] = $this->mm->get_data_all_where("setting", array("is_del_setting"=>"0"));
        
        $tgl_before = date('Y-m-d', strtotime('-1 day', strtotime(date("Y-m-d"))));

        $data["str_periode"] = "";

        $bln_now = date("m");
        $y_now = date("Y");

        $bln = (int)$bln_now;
        $y = (int)$y_now;

        $list_bln = [1,2,3];
        $str_triwulan = "Triwulan I";
        if (in_array($bln, [1,2,3])) {
            $list_bln = [1,2,3];

            $str_triwulan = "Triwulan I";
        }elseif (in_array($bln, [4,5,6])) {
            $list_bln = [4,5,6];
        

            $str_triwulan = "Triwulan II";
        }elseif (in_array($bln, [7,8,9])) {
            $list_bln = [7,8,9];
        

            $str_triwulan = "Triwulan III";
        }elseif (in_array($bln, [10,11,12])) {
            $list_bln = [10,11,12];


            $str_triwulan = "Triwulan IV";
        }

        $data["str_periode"] = $str_triwulan." - ".$y_now;

        $data["list_data"] = array();
        $data["item_v"] = [];
        $data["list_all_data"] = [];

        // print('Next Date ' . );

        $data["list_record_stok"] = [];
        $data["list_item_stok"] = [];

        if($bln != "0" && $y != "0"){
            foreach ($data["item"] as $key => $value) {
                $id_item = $value->id_item;
                $stok = $value->stok;

                $tmp_list = ["item_now"=>[],
                                "record_item_before"=>[],
                                "pemasukan"=>[
                                            "pabrik"=>0,
                                            "pbf"=>0,
                                            "retur"=>0,
                                            "lainnya"=>0
                                        ],
                                "pemasukan_all"=>0,
                                "pengeluaran"=>[
                                            "rs"=>0,
                                            "pbf"=>0,
                                            "apotik"=>0,
                                            "sp"=>0,
                                            "pm"=>0,
                                            "kl"=>0,
                                            "to"=>0,
                                            "ln"=>0,
                                            "retur"=>0,
                                            "lainnya"=>0
                                        ],
                                "pengeluaran_all"=>0,
                                "stok"=>0,
                                "stok_before"=>0,
                                "stok_move"=>0
                            ];


                $where_main = [
                            "MONTH(th.tgl_transaksi_tr_header)="=>$bln,
                            "YEAR(th.tgl_transaksi_tr_header)="=>$y,
                            "td.id_item"=>$id_item
                        ];

                $where_main_no_item_rusak = [
                            "MONTH(th.tgl_transaksi_tr_header)="=>$bln,
                            "YEAR(th.tgl_transaksi_tr_header)="=>$y,
                            "td.status_rt_tr_detail"=>"0",
                            "td.id_item"=>$id_item
                        ];

                // print_r("<pre>");
                // print_r("<br>");
                // print_r("----------------------------------------");
                // print_r("<br>");

                
                // get rekam stok
                $tmp_list["item_now"] = $value;
                $tmp_list["record_item_before"] = $this->mm->get_data_each("record_item", ["id_item"=>$value->id_item, "periode_record_item"=>$tgl_before]);

                $stok_before = $tmp_list["record_item_before"]["stok_record_item"];
                // $data["list_record_stok"][$id_item] = $this->mm->get_data_each("record_item", ["id_item"=>$value->id_item, "periode_record_item"=>$tgl_before]);

                // $data["list_item_stok"][$id_item] = $value;

            // --------------Pemasukan
                $pemasukan_all = 0;

                $pemasukan_pabrik   = 0;
                $pemasukan_pbf      = 0;
                $pemasukan_retur    = 0;
                $pemasukan_lainnya  = 0;

                // ---------------pembelian
                    // $stok_masuk = 0;
                    
                    $data_pembelian = $this->rp->get_pembelian_item_triwulan($y, $list_bln, array("td.id_item"=>$id_item));

                    foreach ($data_pembelian as $key_p => $value_p) {
                        $jml_item = $value_p->jml_item_tr_detail;
                        $jenis_suplier = $value_p->jenis_suplier;

                        if($jenis_suplier == "pabrik"){
                            $pemasukan_pabrik   += $jml_item;

                        }elseif ($jenis_suplier == "pbf") {
                            $pemasukan_pbf      += $jml_item;
                        
                        }
                        $pemasukan_all += $jml_item;
                    }

                // ---------------retur_penjualan
                    // $stok_masuk = 0;
                    $data_retur_penjualan = $this->rp->get_retur_penjualan_detail_triwulan($list_bln, $where_main_no_item_rusak);
                    
                    foreach ($data_retur_penjualan as $key_p => $value_p) {
                        $jml_item = $value_p->jml_item_tr_detail;
                        $jenis_rekanan = $value_p->jenis_rekanan;

                        $pemasukan_retur   += $jml_item;

                        $pemasukan_all += $jml_item;
                    }

                // ---------------pengganti_retur_pembelian
                    $data_retur_pembelian_p = $this->rp->get_retur_pembelian_detail_p_triwulan($list_bln, $where_main);
                    
                    foreach ($data_retur_pembelian_p as $key_p => $value_p) {
                        $jml_item = $value_p->jml_item_tr_detail;
                        $jenis_suplier = $value_p->jenis_suplier;

                        $pemasukan_retur   += $jml_item;

                        $pemasukan_all += $jml_item;
                    }

                // ---------------Pengganti Retur Sample
                    $data_retur_sample_p = $this->rp->get_retur_sample_detail_p_triwulan($list_bln, $where_main);
                    
                    foreach ($data_retur_sample_p as $key_p => $value_p) {
                        $jml_item = $value_p->jml_item_tr_detail;
                        // $jenis_rekanan = $value_p->jenis_rekanan;

                        // if($jenis_suplier == "pabrik"){
                        //     $pemasukan_pabrik   += $jml_item;

                        // }elseif ($jenis_suplier == "pbf") {
                        //     $pemasukan_pbf      += $jml_item;
                        
                        // }

                        $pemasukan_lainnya += $jml_item;

                        $pemasukan_all += $jml_item;
                    }

                $tmp_list["pemasukan"]["pabrik"]    = $pemasukan_pabrik;
                $tmp_list["pemasukan"]["pbf"]       = $pemasukan_pbf;
                $tmp_list["pemasukan"]["retur"]     = $pemasukan_retur;
                $tmp_list["pemasukan"]["lainnya"]   = $pemasukan_lainnya;

                $tmp_list["pemasukan_all"] = $pemasukan_all;


            // --------------Pemasukan
                $pengeluaran_all = 0;

                $pengeluaran_rs     = 0;
                $pengeluaran_pbf    = 0;
                $pengeluaran_apotik = 0;
                $pengeluaran_sp     = 0;
                $pengeluaran_pm     = 0;
                $pengeluaran_kl     = 0;
                $pengeluaran_to     = 0;
                $pengeluaran_ln     = 0;
                $pengeluaran_retur  = 0;
                $pengeluaran_lainnya= 0;


                // ---------------Penjualan
                    $data_penjualan = $this->rp->get_penjualan_item_triwulan($y, $list_bln, array("td.id_item"=>$id_item));

                    foreach ($data_penjualan as $key_p => $value_p) {
                        $jml_item = $value_p->jml_item_tr_detail;
                        $jenis_rekanan = $value_p->jenis_rekanan;

                        switch ($jenis_rekanan) {
                            case 'rs':
                                $pengeluaran_rs   += $jml_item;
                                break;

                            case 'pbf':
                                $pengeluaran_pbf   += $jml_item;
                                break;

                            case 'apotik':
                                $pengeluaran_apotik   += $jml_item;
                                break;

                            case 'sp':
                                $pengeluaran_sp   += $jml_item;
                                break;

                            case 'pm':
                                $pengeluaran_pm   += $jml_item;
                                break;

                            case 'kl':
                                $pengeluaran_kl   += $jml_item;
                                break;

                            case 'to':
                                $pengeluaran_to   += $jml_item;
                                break;

                            case 'ln':
                                $pengeluaran_ln   += $jml_item;
                                break;

                            case 'retur':
                                $pengeluaran_retur   += $jml_item;
                                break;

                            case 'lainnya':
                                $pengeluaran_lainnya   += $jml_item;
                                break;
                            
                            default:
                                $pengeluaran_rs   += $jml_item;
                                break;
                        }

                        $pengeluaran_all += $jml_item;
                    }

                // ---------------Pengganti retur penjualan
                    $data_retur_penjualan_p = $this->rp->get_retur_penjualan_detail_p_triwulan($list_bln, $where_main);

                    foreach ($data_retur_penjualan_p as $key_p => $value_p) {
                        $jml_item = $value_p->jml_item_tr_detail;
                        $jenis_rekanan = $value_p->jenis_rekanan;

                        $pengeluaran_retur   += $jml_item;

                        $pengeluaran_all += $jml_item;
                    }

                // ---------------retur_pembelian
                    $data_retur_pembelian = $this->rp->get_retur_pembelian_detail_triwulan($list_bln, $where_main_no_item_rusak);

                    foreach ($data_retur_pembelian as $key_p => $value_p) {
                        $jml_item = $value_p->jml_item_tr_detail;
                        $jenis_suplier = $value_p->jenis_suplier;

                        $pengeluaran_retur   += $jml_item;

                        $pengeluaran_all += $jml_item;
                    }

                // ---------------sample
                    $data_sample = $this->rp->get_sample_detail_triwulan($list_bln, $where_main);

                    foreach ($data_sample as $key_p => $value_p) {
                        $jml_item = $value_p->jml_item_tr_detail;
                        $jenis_rekanan = $value_p->jenis_rekanan;

                        $pengeluaran_ln   += $jml_item;

                        $pengeluaran_all += $jml_item;
                    }

                $tmp_list["pengeluaran"]["rs"]     = $pengeluaran_rs;
                $tmp_list["pengeluaran"]["pbf"]    = $pengeluaran_pbf;
                $tmp_list["pengeluaran"]["apotik"] = $pengeluaran_apotik;
                $tmp_list["pengeluaran"]["sp"]     = $pengeluaran_sp;
                $tmp_list["pengeluaran"]["pm"]     = $pengeluaran_pm;
                $tmp_list["pengeluaran"]["kl"]     = $pengeluaran_kl;
                $tmp_list["pengeluaran"]["to"]     = $pengeluaran_to;
                $tmp_list["pengeluaran"]["ln"]     = $pengeluaran_ln;
                $tmp_list["pengeluaran"]["retur"]  = $pengeluaran_retur;
                $tmp_list["pengeluaran"]["lainnya"]= $pengeluaran_lainnya;

                $tmp_list["pengeluaran_all"] = $pengeluaran_all;

                $tmp_list["stok"] = $stok;
                $tmp_list["stok_move"] = $pemasukan_all - $pengeluaran_all;

                $tmp_list["stok_before"] = $stok - $pemasukan_all + $pengeluaran_all;

                
                // print_r("<br>");
                // print_r("----------------------------------------");
                // print_r("<br>");


                $data["list_data"][$id_item] = $tmp_list;
            }
        }
        // print_r($data["list_data"]);
        // print_r($data["list_data_retur_sample_p"]);
        // print_r($check_data);
        $this->load->view('index', $data);
    }

    public function get_bpom_bulan(){
        $data["page"] = "report_bpom_main";
        $data["item"] = $this->rp->get_item_asc(array("is_del_item"=>"0"));
        $data["setting"] = $this->mm->get_data_all_where("setting", array("is_del_setting"=>"0"));
        
        $tgl_before = date('Y-m-d', strtotime('-1 day', strtotime(date("Y-m-d"))));

        $data["str_periode"] = "";

        $bln_now = date("m");
        $y_now = date("Y");

        $bln = (int)$bln_now;
        $y = (int)$y_now;

        $data["str_periode"] = $this->array_of_month[$bln]." ".$y_now;

        $data["list_data"] = array();
        $data["item_v"] = [];
        $data["list_all_data"] = [];

        // print('Next Date ' . );

        $data["list_record_stok"] = [];
        $data["list_item_stok"] = [];

        if($bln != "0" && $y != "0"){
            foreach ($data["item"] as $key => $value) {
                $id_item = $value->id_item;
                $stok = $value->stok;

                $tmp_list = ["item_now"=>[],
                                "record_item_before"=>[],
                                "pemasukan"=>[
                                            "pabrik"=>0,
                                            "pbf"=>0,
                                            "retur"=>0,
                                            "lainnya"=>0
                                        ],
                                "pemasukan_all"=>0,
                                "pengeluaran"=>[
                                            "rs"=>0,
                                            "pbf"=>0,
                                            "apotik"=>0,
                                            "sp"=>0,
                                            "pm"=>0,
                                            "kl"=>0,
                                            "to"=>0,
                                            "ln"=>0,
                                            "retur"=>0,
                                            "lainnya"=>0
                                        ],
                                "pengeluaran_all"=>0,
                                "stok"=>0,
                                "stok_before"=>0,
                                "stok_move"=>0
                            ];


                $where_main = [
                            "MONTH(th.tgl_transaksi_tr_header)="=>$bln,
                            "YEAR(th.tgl_transaksi_tr_header)="=>$y,
                            "td.id_item"=>$id_item
                        ];

                $where_main_no_item_rusak = [
                            "MONTH(th.tgl_transaksi_tr_header)="=>$bln,
                            "YEAR(th.tgl_transaksi_tr_header)="=>$y,
                            "td.status_rt_tr_detail"=>"0",
                            "td.id_item"=>$id_item
                        ];

                // print_r("<pre>");
                // print_r("<br>");
                // print_r("----------------------------------------");
                // print_r("<br>");

                
                // get rekam stok
                $tmp_list["item_now"] = $value;
                $tmp_list["record_item_before"] = $this->mm->get_data_each("record_item", ["id_item"=>$value->id_item, "periode_record_item"=>$tgl_before]);

                $stok_before = $tmp_list["record_item_before"]["stok_record_item"];
                // $data["list_record_stok"][$id_item] = $this->mm->get_data_each("record_item", ["id_item"=>$value->id_item, "periode_record_item"=>$tgl_before]);

                // $data["list_item_stok"][$id_item] = $value;

            // --------------Pemasukan
                $pemasukan_all = 0;

                $pemasukan_pabrik   = 0;
                $pemasukan_pbf      = 0;
                $pemasukan_retur    = 0;
                $pemasukan_lainnya  = 0;

                // ---------------pembelian
                    // $stok_masuk = 0;
                    
                    $data_pembelian = $this->rp->get_pembelian_item_bulan($bln, $y, array("td.id_item"=>$id_item));

                    foreach ($data_pembelian as $key_p => $value_p) {
                        $jml_item = $value_p->jml_item_tr_detail;
                        $jenis_suplier = $value_p->jenis_suplier;

                        if($jenis_suplier == "pabrik"){
                            $pemasukan_pabrik   += $jml_item;

                        }elseif ($jenis_suplier == "pbf") {
                            $pemasukan_pbf      += $jml_item;
                        
                        }
                        $pemasukan_all += $jml_item;
                    }

                // ---------------retur_penjualan
                    // $stok_masuk = 0;
                    $data_retur_penjualan = $this->rp->get_retur_penjualan_detail($where_main_no_item_rusak);
                    
                    foreach ($data_retur_penjualan as $key_p => $value_p) {
                        $jml_item = $value_p->jml_item_tr_detail;
                        $jenis_rekanan = $value_p->jenis_rekanan;

                        $pemasukan_retur   += $jml_item;

                        $pemasukan_all += $jml_item;
                    }

                // ---------------pengganti_retur_pembelian
                    $data_retur_pembelian_p = $this->rp->get_retur_pembelian_detail_p($where_main);
                    
                    foreach ($data_retur_pembelian_p as $key_p => $value_p) {
                        $jml_item = $value_p->jml_item_tr_detail;
                        $jenis_suplier = $value_p->jenis_suplier;

                        $pemasukan_retur   += $jml_item;

                        $pemasukan_all += $jml_item;
                    }

                // ---------------Pengganti Retur Sample
                    $data_retur_sample_p = $this->rp->get_retur_sample_detail_p($where_main);
                    
                    foreach ($data_retur_sample_p as $key_p => $value_p) {
                        $jml_item = $value_p->jml_item_tr_detail;
                        // $jenis_rekanan = $value_p->jenis_rekanan;

                        // if($jenis_suplier == "pabrik"){
                        //     $pemasukan_pabrik   += $jml_item;

                        // }elseif ($jenis_suplier == "pbf") {
                        //     $pemasukan_pbf      += $jml_item;
                        
                        // }

                        $pemasukan_lainnya += $jml_item;

                        $pemasukan_all += $jml_item;
                    }

                $tmp_list["pemasukan"]["pabrik"]    = $pemasukan_pabrik;
                $tmp_list["pemasukan"]["pbf"]       = $pemasukan_pbf;
                $tmp_list["pemasukan"]["retur"]     = $pemasukan_retur;
                $tmp_list["pemasukan"]["lainnya"]   = $pemasukan_lainnya;

                $tmp_list["pemasukan_all"] = $pemasukan_all;


            // --------------Pemasukan
                $pengeluaran_all = 0;

                $pengeluaran_rs     = 0;
                $pengeluaran_pbf    = 0;
                $pengeluaran_apotik = 0;
                $pengeluaran_sp     = 0;
                $pengeluaran_pm     = 0;
                $pengeluaran_kl     = 0;
                $pengeluaran_to     = 0;
                $pengeluaran_ln     = 0;
                $pengeluaran_retur  = 0;
                $pengeluaran_lainnya= 0;


                // ---------------Penjualan
                    $data_penjualan = $this->rp->get_penjualan_item_bulan($bln, $y, array("td.id_item"=>$id_item));

                    foreach ($data_penjualan as $key_p => $value_p) {
                        $jml_item = $value_p->jml_item_tr_detail;
                        $jenis_rekanan = $value_p->jenis_rekanan;

                        switch ($jenis_rekanan) {
                            case 'rs':
                                $pengeluaran_rs   += $jml_item;
                                break;

                            case 'pbf':
                                $pengeluaran_pbf   += $jml_item;
                                break;

                            case 'apotik':
                                $pengeluaran_apotik   += $jml_item;
                                break;

                            case 'sp':
                                $pengeluaran_sp   += $jml_item;
                                break;

                            case 'pm':
                                $pengeluaran_pm   += $jml_item;
                                break;

                            case 'kl':
                                $pengeluaran_kl   += $jml_item;
                                break;

                            case 'to':
                                $pengeluaran_to   += $jml_item;
                                break;

                            case 'ln':
                                $pengeluaran_ln   += $jml_item;
                                break;

                            case 'retur':
                                $pengeluaran_retur   += $jml_item;
                                break;

                            case 'lainnya':
                                $pengeluaran_lainnya   += $jml_item;
                                break;
                            
                            default:
                                $pengeluaran_rs   += $jml_item;
                                break;
                        }

                        $pengeluaran_all += $jml_item;
                    }

                // ---------------Pengganti retur penjualan
                    $data_retur_penjualan_p = $this->rp->get_retur_penjualan_detail_p($where_main);

                    foreach ($data_retur_penjualan_p as $key_p => $value_p) {
                        $jml_item = $value_p->jml_item_tr_detail;
                        $jenis_rekanan = $value_p->jenis_rekanan;

                        $pengeluaran_retur   += $jml_item;

                        $pengeluaran_all += $jml_item;
                    }

                // ---------------retur_pembelian
                    $data_retur_pembelian = $this->rp->get_retur_pembelian_detail($where_main_no_item_rusak);

                    foreach ($data_retur_pembelian as $key_p => $value_p) {
                        $jml_item = $value_p->jml_item_tr_detail;
                        $jenis_suplier = $value_p->jenis_suplier;

                        $pengeluaran_retur   += $jml_item;

                        $pengeluaran_all += $jml_item;
                    }

                // ---------------sample
                    $data_sample = $this->rp->get_sample_detail($where_main);

                    foreach ($data_sample as $key_p => $value_p) {
                        $jml_item = $value_p->jml_item_tr_detail;
                        $jenis_rekanan = $value_p->jenis_rekanan;

                        $pengeluaran_ln   += $jml_item;

                        $pengeluaran_all += $jml_item;
                    }

                $tmp_list["pengeluaran"]["rs"]     = $pengeluaran_rs;
                $tmp_list["pengeluaran"]["pbf"]    = $pengeluaran_pbf;
                $tmp_list["pengeluaran"]["apotik"] = $pengeluaran_apotik;
                $tmp_list["pengeluaran"]["sp"]     = $pengeluaran_sp;
                $tmp_list["pengeluaran"]["pm"]     = $pengeluaran_pm;
                $tmp_list["pengeluaran"]["kl"]     = $pengeluaran_kl;
                $tmp_list["pengeluaran"]["to"]     = $pengeluaran_to;
                $tmp_list["pengeluaran"]["ln"]     = $pengeluaran_ln;
                $tmp_list["pengeluaran"]["retur"]  = $pengeluaran_retur;
                $tmp_list["pengeluaran"]["lainnya"]= $pengeluaran_lainnya;

                $tmp_list["pengeluaran_all"] = $pengeluaran_all;

                $tmp_list["stok"] = $stok;
                $tmp_list["stok_move"] = $pemasukan_all - $pengeluaran_all;

                $tmp_list["stok_before"] = $stok - $pemasukan_all + $pengeluaran_all;

                
                // print_r("<br>");
                // print_r("----------------------------------------");
                // print_r("<br>");


                $data["list_data"][$id_item] = $tmp_list;
            }
        }
        // print_r($data["list_data"]);
        // print_r($data["list_data_retur_sample_p"]);
        // print_r($check_data);
        $this->load->view('index', $data);
    }

    public function get_bpom_th(){
        $data["page"] = "report_bpom_main";
        $data["item"] = $this->rp->get_item_asc(array("is_del_item"=>"0"));
        $data["setting"] = $this->mm->get_data_all_where("setting", array("is_del_setting"=>"0"));
        
        $tgl_before = date('Y-m-d', strtotime('-1 day', strtotime(date("Y-m-d"))));

        $data["str_periode"] = "";

        $y_now = date("Y");

        $y = (int)$y_now;

        $data["str_periode"] = $y_now;

        $data["list_data"] = array();
        $data["item_v"] = [];
        $data["list_all_data"] = [];

        // print('Next Date ' . );

        $data["list_record_stok"] = [];
        $data["list_item_stok"] = [];

        if($y != "0"){
            foreach ($data["item"] as $key => $value) {
                $id_item = $value->id_item;
                $stok = $value->stok;

                $tmp_list = ["item_now"=>[],
                                "record_item_before"=>[],
                                "pemasukan"=>[
                                            "pabrik"=>0,
                                            "pbf"=>0,
                                            "retur"=>0,
                                            "lainnya"=>0
                                        ],
                                "pemasukan_all"=>0,
                                "pengeluaran"=>[
                                            "rs"=>0,
                                            "pbf"=>0,
                                            "apotik"=>0,
                                            "sp"=>0,
                                            "pm"=>0,
                                            "kl"=>0,
                                            "to"=>0,
                                            "ln"=>0,
                                            "retur"=>0,
                                            "lainnya"=>0
                                        ],
                                "pengeluaran_all"=>0,
                                "stok"=>0,
                                "stok_before"=>0,
                                "stok_move"=>0
                            ];


                $where_main = [
                            "YEAR(th.tgl_transaksi_tr_header)="=>$y,
                            "td.id_item"=>$id_item
                        ];

                $where_main_no_item_rusak = [
                            "YEAR(th.tgl_transaksi_tr_header)="=>$y,
                            "td.status_rt_tr_detail"=>"0",
                            "td.id_item"=>$id_item
                        ];

                // print_r("<pre>");
                // print_r("<br>");
                // print_r("----------------------------------------");
                // print_r("<br>");

                
                // get rekam stok
                $tmp_list["item_now"] = $value;
                $tmp_list["record_item_before"] = $this->mm->get_data_each("record_item", 
                                                                                ["id_item"=>$value->id_item, 
                                                                                "periode_record_item"=>$tgl_before]);

                $stok_before = $tmp_list["record_item_before"]["stok_record_item"];
                // $data["list_record_stok"][$id_item] = $this->mm->get_data_each("record_item", ["id_item"=>$value->id_item, "periode_record_item"=>$tgl_before]);

                // $data["list_item_stok"][$id_item] = $value;

            // --------------Pemasukan
                $pemasukan_all = 0;

                $pemasukan_pabrik   = 0;
                $pemasukan_pbf      = 0;
                $pemasukan_retur    = 0;
                $pemasukan_lainnya  = 0;

                // ---------------pembelian
                    // $stok_masuk = 0;
                    
                    $data_pembelian = $this->rp->get_pembelian_item_th($y, array("td.id_item"=>$id_item));

                    foreach ($data_pembelian as $key_p => $value_p) {
                        $jml_item = $value_p->jml_item_tr_detail;
                        $jenis_suplier = $value_p->jenis_suplier;

                        if($jenis_suplier == "pabrik"){
                            $pemasukan_pabrik   += $jml_item;

                        }elseif ($jenis_suplier == "pbf") {
                            $pemasukan_pbf      += $jml_item;
                        
                        }
                        $pemasukan_all += $jml_item;
                    }

                // ---------------retur_penjualan
                    // $stok_masuk = 0;
                    $data_retur_penjualan = $this->rp->get_retur_penjualan_detail($where_main_no_item_rusak);
                    
                    foreach ($data_retur_penjualan as $key_p => $value_p) {
                        $jml_item = $value_p->jml_item_tr_detail;
                        $jenis_rekanan = $value_p->jenis_rekanan;

                        $pemasukan_retur   += $jml_item;

                        $pemasukan_all += $jml_item;
                    }

                // ---------------pengganti_retur_pembelian
                    $data_retur_pembelian_p = $this->rp->get_retur_pembelian_detail_p($where_main);
                    
                    foreach ($data_retur_pembelian_p as $key_p => $value_p) {
                        $jml_item = $value_p->jml_item_tr_detail;
                        $jenis_suplier = $value_p->jenis_suplier;

                        $pemasukan_retur   += $jml_item;

                        $pemasukan_all += $jml_item;
                    }

                // ---------------Pengganti Retur Sample
                    $data_retur_sample_p = $this->rp->get_retur_sample_detail_p($where_main);
                    
                    foreach ($data_retur_sample_p as $key_p => $value_p) {
                        $jml_item = $value_p->jml_item_tr_detail;
                        // $jenis_rekanan = $value_p->jenis_rekanan;

                        // if($jenis_suplier == "pabrik"){
                        //     $pemasukan_pabrik   += $jml_item;

                        // }elseif ($jenis_suplier == "pbf") {
                        //     $pemasukan_pbf      += $jml_item;
                        
                        // }

                        $pemasukan_lainnya += $jml_item;

                        $pemasukan_all += $jml_item;
                    }

                $tmp_list["pemasukan"]["pabrik"]    = $pemasukan_pabrik;
                $tmp_list["pemasukan"]["pbf"]       = $pemasukan_pbf;
                $tmp_list["pemasukan"]["retur"]     = $pemasukan_retur;
                $tmp_list["pemasukan"]["lainnya"]   = $pemasukan_lainnya;

                $tmp_list["pemasukan_all"] = $pemasukan_all;


            // --------------Pemasukan
                $pengeluaran_all = 0;

                $pengeluaran_rs     = 0;
                $pengeluaran_pbf    = 0;
                $pengeluaran_apotik = 0;
                $pengeluaran_sp     = 0;
                $pengeluaran_pm     = 0;
                $pengeluaran_kl     = 0;
                $pengeluaran_to     = 0;
                $pengeluaran_ln     = 0;
                $pengeluaran_retur  = 0;
                $pengeluaran_lainnya= 0;


                // ---------------Penjualan
                    $data_penjualan = $this->rp->get_penjualan_item_th($y, array("td.id_item"=>$id_item));

                    foreach ($data_penjualan as $key_p => $value_p) {
                        $jml_item = $value_p->jml_item_tr_detail;
                        $jenis_rekanan = $value_p->jenis_rekanan;

                        switch ($jenis_rekanan) {
                            case 'rs':
                                $pengeluaran_rs   += $jml_item;
                                break;

                            case 'pbf':
                                $pengeluaran_pbf   += $jml_item;
                                break;

                            case 'apotik':
                                $pengeluaran_apotik   += $jml_item;
                                break;

                            case 'sp':
                                $pengeluaran_sp   += $jml_item;
                                break;

                            case 'pm':
                                $pengeluaran_pm   += $jml_item;
                                break;

                            case 'kl':
                                $pengeluaran_kl   += $jml_item;
                                break;

                            case 'to':
                                $pengeluaran_to   += $jml_item;
                                break;

                            case 'ln':
                                $pengeluaran_ln   += $jml_item;
                                break;

                            case 'retur':
                                $pengeluaran_retur   += $jml_item;
                                break;

                            case 'lainnya':
                                $pengeluaran_lainnya   += $jml_item;
                                break;
                            
                            default:
                                $pengeluaran_rs   += $jml_item;
                                break;
                        }

                        $pengeluaran_all += $jml_item;
                    }

                // ---------------Pengganti retur penjualan
                    $data_retur_penjualan_p = $this->rp->get_retur_penjualan_detail_p($where_main);

                    foreach ($data_retur_penjualan_p as $key_p => $value_p) {
                        $jml_item = $value_p->jml_item_tr_detail;
                        $jenis_rekanan = $value_p->jenis_rekanan;

                        $pengeluaran_retur   += $jml_item;

                        $pengeluaran_all += $jml_item;
                    }

                // ---------------retur_pembelian
                    $data_retur_pembelian = $this->rp->get_retur_pembelian_detail($where_main_no_item_rusak);

                    foreach ($data_retur_pembelian as $key_p => $value_p) {
                        $jml_item = $value_p->jml_item_tr_detail;
                        $jenis_suplier = $value_p->jenis_suplier;

                        $pengeluaran_retur   += $jml_item;

                        $pengeluaran_all += $jml_item;
                    }

                // ---------------sample
                    $data_sample = $this->rp->get_sample_detail($where_main);

                    foreach ($data_sample as $key_p => $value_p) {
                        $jml_item = $value_p->jml_item_tr_detail;
                        $jenis_rekanan = $value_p->jenis_rekanan;

                        $pengeluaran_ln   += $jml_item;

                        $pengeluaran_all += $jml_item;
                    }

                $tmp_list["pengeluaran"]["rs"]     = $pengeluaran_rs;
                $tmp_list["pengeluaran"]["pbf"]    = $pengeluaran_pbf;
                $tmp_list["pengeluaran"]["apotik"] = $pengeluaran_apotik;
                $tmp_list["pengeluaran"]["sp"]     = $pengeluaran_sp;
                $tmp_list["pengeluaran"]["pm"]     = $pengeluaran_pm;
                $tmp_list["pengeluaran"]["kl"]     = $pengeluaran_kl;
                $tmp_list["pengeluaran"]["to"]     = $pengeluaran_to;
                $tmp_list["pengeluaran"]["ln"]     = $pengeluaran_ln;
                $tmp_list["pengeluaran"]["retur"]  = $pengeluaran_retur;
                $tmp_list["pengeluaran"]["lainnya"]= $pengeluaran_lainnya;

                $tmp_list["pengeluaran_all"] = $pengeluaran_all;

                $tmp_list["stok"] = $stok;
                $tmp_list["stok_move"] = $pemasukan_all - $pengeluaran_all;

                $tmp_list["stok_before"] = $stok - $pemasukan_all + $pengeluaran_all;

                
                // print_r("<br>");
                // print_r("----------------------------------------");
                // print_r("<br>");


                $data["list_data"][$id_item] = $tmp_list;
            }
        }
        // print_r($data["list_data"]);
        // print_r($data["list_data_retur_sample_p"]);
        // print_r($check_data);
        $this->load->view('index', $data);
    }
#------------------------------show----------------------------------#

#------------------------------main----------------------------------#
    public function main_get_bpom_tgl(){
        $data["page"] = "report_bpom_main";
        $data["item"] = $this->rp->get_item_asc(array("is_del_item"=>"0"));
        $data["setting"] = $this->mm->get_data_all_where("setting", array("is_del_setting"=>"0"));

        $tgl_before = date('Y-m-d', strtotime('-1 day', strtotime(date("Y-m-d"))));

        $data["str_periode"] = "";

        $date_now = date("Y-m-d");

        $data["str_periode"] = $date_now;

        $tgl_start = $date_now;
        $tgl_finish = $date_now;
        // $data["item"] = $this->mm->get_data_all_where("item", array("is_del_item"=>"0"));

        $data["list_data"] = array();
        $data["item_v"] = [];
        $data["list_all_data"] = [];

        // print('Next Date ' . );

        $data["list_record_stok"] = [];
        $data["list_item_stok"] = [];

        if($tgl_start != "0" && $tgl_finish != "0"){
            foreach ($data["item"] as $key => $value) {
                $id_item = $value->id_item;
                $stok = $value->stok;

                $tmp_list = ["item_now"=>[],
                                "record_item_before"=>[],
                                "pemasukan"=>[
                                            "pabrik"=>0,
                                            "pbf"=>0,
                                            "retur"=>0,
                                            "lainnya"=>0
                                        ],
                                "pemasukan_all"=>0,
                                "pengeluaran"=>[
                                            "rs"=>0,
                                            "pbf"=>0,
                                            "apotik"=>0,
                                            "sp"=>0,
                                            "pm"=>0,
                                            "kl"=>0,
                                            "to"=>0,
                                            "ln"=>0,
                                            "retur"=>0,
                                            "lainnya"=>0
                                        ],
                                "pengeluaran_all"=>0,
                                "stok"=>0,
                                "stok_before"=>0,
                                "stok_move"=>0
                            ];


                $where_main_no_item_rusak = [
                            "th.tgl_transaksi_tr_header >="=>$tgl_start,
                            "th.tgl_transaksi_tr_header <="=>$tgl_finish,
                            "td.status_rt_tr_detail"=>"0",
                            "td.id_item"=>$id_item
                        ];

                $where_main = [
                            "th.tgl_transaksi_tr_header >="=>$tgl_start,
                            "th.tgl_transaksi_tr_header <="=>$tgl_finish,
                            "td.id_item"=>$id_item
                        ];

                // print_r("<pre>");
                // print_r("<br>");
                // print_r("----------------------------------------");
                // print_r("<br>");

                
                // get rekam stok
                $tmp_list["item_now"] = $value;
                $tmp_list["record_item_before"] = $this->mm->get_data_each("record_item", ["id_item"=>$value->id_item, "periode_record_item"=>$tgl_before]);

                $stok_before = $tmp_list["record_item_before"]["stok_record_item"];
                // $data["list_record_stok"][$id_item] = $this->mm->get_data_each("record_item", ["id_item"=>$value->id_item, "periode_record_item"=>$tgl_before]);

                // $data["list_item_stok"][$id_item] = $value;

            // --------------Pemasukan
                $pemasukan_all = 0;

                $pemasukan_pabrik   = 0;
                $pemasukan_pbf      = 0;
                $pemasukan_retur    = 0;
                $pemasukan_lainnya  = 0;

                // ---------------pembelian
                    // $stok_masuk = 0;
                    
                    $data_pembelian = $this->rp->get_pembelian_item_tgl($tgl_start, $tgl_finish, array("td.id_item"=>$id_item));

                    foreach ($data_pembelian as $key_p => $value_p) {
                        $jml_item = $value_p->jml_item_tr_detail;
                        $jenis_suplier = $value_p->jenis_suplier;

                        if($jenis_suplier == "pabrik"){
                            $pemasukan_pabrik   += $jml_item;

                        }elseif ($jenis_suplier == "pbf") {
                            $pemasukan_pbf      += $jml_item;
                        
                        }
                        $pemasukan_all += $jml_item;
                    }

                // ---------------retur_penjualan
                    // $stok_masuk = 0;
                    $data_retur_penjualan = $this->rp->get_retur_penjualan_detail($where_main_no_item_rusak);
                    
                    foreach ($data_retur_penjualan as $key_p => $value_p) {
                        $jml_item = $value_p->jml_item_tr_detail;
                        $jenis_rekanan = $value_p->jenis_rekanan;

                        $pemasukan_retur   += $jml_item;

                        $pemasukan_all += $jml_item;
                    }

                // ---------------pengganti_retur_pembelian
                    $data_retur_pembelian_p = $this->rp->get_retur_pembelian_detail_p($where_main);
                    
                    foreach ($data_retur_pembelian_p as $key_p => $value_p) {
                        $jml_item = $value_p->jml_item_tr_detail;
                        $jenis_suplier = $value_p->jenis_suplier;

                        $pemasukan_retur   += $jml_item;

                        $pemasukan_all += $jml_item;
                    }

                // ---------------Pengganti Retur Sample
                    $data_retur_sample_p = $this->rp->get_retur_sample_detail_p($where_main);
                    
                    foreach ($data_retur_sample_p as $key_p => $value_p) {
                        $jml_item = $value_p->jml_item_tr_detail;
                        // $jenis_rekanan = $value_p->jenis_rekanan;

                        // if($jenis_suplier == "pabrik"){
                        //     $pemasukan_pabrik   += $jml_item;

                        // }elseif ($jenis_suplier == "pbf") {
                        //     $pemasukan_pbf      += $jml_item;
                        
                        // }
                        $pemasukan_lainnya += $jml_item;
                        
                        $pemasukan_all += $jml_item;
                    }

                $tmp_list["pemasukan"]["pabrik"]    = $pemasukan_pabrik;
                $tmp_list["pemasukan"]["pbf"]       = $pemasukan_pbf;
                $tmp_list["pemasukan"]["retur"]     = $pemasukan_retur;
                $tmp_list["pemasukan"]["lainnya"]   = $pemasukan_lainnya;

                $tmp_list["pemasukan_all"] = $pemasukan_all;


            // --------------Pemasukan
                $pengeluaran_all = 0;

                $pengeluaran_rs     = 0;
                $pengeluaran_pbf    = 0;
                $pengeluaran_apotik = 0;
                $pengeluaran_sp     = 0;
                $pengeluaran_pm     = 0;
                $pengeluaran_kl     = 0;
                $pengeluaran_to     = 0;
                $pengeluaran_ln     = 0;
                $pengeluaran_retur  = 0;
                $pengeluaran_lainnya= 0;


                // ---------------Penjualan
                    $data_penjualan = $this->rp->get_penjualan_item_tgl($tgl_start, $tgl_finish, array("td.id_item"=>$id_item));

                    foreach ($data_penjualan as $key_p => $value_p) {
                        $jml_item = $value_p->jml_item_tr_detail;
                        $jenis_rekanan = $value_p->jenis_rekanan;

                        switch ($jenis_rekanan) {
                            case 'rs':
                                $pengeluaran_rs   += $jml_item;
                                break;

                            case 'pbf':
                                $pengeluaran_pbf   += $jml_item;
                                break;

                            case 'apotik':
                                $pengeluaran_apotik   += $jml_item;
                                break;

                            case 'sp':
                                $pengeluaran_sp   += $jml_item;
                                break;

                            case 'pm':
                                $pengeluaran_pm   += $jml_item;
                                break;

                            case 'kl':
                                $pengeluaran_kl   += $jml_item;
                                break;

                            case 'to':
                                $pengeluaran_to   += $jml_item;
                                break;

                            case 'ln':
                                $pengeluaran_ln   += $jml_item;
                                break;

                            case 'retur':
                                $pengeluaran_retur   += $jml_item;
                                break;

                            case 'lainnya':
                                $pengeluaran_lainnya   += $jml_item;
                                break;
                            
                            default:
                                $pengeluaran_rs   += $jml_item;
                                break;
                        }

                        $pengeluaran_all += $jml_item;
                    }

                // ---------------Pengganti retur penjualan
                    $data_retur_penjualan_p = $this->rp->get_retur_penjualan_detail_p($where_main);

                    foreach ($data_retur_penjualan_p as $key_p => $value_p) {
                        $jml_item = $value_p->jml_item_tr_detail;
                        $jenis_rekanan = $value_p->jenis_rekanan;

                        $pengeluaran_retur   += $jml_item;

                        $pengeluaran_all += $jml_item;
                    }

                // ---------------retur_pembelian
                    $data_retur_pembelian = $this->rp->get_retur_pembelian_detail($where_main_no_item_rusak);

                    foreach ($data_retur_pembelian as $key_p => $value_p) {
                        $jml_item = $value_p->jml_item_tr_detail;
                        $jenis_suplier = $value_p->jenis_suplier;

                        $pengeluaran_retur   += $jml_item;

                        $pengeluaran_all += $jml_item;
                    }

                // ---------------sample
                    $data_sample = $this->rp->get_sample_detail($where_main);

                    foreach ($data_sample as $key_p => $value_p) {
                        $jml_item = $value_p->jml_item_tr_detail;
                        $jenis_rekanan = $value_p->jenis_rekanan;

                        $pengeluaran_ln   += $jml_item;

                        $pengeluaran_all += $jml_item;
                    }

                $tmp_list["pengeluaran"]["rs"]     = $pengeluaran_rs;
                $tmp_list["pengeluaran"]["pbf"]    = $pengeluaran_pbf;
                $tmp_list["pengeluaran"]["apotik"] = $pengeluaran_apotik;
                $tmp_list["pengeluaran"]["sp"]     = $pengeluaran_sp;
                $tmp_list["pengeluaran"]["pm"]     = $pengeluaran_pm;
                $tmp_list["pengeluaran"]["kl"]     = $pengeluaran_kl;
                $tmp_list["pengeluaran"]["to"]     = $pengeluaran_to;
                $tmp_list["pengeluaran"]["ln"]     = $pengeluaran_ln;
                $tmp_list["pengeluaran"]["retur"]  = $pengeluaran_retur;
                $tmp_list["pengeluaran"]["lainnya"]= $pengeluaran_lainnya;

                $tmp_list["pengeluaran_all"] = $pengeluaran_all;

                $tmp_list["stok"] = $stok;
                $tmp_list["stok_move"] = $pemasukan_all - $pengeluaran_all;

                $tmp_list["stok_before"] = $stok - $pemasukan_all + $pengeluaran_all;

                
                // print_r("<br>");
                // print_r("----------------------------------------");
                // print_r("<br>");


                $data["list_data"][$id_item] = $tmp_list;
            }
        }

        return $data;
    }

    public function main_get_bpom_triwulan(){
        $data["page"] = "report_bpom_main";
        $data["item"] = $this->rp->get_item_asc(array("is_del_item"=>"0"));
        $data["setting"] = $this->mm->get_data_all_where("setting", array("is_del_setting"=>"0"));
        
        $tgl_before = date('Y-m-d', strtotime('-1 day', strtotime(date("Y-m-d"))));

        $data["str_periode"] = "";

        $bln_now = date("m");
        $y_now = date("Y");

        $bln = (int)$bln_now;
        $y = (int)$y_now;

        $list_bln = [1,2,3];
        $str_triwulan = "Triwulan I";
        if (in_array($bln, [1,2,3])) {
            $list_bln = [1,2,3];

            $str_triwulan = "Triwulan I";
        }elseif (in_array($bln, [4,5,6])) {
            $list_bln = [4,5,6];
        

            $str_triwulan = "Triwulan II";
        }elseif (in_array($bln, [7,8,9])) {
            $list_bln = [7,8,9];
        

            $str_triwulan = "Triwulan III";
        }elseif (in_array($bln, [10,11,12])) {
            $list_bln = [10,11,12];


            $str_triwulan = "Triwulan IV";
        }

        $data["str_periode"] = $str_triwulan." - ".$y_now;

        $data["list_data"] = array();
        $data["item_v"] = [];
        $data["list_all_data"] = [];

        // print('Next Date ' . );

        $data["list_record_stok"] = [];
        $data["list_item_stok"] = [];

        if($bln != "0" && $y != "0"){
            foreach ($data["item"] as $key => $value) {
                $id_item = $value->id_item;
                $stok = $value->stok;

                $tmp_list = ["item_now"=>[],
                                "record_item_before"=>[],
                                "pemasukan"=>[
                                            "pabrik"=>0,
                                            "pbf"=>0,
                                            "retur"=>0,
                                            "lainnya"=>0
                                        ],
                                "pemasukan_all"=>0,
                                "pengeluaran"=>[
                                            "rs"=>0,
                                            "pbf"=>0,
                                            "apotik"=>0,
                                            "sp"=>0,
                                            "pm"=>0,
                                            "kl"=>0,
                                            "to"=>0,
                                            "ln"=>0,
                                            "retur"=>0,
                                            "lainnya"=>0
                                        ],
                                "pengeluaran_all"=>0,
                                "stok"=>0,
                                "stok_before"=>0,
                                "stok_move"=>0
                            ];


                $where_main = [
                            "MONTH(th.tgl_transaksi_tr_header)="=>$bln,
                            "YEAR(th.tgl_transaksi_tr_header)="=>$y,
                            "td.id_item"=>$id_item
                        ];

                $where_main_no_item_rusak = [
                            "MONTH(th.tgl_transaksi_tr_header)="=>$bln,
                            "YEAR(th.tgl_transaksi_tr_header)="=>$y,
                            "td.status_rt_tr_detail"=>"0",
                            "td.id_item"=>$id_item
                        ];

                // print_r("<pre>");
                // print_r("<br>");
                // print_r("----------------------------------------");
                // print_r("<br>");

                
                // get rekam stok
                $tmp_list["item_now"] = $value;
                $tmp_list["record_item_before"] = $this->mm->get_data_each("record_item", ["id_item"=>$value->id_item, "periode_record_item"=>$tgl_before]);

                $stok_before = $tmp_list["record_item_before"]["stok_record_item"];
                // $data["list_record_stok"][$id_item] = $this->mm->get_data_each("record_item", ["id_item"=>$value->id_item, "periode_record_item"=>$tgl_before]);

                // $data["list_item_stok"][$id_item] = $value;

            // --------------Pemasukan
                $pemasukan_all = 0;

                $pemasukan_pabrik   = 0;
                $pemasukan_pbf      = 0;
                $pemasukan_retur    = 0;
                $pemasukan_lainnya  = 0;

                // ---------------pembelian
                    // $stok_masuk = 0;
                    
                    $data_pembelian = $this->rp->get_pembelian_item_triwulan($y, $list_bln, array("td.id_item"=>$id_item));

                    foreach ($data_pembelian as $key_p => $value_p) {
                        $jml_item = $value_p->jml_item_tr_detail;
                        $jenis_suplier = $value_p->jenis_suplier;

                        if($jenis_suplier == "pabrik"){
                            $pemasukan_pabrik   += $jml_item;

                        }elseif ($jenis_suplier == "pbf") {
                            $pemasukan_pbf      += $jml_item;
                        
                        }
                        $pemasukan_all += $jml_item;
                    }

                // ---------------retur_penjualan
                    // $stok_masuk = 0;
                    $data_retur_penjualan = $this->rp->get_retur_penjualan_detail_triwulan($list_bln, $where_main_no_item_rusak);
                    
                    foreach ($data_retur_penjualan as $key_p => $value_p) {
                        $jml_item = $value_p->jml_item_tr_detail;
                        $jenis_rekanan = $value_p->jenis_rekanan;

                        $pemasukan_retur   += $jml_item;

                        $pemasukan_all += $jml_item;
                    }

                // ---------------pengganti_retur_pembelian
                    $data_retur_pembelian_p = $this->rp->get_retur_pembelian_detail_p_triwulan($list_bln, $where_main);
                    
                    foreach ($data_retur_pembelian_p as $key_p => $value_p) {
                        $jml_item = $value_p->jml_item_tr_detail;
                        $jenis_suplier = $value_p->jenis_suplier;

                        $pemasukan_retur   += $jml_item;

                        $pemasukan_all += $jml_item;
                    }

                // ---------------Pengganti Retur Sample
                    $data_retur_sample_p = $this->rp->get_retur_sample_detail_p_triwulan($list_bln, $where_main);
                    
                    foreach ($data_retur_sample_p as $key_p => $value_p) {
                        $jml_item = $value_p->jml_item_tr_detail;
                        // $jenis_rekanan = $value_p->jenis_rekanan;

                        // if($jenis_suplier == "pabrik"){
                        //     $pemasukan_pabrik   += $jml_item;

                        // }elseif ($jenis_suplier == "pbf") {
                        //     $pemasukan_pbf      += $jml_item;
                        
                        // }

                        $pemasukan_lainnya += $jml_item;

                        $pemasukan_all += $jml_item;
                    }

                $tmp_list["pemasukan"]["pabrik"]    = $pemasukan_pabrik;
                $tmp_list["pemasukan"]["pbf"]       = $pemasukan_pbf;
                $tmp_list["pemasukan"]["retur"]     = $pemasukan_retur;
                $tmp_list["pemasukan"]["lainnya"]   = $pemasukan_lainnya;

                $tmp_list["pemasukan_all"] = $pemasukan_all;


            // --------------Pemasukan
                $pengeluaran_all = 0;

                $pengeluaran_rs     = 0;
                $pengeluaran_pbf    = 0;
                $pengeluaran_apotik = 0;
                $pengeluaran_sp     = 0;
                $pengeluaran_pm     = 0;
                $pengeluaran_kl     = 0;
                $pengeluaran_to     = 0;
                $pengeluaran_ln     = 0;
                $pengeluaran_retur  = 0;
                $pengeluaran_lainnya= 0;


                // ---------------Penjualan
                    $data_penjualan = $this->rp->get_penjualan_item_triwulan($y, $list_bln, array("td.id_item"=>$id_item));

                    foreach ($data_penjualan as $key_p => $value_p) {
                        $jml_item = $value_p->jml_item_tr_detail;
                        $jenis_rekanan = $value_p->jenis_rekanan;

                        switch ($jenis_rekanan) {
                            case 'rs':
                                $pengeluaran_rs   += $jml_item;
                                break;

                            case 'pbf':
                                $pengeluaran_pbf   += $jml_item;
                                break;

                            case 'apotik':
                                $pengeluaran_apotik   += $jml_item;
                                break;

                            case 'sp':
                                $pengeluaran_sp   += $jml_item;
                                break;

                            case 'pm':
                                $pengeluaran_pm   += $jml_item;
                                break;

                            case 'kl':
                                $pengeluaran_kl   += $jml_item;
                                break;

                            case 'to':
                                $pengeluaran_to   += $jml_item;
                                break;

                            case 'ln':
                                $pengeluaran_ln   += $jml_item;
                                break;

                            case 'retur':
                                $pengeluaran_retur   += $jml_item;
                                break;

                            case 'lainnya':
                                $pengeluaran_lainnya   += $jml_item;
                                break;
                            
                            default:
                                $pengeluaran_rs   += $jml_item;
                                break;
                        }

                        $pengeluaran_all += $jml_item;
                    }

                // ---------------Pengganti retur penjualan
                    $data_retur_penjualan_p = $this->rp->get_retur_penjualan_detail_p_triwulan($list_bln, $where_main);

                    foreach ($data_retur_penjualan_p as $key_p => $value_p) {
                        $jml_item = $value_p->jml_item_tr_detail;
                        $jenis_rekanan = $value_p->jenis_rekanan;

                        $pengeluaran_retur   += $jml_item;

                        $pengeluaran_all += $jml_item;
                    }

                // ---------------retur_pembelian
                    $data_retur_pembelian = $this->rp->get_retur_pembelian_detail_triwulan($list_bln, $where_main_no_item_rusak);

                    foreach ($data_retur_pembelian as $key_p => $value_p) {
                        $jml_item = $value_p->jml_item_tr_detail;
                        $jenis_suplier = $value_p->jenis_suplier;

                        $pengeluaran_retur   += $jml_item;

                        $pengeluaran_all += $jml_item;
                    }

                // ---------------sample
                    $data_sample = $this->rp->get_sample_detail_triwulan($list_bln, $where_main);

                    foreach ($data_sample as $key_p => $value_p) {
                        $jml_item = $value_p->jml_item_tr_detail;
                        $jenis_rekanan = $value_p->jenis_rekanan;

                        $pengeluaran_ln   += $jml_item;

                        $pengeluaran_all += $jml_item;
                    }

                $tmp_list["pengeluaran"]["rs"]     = $pengeluaran_rs;
                $tmp_list["pengeluaran"]["pbf"]    = $pengeluaran_pbf;
                $tmp_list["pengeluaran"]["apotik"] = $pengeluaran_apotik;
                $tmp_list["pengeluaran"]["sp"]     = $pengeluaran_sp;
                $tmp_list["pengeluaran"]["pm"]     = $pengeluaran_pm;
                $tmp_list["pengeluaran"]["kl"]     = $pengeluaran_kl;
                $tmp_list["pengeluaran"]["to"]     = $pengeluaran_to;
                $tmp_list["pengeluaran"]["ln"]     = $pengeluaran_ln;
                $tmp_list["pengeluaran"]["retur"]  = $pengeluaran_retur;
                $tmp_list["pengeluaran"]["lainnya"]= $pengeluaran_lainnya;

                $tmp_list["pengeluaran_all"] = $pengeluaran_all;

                $tmp_list["stok"] = $stok;
                $tmp_list["stok_move"] = $pemasukan_all - $pengeluaran_all;

                $tmp_list["stok_before"] = $stok - $pemasukan_all + $pengeluaran_all;

                
                // print_r("<br>");
                // print_r("----------------------------------------");
                // print_r("<br>");


                $data["list_data"][$id_item] = $tmp_list;
            }
        }
        // print_r($data["list_data"]);
        // print_r($data["list_data_retur_sample_p"]);
        // print_r($check_data);
        return $data;
    }

    public function main_get_bpom_bulan(){
        $data["page"] = "report_bpom_main";
        $data["item"] = $this->rp->get_item_asc(array("is_del_item"=>"0"));
        $data["setting"] = $this->mm->get_data_all_where("setting", array("is_del_setting"=>"0"));
        
        $tgl_before = date('Y-m-d', strtotime('-1 day', strtotime(date("Y-m-d"))));

        $data["str_periode"] = "";

        $bln_now = date("m");
        $y_now = date("Y");

        $bln = (int)$bln_now;
        $y = (int)$y_now;

        $data["str_periode"] = $this->array_of_month[$bln]." ".$y_now;

        $data["list_data"] = array();
        $data["item_v"] = [];
        $data["list_all_data"] = [];

        // print('Next Date ' . );

        $data["list_record_stok"] = [];
        $data["list_item_stok"] = [];

        if($bln != "0" && $y != "0"){
            foreach ($data["item"] as $key => $value) {
                $id_item = $value->id_item;
                $stok = $value->stok;

                $tmp_list = ["item_now"=>[],
                                "record_item_before"=>[],
                                "pemasukan"=>[
                                            "pabrik"=>0,
                                            "pbf"=>0,
                                            "retur"=>0,
                                            "lainnya"=>0
                                        ],
                                "pemasukan_all"=>0,
                                "pengeluaran"=>[
                                            "rs"=>0,
                                            "pbf"=>0,
                                            "apotik"=>0,
                                            "sp"=>0,
                                            "pm"=>0,
                                            "kl"=>0,
                                            "to"=>0,
                                            "ln"=>0,
                                            "retur"=>0,
                                            "lainnya"=>0
                                        ],
                                "pengeluaran_all"=>0,
                                "stok"=>0,
                                "stok_before"=>0,
                                "stok_move"=>0
                            ];


                $where_main = [
                            "MONTH(th.tgl_transaksi_tr_header)="=>$bln,
                            "YEAR(th.tgl_transaksi_tr_header)="=>$y,
                            "td.id_item"=>$id_item
                        ];

                $where_main_no_item_rusak = [
                            "MONTH(th.tgl_transaksi_tr_header)="=>$bln,
                            "YEAR(th.tgl_transaksi_tr_header)="=>$y,
                            "td.status_rt_tr_detail"=>"0",
                            "td.id_item"=>$id_item
                        ];

                // print_r("<pre>");
                // print_r("<br>");
                // print_r("----------------------------------------");
                // print_r("<br>");

                
                // get rekam stok
                $tmp_list["item_now"] = $value;
                $tmp_list["record_item_before"] = $this->mm->get_data_each("record_item", ["id_item"=>$value->id_item, "periode_record_item"=>$tgl_before]);

                $stok_before = $tmp_list["record_item_before"]["stok_record_item"];
                // $data["list_record_stok"][$id_item] = $this->mm->get_data_each("record_item", ["id_item"=>$value->id_item, "periode_record_item"=>$tgl_before]);

                // $data["list_item_stok"][$id_item] = $value;

            // --------------Pemasukan
                $pemasukan_all = 0;

                $pemasukan_pabrik   = 0;
                $pemasukan_pbf      = 0;
                $pemasukan_retur    = 0;
                $pemasukan_lainnya  = 0;

                // ---------------pembelian
                    // $stok_masuk = 0;
                    
                    $data_pembelian = $this->rp->get_pembelian_item_bulan($bln, $y, array("td.id_item"=>$id_item));

                    foreach ($data_pembelian as $key_p => $value_p) {
                        $jml_item = $value_p->jml_item_tr_detail;
                        $jenis_suplier = $value_p->jenis_suplier;

                        if($jenis_suplier == "pabrik"){
                            $pemasukan_pabrik   += $jml_item;

                        }elseif ($jenis_suplier == "pbf") {
                            $pemasukan_pbf      += $jml_item;
                        
                        }
                        $pemasukan_all += $jml_item;
                    }

                // ---------------retur_penjualan
                    // $stok_masuk = 0;
                    $data_retur_penjualan = $this->rp->get_retur_penjualan_detail($where_main_no_item_rusak);
                    
                    foreach ($data_retur_penjualan as $key_p => $value_p) {
                        $jml_item = $value_p->jml_item_tr_detail;
                        $jenis_rekanan = $value_p->jenis_rekanan;

                        $pemasukan_retur   += $jml_item;

                        $pemasukan_all += $jml_item;
                    }

                // ---------------pengganti_retur_pembelian
                    $data_retur_pembelian_p = $this->rp->get_retur_pembelian_detail_p($where_main);
                    
                    foreach ($data_retur_pembelian_p as $key_p => $value_p) {
                        $jml_item = $value_p->jml_item_tr_detail;
                        $jenis_suplier = $value_p->jenis_suplier;

                        $pemasukan_retur   += $jml_item;

                        $pemasukan_all += $jml_item;
                    }

                // ---------------Pengganti Retur Sample
                    $data_retur_sample_p = $this->rp->get_retur_sample_detail_p($where_main);
                    
                    foreach ($data_retur_sample_p as $key_p => $value_p) {
                        $jml_item = $value_p->jml_item_tr_detail;
                        // $jenis_rekanan = $value_p->jenis_rekanan;

                        // if($jenis_suplier == "pabrik"){
                        //     $pemasukan_pabrik   += $jml_item;

                        // }elseif ($jenis_suplier == "pbf") {
                        //     $pemasukan_pbf      += $jml_item;
                        
                        // }

                        $pemasukan_lainnya += $jml_item;

                        $pemasukan_all += $jml_item;
                    }

                $tmp_list["pemasukan"]["pabrik"]    = $pemasukan_pabrik;
                $tmp_list["pemasukan"]["pbf"]       = $pemasukan_pbf;
                $tmp_list["pemasukan"]["retur"]     = $pemasukan_retur;
                $tmp_list["pemasukan"]["lainnya"]   = $pemasukan_lainnya;

                $tmp_list["pemasukan_all"] = $pemasukan_all;


            // --------------Pemasukan
                $pengeluaran_all = 0;

                $pengeluaran_rs     = 0;
                $pengeluaran_pbf    = 0;
                $pengeluaran_apotik = 0;
                $pengeluaran_sp     = 0;
                $pengeluaran_pm     = 0;
                $pengeluaran_kl     = 0;
                $pengeluaran_to     = 0;
                $pengeluaran_ln     = 0;
                $pengeluaran_retur  = 0;
                $pengeluaran_lainnya= 0;


                // ---------------Penjualan
                    $data_penjualan = $this->rp->get_penjualan_item_bulan($bln, $y, array("td.id_item"=>$id_item));

                    foreach ($data_penjualan as $key_p => $value_p) {
                        $jml_item = $value_p->jml_item_tr_detail;
                        $jenis_rekanan = $value_p->jenis_rekanan;

                        switch ($jenis_rekanan) {
                            case 'rs':
                                $pengeluaran_rs   += $jml_item;
                                break;

                            case 'pbf':
                                $pengeluaran_pbf   += $jml_item;
                                break;

                            case 'apotik':
                                $pengeluaran_apotik   += $jml_item;
                                break;

                            case 'sp':
                                $pengeluaran_sp   += $jml_item;
                                break;

                            case 'pm':
                                $pengeluaran_pm   += $jml_item;
                                break;

                            case 'kl':
                                $pengeluaran_kl   += $jml_item;
                                break;

                            case 'to':
                                $pengeluaran_to   += $jml_item;
                                break;

                            case 'ln':
                                $pengeluaran_ln   += $jml_item;
                                break;

                            case 'retur':
                                $pengeluaran_retur   += $jml_item;
                                break;

                            case 'lainnya':
                                $pengeluaran_lainnya   += $jml_item;
                                break;
                            
                            default:
                                $pengeluaran_rs   += $jml_item;
                                break;
                        }

                        $pengeluaran_all += $jml_item;
                    }

                // ---------------Pengganti retur penjualan
                    $data_retur_penjualan_p = $this->rp->get_retur_penjualan_detail_p($where_main);

                    foreach ($data_retur_penjualan_p as $key_p => $value_p) {
                        $jml_item = $value_p->jml_item_tr_detail;
                        $jenis_rekanan = $value_p->jenis_rekanan;

                        $pengeluaran_retur   += $jml_item;

                        $pengeluaran_all += $jml_item;
                    }

                // ---------------retur_pembelian
                    $data_retur_pembelian = $this->rp->get_retur_pembelian_detail($where_main_no_item_rusak);

                    foreach ($data_retur_pembelian as $key_p => $value_p) {
                        $jml_item = $value_p->jml_item_tr_detail;
                        $jenis_suplier = $value_p->jenis_suplier;

                        $pengeluaran_retur   += $jml_item;

                        $pengeluaran_all += $jml_item;
                    }

                // ---------------sample
                    $data_sample = $this->rp->get_sample_detail($where_main);

                    foreach ($data_sample as $key_p => $value_p) {
                        $jml_item = $value_p->jml_item_tr_detail;
                        $jenis_rekanan = $value_p->jenis_rekanan;

                        $pengeluaran_ln   += $jml_item;

                        $pengeluaran_all += $jml_item;
                    }

                $tmp_list["pengeluaran"]["rs"]     = $pengeluaran_rs;
                $tmp_list["pengeluaran"]["pbf"]    = $pengeluaran_pbf;
                $tmp_list["pengeluaran"]["apotik"] = $pengeluaran_apotik;
                $tmp_list["pengeluaran"]["sp"]     = $pengeluaran_sp;
                $tmp_list["pengeluaran"]["pm"]     = $pengeluaran_pm;
                $tmp_list["pengeluaran"]["kl"]     = $pengeluaran_kl;
                $tmp_list["pengeluaran"]["to"]     = $pengeluaran_to;
                $tmp_list["pengeluaran"]["ln"]     = $pengeluaran_ln;
                $tmp_list["pengeluaran"]["retur"]  = $pengeluaran_retur;
                $tmp_list["pengeluaran"]["lainnya"]= $pengeluaran_lainnya;

                $tmp_list["pengeluaran_all"] = $pengeluaran_all;

                $tmp_list["stok"] = $stok;
                $tmp_list["stok_move"] = $pemasukan_all - $pengeluaran_all;

                $tmp_list["stok_before"] = $stok - $pemasukan_all + $pengeluaran_all;

                
                // print_r("<br>");
                // print_r("----------------------------------------");
                // print_r("<br>");


                $data["list_data"][$id_item] = $tmp_list;
            }
        }

        // print_r($data);
        // $this->load->view('index', $data);
        return $data;
    }

    public function main_get_bpom_th(){
        $data["page"] = "report_bpom_main";
        $data["item"] = $this->rp->get_item_asc(array("is_del_item"=>"0"));
        $data["setting"] = $this->mm->get_data_all_where("setting", array("is_del_setting"=>"0"));
        
        $tgl_before = date('Y-m-d', strtotime('-1 day', strtotime(date("Y-m-d"))));

        $data["str_periode"] = "";

        $y_now = date("Y");

        $y = (int)$y_now;

        $data["str_periode"] = $y_now;

        $data["list_data"] = array();
        $data["item_v"] = [];
        $data["list_all_data"] = [];

        // print('Next Date ' . );

        $data["list_record_stok"] = [];
        $data["list_item_stok"] = [];

        if($y != "0"){
            foreach ($data["item"] as $key => $value) {
                $id_item = $value->id_item;
                $stok = $value->stok;

                $tmp_list = ["item_now"=>[],
                                "record_item_before"=>[],
                                "pemasukan"=>[
                                            "pabrik"=>0,
                                            "pbf"=>0,
                                            "retur"=>0,
                                            "lainnya"=>0
                                        ],
                                "pemasukan_all"=>0,
                                "pengeluaran"=>[
                                            "rs"=>0,
                                            "pbf"=>0,
                                            "apotik"=>0,
                                            "sp"=>0,
                                            "pm"=>0,
                                            "kl"=>0,
                                            "to"=>0,
                                            "ln"=>0,
                                            "retur"=>0,
                                            "lainnya"=>0
                                        ],
                                "pengeluaran_all"=>0,
                                "stok"=>0,
                                "stok_before"=>0,
                                "stok_move"=>0
                            ];


                $where_main = [
                            "YEAR(th.tgl_transaksi_tr_header)="=>$y,
                            "td.id_item"=>$id_item
                        ];

                $where_main_no_item_rusak = [
                            "YEAR(th.tgl_transaksi_tr_header)="=>$y,
                            "td.status_rt_tr_detail"=>"0",
                            "td.id_item"=>$id_item
                        ];

                // print_r("<pre>");
                // print_r("<br>");
                // print_r("----------------------------------------");
                // print_r("<br>");

                
                // get rekam stok
                $tmp_list["item_now"] = $value;
                $tmp_list["record_item_before"] = $this->mm->get_data_each("record_item", 
                                                                                ["id_item"=>$value->id_item, 
                                                                                "periode_record_item"=>$tgl_before]);

                $stok_before = $tmp_list["record_item_before"]["stok_record_item"];
                // $data["list_record_stok"][$id_item] = $this->mm->get_data_each("record_item", ["id_item"=>$value->id_item, "periode_record_item"=>$tgl_before]);

                // $data["list_item_stok"][$id_item] = $value;

            // --------------Pemasukan
                $pemasukan_all = 0;

                $pemasukan_pabrik   = 0;
                $pemasukan_pbf      = 0;
                $pemasukan_retur    = 0;
                $pemasukan_lainnya  = 0;

                // ---------------pembelian
                    // $stok_masuk = 0;
                    
                    $data_pembelian = $this->rp->get_pembelian_item_th($y, array("td.id_item"=>$id_item));

                    foreach ($data_pembelian as $key_p => $value_p) {
                        $jml_item = $value_p->jml_item_tr_detail;
                        $jenis_suplier = $value_p->jenis_suplier;

                        if($jenis_suplier == "pabrik"){
                            $pemasukan_pabrik   += $jml_item;

                        }elseif ($jenis_suplier == "pbf") {
                            $pemasukan_pbf      += $jml_item;
                        
                        }
                        $pemasukan_all += $jml_item;
                    }

                // ---------------retur_penjualan
                    // $stok_masuk = 0;
                    $data_retur_penjualan = $this->rp->get_retur_penjualan_detail($where_main_no_item_rusak);
                    
                    foreach ($data_retur_penjualan as $key_p => $value_p) {
                        $jml_item = $value_p->jml_item_tr_detail;
                        $jenis_rekanan = $value_p->jenis_rekanan;

                        $pemasukan_retur   += $jml_item;

                        $pemasukan_all += $jml_item;
                    }

                // ---------------pengganti_retur_pembelian
                    $data_retur_pembelian_p = $this->rp->get_retur_pembelian_detail_p($where_main);
                    
                    foreach ($data_retur_pembelian_p as $key_p => $value_p) {
                        $jml_item = $value_p->jml_item_tr_detail;
                        $jenis_suplier = $value_p->jenis_suplier;

                        $pemasukan_retur   += $jml_item;

                        $pemasukan_all += $jml_item;
                    }

                // ---------------Pengganti Retur Sample
                    $data_retur_sample_p = $this->rp->get_retur_sample_detail_p($where_main);
                    
                    foreach ($data_retur_sample_p as $key_p => $value_p) {
                        $jml_item = $value_p->jml_item_tr_detail;
                        // $jenis_rekanan = $value_p->jenis_rekanan;

                        // if($jenis_suplier == "pabrik"){
                        //     $pemasukan_pabrik   += $jml_item;

                        // }elseif ($jenis_suplier == "pbf") {
                        //     $pemasukan_pbf      += $jml_item;
                        
                        // }

                        $pemasukan_lainnya += $jml_item;

                        $pemasukan_all += $jml_item;
                    }

                $tmp_list["pemasukan"]["pabrik"]    = $pemasukan_pabrik;
                $tmp_list["pemasukan"]["pbf"]       = $pemasukan_pbf;
                $tmp_list["pemasukan"]["retur"]     = $pemasukan_retur;
                $tmp_list["pemasukan"]["lainnya"]   = $pemasukan_lainnya;

                $tmp_list["pemasukan_all"] = $pemasukan_all;


            // --------------Pemasukan
                $pengeluaran_all = 0;

                $pengeluaran_rs     = 0;
                $pengeluaran_pbf    = 0;
                $pengeluaran_apotik = 0;
                $pengeluaran_sp     = 0;
                $pengeluaran_pm     = 0;
                $pengeluaran_kl     = 0;
                $pengeluaran_to     = 0;
                $pengeluaran_ln     = 0;
                $pengeluaran_retur  = 0;
                $pengeluaran_lainnya= 0;


                // ---------------Penjualan
                    $data_penjualan = $this->rp->get_penjualan_item_th($y, array("td.id_item"=>$id_item));

                    foreach ($data_penjualan as $key_p => $value_p) {
                        $jml_item = $value_p->jml_item_tr_detail;
                        $jenis_rekanan = $value_p->jenis_rekanan;

                        switch ($jenis_rekanan) {
                            case 'rs':
                                $pengeluaran_rs   += $jml_item;
                                break;

                            case 'pbf':
                                $pengeluaran_pbf   += $jml_item;
                                break;

                            case 'apotik':
                                $pengeluaran_apotik   += $jml_item;
                                break;

                            case 'sp':
                                $pengeluaran_sp   += $jml_item;
                                break;

                            case 'pm':
                                $pengeluaran_pm   += $jml_item;
                                break;

                            case 'kl':
                                $pengeluaran_kl   += $jml_item;
                                break;

                            case 'to':
                                $pengeluaran_to   += $jml_item;
                                break;

                            case 'ln':
                                $pengeluaran_ln   += $jml_item;
                                break;

                            case 'retur':
                                $pengeluaran_retur   += $jml_item;
                                break;

                            case 'lainnya':
                                $pengeluaran_lainnya   += $jml_item;
                                break;
                            
                            default:
                                $pengeluaran_rs   += $jml_item;
                                break;
                        }

                        $pengeluaran_all += $jml_item;
                    }

                // ---------------Pengganti retur penjualan
                    $data_retur_penjualan_p = $this->rp->get_retur_penjualan_detail_p($where_main);

                    foreach ($data_retur_penjualan_p as $key_p => $value_p) {
                        $jml_item = $value_p->jml_item_tr_detail;
                        $jenis_rekanan = $value_p->jenis_rekanan;

                        $pengeluaran_retur   += $jml_item;

                        $pengeluaran_all += $jml_item;
                    }

                // ---------------retur_pembelian
                    $data_retur_pembelian = $this->rp->get_retur_pembelian_detail($where_main_no_item_rusak);

                    foreach ($data_retur_pembelian as $key_p => $value_p) {
                        $jml_item = $value_p->jml_item_tr_detail;
                        $jenis_suplier = $value_p->jenis_suplier;

                        $pengeluaran_retur   += $jml_item;

                        $pengeluaran_all += $jml_item;
                    }

                // ---------------sample
                    $data_sample = $this->rp->get_sample_detail($where_main);

                    foreach ($data_sample as $key_p => $value_p) {
                        $jml_item = $value_p->jml_item_tr_detail;
                        $jenis_rekanan = $value_p->jenis_rekanan;

                        $pengeluaran_ln   += $jml_item;

                        $pengeluaran_all += $jml_item;
                    }

                $tmp_list["pengeluaran"]["rs"]     = $pengeluaran_rs;
                $tmp_list["pengeluaran"]["pbf"]    = $pengeluaran_pbf;
                $tmp_list["pengeluaran"]["apotik"] = $pengeluaran_apotik;
                $tmp_list["pengeluaran"]["sp"]     = $pengeluaran_sp;
                $tmp_list["pengeluaran"]["pm"]     = $pengeluaran_pm;
                $tmp_list["pengeluaran"]["kl"]     = $pengeluaran_kl;
                $tmp_list["pengeluaran"]["to"]     = $pengeluaran_to;
                $tmp_list["pengeluaran"]["ln"]     = $pengeluaran_ln;
                $tmp_list["pengeluaran"]["retur"]  = $pengeluaran_retur;
                $tmp_list["pengeluaran"]["lainnya"]= $pengeluaran_lainnya;

                $tmp_list["pengeluaran_all"] = $pengeluaran_all;

                $tmp_list["stok"] = $stok;
                $tmp_list["stok_move"] = $pemasukan_all - $pengeluaran_all;

                $tmp_list["stok_before"] = $stok - $pemasukan_all + $pengeluaran_all;

                
                // print_r("<br>");
                // print_r("----------------------------------------");
                // print_r("<br>");


                $data["list_data"][$id_item] = $tmp_list;
            }
        }

        // print_r($data);
        // $this->load->view('index', $data);
        return $data;
    }

#------------------------------main----------------------------------#

#------------------------------print----------------------------------#
    public function print_get_bpom_tgl(){
        $data = array();
        $data = $this->main_get_bpom_tgl();
        $this->load->view('print/print_bpom_main_new', $data);
    }

    public function print_get_bpom_triwulan(){
        $data = array();
        $data = $this->main_get_bpom_triwulan();
        $this->load->view('print/print_bpom_main_new', $data);
    }

    public function print_get_bpom_bulan(){
        $data = array();
        $data = $this->main_get_bpom_bulan();
        $this->load->view('print/print_bpom_main_new', $data);
    }

    public function print_get_bpom_th(){
        $data = array();
        $data = $this->main_get_bpom_th();
        $this->load->view('print/print_bpom_main_new', $data);
    }
#------------------------------print----------------------------------#

#------------------------------excel----------------------------------#
    public function excel_get_bpom_tgl(){
        $data = array();
            $data = $this->main_get_bpom_tgl();
            $this->convert_excel($data);
    }

    public function excel_get_bpom_triwulan(){
        $data = array();
            $data = $this->main_get_bpom_triwulan();
            $this->convert_excel($data);
    }

    public function excel_get_bpom_bulan(){
        $data = array();
            $data = $this->main_get_bpom_bulan();
            $this->convert_excel($data);
        
    }

    public function excel_get_bpom_th(){
        $data = array();
            $data = $this->main_get_bpom_th();
            $this->convert_excel($data);
    }
#------------------------------excel----------------------------------#


    public function convert_excel($data){
        /** Error reporting */
        error_reporting(E_ALL);
        require_once APPPATH.'third_party/PHPExcel.php';

        $objPHPExcel = new PHPExcel();


       

        // Set document properties
        $objPHPExcel->getProperties()->setCreator("Filosofi_code")
                                     ->setLastModifiedBy("Filosofi_code Application")
                                     ->setTitle("Laporan Transaksi Penjualan")
                                     ->setSubject("Office 2007 XLSX Laporan Transaksi Penjualan")
                                     ->setDescription("Laporan Transaksi Penjualan for Office 2007 XLSX")
                                     ->setKeywords("office 2007 openxml php")
                                     ->setCategory("Laporan Transaksi Penjualan");


        // Add some data
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'REKAPITULASI LAPORAN OBAT')
                    ->setCellValue('A2', $data["str_periode"])
                    ->setCellValue('A3', '')
                    ->setCellValue('A4', '')
                    ->setCellValue('A5', '');

        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A6', 'NO')
                    ->setCellValue('B6', 'PERIODE')
                    ->setCellValue('C6', 'KODE OBAT(NIE)')
                    ->setCellValue('D6', 'NAMA OBAT')
                    ->setCellValue('E6', 'STOK AWAL')
                    ->setCellValue('F6', 'PEMASUKAN')
                    ->setCellValue('J6', 'PENGELUARAN')
                    ->setCellValue('S6', 'STOK AKHIR');

        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A6:A7');
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B6:B7');
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('C6:C7');
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('D6:D7');
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('E6:E7');
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('S6:S7');

        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('F6:I7');
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('J6:R7');


        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('F8', 'PABRIK')
                    ->setCellValue('G8', 'PBF')
                    ->setCellValue('H8', 'RETUR')
                    ->setCellValue('I8', 'LAINNYA')
                    ->setCellValue('J8', 'RUMAH SAKIT')
                    ->setCellValue('K8', 'APOTIK')
                    ->setCellValue('L8', 'PBF')
                    ->setCellValue('M8', 'SARANA PEMERINTAH')
                    ->setCellValue('N8', 'PUSKESMAS')
                    ->setCellValue('O8', 'KLINIK')
                    ->setCellValue('P8', 'TOKO OBAT')
                    ->setCellValue('Q8', 'RETUR')
                    ->setCellValue('R8', 'LAINNYA');

        $no_row = 10;
        $no = 1;
        foreach ($data["list_data"] as $key => $value) {
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A'.$no_row, $no)
                    ->setCellValue('B'.$no_row, $data["str_periode"])
                    ->setCellValue('C'.$no_row, $value["item_now"]->id_item)
                    ->setCellValue('D'.$no_row, $value["item_now"]->nama_item.", ".$value["item_now"]->satuan." (".$value["item_now"]->kode_produksi_item.")")
                    ->setCellValue('E'.$no_row, $value["stok_before"])
                    ->setCellValue('F'.$no_row, $value["pemasukan"]["pabrik"])
                    ->setCellValue('G'.$no_row, $value["pemasukan"]["pbf"])
                    ->setCellValue('H'.$no_row, $value["pemasukan"]["retur"])
                    ->setCellValue('I'.$no_row, $value["pemasukan"]["lainnya"])

                    ->setCellValue('J'.$no_row, $value["pengeluaran"]["rs"])
                    ->setCellValue('K'.$no_row, $value["pengeluaran"]["apotik"])
                    ->setCellValue('L'.$no_row, $value["pengeluaran"]["pbf"])
                    ->setCellValue('M'.$no_row, $value["pengeluaran"]["sp"])
                    ->setCellValue('N'.$no_row, $value["pengeluaran"]["pm"])
                    ->setCellValue('O'.$no_row, $value["pengeluaran"]["kl"])
                    ->setCellValue('P'.$no_row, $value["pengeluaran"]["to"])
                    ->setCellValue('Q'.$no_row, $value["pengeluaran"]["retur"])
                    ->setCellValue('R'.$no_row, $value["pengeluaran"]["lainnya"])
                    ->setCellValue('S'.$no_row, $value["stok"]);
            // print_r($value);
            $no++;
            $no_row++;
        }
        
        

        // Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle('Laporan_format_bpom');


        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);


        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel); 
        $objWriter->save('./Laporan_format_bpom.xlsx'); 

        $this->load->helper('download');
        force_download('./Laporan_format_bpom.xlsx', NULL);
    }
}
