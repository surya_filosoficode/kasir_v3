<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reportpenjualanitemakhir extends CI_Controller {

    public $keterangan_record_stok = "panjualan detail";
    public $array_of_month = ["", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];

    public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');
        $this->load->model('report/report_penjualan_item', 'rp');
        $this->load->model('report/report_retur_penjualan', 'rrp');
        $this->load->model('report/Report_item_akhir', 'ria');

        $this->load->model('other/item_main', 'oi');

        $this->load->library("response_message");
        $this->load->library("Auth_v0");
        
        date_default_timezone_set("Asia/Bangkok");
        // $this->auth_v0->check_session_active_ad();
    }

    public function index(){
        $data["page"] = "report_penjualan_item_akhir";
        $data["str_periode"] = "";

        $date_y = date("Y");
        $date_m = date("m");
        $date_d = date("d");

        $date_str = $date_d." ".$this->array_of_month[(int)$date_m]." ".$date_y;

        $data["str_periode"] = $date_str;

        // $data["list_data"] = array();

        $data["array_data_main"] = $this->ria->get_all_item_short(array("is_del_item"=>"0"), "nama_item", "ASC");
        // print_r($data);
        $this->load->view('index', $data);
    }

#------------------------------show---------------------------------#


    public function get_penjualan_item_main(){
        // print_r("<pre>");
        $data["page"] = "report_penjualan_item_akhir";
        $data["str_periode"] = "";
        $data["item"] = $this->mm->get_data_all_where("item", array("is_del_item"=>"0"));

        $this->load->view('index', $data);
    }

    
#------------------------------show---------------------------------#


#------------------------------main---------------------------------#
    public function main_get_item_main(){
        $data["page"] = "report_penjualan_item_akhir";
        $data["str_periode"] = "";

        $date_y = date("Y");
        $date_m = date("m");
        $date_d = date("d");

        $date_str = $date_d." ".$this->array_of_month[(int)$date_m]." ".$date_y;

        $data["str_periode"] = $date_str;

        // $data["list_data"] = array();

        $data["array_data_main"] = $this->ria->get_all_item_short(array("is_del_item"=>"0"), "nama_item", "ASC");

        return $data;
    }

#------------------------------main---------------------------------#


#------------------------------print---------------------------------#
    public function print_get_item_main(){
        $data = array();
        
        $data = $this->main_get_item_main();
        
        // print_r($data);
        $this->load->view('print/print_penjualan_item_akhir', $data);
    }
#------------------------------print---------------------------------#


#------------------------------excel---------------------------------#
    public function excel_get_item_main(){
        $data = array();
        
        $data = $this->main_get_item_main();
        $this->convert_excel($data);
    }

#------------------------------excel---------------------------------#




    public function convert_excel($data){
        /** Error reporting */
        error_reporting(E_ALL);
        require_once APPPATH.'third_party/PHPExcel.php';

        $objPHPExcel = new PHPExcel();


       

        // Set document properties
        $objPHPExcel->getProperties()->setCreator("Filosofi_code")
                                     ->setLastModifiedBy("Filosofi_code Application")
                                     ->setTitle("Laporan Transaksi Penjualan")
                                     ->setSubject("Office 2007 XLSX Laporan Transaksi Penjualan")
                                     ->setDescription("Laporan Transaksi Penjualan for Office 2007 XLSX")
                                     ->setKeywords("office 2007 openxml php")
                                     ->setCategory("Laporan Transaksi Penjualan");


        // Add some data
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'LAPORAN STOK PRODUK AKHIR')
                    ->setCellValue('A2', $data["str_periode"])
                    ->setCellValue('A3', '')
                    ->setCellValue('A4', '')
                    ->setCellValue('A5', '');

        // Pembelian

            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A8', 'No')
                        ->setCellValue('B8', 'Kode Produk')
                        ->setCellValue('C8', 'Nama Produk')
                        ->setCellValue('D8', 'Satuan')
                        ->setCellValue('E8', 'Kode Produksi')
                        ->setCellValue('F8', 'Harga Satuan')
                        ->setCellValue('G8', 'Total Harga')
                        ->setCellValue('H8', 'Stok Produk');

            $no_row = 9;
            $no = 1;

            if(isset($data["array_data_main"])){
                $no = 1;
                $t_harga_all = 0;
                foreach ($data["array_data_main"] as $key => $value) {
                    $val_sisa = $value->stok;
                    $val_harga_satuan = $value->harga_bruto;

                    $val_t_harga = (float)$val_sisa * (float)$val_harga_satuan;

                    $t_harga_all += $val_t_harga;

                    $str_harga_satuan = number_format($val_harga_satuan, 0, ".", ",");
                    $str_t_harga = number_format($val_t_harga, 0, ".", ",");

                    $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('A'.$no_row, $no)
                            ->setCellValue('B'.$no_row, $value->id_item)
                            ->setCellValue('C'.$no_row, $value->nama_item)
                            ->setCellValue('D'.$no_row, $value->satuan)
                            ->setCellValue('E'.$no_row, $value->kode_produksi_item)
                            ->setCellValue('F'.$no_row, $str_harga_satuan)
                            ->setCellValue('G'.$no_row, $str_t_harga)
                            ->setCellValue('H'.$no_row, $val_sisa);

                    
                    $no_row++;
                    $no++;
                }

                $str_t_harga_all = number_format($t_harga_all, 0, ".", ",");

                $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('A'.$no_row, "Total")
                            ->setCellValue('G'.$no_row, $str_t_harga_all)
                            ->setCellValue('H'.$no_row, "-");
            }
        // Pembelian


        // Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle('Laporan_stok_produk_akhir');


        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);


        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel); 
        $objWriter->save('./Laporan_stok_produk_akhir.xlsx'); 

        $this->load->helper('download');
        force_download('./Laporan_stok_produk_akhir.xlsx', NULL);
    }
}
