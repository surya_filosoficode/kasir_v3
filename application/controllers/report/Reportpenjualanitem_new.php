<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reportpenjualanitem_new extends CI_Controller {

    public $keterangan_record_stok = "panjualan detail";
    public $array_of_month = ["", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];

    public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');
        $this->load->model('report/report_penjualan_item', 'rp');
        $this->load->model('report/report_retur_penjualan', 'rrp');

        $this->load->library("response_message");
        $this->load->library("Auth_v0");
        
        date_default_timezone_set("Asia/Bangkok");
        // $this->auth_v0->check_session_active_ad();
    }

    public function index(){
        $data["page"] = "report_penjualan_item";
        $data["str_periode"] = "";
        // $data["list_data"] = array();
        $data["item"] = $this->mm->get_data_all_where("item", array("is_del_item"=>"0"));
        $this->load->view('index', $data);
    }

#------------------------------show---------------------------------#


    public function get_penjualan_item_tgl($id_item = "0"){
        $data["page"] = "report_penjualan_item";
        $data["item"] = $this->mm->get_data_all_where("item", array("is_del_item"=>"0"));
        

        $data["str_periode"] = "";

        $date_now = date("Y-m-d");

        $tgl_start = $date_now;

        $tgl_finish = $date_now;
        // $data["item"] = $this->mm->get_data_all_where("item", array("is_del_item"=>"0"));

        $data["list_data"] = array();

        $data["item_v"] = [];


        if($tgl_start != "0" && $tgl_finish != "0"){

            $data_item = $this->mm->get_data_all_where("item", ["id_item"=> $id_item,"is_del_item"=>"0"]);
            foreach ($data_item as $key_item => $value_item) {
                // print_r($value_item);
            
                $data["item_v"] = $value_item;
                // print_r($value_item);
                $id_item = $value_item->id_item;

                $data_penjualan = $this->rp->get_penjualan_item_tgl($tgl_start, $tgl_finish, array("td.id_item"=>$id_item));
                $data["data_penjualan"] = $data_penjualan;

                $data_pembelian = $this->rp->get_pembelian_item_tgl($tgl_start, $tgl_finish, array("td.id_item"=>$id_item));
                $data["data_pembelian"] = $data_pembelian;

                $where_main = [
                            "th.tgl_transaksi_tr_header >="=>$tgl_start,
                            "th.tgl_transaksi_tr_header <="=>$tgl_finish,
                            "td.id_item"=>$id_item
                        ];

                $where_main_no_item_rusak = [
                            "th.tgl_transaksi_tr_header >="=>$tgl_start,
                            "th.tgl_transaksi_tr_header <="=>$tgl_finish,
                            "td.status_rt_tr_detail"=>"0",
                            "td.id_item"=>$id_item
                        ];

                $data_retur_penjualan = $this->rp->get_retur_penjualan_detail($where_main_no_item_rusak);
                $data["data_retur_penjualan"] = $data_retur_penjualan;
                
                $data_retur_penjualan_p = $this->rp->get_retur_penjualan_detail_p($where_main);
                $data["data_retur_penjualan_p"] = $data_retur_penjualan_p;

                $data_retur_pembelian = $this->rp->get_retur_pembelian_detail($where_main_no_item_rusak);
                $data["data_retur_pembelian"] = $data_retur_pembelian;
                $data_retur_pembelian_p = $this->rp->get_retur_pembelian_detail_p($where_main);
                $data["data_retur_pembelian_p"] = $data_retur_pembelian_p;

                $data_sample = $this->rp->get_sample_detail($where_main);
                $data["data_sample"] = $data_sample;
                $data_retur_sample_p = $this->rp->get_retur_sample_detail_p($where_main);
                $data["data_retur_sample_p"] = $data_retur_sample_p;

                // print_r($data["item"]);
            }  

        }
        
        // print_r($data["list_data_retur_sample_p"]);
        // print_r($check_data);
        $this->load->view('index', $data);
    }

    public function get_penjualan_item_triwulan($id_item = "0"){
        $data["page"] = "report_penjualan_item";
        $data["item"] = $this->mm->get_data_all_where("item", array("is_del_item"=>"0"));
        
        $data["str_periode"] = "";

        $bln_now = date("m");
        $y_now = date("Y");

        $bln = (int)$bln_now;
        $y = (int)$y_now;

        $list_bln = [1,2,3];
        $str_triwulan = "Triwulan I";
        if (in_array($bln, [1,2,3])) {
            $list_bln = [1,2,3];

            $str_triwulan = "Triwulan I";
        }elseif (in_array($bln, [4,5,6])) {
            $list_bln = [4,5,6];
        

            $str_triwulan = "Triwulan II";
        }elseif (in_array($bln, [7,8,9])) {
            $list_bln = [7,8,9];
        

            $str_triwulan = "Triwulan III";
        }elseif (in_array($bln, [10,11,12])) {
            $list_bln = [10,11,12];


            $str_triwulan = "Triwulan IV";
        }

        $data["str_periode"] = $str_triwulan." ".$y_now;

        $data["list_data"] = array();
        $data["item_v"] = [];


        if($bln != 0 && $y != 0 && $list_bln){

            $data_item = $this->mm->get_data_all_where("item", ["id_item"=> $id_item,"is_del_item"=>"0"]);
            foreach ($data_item as $key_item => $value_item) {
            
                $data["item_v"] = $value_item;
                // print_r($value_item);
                $id_item = $value_item->id_item;

                $data_penjualan = $this->rp->get_penjualan_item_triwulan($y, $list_bln, array("td.id_item"=>$id_item));
                $data["data_penjualan"] = $data_penjualan;

                $data_pembelian = $this->rp->get_pembelian_item_triwulan($y, $list_bln, array("td.id_item"=>$id_item));
                $data["data_pembelian"] = $data_pembelian;

                $where_main = [
                            "YEAR(th.tgl_transaksi_tr_header)="=>$y,
                            "td.id_item"=>$id_item
                        ];

                $where_main_no_item_rusak = [
                            "YEAR(th.tgl_transaksi_tr_header)="=>$y,
                            "td.status_rt_tr_detail"=>"0",
                            "td.id_item"=>$id_item
                        ];

                $data_retur_penjualan = $this->rp->get_retur_penjualan_detail_triwulan($list_bln, $where_main_no_item_rusak);
                $data["data_retur_penjualan"] = $data_retur_penjualan;
                $data_retur_penjualan_p = $this->rp->get_retur_penjualan_detail_p_triwulan($list_bln, $where_main);
                $data["data_retur_penjualan_p"] = $data_retur_penjualan_p;

                $data_retur_pembelian = $this->rp->get_retur_pembelian_detail_triwulan($list_bln, $where_main_no_item_rusak);
                $data["data_retur_pembelian"] = $data_retur_pembelian;
                $data_retur_pembelian_p = $this->rp->get_retur_pembelian_detail_p_triwulan($list_bln, $where_main);
                $data["data_retur_pembelian_p"] = $data_retur_pembelian_p;

                $data_sample = $this->rp->get_sample_detail_triwulan($list_bln, $where_main);
                $data["data_sample"] = $data_sample;
                $data_retur_sample_p = $this->rp->get_retur_sample_detail_p_triwulan($list_bln, $where_main);
                $data["data_retur_sample_p"] = $data_retur_sample_p;

                // print_r($data_pembelian);
            }  

        }
        
        $this->load->view('index', $data);
    }

    public function get_penjualan_item_th($id_item = "0"){
        $data["page"] = "report_penjualan_item";
        $data["item"] = $this->mm->get_data_all_where("item", array("is_del_item"=>"0"));
        
        $data["str_periode"] = "";

        // $bln_now = date("m");
        $y_now = date("Y");

        // $bln = (int)$bln_now;
        $y = (int)$y_now;

        $data["str_periode"] = $y_now;

        // $tgl_finish = $date_now;
        // $data["item"] = $this->mm->get_data_all_where("item", array("is_del_item"=>"0"));

        $data["list_data"] = array();
        $data["item_v"] = [];


        if($y != 0 ){

            $data_item = $this->mm->get_data_all_where("item", ["id_item"=> $id_item,"is_del_item"=>"0"]);
            foreach ($data_item as $key_item => $value_item) {

                $data["item_v"] = $value_item;
                // print_r($value_item);
                $id_item = $value_item->id_item;

                $data_penjualan = $this->rp->get_penjualan_item_th($y, array("td.id_item"=>$id_item));
                $data["data_penjualan"] = $data_penjualan;

                $data_pembelian = $this->rp->get_pembelian_item_th($y, array("td.id_item"=>$id_item));
                $data["data_pembelian"] = $data_pembelian;

                $where_main = [
                            "YEAR(th.tgl_transaksi_tr_header)="=>$y,
                            "td.id_item"=>$id_item
                        ];

                $where_main_no_item_rusak = [
                            "YEAR(th.tgl_transaksi_tr_header)="=>$y,
                            "td.status_rt_tr_detail"=>"0",
                            "td.id_item"=>$id_item
                        ];

                $data_retur_penjualan = $this->rp->get_retur_penjualan_detail($where_main_no_item_rusak);
                $data["data_retur_penjualan"] = $data_retur_penjualan;
                $data_retur_penjualan_p = $this->rp->get_retur_penjualan_detail_p($where_main);
                $data["data_retur_penjualan_p"] = $data_retur_penjualan_p;

                $data_retur_pembelian = $this->rp->get_retur_pembelian_detail($where_main_no_item_rusak);
                $data["data_retur_pembelian"] = $data_retur_pembelian;
                $data_retur_pembelian_p = $this->rp->get_retur_pembelian_detail_p($where_main);
                $data["data_retur_pembelian_p"] = $data_retur_pembelian_p;

                $data_sample = $this->rp->get_sample_detail($where_main);
                $data["data_sample"] = $data_sample;
                $data_retur_sample_p = $this->rp->get_retur_sample_detail_p($where_main);
                $data["data_retur_sample_p"] = $data_retur_sample_p;

                // print_r($data_pembelian);
            }  

        }
        
        // print_r($data["list_data_retur_sample_p"]);
        // print_r($check_data);
        $this->load->view('index', $data);
    }

    public function get_penjualan_item_bulan($id_item = "0"){
        $data["page"] = "report_penjualan_item";
        $data["item"] = $this->mm->get_data_all_where("item", array("is_del_item"=>"0"));
        
        $data["str_periode"] = "";

        $bln_now = date("m");
        $y_now = date("Y");

        $bln = (int)$bln_now;
        $y = (int)$y_now;

        $data["str_periode"] = $this->array_of_month[$bln]." ".$y_now;

        // $tgl_finish = $date_now;
        // $data["item"] = $this->mm->get_data_all_where("item", array("is_del_item"=>"0"));

        $data["list_data"] = array();
        $data["item_v"] = [];


        if($bln != 0 && $y != 0 ){

            $data_item = $this->mm->get_data_all_where("item", ["id_item"=> $id_item,"is_del_item"=>"0"]);
            foreach ($data_item as $key_item => $value_item) {
                // print_r($bln);
                // print_r($y);
                // print_r($value_item);
            
                $data["item_v"] = $value_item;
                // print_r($value_item);
                $id_item = $value_item->id_item;

                $data_penjualan = $this->rp->get_penjualan_item_bulan($bln, $y, array("td.id_item"=>$id_item));
                $data["data_penjualan"] = $data_penjualan;

                $data_pembelian = $this->rp->get_pembelian_item_bulan($bln, $y, array("td.id_item"=>$id_item));
                $data["data_pembelian"] = $data_pembelian;

                $where_main = [
                            "MONTH(th.tgl_transaksi_tr_header)="=>$bln,
                            "YEAR(th.tgl_transaksi_tr_header)="=>$y,
                            "td.id_item"=>$id_item
                        ];

                $where_main_no_item_rusak = [
                            "MONTH(th.tgl_transaksi_tr_header)="=>$bln,
                            "YEAR(th.tgl_transaksi_tr_header)="=>$y,
                            "td.status_rt_tr_detail"=>"0",
                            "td.id_item"=>$id_item
                        ];

                $data_retur_penjualan = $this->rp->get_retur_penjualan_detail($where_main_no_item_rusak);
                $data["data_retur_penjualan"] = $data_retur_penjualan;
                $data_retur_penjualan_p = $this->rp->get_retur_penjualan_detail_p($where_main);
                $data["data_retur_penjualan_p"] = $data_retur_penjualan_p;

                $data_retur_pembelian = $this->rp->get_retur_pembelian_detail($where_main_no_item_rusak);
                $data["data_retur_pembelian"] = $data_retur_pembelian;
                $data_retur_pembelian_p = $this->rp->get_retur_pembelian_detail_p($where_main);
                $data["data_retur_pembelian_p"] = $data_retur_pembelian_p;

                $data_sample = $this->rp->get_sample_detail($where_main);
                $data["data_sample"] = $data_sample;
                $data_retur_sample_p = $this->rp->get_retur_sample_detail_p($where_main);
                $data["data_retur_sample_p"] = $data_retur_sample_p;

                // print_r($data_pembelian);
            }  

        }
        
        // print_r($data["list_data_retur_sample_p"]);
        // print_r($check_data);
        $this->load->view('index', $data);
    }


#------------------------------show---------------------------------#


#------------------------------main---------------------------------#
    public function main_get_penjualan_item_tgl($id_item = "0"){
        $data["page"] = "report_penjualan_item";
        $data["item"] = $this->mm->get_data_all_where("item", array("is_del_item"=>"0"));
        

        $data["str_periode"] = "";

        $date_now = date("Y-m-d");

        $tgl_start = $date_now;

        $tgl_finish = $date_now;
        // $data["item"] = $this->mm->get_data_all_where("item", array("is_del_item"=>"0"));

        $data["list_data"] = array();

        $data["item_v"] = [];


        if($tgl_start != "0" && $tgl_finish != "0"){

            $data_item = $this->mm->get_data_all_where("item", ["id_item"=> $id_item,"is_del_item"=>"0"]);
            foreach ($data_item as $key_item => $value_item) {
                // print_r($value_item);
            
                $data["item_v"] = $value_item;
                // print_r($value_item);
                $id_item = $value_item->id_item;

                $data_penjualan = $this->rp->get_penjualan_item_tgl($tgl_start, $tgl_finish, array("td.id_item"=>$id_item));
                $data["data_penjualan"] = $data_penjualan;

                $data_pembelian = $this->rp->get_pembelian_item_tgl($tgl_start, $tgl_finish, array("td.id_item"=>$id_item));
                $data["data_pembelian"] = $data_pembelian;

                $where_main = [
                            "th.tgl_transaksi_tr_header >="=>$tgl_start,
                            "th.tgl_transaksi_tr_header <="=>$tgl_finish,
                            "td.id_item"=>$id_item
                        ];

                $where_main_no_item_rusak = [
                            "th.tgl_transaksi_tr_header >="=>$tgl_start,
                            "th.tgl_transaksi_tr_header <="=>$tgl_finish,
                            "td.status_rt_tr_detail"=>"0",
                            "td.id_item"=>$id_item
                        ];

                $data_retur_penjualan = $this->rp->get_retur_penjualan_detail($where_main_no_item_rusak);
                $data["data_retur_penjualan"] = $data_retur_penjualan;
                
                $data_retur_penjualan_p = $this->rp->get_retur_penjualan_detail_p($where_main);
                $data["data_retur_penjualan_p"] = $data_retur_penjualan_p;

                $data_retur_pembelian = $this->rp->get_retur_pembelian_detail($where_main_no_item_rusak);
                $data["data_retur_pembelian"] = $data_retur_pembelian;
                $data_retur_pembelian_p = $this->rp->get_retur_pembelian_detail_p($where_main);
                $data["data_retur_pembelian_p"] = $data_retur_pembelian_p;

                $data_sample = $this->rp->get_sample_detail($where_main);
                $data["data_sample"] = $data_sample;
                $data_retur_sample_p = $this->rp->get_retur_sample_detail_p($where_main);
                $data["data_retur_sample_p"] = $data_retur_sample_p;

                // print_r($data["item"]);
            }  

        }

        return $data;
    }

    public function main_get_penjualan_item_triwulan($id_item = "0"){
        $data["page"] = "report_penjualan_item";
        $data["item"] = $this->mm->get_data_all_where("item", array("is_del_item"=>"0"));
        
        $data["str_periode"] = "";

        $bln_now = date("m");
        $y_now = date("Y");

        $bln = (int)$bln_now;
        $y = (int)$y_now;

        $list_bln = [1,2,3];
        $str_triwulan = "Triwulan I";
        if (in_array($bln, [1,2,3])) {
            $list_bln = [1,2,3];

            $str_triwulan = "Triwulan I";
        }elseif (in_array($bln, [4,5,6])) {
            $list_bln = [4,5,6];
        

            $str_triwulan = "Triwulan II";
        }elseif (in_array($bln, [7,8,9])) {
            $list_bln = [7,8,9];
        

            $str_triwulan = "Triwulan III";
        }elseif (in_array($bln, [10,11,12])) {
            $list_bln = [10,11,12];


            $str_triwulan = "Triwulan IV";
        }

        $data["str_periode"] = $str_triwulan." ".$y_now;

        $data["list_data"] = array();
        $data["item_v"] = [];


        if($bln != 0 && $y != 0 && $list_bln){

            $data_item = $this->mm->get_data_all_where("item", ["id_item"=> $id_item,"is_del_item"=>"0"]);
            foreach ($data_item as $key_item => $value_item) {
            
                $data["item_v"] = $value_item;
                // print_r($value_item);
                $id_item = $value_item->id_item;

                $data_penjualan = $this->rp->get_penjualan_item_triwulan($y, $list_bln, array("td.id_item"=>$id_item));
                $data["data_penjualan"] = $data_penjualan;

                $data_pembelian = $this->rp->get_pembelian_item_triwulan($y, $list_bln, array("td.id_item"=>$id_item));
                $data["data_pembelian"] = $data_pembelian;

                $where_main = [
                            "YEAR(th.tgl_transaksi_tr_header)="=>$y,
                            "td.id_item"=>$id_item
                        ];

                $where_main_no_item_rusak = [
                            "YEAR(th.tgl_transaksi_tr_header)="=>$y,
                            "td.status_rt_tr_detail"=>"0",
                            "td.id_item"=>$id_item
                        ];

                $data_retur_penjualan = $this->rp->get_retur_penjualan_detail_triwulan($list_bln, $where_main_no_item_rusak);
                $data["data_retur_penjualan"] = $data_retur_penjualan;
                $data_retur_penjualan_p = $this->rp->get_retur_penjualan_detail_p_triwulan($list_bln, $where_main);
                $data["data_retur_penjualan_p"] = $data_retur_penjualan_p;

                $data_retur_pembelian = $this->rp->get_retur_pembelian_detail_triwulan($list_bln, $where_main_no_item_rusak);
                $data["data_retur_pembelian"] = $data_retur_pembelian;
                $data_retur_pembelian_p = $this->rp->get_retur_pembelian_detail_p_triwulan($list_bln, $where_main);
                $data["data_retur_pembelian_p"] = $data_retur_pembelian_p;

                $data_sample = $this->rp->get_sample_detail_triwulan($list_bln, $where_main);
                $data["data_sample"] = $data_sample;
                $data_retur_sample_p = $this->rp->get_retur_sample_detail_p_triwulan($list_bln, $where_main);
                $data["data_retur_sample_p"] = $data_retur_sample_p;

                // print_r($data_pembelian);
            }  

        }
        
        return $data;
    }

    public function main_get_penjualan_item_th($id_item = "0"){
        $data["page"] = "report_penjualan_item";
        $data["item"] = $this->mm->get_data_all_where("item", array("is_del_item"=>"0"));
        
        $data["str_periode"] = "";

        // $bln_now = date("m");
        $y_now = date("Y");

        // $bln = (int)$bln_now;
        $y = (int)$y_now;

        $data["str_periode"] = $y_now;

        // $tgl_finish = $date_now;
        // $data["item"] = $this->mm->get_data_all_where("item", array("is_del_item"=>"0"));

        $data["list_data"] = array();
        $data["item_v"] = [];


        if($y != 0 ){

            $data_item = $this->mm->get_data_all_where("item", ["id_item"=> $id_item,"is_del_item"=>"0"]);
            foreach ($data_item as $key_item => $value_item) {

                $data["item_v"] = $value_item;
                // print_r($value_item);
                $id_item = $value_item->id_item;

                $data_penjualan = $this->rp->get_penjualan_item_th($y, array("td.id_item"=>$id_item));
                $data["data_penjualan"] = $data_penjualan;

                $data_pembelian = $this->rp->get_pembelian_item_th($y, array("td.id_item"=>$id_item));
                $data["data_pembelian"] = $data_pembelian;

                $where_main = [
                            "YEAR(th.tgl_transaksi_tr_header)="=>$y,
                            "td.id_item"=>$id_item
                        ];

                $where_main_no_item_rusak = [
                            "YEAR(th.tgl_transaksi_tr_header)="=>$y,
                            "td.status_rt_tr_detail"=>"0",
                            "td.id_item"=>$id_item
                        ];

                $data_retur_penjualan = $this->rp->get_retur_penjualan_detail($where_main_no_item_rusak);
                $data["data_retur_penjualan"] = $data_retur_penjualan;
                $data_retur_penjualan_p = $this->rp->get_retur_penjualan_detail_p($where_main);
                $data["data_retur_penjualan_p"] = $data_retur_penjualan_p;

                $data_retur_pembelian = $this->rp->get_retur_pembelian_detail($where_main_no_item_rusak);
                $data["data_retur_pembelian"] = $data_retur_pembelian;
                $data_retur_pembelian_p = $this->rp->get_retur_pembelian_detail_p($where_main);
                $data["data_retur_pembelian_p"] = $data_retur_pembelian_p;

                $data_sample = $this->rp->get_sample_detail($where_main);
                $data["data_sample"] = $data_sample;
                $data_retur_sample_p = $this->rp->get_retur_sample_detail_p($where_main);
                $data["data_retur_sample_p"] = $data_retur_sample_p;

                // print_r($data_pembelian);
            }  

        }

        // print_r($data);
        // $this->load->view('index', $data);
        return $data;
    }

    public function main_get_penjualan_item_bulan($id_item = "0"){
        $data["page"] = "report_penjualan_item";
        $data["item"] = $this->mm->get_data_all_where("item", array("is_del_item"=>"0"));
        
        $data["str_periode"] = "";

        $bln_now = date("m");
        $y_now = date("Y");

        $bln = (int)$bln_now;
        $y = (int)$y_now;

        $data["str_periode"] = $this->array_of_month[$bln]." ".$y_now;

        // $tgl_finish = $date_now;
        // $data["item"] = $this->mm->get_data_all_where("item", array("is_del_item"=>"0"));

        $data["list_data"] = array();
        $data["item_v"] = [];


        if($bln != 0 && $y != 0 ){

            $data_item = $this->mm->get_data_all_where("item", ["id_item"=> $id_item,"is_del_item"=>"0"]);
            foreach ($data_item as $key_item => $value_item) {
                // print_r($bln);
                // print_r($y);
                // print_r($value_item);
            
                $data["item_v"] = $value_item;
                // print_r($value_item);
                $id_item = $value_item->id_item;

                $data_penjualan = $this->rp->get_penjualan_item_bulan($bln, $y, array("td.id_item"=>$id_item));
                $data["data_penjualan"] = $data_penjualan;

                $data_pembelian = $this->rp->get_pembelian_item_bulan($bln, $y, array("td.id_item"=>$id_item));
                $data["data_pembelian"] = $data_pembelian;

                $where_main = [
                            "MONTH(th.tgl_transaksi_tr_header)="=>$bln,
                            "YEAR(th.tgl_transaksi_tr_header)="=>$y,
                            "td.id_item"=>$id_item
                        ];

                $where_main_no_item_rusak = [
                            "MONTH(th.tgl_transaksi_tr_header)="=>$bln,
                            "YEAR(th.tgl_transaksi_tr_header)="=>$y,
                            "td.status_rt_tr_detail"=>"0",
                            "td.id_item"=>$id_item
                        ];

                $data_retur_penjualan = $this->rp->get_retur_penjualan_detail($where_main_no_item_rusak);
                $data["data_retur_penjualan"] = $data_retur_penjualan;
                $data_retur_penjualan_p = $this->rp->get_retur_penjualan_detail_p($where_main);
                $data["data_retur_penjualan_p"] = $data_retur_penjualan_p;

                $data_retur_pembelian = $this->rp->get_retur_pembelian_detail($where_main_no_item_rusak);
                $data["data_retur_pembelian"] = $data_retur_pembelian;
                $data_retur_pembelian_p = $this->rp->get_retur_pembelian_detail_p($where_main);
                $data["data_retur_pembelian_p"] = $data_retur_pembelian_p;

                $data_sample = $this->rp->get_sample_detail($where_main);
                $data["data_sample"] = $data_sample;
                $data_retur_sample_p = $this->rp->get_retur_sample_detail_p($where_main);
                $data["data_retur_sample_p"] = $data_retur_sample_p;

                // print_r($data_pembelian);
            }  

        }

        // print_r($data);
        // $this->load->view('index', $data);
        return $data;
    }
#------------------------------main---------------------------------#


#------------------------------print---------------------------------#
    public function print_get_penjualan_item_tgl($id_item = "0"){
        $data = array();
        if($id_item != "0"){
            $data = $this->main_get_penjualan_item_tgl($id_item);
        }
        
        // print_r($data);
        $this->load->view('print/print_penjualan_item_new', $data);
    }

    public function print_get_penjualan_item_triwulan($id_item = "0"){
        $data = array();
        if($id_item != "0"){
            $data = $this->main_get_penjualan_item_triwulan($id_item);
        }

        // print_r($data);
        $this->load->view('print/print_penjualan_item_new', $data);
    }

    public function print_get_penjualan_item_th($id_item = "0"){
        $data = array();
        if($id_item != "0"){
            $data = $this->main_get_penjualan_item_th($id_item);
        }

        // print_r($data);
        $this->load->view('print/print_penjualan_item_new', $data);
    }

    public function print_get_penjualan_item_bulan($id_item = "0"){
        $data = array();
        if($id_item != "0"){
            $data = $this->main_get_penjualan_item_bulan($id_item);
        }

        // print_r($data);
        $this->load->view('print/print_penjualan_item_new', $data);
    }
#------------------------------print---------------------------------#


#------------------------------excel---------------------------------#
    public function excel_get_penjualan_item_tgl($id_item = "0"){
        $data = array();
        if($id_item != "0"){
            $data = $this->main_get_penjualan_item_tgl($id_item);
            $this->convert_excel($data);
        }
        
        // excel_r($data);
        // $this->load->view('excel/excel_penjualan_item', $data);
    }

    public function excel_get_penjualan_item_triwulan($id_item = "0"){
        $data = array();
        if($id_item != "0"){
            $data = $this->main_get_penjualan_item_triwulan($id_item);
            $this->convert_excel($data);
        }

        // excel_r($data);
        // $this->load->view('excel/excel_penjualan_item', $data);
    }

    public function excel_get_penjualan_item_th($id_item = "0"){
        $data = array();
        if($id_item != "0"){
            $data = $this->main_get_penjualan_item_th($id_item);
            $this->convert_excel($data);
        }

        // excel_r($data);
        // $this->load->view('excel/excel_penjualan_item', $data);
    }

    public function excel_get_penjualan_item_bulan($id_item = "0"){
        $data = array();
        if($id_item != "0"){
            $data = $this->main_get_penjualan_item_bulan($id_item);
            $this->convert_excel($data);
        }

        // excel_r($data);
        // $this->load->view('excel/excel_penjualan_item', $data);
    }
#------------------------------excel---------------------------------#



    public function convert_excel($data){
        /** Error reporting */
        error_reporting(E_ALL);
        require_once APPPATH.'third_party/PHPExcel.php';

        $objPHPExcel = new PHPExcel();


       

        // Set document properties
        $objPHPExcel->getProperties()->setCreator("Filosofi_code")
                                     ->setLastModifiedBy("Filosofi_code Application")
                                     ->setTitle("Laporan Transaksi Penjualan")
                                     ->setSubject("Office 2007 XLSX Laporan Transaksi Penjualan")
                                     ->setDescription("Laporan Transaksi Penjualan for Office 2007 XLSX")
                                     ->setKeywords("office 2007 openxml php")
                                     ->setCategory("Laporan Transaksi Penjualan");


        // Add some data
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'LAPORAN TRANSAKSI PENJUALAN PRODUK')
                    ->setCellValue('A2', $data["str_periode"])
                    ->setCellValue('A3', '')
                    ->setCellValue('A4', '')
                    ->setCellValue('A5', '');

        // Pembelian
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A6', 'Daftar Masuk (Pemebelian)')
                        ->setCellValue('A7', '');

            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A8', 'No')
                        ->setCellValue('B8', 'Tanggal')
                        ->setCellValue('C8', 'No Faktur')
                        ->setCellValue('D8', 'Suplier')
                        ->setCellValue('E8', 'Masuk')
                        ->setCellValue('F8', 'RS')
                        ->setCellValue('G8', 'PT')
                        ->setCellValue('H8', 'APT')
                        ->setCellValue('I8', 'Lainnya')
                        ->setCellValue('J8', 'Total');

            $t_masuk = 0;
            $t_keluar = 0;

            $nama_suplier = "";
            $first_val = 0;
            
            $val_sisa = 0;

            $no_row = 9;
            $no = 1;

            if(isset($data["data_pembelian"])){   
                $t_pembelian = 0;
                foreach ($data["data_pembelian"] as $key => $value) {
                    $val_rs = "";
                    $val_pt = "";
                    $val_apt = "";
                    $val_lain = "";

                    $nama_suplier = $value->nama_suplier;   

                    $t_pembelian += $value->jml_item_tr_detail;

                    $val_sisa += $value->jml_item_tr_detail;
                    // print_r($value);
                    $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('A'.$no_row, $no)
                            ->setCellValue('B'.$no_row, $value->tgl_transaksi_tr_header)
                            ->setCellValue('C'.$no_row, $value->id_tr_header)
                            ->setCellValue('D'.$no_row, $value->nama_suplier)
                            ->setCellValue('E'.$no_row, $value->jml_item_tr_detail)
                            ->setCellValue('F'.$no_row, $val_rs)
                            ->setCellValue('G'.$no_row, $val_pt)
                            ->setCellValue('H'.$no_row, $val_apt)
                            ->setCellValue('I'.$no_row, $val_lain)
                            ->setCellValue('J'.$no_row, $t_pembelian);

                    
                    $no_row++;
                    $no++;
                }

                // $no_row += 1;
                $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('A'.$no_row, "")
                            ->setCellValue('B'.$no_row, "")
                            ->setCellValue('C'.$no_row, "")
                            ->setCellValue('D'.$no_row, "")
                            ->setCellValue('E'.$no_row, "")
                            ->setCellValue('F'.$no_row, "")
                            ->setCellValue('G'.$no_row, "")
                            ->setCellValue('H'.$no_row, "")
                            ->setCellValue('I'.$no_row, "")
                            ->setCellValue('J'.$no_row, $t_pembelian);

                $t_masuk += $t_pembelian;
            }
        // Pembelian


        // Retur Penjualan
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.($no_row+2), 'Daftar Masuk (Retur Penjualan)')
                        ->setCellValue('A'.($no_row+3), '');

            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.($no_row+4), 'No')
                        ->setCellValue('B'.($no_row+4), 'Tanggal')
                        ->setCellValue('C'.($no_row+4), 'No Faktur')
                        ->setCellValue('D'.($no_row+4), 'Pelanggan')

                        ->setCellValue('E'.($no_row+4), 'Masuk')
                        ->setCellValue('F'.($no_row+4), 'RS')
                        ->setCellValue('G'.($no_row+4), 'PT')
                        ->setCellValue('H'.($no_row+4), 'APT')
                        ->setCellValue('I'.($no_row+4), 'Lainnya')
                        ->setCellValue('J'.($no_row+4), 'Total');

            $no_row_next = $no_row+5;
            $no = 1;

        if(isset($data["data_retur_penjualan"])){
            $t_retur_penjualan = 0;
            foreach ($data["data_retur_penjualan"] as $key => $value) {
                $val_rs = "";
                $val_pt = "";
                $val_apt = "";
                $val_lain = "";

                switch ($value->jenis_rekanan) {
                    case 'rs':
                        $val_rs = $value->jml_item_tr_detail; 
                        break;

                    case 'apotik':
                        $val_apt = $value->jml_item_tr_detail;
                        break;

                    case 'pbf':
                        $val_pt = $value->jml_item_tr_detail;
                        break;
                    
                    default:
                        $val_lain = $value->jml_item_tr_detail;
                        break;
                }

                $t_retur_penjualan += $value->jml_item_tr_detail;

                $val_sisa += $value->jml_item_tr_detail;

                $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.$no_row_next, $no)
                        ->setCellValue('B'.$no_row_next, $value->tgl_transaksi_tr_header)
                        ->setCellValue('C'.$no_row_next, $value->id_tr_header_p)
                        ->setCellValue('D'.$no_row_next, $value->nama_rekanan)
                        ->setCellValue('E'.$no_row_next, "")
                        ->setCellValue('F'.$no_row_next, $val_rs)
                        ->setCellValue('G'.$no_row_next, $val_pt)
                        ->setCellValue('H'.$no_row_next, $val_apt)
                        ->setCellValue('I'.$no_row_next, $val_lain)
                        ->setCellValue('J'.$no_row_next, $t_retur_penjualan);
                
                $no_row_next++;
                $no++;
            }

            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.$no_row_next, "")
                        ->setCellValue('B'.$no_row_next, "")
                        ->setCellValue('C'.$no_row_next, "")
                        ->setCellValue('D'.$no_row_next, "")
                        ->setCellValue('E'.$no_row_next, "")
                        ->setCellValue('F'.$no_row_next, "")
                        ->setCellValue('G'.$no_row_next, "")
                        ->setCellValue('H'.$no_row_next, "")
                        ->setCellValue('I'.$no_row_next, "")
                        ->setCellValue('J'.$no_row_next, $t_retur_penjualan);

            $t_masuk += $t_retur_penjualan;
        }
        // Retur Penjualan


        // Retur Pengganti Retur Pembelian
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.($no_row_next+2), 'Daftar Masuk (Pengganti Retur Pembelian)')
                        ->setCellValue('A'.($no_row_next+3), '');

            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.($no_row_next+4), 'No')
                        ->setCellValue('B'.($no_row_next+4), 'Tanggal')
                        ->setCellValue('C'.($no_row_next+4), 'No Faktur')
                        ->setCellValue('D'.($no_row_next+4), 'Suplier')

                        ->setCellValue('E'.($no_row_next+4), 'Masuk')
                        ->setCellValue('F'.($no_row_next+4), 'RS')
                        ->setCellValue('G'.($no_row_next+4), 'PT')
                        ->setCellValue('H'.($no_row_next+4), 'APT')
                        ->setCellValue('I'.($no_row_next+4), 'Lainnya')
                        ->setCellValue('J'.($no_row_next+4), 'Total');

            $no_row_next = $no_row_next+5;
            $no = 1;
        if(isset($data["data_retur_pembelian_p"])){
            // $val_sisa = 0;
            $t_retur_pembelian_p = 0;
            foreach ($data["data_retur_pembelian_p"] as $key => $value) {
                $val_rs = "";
                $val_pt = "";
                $val_apt = "";
                $val_lain = "";

                $nama_suplier = $value->nama_suplier;

                $t_retur_pembelian_p += $value->jml_item_tr_detail;

                $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.$no_row_next, $no)
                        ->setCellValue('B'.$no_row_next, $value->tgl_transaksi_tr_header)
                        ->setCellValue('C'.$no_row_next, $value->id_tr_header_p)
                        ->setCellValue('D'.$no_row_next, $nama_suplier)
                        ->setCellValue('E'.$no_row_next, $value->jml_item_tr_detail)
                        ->setCellValue('F'.$no_row_next, $val_rs)
                        ->setCellValue('G'.$no_row_next, $val_pt)
                        ->setCellValue('H'.$no_row_next, $val_apt)
                        ->setCellValue('I'.$no_row_next, $val_lain)
                        ->setCellValue('J'.$no_row_next, $t_retur_pembelian_p);
                
                $no_row_next++;
                $no++;
            }

            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.$no_row_next, "")
                        ->setCellValue('B'.$no_row_next, "")
                        ->setCellValue('C'.$no_row_next, "")
                        ->setCellValue('D'.$no_row_next, "")
                        ->setCellValue('E'.$no_row_next, "")
                        ->setCellValue('F'.$no_row_next, "")
                        ->setCellValue('G'.$no_row_next, "")
                        ->setCellValue('H'.$no_row_next, "")
                        ->setCellValue('I'.$no_row_next, "")
                        ->setCellValue('J'.$no_row_next, $t_retur_pembelian_p);

            $t_masuk += $t_retur_pembelian_p;
        }
        // Retur Pengganti Retur Pembelian


        // Retur Pengganti Retur Sample
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.($no_row_next+2), 'Daftar Masuk (Pengganti Sample)')
                        ->setCellValue('A'.($no_row_next+3), '');

            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.($no_row_next+4), 'No')
                        ->setCellValue('B'.($no_row_next+4), 'Tanggal')
                        ->setCellValue('C'.($no_row_next+4), 'No Faktur')
                        ->setCellValue('D'.($no_row_next+4), 'Pelanggan')

                        ->setCellValue('E'.($no_row_next+4), 'Masuk')
                        ->setCellValue('F'.($no_row_next+4), 'RS')
                        ->setCellValue('G'.($no_row_next+4), 'PT')
                        ->setCellValue('H'.($no_row_next+4), 'APT')
                        ->setCellValue('I'.($no_row_next+4), 'Lainnya')
                        ->setCellValue('J'.($no_row_next+4), 'Total');

            $no_row_next = $no_row_next+5;
            $no = 1;
        if(isset($data["data_retur_sample_p"])){
            $t_retur_sample_p = 0;
            // $val_sisa = 0;
            foreach ($data["data_retur_sample_p"] as $key => $value) {
                $val_rs = "";
                $val_pt = "";
                $val_apt = "";
                $val_lain = "";

                $val_sisa += $value->jml_item_tr_detail;

                $t_retur_sample_p += $value->jml_item_tr_detail;

                $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.$no_row_next, $no)
                        ->setCellValue('B'.$no_row_next, $value->tgl_transaksi_tr_header)
                        ->setCellValue('C'.$no_row_next, $value->id_tr_header_p)
                        ->setCellValue('D'.$no_row_next, $nama_suplier)
                        ->setCellValue('E'.$no_row_next, $value->jml_item_tr_detail)
                        ->setCellValue('F'.$no_row_next, $val_rs)
                        ->setCellValue('G'.$no_row_next, $val_pt)
                        ->setCellValue('H'.$no_row_next, $val_apt)
                        ->setCellValue('I'.$no_row_next, $val_lain)
                        ->setCellValue('J'.$no_row_next, $t_retur_sample_p);
                
                $no_row_next++;
                $no++;
            }


            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.$no_row_next, "")
                        ->setCellValue('B'.$no_row_next, "")
                        ->setCellValue('C'.$no_row_next, "")
                        ->setCellValue('D'.$no_row_next, "")
                        ->setCellValue('E'.$no_row_next, "")
                        ->setCellValue('F'.$no_row_next, "")
                        ->setCellValue('G'.$no_row_next, "")
                        ->setCellValue('H'.$no_row_next, "")
                        ->setCellValue('I'.$no_row_next, "")
                        ->setCellValue('J'.$no_row_next, $t_retur_sample_p);

            $t_masuk += $t_retur_sample_p;
        }
        // Retur Pengganti Retur Sample




        // Penjualan
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.($no_row_next+2), 'Daftar Keluar (Penjualan)')
                        ->setCellValue('A'.($no_row_next+3), '');

            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.($no_row_next+4), 'No')
                        ->setCellValue('B'.($no_row_next+4), 'Tanggal')
                        ->setCellValue('C'.($no_row_next+4), 'No Faktur')
                        ->setCellValue('D'.($no_row_next+4), 'Pelanggan')

                        ->setCellValue('E'.($no_row_next+4), 'Masuk')
                        ->setCellValue('F'.($no_row_next+4), 'RS')
                        ->setCellValue('G'.($no_row_next+4), 'PT')
                        ->setCellValue('H'.($no_row_next+4), 'APT')
                        ->setCellValue('I'.($no_row_next+4), 'Lainnya')
                        ->setCellValue('J'.($no_row_next+4), 'Total');

            $no_row_next = $no_row_next+5;
            $no = 1;
        if(isset($data["data_penjualan"])){
            // $val_sisa = 0;
            $t_penjualan = 0;
            foreach ($data["data_penjualan"] as $key => $value) {

                $val_rs = "";
                $val_pt = "";
                $val_apt = "";
                $val_lain = "";

                switch ($value->jenis_rekanan) {
                    case 'rs':
                        $val_rs = $value->jml_item_tr_detail; 
                        break;

                    case 'apotik':
                        $val_apt = $value->jml_item_tr_detail;
                        break;

                    case 'pbf':
                        $val_pt = $value->jml_item_tr_detail;
                        break;
                    
                    default:
                        $val_lain = $value->jml_item_tr_detail;
                        break;
                }

                $t_penjualan += $value->jml_item_tr_detail;
                $val_sisa -= $value->jml_item_tr_detail;

                $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.$no_row_next, $no)
                        ->setCellValue('B'.$no_row_next, $value->tgl_transaksi_tr_header)
                        ->setCellValue('C'.$no_row_next, $value->id_tr_header)
                        ->setCellValue('D'.$no_row_next, $value->nama_rekanan)
                        ->setCellValue('E'.$no_row_next, "")
                        ->setCellValue('F'.$no_row_next, $val_rs)
                        ->setCellValue('G'.$no_row_next, $val_pt)
                        ->setCellValue('H'.$no_row_next, $val_apt)
                        ->setCellValue('I'.$no_row_next, $val_lain)
                        ->setCellValue('J'.$no_row_next, $t_penjualan);
                
                $no_row_next++;
                $no++;
            }

            // $no_row += 1;
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.$no_row_next, "")
                        ->setCellValue('B'.$no_row_next, "")
                        ->setCellValue('C'.$no_row_next, "")
                        ->setCellValue('D'.$no_row_next, "")
                        ->setCellValue('E'.$no_row_next, "")
                        ->setCellValue('F'.$no_row_next, "")
                        ->setCellValue('G'.$no_row_next, "")
                        ->setCellValue('H'.$no_row_next, "")
                        ->setCellValue('I'.$no_row_next, "")
                        ->setCellValue('J'.$no_row_next, $t_penjualan);

            $t_keluar += $t_penjualan;
        }
        // Penjualan


        // Pengganti Retur Penjualan
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.($no_row_next+2), 'Daftar Keluar (Pengganti Retur Penjualan)')
                        ->setCellValue('A'.($no_row_next+3), '');

            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.($no_row_next+4), 'No')
                        ->setCellValue('B'.($no_row_next+4), 'Tanggal')
                        ->setCellValue('C'.($no_row_next+4), 'No Faktur')
                        ->setCellValue('D'.($no_row_next+4), 'Pelanggan')

                        ->setCellValue('E'.($no_row_next+4), 'Masuk')
                        ->setCellValue('F'.($no_row_next+4), 'RS')
                        ->setCellValue('G'.($no_row_next+4), 'PT')
                        ->setCellValue('H'.($no_row_next+4), 'APT')
                        ->setCellValue('I'.($no_row_next+4), 'Lainnya')
                        ->setCellValue('J'.($no_row_next+4), 'Total');

            $no_row_next = $no_row_next+5;
            $no = 1;
        if(isset($data["data_retur_penjualan_p"])){
            $t_retur_penjualan_p = 0;
            // $val_sisa = 0;
            foreach ($data["data_retur_penjualan_p"] as $key => $value) {
                // $first_val = 0;
                // if($key == 0){
                //     $item_fine = $check_data[0]->stok_record_item;
                //     $item_broken = $check_data[0]->rusak_record_item;

                //     $first_val = $item_fine+$item_broken;
                //     $val_sisa +=$first_val;
                // }

                $val_rs = "";
                $val_pt = "";
                $val_apt = "";
                $val_lain = "";

                switch ($value->jenis_rekanan) {
                    case 'rs':
                        $val_rs = $value->jml_item_tr_detail; 
                        break;

                    case 'apotik':
                        $val_apt = $value->jml_item_tr_detail;
                        break;

                    case 'pbf':
                        $val_pt = $value->jml_item_tr_detail;
                        break;
                    
                    default:
                        $val_lain = $value->jml_item_tr_detail;
                        break;
                }

                $t_retur_penjualan_p += $value->jml_item_tr_detail;
                $val_sisa -= $value->jml_item_tr_detail;

                $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.$no_row_next, $no)
                        ->setCellValue('B'.$no_row_next, $value->tgl_transaksi_tr_header)
                        ->setCellValue('C'.$no_row_next, $value->id_tr_header_p)
                        ->setCellValue('D'.$no_row_next, $value->nama_rekanan)
                        ->setCellValue('E'.$no_row_next, "")
                        ->setCellValue('F'.$no_row_next, $val_rs)
                        ->setCellValue('G'.$no_row_next, $val_pt)
                        ->setCellValue('H'.$no_row_next, $val_apt)
                        ->setCellValue('I'.$no_row_next, $val_lain)
                        ->setCellValue('J'.$no_row_next, $t_retur_penjualan_p);
                
                $no_row_next++;
                $no++;
            }

            // $no_row_next += 1;
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.$no_row_next, "")
                        ->setCellValue('B'.$no_row_next, "")
                        ->setCellValue('C'.$no_row_next, "")
                        ->setCellValue('D'.$no_row_next, "")
                        ->setCellValue('E'.$no_row_next, "")
                        ->setCellValue('F'.$no_row_next, "")
                        ->setCellValue('G'.$no_row_next, "")
                        ->setCellValue('H'.$no_row_next, "")
                        ->setCellValue('I'.$no_row_next, "")
                        ->setCellValue('J'.$no_row_next, $t_retur_penjualan_p);

            $t_keluar += $t_retur_penjualan_p;
        }
        // Pengganti Retur Penjualan


        // Retur Pembelian
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.($no_row_next+2), 'Daftar Keluar (Retur Pembelian)')
                        ->setCellValue('A'.($no_row_next+3), '');

            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.($no_row_next+4), 'No')
                        ->setCellValue('B'.($no_row_next+4), 'Tanggal')
                        ->setCellValue('C'.($no_row_next+4), 'No Faktur')
                        ->setCellValue('D'.($no_row_next+4), 'Suplier')

                        ->setCellValue('E'.($no_row_next+4), 'Masuk')
                        ->setCellValue('F'.($no_row_next+4), 'RS')
                        ->setCellValue('G'.($no_row_next+4), 'PT')
                        ->setCellValue('H'.($no_row_next+4), 'APT')
                        ->setCellValue('I'.($no_row_next+4), 'Lainnya')
                        ->setCellValue('J'.($no_row_next+4), 'Total');

            $no_row_next = $no_row_next+5;
            $no = 1;
        if(isset($data["data_retur_pembelian"])){
            $t_retur_pembelian = 0;
            foreach ($data["data_retur_pembelian"] as $key => $value) {

                $val_rs = "";
                $val_pt = "";
                $val_apt = "";
                $val_lain = "";

                $val_pt = $value->jml_item_tr_detail;
                        
                $t_retur_pembelian += $value->jml_item_tr_detail;
                $val_sisa -= $value->jml_item_tr_detail;

                $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.$no_row_next, $no)
                        ->setCellValue('B'.$no_row_next, $value->tgl_transaksi_tr_header)
                        ->setCellValue('C'.$no_row_next, $value->id_tr_header_p)
                        ->setCellValue('D'.$no_row_next, $nama_suplier)
                        ->setCellValue('E'.$no_row_next, "")
                        ->setCellValue('F'.$no_row_next, $val_rs)
                        ->setCellValue('G'.$no_row_next, $val_pt)
                        ->setCellValue('H'.$no_row_next, $val_apt)
                        ->setCellValue('I'.$no_row_next, $val_lain)
                        ->setCellValue('J'.$no_row_next, $t_retur_pembelian);
                
                $no_row_next++;
                $no++;
            }

            // $no_row_next += 1;
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.$no_row_next, "")
                        ->setCellValue('B'.$no_row_next, "")
                        ->setCellValue('C'.$no_row_next, "")
                        ->setCellValue('D'.$no_row_next, "")
                        ->setCellValue('E'.$no_row_next, "")
                        ->setCellValue('F'.$no_row_next, "")
                        ->setCellValue('G'.$no_row_next, "")
                        ->setCellValue('H'.$no_row_next, "")
                        ->setCellValue('I'.$no_row_next, "")
                        ->setCellValue('J'.$no_row_next, $t_retur_pembelian);

            $t_keluar += $t_retur_pembelian;
        }
        // Retur Pembelian


        // Pengganti Sample
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.($no_row_next+2), 'Daftar Keluar (Sample)')
                        ->setCellValue('A'.($no_row_next+3), '');

            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.($no_row_next+4), 'No')
                        ->setCellValue('B'.($no_row_next+4), 'Tanggal')
                        ->setCellValue('C'.($no_row_next+4), 'No Faktur')
                        ->setCellValue('D'.($no_row_next+4), 'Pelanggan')

                        ->setCellValue('E'.($no_row_next+4), 'Masuk')
                        ->setCellValue('F'.($no_row_next+4), 'RS')
                        ->setCellValue('G'.($no_row_next+4), 'PT')
                        ->setCellValue('H'.($no_row_next+4), 'APT')
                        ->setCellValue('I'.($no_row_next+4), 'Lainnya')
                        ->setCellValue('J'.($no_row_next+4), 'Total');

            $no_row_next = $no_row_next+5;
            $no = 1;
        if(isset($data["data_sample"])){
            // $val_sisa = 0;
            $t_sample = 0;
            foreach ($data["data_sample"] as $key => $value) {
                // $first_val = 0;
                // if($key == 0){
                //     $item_fine = $check_data[0]->stok_record_item;
                //     $item_broken = $check_data[0]->rusak_record_item;

                //     $first_val = $item_fine+$item_broken;
                //     $val_sisa +=$first_val;
                // }

                $val_rs = "";
                $val_pt = "";
                $val_apt = "";
                $val_lain = "";

                switch ($value->jenis_rekanan) {
                    case 'rs':
                        $val_rs = $value->jml_item_tr_detail; 
                        break;

                    case 'apotik':
                        $val_apt = $value->jml_item_tr_detail;
                        break;

                    case 'pbf':
                        $val_pt = $value->jml_item_tr_detail;
                        break;
                    
                    default:
                        $val_lain = $value->jml_item_tr_detail;
                        break;
                }

                $t_sample += $value->jml_item_tr_detail;
                $val_sisa -= $value->jml_item_tr_detail;

                $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.$no_row_next, $no)
                        ->setCellValue('B'.$no_row_next, $value->tgl_transaksi_tr_header)
                        ->setCellValue('C'.$no_row_next, $value->id_tr_header)
                        ->setCellValue('D'.$no_row_next, $value->nama_rekanan)
                        ->setCellValue('E'.$no_row_next, "")
                        ->setCellValue('F'.$no_row_next, $val_rs)
                        ->setCellValue('G'.$no_row_next, $val_pt)
                        ->setCellValue('H'.$no_row_next, $val_apt)
                        ->setCellValue('I'.$no_row_next, $val_lain)
                        ->setCellValue('J'.$no_row_next, $t_sample);
                
                $no_row_next++;
                $no++;
            }

            // $no_row_next += 1;
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.$no_row_next, "")
                        ->setCellValue('B'.$no_row_next, "")
                        ->setCellValue('C'.$no_row_next, "")
                        ->setCellValue('D'.$no_row_next, "")
                        ->setCellValue('E'.$no_row_next, "")
                        ->setCellValue('F'.$no_row_next, "")
                        ->setCellValue('G'.$no_row_next, "")
                        ->setCellValue('H'.$no_row_next, "")
                        ->setCellValue('I'.$no_row_next, "")
                        ->setCellValue('J'.$no_row_next, $t_sample);

            $t_keluar += $t_sample;
        }
        // Pengganti Sample
        
        
        $no_row_next += 1;
        $no_row_next += 1;
        $no_row_next += 1;


        $id_item            = "";
        $nama_item          = "";
        $kode_produksi_item = "";
        $stok               = 0;

        if(isset($data["item_v"])){
            if($data["item_v"]){
                $id_item            = $data["item_v"]->id_item;
                $nama_item          = $data["item_v"]->nama_item;
                $kode_produksi_item = $data["item_v"]->kode_produksi_item;
                $stok               = $data["item_v"]->stok;

                
            }
        }
        
        $stok_awal = (int)$stok -(int)$t_masuk +(int)$t_keluar;

        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A'.$no_row_next, "Kode Item")
                    ->setCellValue('B'.$no_row_next, "Nama Item")
                    ->setCellValue('C'.$no_row_next, "Kode Produksi Item")
                    ->setCellValue('D'.$no_row_next, "Estimasi Stok Awal")
                    ->setCellValue('E'.$no_row_next, "Total Barang Masuk")
                    ->setCellValue('F'.$no_row_next, "Total Barang Keluar")
                    ->setCellValue('G'.$no_row_next, "Stok Real");

        $no_row_next += 1;
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A'.$no_row_next, $id_item)
                    ->setCellValue('B'.$no_row_next, $nama_item)
                    ->setCellValue('C'.$no_row_next, $kode_produksi_item)
                    ->setCellValue('D'.$no_row_next, $stok_awal)
                    ->setCellValue('E'.$no_row_next, $t_masuk)
                    ->setCellValue('F'.$no_row_next, $t_keluar)
                    ->setCellValue('G'.$no_row_next, $stok);

        // Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle('Laporan_retur');


        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);


        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel); 
        $objWriter->save('./Laporan_penjualan_item.xlsx'); 

        $this->load->helper('download');
        force_download('./Laporan_penjualan_item.xlsx', NULL);
    }
   
}
