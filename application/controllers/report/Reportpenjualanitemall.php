<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reportpenjualanitemall extends CI_Controller {

    public $keterangan_record_stok = "panjualan detail";
    public $array_of_month = ["", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];

    public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');
        $this->load->model('report/report_penjualan_item', 'rp');
        $this->load->model('report/report_retur_penjualan', 'rrp');

        $this->load->model('other/item_main', 'oi');

        $this->load->library("response_message");
        $this->load->library("Auth_v0");
        
        date_default_timezone_set("Asia/Bangkok");
        // $this->auth_v0->check_session_active_ad();
    }

    public function index(){
        $data["page"] = "report_penjualan_item_all";
        $data["str_periode"] = "";
        // $data["list_data"] = array();
        $data["item"] = $this->mm->get_data_all_where("item", array("is_del_item"=>"0"));
        $this->load->view('index', $data);
    }

#------------------------------show---------------------------------#


    public function get_penjualan_item_tgl($tgl_start = "0", $tgl_finish = "0"){
        // print_r("<pre>");
        $data["page"] = "report_penjualan_item_all";
        $data["str_periode"] = "";
        $data["item"] = $this->mm->get_data_all_where("item", array("is_del_item"=>"0"));

        $data["list_data"] = array();
        if($tgl_start != "0" && $tgl_finish != "0"){
            $array_start = explode("-", $tgl_start);
            $m_start = $this->array_of_month[(int)$array_start[1]];

            $array_finish = explode("-", $tgl_finish);
            $m_finish = $this->array_of_month[(int)$array_finish[1]];

            $data["str_periode"] = "Periode ".$array_start[2]." ".$m_start." ".$array_start[0]." - "
            .$array_finish[2]." ".$m_finish." ".$array_finish[0];

            $check_data = $this->mm->get_data_all_where("record_item", array(
                                                                        "periode_record_item >="=>$tgl_start,
                                                                        "periode_record_item <="=>$tgl_finish
                                                                    ));
            // print_r($check_data);

            $array_data_main = [];
            if($check_data){
                $periode_fisrt = $check_data[0]->periode_record_item;

                $where_in = array(
                        "ri.periode_record_item"=>$periode_fisrt
                    );

                // $check_data = $this->mm->get_data_all_where("record_item", $where_in);
                $check_data = $this->oi->get_active_record_item($where_in);

                foreach ($check_data as $key_c => $value_c) {
                    // print_r("<hr>");
                    // print_r($value_c);
                    // print_r("<br>");

                    $id_item = $value_c->id_item;
                    $array_data_main[$id_item]["header"] = $value_c;
                    $array_data_main[$id_item]["item"] = $this->mm->get_data_each("item", ["id_item"=>$id_item]);

                    
                    $data["list_data"] = $this->rp->get_penjualan_item_tgl($tgl_start, $tgl_finish, array("td.id_item"=>$id_item));

                    $data["list_data_pembelian"] = $this->rp->get_pembelian_item_tgl($tgl_start, $tgl_finish, array("td.id_item"=>$id_item));

                    $where_main = [
                                "th.tgl_transaksi_tr_header >="=>$tgl_start,
                                "th.tgl_transaksi_tr_header <="=>$tgl_finish,
                                "td.id_item"=>$id_item
                            ];

                    $data["list_data_retur_penjualan"] = $this->rp->get_retur_penjualan_detail($where_main);
                    $data["list_data_retur_penjualan_p"] = $this->rp->get_retur_penjualan_detail_p($where_main);

                    $data["list_data_retur_pembalian"] = $this->rp->get_retur_pembelian_detail($where_main);
                    $data["list_data_retur_pembelian_p"] = $this->rp->get_retur_pembelian_detail_p($where_main);

                    $data["list_data_sample"] = $this->rp->get_sample_detail($where_main);
                    $data["list_data_retur_sample_p"] = $this->rp->get_retur_sample_detail_p($where_main);

                    $data["check_data"] = $check_data;

                        $first_val = 0;
                        $val_sisa = 0;
                        $no = 1;

                        if(isset($data["check_data"])){
                            $item_fine = $value_c->stok_record_item;
                            $item_broken = $value_c->rusak_record_item;

                            $first_val = $item_fine+$item_broken;
                        }

                        $val_sisa +=$first_val;

                    // print_r($data["list_data"]);


                    // Pembelian
                        if($data["list_data_pembelian"] && isset($data["check_data"])){
                            foreach ($data["list_data_pembelian"] as $key => $value) {
                                // print_r("Pembelian ==> ". $value->jml_item_tr_detail);
                                // print_r("<br>");
                                $val_sisa += $value->jml_item_tr_detail;
                                // print_r("Stok ==>". $val_sisa);
                                // print_r("<br>");
                            }
                        }


                    // Retur Penjualan
                        if(isset($data["list_data_retur_penjualan"]) && isset($data["check_data"])){
                            foreach ($data["list_data_retur_penjualan"] as $key => $value) {
                                // print_r("Retur Penjualan ==> ". $value->jml_item_tr_detail);
                                // print_r("<br>");
                                $val_sisa += $value->jml_item_tr_detail;
                                // print_r("Stok ==>". $val_sisa);
                                // print_r("<br>");
                            }
                        }


                    // Retur Pengganti Retur Pembelian
                        if(isset($data["list_data_retur_pembelian_p"]) && isset($data["check_data"])){
                            foreach ($data["list_data_retur_pembelian_p"] as $key => $value) {
                                // print_r("Retur Pengganti Retur Pembelian ==> ". $value->jml_item_tr_detail);
                                // print_r("<br>");
                                $val_sisa += $value->jml_item_tr_detail;
                                // print_r("Stok ==>". $val_sisa);
                                // print_r("<br>");
                            }
                        }


                    // Retur Pengganti Retur Sample
                        if(isset($data["list_data_retur_sample_p"]) && isset($data["check_data"])){
                            foreach ($data["list_data_retur_sample_p"] as $key => $value) {
                                // print_r("Retur Pengganti Retur Sample ==> ". $value->jml_item_tr_detail);
                                // print_r("<br>");
                                $val_sisa += $value->jml_item_tr_detail;
                                // print_r("Stok ==>". $val_sisa);
                                // print_r("<br>");
                            }
                        }




                    // Penjualan
                        if(isset($data["list_data"]) && isset($data["check_data"])){
                            foreach ($data["list_data"] as $key => $value) {
                                // print_r("Penjualan ==> ". $value->jml_item_tr_detail);
                                // print_r("<br>");
                                $val_sisa -= $value->jml_item_tr_detail;
                                // print_r("Stok ==>". $val_sisa);
                                // print_r("<br>");
                            }
                        }


                    // Pengganti Retur Penjualan
                        if(isset($data["list_data_retur_penjualan_p"]) && isset($data["check_data"])){
                            foreach ($data["list_data_retur_penjualan_p"] as $key => $value) {
                                // print_r("Pengganti Retur Penjualan ==> ". $value->jml_item_tr_detail);
                                // print_r("<br>");
                                $val_sisa -= $value->jml_item_tr_detail;
                                // print_r("Stok ==>". $val_sisa);
                                // print_r("<br>");
                            }
                        }


                    // Retur Pembelian
                        if(isset($data["list_data_retur_pembalian"]) && isset($data["check_data"])){
                            foreach ($data["list_data_retur_pembalian"] as $key => $value) {
                                // print_r("Retur Pembelian ==> ". $value->jml_item_tr_detail);
                                // print_r("<br>");
                                $val_sisa -= $value->jml_item_tr_detail;
                                // print_r("Stok ==>". $val_sisa);
                                // print_r("<br>");
                            }
                        }


                    // Pengganti Sample
                        if(isset($data["list_data_sample"]) && isset($data["check_data"])){
                            foreach ($data["list_data_sample"] as $key => $value) {
                                // print_r("Pengganti Sample ==> ". $value->jml_item_tr_detail);
                                // print_r("<br>");
                                $val_sisa -= $value->jml_item_tr_detail;
                                // print_r("Stok ==>". $val_sisa);
                                // print_r("<br>");
                            }
                        }

                    // print_r("-------------------");
                    // print_r("<br>");
                    // print_r("Sisa ==>". $val_sisa);
                    // print_r("<br>");
                    
                    $array_data_main[$id_item]["stok_now"] = $val_sisa;
                }
            }

            $data["array_data_main"] = $array_data_main;
        }
        
        // print_r($data["list_data_retur_sample_p"]);
        // print_r("<pre>");
        // print_r($data["array_data_main"]);

        $this->load->view('index', $data);
    }

    public function get_penjualan_item_triwulan($triwulan = "0", $th_triwulan = "0"){
        $data["page"] = "report_penjualan_item_all";
        $data["str_periode"] = "";
        $data["item"] = $this->mm->get_data_all_where("item", array("is_del_item"=>"0"));

        $data["list_data"] = array();
        if($triwulan != "0" && $th_triwulan != "0"){
            $array_periode = explode("-", $triwulan);
            $array_where_in = array();
            for ($i=$array_periode[0]; $i <= $array_periode[1]; $i++) { 
                array_push($array_where_in, $i);
            }

            $array_triwulan = explode("-", $triwulan);

            $data["str_periode"] = "Periode ".$this->array_of_month[(int)$array_triwulan[0]]." - ".$this->array_of_month[(int)$array_triwulan[1]]." ". $th_triwulan;

            $check_data = $this->rp->get_record_item(array(),$array_where_in);

            $array_data_main = [];
            if($check_data){
                $periode_fisrt = $check_data[0]->periode_record_item;
            
                $where_in = array(
                        "ri.periode_record_item"=>$periode_fisrt
                    );

                // $check_data = $this->mm->get_data_all_where("record_item", $where_in);
                $check_data = $this->oi->get_active_record_item($where_in);

                foreach ($check_data as $key_c => $value_c) {
                    $id_item = $value_c->id_item;
                    // print_r($value_c);
                    $array_data_main[$id_item]["header"] = $value_c;
                    $array_data_main[$id_item]["item"] = $this->mm->get_data_each("item", ["id_item"=>$id_item]);

                    $data["list_data"] = $this->rp->get_penjualan_item_triwulan($th_triwulan, $array_where_in, array("td.id_item"=>$id_item));

                    $data["list_data_pembelian"] = $this->rp->get_pembelian_item_triwulan($th_triwulan, $array_where_in, array("td.id_item"=>$id_item));

                    $where_main = ["td.id_item"=>$id_item,
                                    "YEAR(th.tgl_transaksi_tr_header)="=>$th_triwulan];

                    $data["list_data_retur_penjualan"] = $this->rp->get_retur_penjualan_detail_triwulan($array_where_in, $where_main);
                    $data["list_data_retur_penjualan_p"] = $this->rp->get_retur_penjualan_detail_p_triwulan($array_where_in, $where_main);

                    $data["list_data_retur_pembalian"] = $this->rp->get_retur_pembelian_detail_triwulan($array_where_in, $where_main);
                    $data["list_data_retur_pembelian_p"] = $this->rp->get_retur_pembelian_detail_p_triwulan($array_where_in, $where_main);

                    $data["list_data_sample"] = $this->rp->get_sample_detail_triwulan($array_where_in, $where_main);
                    $data["list_data_retur_sample_p"] = $this->rp->get_retur_sample_detail_p_triwulan($array_where_in, $where_main);

                    $data["check_data"] = $check_data;
                    // print_r($data["check_data"]);

                        $first_val = 0;
                        $val_sisa = 0;
                        $no = 1;

                        if(isset($data["check_data"])){
                            $item_fine = $value_c->stok_record_item;
                            $item_broken = $value_c->rusak_record_item;

                            $first_val = $item_fine+$item_broken;
                        }

                        $val_sisa +=$first_val;
                        // print_r($val_sisa);
                        // print_r("<br>");


                    // Pembelian
                        if($data["list_data_pembelian"] && isset($data["check_data"])){
                            foreach ($data["list_data_pembelian"] as $key => $value) {
                                // print_r($val_sisa."-".$value->jml_item_tr_detail);
                                // print_r("<br>");
                                $val_sisa += $value->jml_item_tr_detail;
                                // print_r($val_sisa);

                                // print_r("<br>");
                            }
                        }


                    // Retur Penjualan
                        if(isset($data["list_data_retur_penjualan"]) && isset($data["check_data"])){
                            foreach ($data["list_data_retur_penjualan"] as $key => $value) {
                                // print_r($val_sisa."-".$value->jml_item_tr_detail);
                                // print_r("<br>");
                                $val_sisa += $value->jml_item_tr_detail;
                                // print_r($val_sisa);

                                // print_r("<br>");
                            }
                        }


                    // Retur Pengganti Retur Pembelian
                        if(isset($data["list_data_retur_pembelian_p"]) && isset($data["check_data"])){
                            foreach ($data["list_data_retur_pembelian_p"] as $key => $value) {
                                // print_r($val_sisa."-".$value->jml_item_tr_detail);
                                // print_r("<br>");
                                $val_sisa += $value->jml_item_tr_detail;
                                // print_r($val_sisa);
                                // print_r("<br>");
                            }
                        }


                    // Retur Pengganti Retur Sample
                        if(isset($data["list_data_retur_sample_p"]) && isset($data["check_data"])){
                            foreach ($data["list_data_retur_sample_p"] as $key => $value) {
                                // print_r($val_sisa."-".$value->jml_item_tr_detail);
                                // print_r("<br>");
                                $val_sisa += $value->jml_item_tr_detail;
                                // print_r($val_sisa);
                                // print_r("<br>");
                            }
                        }




                    // Penjualan
                        if(isset($data["list_data"]) && isset($data["check_data"])){
                            foreach ($data["list_data"] as $key => $value) {
                                // print_r($val_sisa."-".$value->jml_item_tr_detail);
                                // print_r("<br>");
                                $val_sisa -= $value->jml_item_tr_detail;
                                // print_r($val_sisa);
                                // print_r("<br>");
                            }
                        }


                    // Pengganti Retur Penjualan
                        if(isset($data["list_data_retur_penjualan_p"]) && isset($data["check_data"])){
                            foreach ($data["list_data_retur_penjualan_p"] as $key => $value) {
                                // print_r($val_sisa."-".$value->jml_item_tr_detail);
                                // print_r("<br>");
                                $val_sisa -= $value->jml_item_tr_detail;
                                // print_r($val_sisa);
                                // print_r("<br>");
                            }
                        }


                    // Retur Pembelian
                        if(isset($data["list_data_retur_pembalian"]) && isset($data["check_data"])){
                            foreach ($data["list_data_retur_pembalian"] as $key => $value) {
                                // print_r($val_sisa."-".$value->jml_item_tr_detail);
                                // print_r("<br>");
                                $val_sisa -= $value->jml_item_tr_detail;
                                // print_r($val_sisa);
                                // print_r("<br>");
                            }
                        }


                    // Sample
                        if(isset($data["list_data_sample"]) && isset($data["check_data"])){
                            foreach ($data["list_data_sample"] as $key => $value) {
                                // print_r($val_sisa."-".$value->jml_item_tr_detail);
                                // print_r("<br>");
                                $val_sisa -= $value->jml_item_tr_detail;
                                // print_r($val_sisa);
                                // print_r("<br>");
                            }
                        }


                        // print_r("-------------------------------");
                        // print_r($id_item);
                        // print_r("-------------------------------");
                        // print_r("<br>");


                    $array_data_main[$id_item]["stok_now"] = $val_sisa;
                }
            }

            $data["array_data_main"] = $array_data_main;
        }
        
        // print_r("<pre>");
        // print_r($array_data_main);
        // print_r($data["list_data_retur_penjualan"]);
        $this->load->view('index', $data);
    }

    public function get_penjualan_item_th($th_start = "0", $th_finish = "0"){
        $data["page"] = "report_penjualan_item_all";
        $data["str_periode"] = "";
        $data["item"] = $this->mm->get_data_all_where("item", array("is_del_item"=>"0"));

        $data["list_data"] = array();
        if($th_start != "0" && $th_finish != "0"){
            // $data["list_data"] = $this->rp->get_penjualan_item_th($th_start, $th_finish, array("td.id_item"=>$id_item));
            $data["str_periode"] = "Periode ".$th_start." - ". $th_finish;

            $check_data = $this->rp->get_first_time([
                                                        "YEAR(periode_record_item) >="=>$th_start,
                                                        "YEAR(periode_record_item) <="=>$th_finish
                                                        ]
                                                    );

            $array_data_main = [];
            if($check_data){
                $periode_fisrt = $check_data[0]->periode_record_item;

                $where_in = array(
                        "ri.periode_record_item"=>$periode_fisrt
                    );

                // $check_data = $this->mm->get_data_all_where("record_item", $where_in);
                $check_data = $this->oi->get_active_record_item($where_in);

                foreach ($check_data as $key_c => $value_c) {
                    $id_item = $value_c->id_item;
                    $array_data_main[$id_item]["header"] = $value_c;
                    $array_data_main[$id_item]["item"] = $this->mm->get_data_each("item", ["id_item"=>$id_item]);

                    $data["list_data"] = $this->rp->get_penjualan_item_th($th_start, $th_finish, array("td.id_item"=>$id_item));

                    $data["list_data_pembelian"] = $this->rp->get_pembelian_item_th($th_start, $th_finish, array("td.id_item"=>$id_item));

                    $where_main = [
                                "YEAR(th.tgl_transaksi_tr_header)>="=>$th_start,
                                "YEAR(th.tgl_transaksi_tr_header)<="=>$th_finish,
                                "td.id_item"=>$id_item
                            ];

                    $data["list_data_retur_penjualan"] = $this->rp->get_retur_penjualan_detail($where_main);
                    $data["list_data_retur_penjualan_p"] = $this->rp->get_retur_penjualan_detail_p($where_main);

                    $data["list_data_retur_pembalian"] = $this->rp->get_retur_pembelian_detail($where_main);
                    $data["list_data_retur_pembelian_p"] = $this->rp->get_retur_pembelian_detail_p($where_main);

                    $data["list_data_sample"] = $this->rp->get_sample_detail($where_main);
                    $data["list_data_retur_sample_p"] = $this->rp->get_retur_sample_detail_p($where_main);

                    $data["check_data"] = $check_data;

                        $first_val = 0;
                        $val_sisa = 0;
                        $no = 1;

                        if(isset($data["check_data"])){
                            $item_fine = $value_c->stok_record_item;
                            $item_broken = $value_c->rusak_record_item;

                            $first_val = $item_fine+$item_broken;
                        }

                        $val_sisa +=$first_val;


                    // Pembelian
                        if($data["list_data_pembelian"] && isset($data["check_data"])){
                            foreach ($data["list_data_pembelian"] as $key => $value) {
                                $val_sisa += $value->jml_item_tr_detail;
                            }
                        }


                    // Retur Penjualan
                        if(isset($data["list_data_retur_penjualan"]) && isset($data["check_data"])){
                            foreach ($data["list_data_retur_penjualan"] as $key => $value) {
                                $val_sisa += $value->jml_item_tr_detail;
                            }
                        }


                    // Retur Pengganti Retur Pembelian
                        if(isset($data["list_data_retur_pembelian_p"]) && isset($data["check_data"])){
                            foreach ($data["list_data_retur_pembelian_p"] as $key => $value) {
                                $val_sisa += $value->jml_item_tr_detail;
                            }
                        }


                    // Retur Pengganti Retur Sample
                        if(isset($data["list_data_retur_sample_p"]) && isset($data["check_data"])){
                            foreach ($data["list_data_retur_sample_p"] as $key => $value) {
                                $val_sisa += $value->jml_item_tr_detail;
                            }
                        }




                    // Penjualan
                        if(isset($data["list_data"]) && isset($data["check_data"])){
                            foreach ($data["list_data"] as $key => $value) {
                                $val_sisa -= $value->jml_item_tr_detail;
                            }
                        }


                    // Pengganti Retur Penjualan
                        if(isset($data["list_data_retur_penjualan_p"]) && isset($data["check_data"])){
                            foreach ($data["list_data_retur_penjualan_p"] as $key => $value) {
                                $val_sisa -= $value->jml_item_tr_detail;
                            }
                        }


                    // Retur Pembelian
                        if(isset($data["list_data_retur_pembalian"]) && isset($data["check_data"])){
                            foreach ($data["list_data_retur_pembalian"] as $key => $value) {
                                $val_sisa -= $value->jml_item_tr_detail;
                            }
                        }


                    // Pengganti Sample
                        if(isset($data["list_data_sample"]) && isset($data["check_data"])){
                            foreach ($data["list_data_sample"] as $key => $value) {
                                $val_sisa -= $value->jml_item_tr_detail;
                            }
                        }

                    $array_data_main[$id_item]["stok_now"] = $val_sisa;
                }
            }

            $data["array_data_main"] = $array_data_main;
        }

        // print_r($data);
        // print_r("<pre>");
        // print_r($array_data_main);
        $this->load->view('index', $data);
    }

    public function get_penjualan_item_bulan($bulan = "0", $th = "0"){
        $data["page"] = "report_penjualan_item_all";
        $data["str_periode"] = "";
        $data["item"] = $this->mm->get_data_all_where("item", array("is_del_item"=>"0"));

        $data["list_data"] = array();
        if($bulan != "0" && $th != "0"){
            // $data["list_data"] = $this->rp->get_penjualan_item_bulan($bulan, $th, array("td.id_item"=>$id_item));
            $data["str_periode"] = "Periode ".$this->array_of_month[(int)$bulan]." ". $th;

            $check_data = $this->rp->get_first_time([
                                                        "MONTH(periode_record_item) ="=>$bulan,
                                                        "YEAR(periode_record_item) ="=>$th
                                                    ]);

            $array_data_main = [];
            if($check_data){
                $periode_fisrt = $check_data[0]->periode_record_item;

                $where_in = array(
                        "ri.periode_record_item"=>$periode_fisrt
                    );

                // $check_data = $this->mm->get_data_all_where("record_item", $where_in);
                $check_data = $this->oi->get_active_record_item($where_in);

                foreach ($check_data as $key_c => $value_c) {
                    $id_item = $value_c->id_item;
                    $array_data_main[$id_item]["header"] = $value_c;
                    $array_data_main[$id_item]["item"] = $this->mm->get_data_each("item", ["id_item"=>$id_item]);

                    $data["list_data"] = $this->rp->get_penjualan_item_bulan($bulan, $th, array("td.id_item"=>$id_item));

                    $data["list_data_pembelian"] = $this->rp->get_pembelian_item_bulan($bulan, $th, array("td.id_item"=>$id_item));

                    $where_main = [
                                "MONTH(th.tgl_transaksi_tr_header)="=>$bulan,
                                "YEAR(th.tgl_transaksi_tr_header)="=>$th,
                                "td.id_item"=>$id_item
                            ];

                    $data["list_data_retur_penjualan"] = $this->rp->get_retur_penjualan_detail($where_main);
                    $data["list_data_retur_penjualan_p"] = $this->rp->get_retur_penjualan_detail_p($where_main);

                    $data["list_data_retur_pembalian"] = $this->rp->get_retur_pembelian_detail($where_main);
                    $data["list_data_retur_pembelian_p"] = $this->rp->get_retur_pembelian_detail_p($where_main);

                    $data["list_data_sample"] = $this->rp->get_sample_detail($where_main);
                    $data["list_data_retur_sample_p"] = $this->rp->get_retur_sample_detail_p($where_main);

                    $data["check_data"] = $check_data;

                        $first_val = 0;
                        $val_sisa = 0;
                        $no = 1;

                        if(isset($data["check_data"])){
                            $item_fine = $value_c->stok_record_item;
                            $item_broken = $value_c->rusak_record_item;

                            $first_val = $item_fine+$item_broken;
                        }

                        $val_sisa +=$first_val;


                    // Pembelian
                        if($data["list_data_pembelian"] && isset($data["check_data"])){
                            foreach ($data["list_data_pembelian"] as $key => $value) {
                                $val_sisa += $value->jml_item_tr_detail;
                            }
                        }


                    // Retur Penjualan
                        if(isset($data["list_data_retur_penjualan"]) && isset($data["check_data"])){
                            foreach ($data["list_data_retur_penjualan"] as $key => $value) {
                                $val_sisa += $value->jml_item_tr_detail;
                            }
                        }


                    // Retur Pengganti Retur Pembelian
                        if(isset($data["list_data_retur_pembelian_p"]) && isset($data["check_data"])){
                            foreach ($data["list_data_retur_pembelian_p"] as $key => $value) {
                                $val_sisa += $value->jml_item_tr_detail;
                            }
                        }


                    // Retur Pengganti Retur Sample
                        if(isset($data["list_data_retur_sample_p"]) && isset($data["check_data"])){
                            foreach ($data["list_data_retur_sample_p"] as $key => $value) {
                                $val_sisa += $value->jml_item_tr_detail;
                            }
                        }




                    // Penjualan
                        if(isset($data["list_data"]) && isset($data["check_data"])){
                            foreach ($data["list_data"] as $key => $value) {
                                $val_sisa -= $value->jml_item_tr_detail;
                            }
                        }


                    // Pengganti Retur Penjualan
                        if(isset($data["list_data_retur_penjualan_p"]) && isset($data["check_data"])){
                            foreach ($data["list_data_retur_penjualan_p"] as $key => $value) {
                                $val_sisa -= $value->jml_item_tr_detail;
                            }
                        }


                    // Retur Pembelian
                        if(isset($data["list_data_retur_pembalian"]) && isset($data["check_data"])){
                            foreach ($data["list_data_retur_pembalian"] as $key => $value) {
                                $val_sisa -= $value->jml_item_tr_detail;
                            }
                        }


                    // Pengganti Sample
                        if(isset($data["list_data_sample"]) && isset($data["check_data"])){
                            foreach ($data["list_data_sample"] as $key => $value) {
                                $val_sisa -= $value->jml_item_tr_detail;
                            }
                        }

                    $array_data_main[$id_item]["stok_now"] = $val_sisa;
                }
            }

            $data["array_data_main"] = $array_data_main;
        }

        // print_r("<pre>");
        // print_r($data["check_data"]);
        // print_r("<pre>");
        // print_r($array_data_main);
        $this->load->view('index', $data);
    }


#------------------------------show---------------------------------#


#------------------------------main---------------------------------#
    public function main_get_penjualan_item_tgl($tgl_start = "0", $tgl_finish = "0"){
        $data["page"] = "report_penjualan_item";
        $data["str_periode"] = "";
        $data["item"] = $this->mm->get_data_all_where("item", array("is_del_item"=>"0"));

        $data["list_data"] = array();
        if($tgl_start != "0" && $tgl_finish != "0"){
            $array_start = explode("-", $tgl_start);
            $m_start = $this->array_of_month[(int)$array_start[1]];

            $array_finish = explode("-", $tgl_finish);
            $m_finish = $this->array_of_month[(int)$array_finish[1]];

            $data["str_periode"] = "Periode ".$array_start[2]." ".$m_start." ".$array_start[0]." - "
            .$array_finish[2]." ".$m_finish." ".$array_finish[0];


            $check_data = $this->mm->get_data_all_where("record_item", array(
                                                                        "periode_record_item >="=>$tgl_start,
                                                                        "periode_record_item <="=>$tgl_finish
                                                                    ));


            $array_data_main = [];
            if($check_data){
                $periode_fisrt = $check_data[0]->periode_record_item;

                $where_in = array(
                        "ri.periode_record_item"=>$periode_fisrt
                    );

                // $check_data = $this->mm->get_data_all_where("record_item", $where_in);
                $check_data = $this->oi->get_active_record_item($where_in);

                foreach ($check_data as $key_c => $value_c) {
                    $id_item = $value_c->id_item;
                    $array_data_main[$id_item]["header"] = $value_c;
                    $array_data_main[$id_item]["item"] = $this->mm->get_data_each("item", ["id_item"=>$id_item]);

                    
                    $data["list_data"] = $this->rp->get_penjualan_item_tgl($tgl_start, $tgl_finish, array("td.id_item"=>$id_item));

                    $data["list_data_pembelian"] = $this->rp->get_pembelian_item_tgl($tgl_start, $tgl_finish, array("td.id_item"=>$id_item));

                    $where_main = [
                                "th.tgl_transaksi_tr_header >="=>$tgl_start,
                                "th.tgl_transaksi_tr_header <="=>$tgl_finish,
                                "td.id_item"=>$id_item
                            ];

                    $data["list_data_retur_penjualan"] = $this->rp->get_retur_penjualan_detail($where_main);
                    $data["list_data_retur_penjualan_p"] = $this->rp->get_retur_penjualan_detail_p($where_main);

                    $data["list_data_retur_pembalian"] = $this->rp->get_retur_pembelian_detail($where_main);
                    $data["list_data_retur_pembelian_p"] = $this->rp->get_retur_pembelian_detail_p($where_main);

                    $data["list_data_sample"] = $this->rp->get_sample_detail($where_main);
                    $data["list_data_retur_sample_p"] = $this->rp->get_retur_sample_detail_p($where_main);

                    $data["check_data"] = $check_data;

                        $first_val = 0;
                        $val_sisa = 0;
                        $no = 1;

                        if(isset($data["check_data"])){
                            $item_fine = $value_c->stok_record_item;
                            $item_broken = $value_c->rusak_record_item;

                            $first_val = $item_fine+$item_broken;
                        }

                        $val_sisa +=$first_val;


                    // Pembelian
                        if($data["list_data_pembelian"] && isset($data["check_data"])){
                            foreach ($data["list_data_pembelian"] as $key => $value) {
                                $val_sisa += $value->jml_item_tr_detail;
                            }
                        }


                    // Retur Penjualan
                        if(isset($data["list_data_retur_penjualan"]) && isset($data["check_data"])){
                            foreach ($data["list_data_retur_penjualan"] as $key => $value) {
                                $val_sisa += $value->jml_item_tr_detail;
                            }
                        }


                    // Retur Pengganti Retur Pembelian
                        if(isset($data["list_data_retur_pembelian_p"]) && isset($data["check_data"])){
                            foreach ($data["list_data_retur_pembelian_p"] as $key => $value) {
                                $val_sisa += $value->jml_item_tr_detail;
                            }
                        }


                    // Retur Pengganti Retur Sample
                        if(isset($data["list_data_retur_sample_p"]) && isset($data["check_data"])){
                            foreach ($data["list_data_retur_sample_p"] as $key => $value) {
                                $val_sisa += $value->jml_item_tr_detail;
                            }
                        }




                    // Penjualan
                        if(isset($data["list_data"]) && isset($data["check_data"])){
                            foreach ($data["list_data"] as $key => $value) {
                                $val_sisa -= $value->jml_item_tr_detail;
                            }
                        }


                    // Pengganti Retur Penjualan
                        if(isset($data["list_data_retur_penjualan_p"]) && isset($data["check_data"])){
                            foreach ($data["list_data_retur_penjualan_p"] as $key => $value) {
                                $val_sisa -= $value->jml_item_tr_detail;
                            }
                        }


                    // Retur Pembelian
                        if(isset($data["list_data_retur_pembalian"]) && isset($data["check_data"])){
                            foreach ($data["list_data_retur_pembalian"] as $key => $value) {
                                $val_sisa -= $value->jml_item_tr_detail;
                            }
                        }


                    // Pengganti Sample
                        if(isset($data["list_data_sample"]) && isset($data["check_data"])){
                            foreach ($data["list_data_sample"] as $key => $value) {
                                $val_sisa -= $value->jml_item_tr_detail;
                            }
                        }

                    $array_data_main[$id_item]["stok_now"] = $val_sisa;
                }
            }

            $data["array_data_main"] = $array_data_main;
        }
        
        // print_r($data);
        // $this->load->view('index', $data);
        return $data;
    }

    public function main_get_penjualan_item_triwulan($triwulan = "0", $th_triwulan = "0"){
        $data["page"] = "report_penjualan_item";
        $data["str_periode"] = "";
        $data["item"] = $this->mm->get_data_all_where("item", array("is_del_item"=>"0"));

        $data["list_data"] = array();
        if($triwulan != "0" && $th_triwulan != "0"){
            $array_periode = explode("-", $triwulan);
            $array_where_in = array();
            for ($i=$array_periode[0]; $i <= $array_periode[1]; $i++) { 
                array_push($array_where_in, $i);
            }

            $array_triwulan = explode("-", $triwulan);

            $data["str_periode"] = "Periode ".$this->array_of_month[(int)$array_triwulan[0]]." - ".$this->array_of_month[(int)$array_triwulan[1]]." ". $th_triwulan;

            $check_data = $this->rp->get_record_item(array(),$array_where_in);

            $array_data_main = [];
            if($check_data){
                $periode_fisrt = $check_data[0]->periode_record_item;
            
                $where_in = array(
                        "ri.periode_record_item"=>$periode_fisrt
                    );

                // $check_data = $this->mm->get_data_all_where("record_item", $where_in);
                $check_data = $this->oi->get_active_record_item($where_in);

                foreach ($check_data as $key_c => $value_c) {
                    $id_item = $value_c->id_item;
                    // print_r($value_c);
                    $array_data_main[$id_item]["header"] = $value_c;
                    $array_data_main[$id_item]["item"] = $this->mm->get_data_each("item", ["id_item"=>$id_item]);


                    $data["list_data"] = $this->rp->get_penjualan_item_triwulan($th_triwulan, $array_where_in, array("td.id_item"=>$id_item));

                    $data["list_data_pembelian"] = $this->rp->get_pembelian_item_triwulan($th_triwulan, $array_where_in, array("td.id_item"=>$id_item));

                    $where_main = ["td.id_item"=>$id_item,
                                    "YEAR(th.tgl_transaksi_tr_header)="=>$th_triwulan];

                    $data["list_data_retur_penjualan"] = $this->rp->get_retur_penjualan_detail_triwulan($array_where_in, $where_main);
                    $data["list_data_retur_penjualan_p"] = $this->rp->get_retur_penjualan_detail_p_triwulan($array_where_in, $where_main);

                    $data["list_data_retur_pembalian"] = $this->rp->get_retur_pembelian_detail_triwulan($array_where_in, $where_main);
                    $data["list_data_retur_pembelian_p"] = $this->rp->get_retur_pembelian_detail_p_triwulan($array_where_in, $where_main);

                    $data["list_data_sample"] = $this->rp->get_sample_detail_triwulan($array_where_in, $where_main);
                    $data["list_data_retur_sample_p"] = $this->rp->get_retur_sample_detail_p_triwulan($array_where_in, $where_main);

                    $data["check_data"] = $check_data;
                    // print_r($data["check_data"]);

                        $first_val = 0;
                        $val_sisa = 0;
                        $no = 1;

                        if(isset($data["check_data"])){
                            $item_fine = $value_c->stok_record_item;
                            $item_broken = $value_c->rusak_record_item;

                            $first_val = $item_fine+$item_broken;
                        }

                        $val_sisa +=$first_val;
                        // print_r($val_sisa);
                        // print_r("<br>");


                    // Pembelian
                        if($data["list_data_pembelian"] && isset($data["check_data"])){
                            foreach ($data["list_data_pembelian"] as $key => $value) {
                                // print_r($val_sisa."-".$value->jml_item_tr_detail);
                                // print_r("<br>");
                                $val_sisa += $value->jml_item_tr_detail;
                                // print_r($val_sisa);

                                // print_r("<br>");
                            }
                        }


                    // Retur Penjualan
                        if(isset($data["list_data_retur_penjualan"]) && isset($data["check_data"])){
                            foreach ($data["list_data_retur_penjualan"] as $key => $value) {
                                // print_r($val_sisa."-".$value->jml_item_tr_detail);
                                // print_r("<br>");
                                $val_sisa += $value->jml_item_tr_detail;
                                // print_r($val_sisa);

                                // print_r("<br>");
                            }
                        }


                    // Retur Pengganti Retur Pembelian
                        if(isset($data["list_data_retur_pembelian_p"]) && isset($data["check_data"])){
                            foreach ($data["list_data_retur_pembelian_p"] as $key => $value) {
                                // print_r($val_sisa."-".$value->jml_item_tr_detail);
                                // print_r("<br>");
                                $val_sisa += $value->jml_item_tr_detail;
                                // print_r($val_sisa);
                                // print_r("<br>");
                            }
                        }


                    // Retur Pengganti Retur Sample
                        if(isset($data["list_data_retur_sample_p"]) && isset($data["check_data"])){
                            foreach ($data["list_data_retur_sample_p"] as $key => $value) {
                                // print_r($val_sisa."-".$value->jml_item_tr_detail);
                                // print_r("<br>");
                                $val_sisa += $value->jml_item_tr_detail;
                                // print_r($val_sisa);
                                // print_r("<br>");
                            }
                        }




                    // Penjualan
                        if(isset($data["list_data"]) && isset($data["check_data"])){
                            foreach ($data["list_data"] as $key => $value) {
                                // print_r($val_sisa."-".$value->jml_item_tr_detail);
                                // print_r("<br>");
                                $val_sisa -= $value->jml_item_tr_detail;
                                // print_r($val_sisa);
                                // print_r("<br>");
                            }
                        }


                    // Pengganti Retur Penjualan
                        if(isset($data["list_data_retur_penjualan_p"]) && isset($data["check_data"])){
                            foreach ($data["list_data_retur_penjualan_p"] as $key => $value) {
                                // print_r($val_sisa."-".$value->jml_item_tr_detail);
                                // print_r("<br>");
                                $val_sisa -= $value->jml_item_tr_detail;
                                // print_r($val_sisa);
                                // print_r("<br>");
                            }
                        }


                    // Retur Pembelian
                        if(isset($data["list_data_retur_pembalian"]) && isset($data["check_data"])){
                            foreach ($data["list_data_retur_pembalian"] as $key => $value) {
                                // print_r($val_sisa."-".$value->jml_item_tr_detail);
                                // print_r("<br>");
                                $val_sisa -= $value->jml_item_tr_detail;
                                // print_r($val_sisa);
                                // print_r("<br>");
                            }
                        }


                    // Sample
                        if(isset($data["list_data_sample"]) && isset($data["check_data"])){
                            foreach ($data["list_data_sample"] as $key => $value) {
                                // print_r($val_sisa."-".$value->jml_item_tr_detail);
                                // print_r("<br>");
                                $val_sisa -= $value->jml_item_tr_detail;
                                // print_r($val_sisa);
                                // print_r("<br>");
                            }
                        }


                        // print_r("-------------------------------");
                        // print_r($id_item);
                        // print_r("-------------------------------");
                        // print_r("<br>");


                    $array_data_main[$id_item]["stok_now"] = $val_sisa;
                }
            }
            
            $data["array_data_main"] = $array_data_main;
        }


        
        // print_r($data);
        // $this->load->view('index', $data);
        return $data;
    }

    public function main_get_penjualan_item_th($th_start = "0", $th_finish = "0"){
        $data["page"] = "report_penjualan_item";
        $data["str_periode"] = "";
        $data["item"] = $this->mm->get_data_all_where("item", array("is_del_item"=>"0"));

        $data["list_data"] = array();
        if($th_start != "0" && $th_finish != "0"){
            $data["str_periode"] = "Periode ".$th_start." - ". $th_finish;

            $check_data = $this->rp->get_first_time([
                                                        "YEAR(periode_record_item) >="=>$th_start,
                                                        "YEAR(periode_record_item) <="=>$th_finish
                                                        ]
                                                    );

            $array_data_main = [];
            if($check_data){
                $periode_fisrt = $check_data[0]->periode_record_item;

                $where_in = array(
                        "ri.periode_record_item"=>$periode_fisrt
                    );

                // $check_data = $this->mm->get_data_all_where("record_item", $where_in);
                $check_data = $this->oi->get_active_record_item($where_in);

                foreach ($check_data as $key_c => $value_c) {
                    $id_item = $value_c->id_item;
                    $array_data_main[$id_item]["header"] = $value_c;
                    $array_data_main[$id_item]["item"] = $this->mm->get_data_each("item", ["id_item"=>$id_item]);


                    $data["list_data"] = $this->rp->get_penjualan_item_th($th_start, $th_finish, array("td.id_item"=>$id_item));

                    $data["list_data_pembelian"] = $this->rp->get_pembelian_item_th($th_start, $th_finish, array("td.id_item"=>$id_item));

                    $where_main = [
                                "YEAR(th.tgl_transaksi_tr_header)>="=>$th_start,
                                "YEAR(th.tgl_transaksi_tr_header)<="=>$th_finish,
                                "td.id_item"=>$id_item
                            ];

                    $data["list_data_retur_penjualan"] = $this->rp->get_retur_penjualan_detail($where_main);
                    $data["list_data_retur_penjualan_p"] = $this->rp->get_retur_penjualan_detail_p($where_main);

                    $data["list_data_retur_pembalian"] = $this->rp->get_retur_pembelian_detail($where_main);
                    $data["list_data_retur_pembelian_p"] = $this->rp->get_retur_pembelian_detail_p($where_main);

                    $data["list_data_sample"] = $this->rp->get_sample_detail($where_main);
                    $data["list_data_retur_sample_p"] = $this->rp->get_retur_sample_detail_p($where_main);

                    $data["check_data"] = $check_data;

                        $first_val = 0;
                        $val_sisa = 0;
                        $no = 1;

                        if(isset($data["check_data"])){
                            $item_fine = $value_c->stok_record_item;
                            $item_broken = $value_c->rusak_record_item;

                            $first_val = $item_fine+$item_broken;
                        }

                        $val_sisa +=$first_val;


                    // Pembelian
                        if($data["list_data_pembelian"] && isset($data["check_data"])){
                            foreach ($data["list_data_pembelian"] as $key => $value) {
                                $val_sisa += $value->jml_item_tr_detail;
                            }
                        }


                    // Retur Penjualan
                        if(isset($data["list_data_retur_penjualan"]) && isset($data["check_data"])){
                            foreach ($data["list_data_retur_penjualan"] as $key => $value) {
                                $val_sisa += $value->jml_item_tr_detail;
                            }
                        }


                    // Retur Pengganti Retur Pembelian
                        if(isset($data["list_data_retur_pembelian_p"]) && isset($data["check_data"])){
                            foreach ($data["list_data_retur_pembelian_p"] as $key => $value) {
                                $val_sisa += $value->jml_item_tr_detail;
                            }
                        }


                    // Retur Pengganti Retur Sample
                        if(isset($data["list_data_retur_sample_p"]) && isset($data["check_data"])){
                            foreach ($data["list_data_retur_sample_p"] as $key => $value) {
                                $val_sisa += $value->jml_item_tr_detail;
                            }
                        }




                    // Penjualan
                        if(isset($data["list_data"]) && isset($data["check_data"])){
                            foreach ($data["list_data"] as $key => $value) {
                                $val_sisa -= $value->jml_item_tr_detail;
                            }
                        }


                    // Pengganti Retur Penjualan
                        if(isset($data["list_data_retur_penjualan_p"]) && isset($data["check_data"])){
                            foreach ($data["list_data_retur_penjualan_p"] as $key => $value) {
                                $val_sisa -= $value->jml_item_tr_detail;
                            }
                        }


                    // Retur Pembelian
                        if(isset($data["list_data_retur_pembalian"]) && isset($data["check_data"])){
                            foreach ($data["list_data_retur_pembalian"] as $key => $value) {
                                $val_sisa -= $value->jml_item_tr_detail;
                            }
                        }


                    // Pengganti Sample
                        if(isset($data["list_data_sample"]) && isset($data["check_data"])){
                            foreach ($data["list_data_sample"] as $key => $value) {
                                $val_sisa -= $value->jml_item_tr_detail;
                            }
                        }

                    $array_data_main[$id_item]["stok_now"] = $val_sisa;
                }
            }

            $data["array_data_main"] = $array_data_main;
        }

        // print_r("<pre>");
        // print_r($array_data_main);
        // print_r($data);
        // $this->load->view('index', $data);
        return $data;
    }

    public function main_get_penjualan_item_bulan($bulan = "0", $th = "0"){
        $data["page"] = "report_penjualan_item";
        $data["str_periode"] = "";
        $data["item"] = $this->mm->get_data_all_where("item", array("is_del_item"=>"0"));

        $data["list_data"] = array();
        if($bulan != "0" && $th != "0"){
            $data["str_periode"] = "Periode ".$this->array_of_month[(int)$bulan]." ". $th;

            $check_data = $this->rp->get_first_time([
                                                        "MONTH(periode_record_item) ="=>$bulan,
                                                        "YEAR(periode_record_item) ="=>$th
                                                    ]);

            $array_data_main = [];
            if($check_data){
                $periode_fisrt = $check_data[0]->periode_record_item;

                $where_in = array(
                        "ri.periode_record_item"=>$periode_fisrt
                    );

                // $check_data = $this->mm->get_data_all_where("record_item", $where_in);
                $check_data = $this->oi->get_active_record_item($where_in);

                foreach ($check_data as $key_c => $value_c) {
                    $id_item = $value_c->id_item;
                    $array_data_main[$id_item]["header"] = $value_c;
                    $array_data_main[$id_item]["item"] = $this->mm->get_data_each("item", ["id_item"=>$id_item]);


                    $data["list_data"] = $this->rp->get_penjualan_item_bulan($bulan, $th, array("td.id_item"=>$id_item));

                    $data["list_data_pembelian"] = $this->rp->get_pembelian_item_bulan($bulan, $th, array("td.id_item"=>$id_item));

                    $where_main = [
                                "MONTH(th.tgl_transaksi_tr_header)="=>$bulan,
                                "YEAR(th.tgl_transaksi_tr_header)="=>$th,
                                "td.id_item"=>$id_item
                            ];

                    $data["list_data_retur_penjualan"] = $this->rp->get_retur_penjualan_detail($where_main);
                    $data["list_data_retur_penjualan_p"] = $this->rp->get_retur_penjualan_detail_p($where_main);

                    $data["list_data_retur_pembalian"] = $this->rp->get_retur_pembelian_detail($where_main);
                    $data["list_data_retur_pembelian_p"] = $this->rp->get_retur_pembelian_detail_p($where_main);

                    $data["list_data_sample"] = $this->rp->get_sample_detail($where_main);
                    $data["list_data_retur_sample_p"] = $this->rp->get_retur_sample_detail_p($where_main);

                    $data["check_data"] = $check_data;

                        $first_val = 0;
                        $val_sisa = 0;
                        $no = 1;

                        if(isset($data["check_data"])){
                            $item_fine = $value_c->stok_record_item;
                            $item_broken = $value_c->rusak_record_item;

                            $first_val = $item_fine+$item_broken;
                        }

                        $val_sisa +=$first_val;


                    // Pembelian
                        if($data["list_data_pembelian"] && isset($data["check_data"])){
                            foreach ($data["list_data_pembelian"] as $key => $value) {
                                $val_sisa += $value->jml_item_tr_detail;
                            }
                        }


                    // Retur Penjualan
                        if(isset($data["list_data_retur_penjualan"]) && isset($data["check_data"])){
                            foreach ($data["list_data_retur_penjualan"] as $key => $value) {
                                $val_sisa += $value->jml_item_tr_detail;
                            }
                        }


                    // Retur Pengganti Retur Pembelian
                        if(isset($data["list_data_retur_pembelian_p"]) && isset($data["check_data"])){
                            foreach ($data["list_data_retur_pembelian_p"] as $key => $value) {
                                $val_sisa += $value->jml_item_tr_detail;
                            }
                        }


                    // Retur Pengganti Retur Sample
                        if(isset($data["list_data_retur_sample_p"]) && isset($data["check_data"])){
                            foreach ($data["list_data_retur_sample_p"] as $key => $value) {
                                $val_sisa += $value->jml_item_tr_detail;
                            }
                        }




                    // Penjualan
                        if(isset($data["list_data"]) && isset($data["check_data"])){
                            foreach ($data["list_data"] as $key => $value) {
                                $val_sisa -= $value->jml_item_tr_detail;
                            }
                        }


                    // Pengganti Retur Penjualan
                        if(isset($data["list_data_retur_penjualan_p"]) && isset($data["check_data"])){
                            foreach ($data["list_data_retur_penjualan_p"] as $key => $value) {
                                $val_sisa -= $value->jml_item_tr_detail;
                            }
                        }


                    // Retur Pembelian
                        if(isset($data["list_data_retur_pembalian"]) && isset($data["check_data"])){
                            foreach ($data["list_data_retur_pembalian"] as $key => $value) {
                                $val_sisa -= $value->jml_item_tr_detail;
                            }
                        }


                    // Pengganti Sample
                        if(isset($data["list_data_sample"]) && isset($data["check_data"])){
                            foreach ($data["list_data_sample"] as $key => $value) {
                                $val_sisa -= $value->jml_item_tr_detail;
                            }
                        }

                    $array_data_main[$id_item]["stok_now"] = $val_sisa;
                }
            }

            $data["array_data_main"] = $array_data_main;
        }

        // print_r($data);
        // $this->load->view('index', $data);
        return $data;
    }
#------------------------------main---------------------------------#


#------------------------------print---------------------------------#
    public function print_get_penjualan_item_tgl($tgl_start = "0", $tgl_finish = "0", $id_item = "0"){
        $data = array();
        if($tgl_start != "0" && $tgl_finish != "0"){
            $data = $this->main_get_penjualan_item_tgl($tgl_start, $tgl_finish, $id_item);
        }
        
        // print_r($data);
        $this->load->view('print/print_penjualan_item_all', $data);
    }

    public function print_get_penjualan_item_triwulan($triwulan = "0", $th_triwulan = "0", $id_item = "0"){
        $data = array();
        if($triwulan != "0" && $th_triwulan != "0"){
            $data = $this->main_get_penjualan_item_triwulan($triwulan, $th_triwulan, $id_item);
        }

        // print_r($data);
        $this->load->view('print/print_penjualan_item_all', $data);
    }

    public function print_get_penjualan_item_th($th_start = "0", $th_finish = "0"){
        $data = array();
        if($th_start != "0" && $th_finish != "0"){
            $data = $this->main_get_penjualan_item_th($th_start, $th_finish);
        }

        // print_r($data);
        $this->load->view('print/print_penjualan_item_all', $data);
    }

    public function print_get_penjualan_item_bulan($bulan = "0", $th = "0", $id_item = "0"){
        $data = array();
        if($bulan != "0" && $th != "0"){
            $data = $this->main_get_penjualan_item_bulan($bulan, $th, $id_item);
        }

        // print_r($data);
        $this->load->view('print/print_penjualan_item_all', $data);
    }
#------------------------------print---------------------------------#


#------------------------------excel---------------------------------#
    public function excel_get_penjualan_item_tgl($tgl_start = "0", $tgl_finish = "0", $id_item = "0"){
        $data = array();
        if($tgl_start != "0" && $tgl_finish != "0"){
            $data = $this->main_get_penjualan_item_tgl($tgl_start, $tgl_finish, $id_item);
            $this->convert_excel($data);
        }
        
        // excel_r($data);
        // $this->load->view('excel/excel_penjualan_item', $data);
    }

    public function excel_get_penjualan_item_triwulan($triwulan = "0", $th_triwulan = "0", $id_item = "0"){
        $data = array();
        if($triwulan != "0" && $th_triwulan != "0"){
            $data = $this->main_get_penjualan_item_triwulan($triwulan, $th_triwulan, $id_item);
            $this->convert_excel($data);
        }

        // excel_r($data);
        // $this->load->view('excel/excel_penjualan_item', $data);
    }

    public function excel_get_penjualan_item_th($th_start = "0", $th_finish = "0", $id_item = "0"){
        $data = array();
        if($th_start != "0" && $th_finish != "0"){
            $data = $this->main_get_penjualan_item_th($th_start, $th_finish, $id_item);
            $this->convert_excel($data);
        }

        // excel_r($data);
        // $this->load->view('excel/excel_penjualan_item', $data);
    }

    public function excel_get_penjualan_item_bulan($bulan = "0", $th = "0", $id_item = "0"){
        $data = array();
        if($bulan != "0" && $th != "0"){
            $data = $this->main_get_penjualan_item_bulan($bulan, $th, $id_item);
            $this->convert_excel($data);
        }

        // excel_r($data);
        // $this->load->view('excel/excel_penjualan_item', $data);
    }
#------------------------------excel---------------------------------#




    public function convert_excel($data){
        /** Error reporting */
        error_reporting(E_ALL);
        require_once APPPATH.'third_party/PHPExcel.php';

        $objPHPExcel = new PHPExcel();


       

        // Set document properties
        $objPHPExcel->getProperties()->setCreator("Filosofi_code")
                                     ->setLastModifiedBy("Filosofi_code Application")
                                     ->setTitle("Laporan Transaksi Penjualan")
                                     ->setSubject("Office 2007 XLSX Laporan Transaksi Penjualan")
                                     ->setDescription("Laporan Transaksi Penjualan for Office 2007 XLSX")
                                     ->setKeywords("office 2007 openxml php")
                                     ->setCategory("Laporan Transaksi Penjualan");


        // Add some data
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'LAPORAN STOK PRODUK')
                    ->setCellValue('A2', $data["str_periode"])
                    ->setCellValue('A3', '')
                    ->setCellValue('A4', '')
                    ->setCellValue('A5', '');

        // Pembelian

            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A8', 'No')
                        ->setCellValue('B8', 'Kode Produk')
                        ->setCellValue('C8', 'Nama Produk')
                        ->setCellValue('D8', 'Satuan')
                        ->setCellValue('E8', 'Kode Produksi')
                        ->setCellValue('F8', 'Harga Satuan')
                        ->setCellValue('G8', 'Total Harga')
                        ->setCellValue('H8', 'Stok Produk');

            $no_row = 9;
            $no = 1;

            if(isset($data["array_data_main"])){
                $no = 1;
                $t_harga_all = 0;
                foreach ($data["array_data_main"] as $key => $value) {
                    $val_sisa = $value["stok_now"];
                    $val_harga_satuan = $value["item"]["harga_bruto"];

                    $val_t_harga = (float)$val_sisa * (float)$val_harga_satuan;

                    $t_harga_all += $val_t_harga;

                    $str_harga_satuan = number_format($val_harga_satuan, 2, ",", ".");
                    $str_t_harga = number_format($val_t_harga, 2, ",", ".");

                    $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('A'.$no_row, $no)
                            ->setCellValue('B'.$no_row, $value["item"]["id_item"])
                            ->setCellValue('C'.$no_row, $value["item"]["nama_item"])
                            ->setCellValue('D'.$no_row, $value["item"]["satuan"])
                            ->setCellValue('E'.$no_row, $value["item"]["kode_produksi_item"])
                            ->setCellValue('F'.$no_row, $str_harga_satuan)
                            ->setCellValue('G'.$no_row, $str_t_harga)
                            ->setCellValue('H'.$no_row, $val_sisa);

                    
                    $no_row++;
                    $no++;
                }

                $str_t_harga_all = number_format($t_harga_all, 2, ",", ".");

                $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('A'.$no_row, "Total")
                            ->setCellValue('G'.$no_row, $str_t_harga_all)
                            ->setCellValue('H'.$no_row, "-");
            }
        // Pembelian


        // Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle('Laporan_stok_produk');


        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);


        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel); 
        $objWriter->save('./Laporan_stok_produk.xlsx'); 

        $this->load->helper('download');
        force_download('./Laporan_stok_produk.xlsx', NULL);
    }
}
