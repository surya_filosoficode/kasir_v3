<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reportpenjualanitem extends CI_Controller {

    public $keterangan_record_stok = "panjualan detail";
    public $array_of_month = ["", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];

    public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');
        $this->load->model('report/report_penjualan_item', 'rp');
        $this->load->model('report/report_retur_penjualan', 'rrp');

        $this->load->library("response_message");
        $this->load->library("Auth_v0");
        
        date_default_timezone_set("Asia/Bangkok");
        // $this->auth_v0->check_session_active_ad();
    }

    public function index(){
        $data["page"] = "report_penjualan_item";
        $data["str_periode"] = "";
        // $data["list_data"] = array();
        $data["item"] = $this->mm->get_data_all_where("item", array("is_del_item"=>"0"));
        $this->load->view('index', $data);
    }

#------------------------------show---------------------------------#


    public function get_penjualan_item_tgl($tgl_start = "0", $tgl_finish = "0", $id_item = "0"){
        $data["page"] = "report_penjualan_item";
        $data["str_periode"] = "";
        $data["item"] = $this->mm->get_data_all_where("item", array("is_del_item"=>"0"));

        $data["list_data"] = array();
        if($tgl_start != "0" && $tgl_finish != "0"){
            $array_start = explode("-", $tgl_start);
            $m_start = $this->array_of_month[(int)$array_start[1]];

            $array_finish = explode("-", $tgl_finish);
            $m_finish = $this->array_of_month[(int)$array_finish[1]];

            $data["str_periode"] = "Periode ".$array_start[2]." ".$m_start." ".$array_start[0]." - "
            .$array_finish[2]." ".$m_finish." ".$array_finish[0];

            $check_data = $this->mm->get_data_all_where("record_item", array(
                                                                        "periode_record_item >="=>$tgl_start,
                                                                        "periode_record_item <="=>$tgl_finish,
                                                                        "id_item"=>$id_item
                                                                    ));

            if($check_data){
                $periode_fisrt = $check_data[0]->periode_record_item;

                $where_in = array(
                        "periode_record_item"=>$periode_fisrt,
                        "id_item"=>$id_item
                    );

                $check_data = $this->mm->get_data_all_where("record_item", $where_in);
                
                $data["list_data"] = $this->rp->get_penjualan_item_tgl($tgl_start, $tgl_finish, array("td.id_item"=>$id_item));

                $data["list_data_pembelian"] = $this->rp->get_pembelian_item_tgl($tgl_start, $tgl_finish, array("td.id_item"=>$id_item));

                $where_main = [
                            "th.tgl_transaksi_tr_header >="=>$tgl_start,
                            "th.tgl_transaksi_tr_header <="=>$tgl_finish,
                            "td.id_item"=>$id_item
                        ];

                $data["list_data_retur_penjualan"] = $this->rp->get_retur_penjualan_detail($where_main);
                $data["list_data_retur_penjualan_p"] = $this->rp->get_retur_penjualan_detail_p($where_main);

                $data["list_data_retur_pembalian"] = $this->rp->get_retur_pembelian_detail($where_main);
                $data["list_data_retur_pembelian_p"] = $this->rp->get_retur_pembelian_detail_p($where_main);

                $data["list_data_sample"] = $this->rp->get_sample_detail($where_main);
                $data["list_data_retur_sample_p"] = $this->rp->get_retur_sample_detail_p($where_main);

                $data["check_data"] = $check_data;
            }

        }
        
        // print_r($data["list_data_retur_sample_p"]);
        // print_r($check_data);
        $this->load->view('index', $data);
    }

    public function get_penjualan_item_triwulan($triwulan = "0", $th_triwulan = "0", $id_item = "0"){
        $data["page"] = "report_penjualan_item";
        $data["str_periode"] = "";
        $data["item"] = $this->mm->get_data_all_where("item", array("is_del_item"=>"0"));

        $data["list_data"] = array();
        if($triwulan != "0" && $th_triwulan != "0"){
            $array_periode = explode("-", $triwulan);
            $array_where_in = array();
            for ($i=$array_periode[0]; $i <= $array_periode[1]; $i++) { 
                array_push($array_where_in, $i);
            }

            $array_triwulan = explode("-", $triwulan);

            $data["str_periode"] = "Periode ".$this->array_of_month[(int)$array_triwulan[0]]." - ".$this->array_of_month[(int)$array_triwulan[1]]." ". $th_triwulan;

            $check_data = $this->rp->get_record_item(array("id_item"=>$id_item),$array_where_in);

            if($check_data){
                $periode_fisrt = $check_data[0]->periode_record_item;
            
                $where_in = array(
                        "periode_record_item"=>$periode_fisrt,
                        "id_item"=>$id_item
                    );

                $check_data = $this->mm->get_data_all_where("record_item", $where_in);

                $data["list_data"] = $this->rp->get_penjualan_item_triwulan($th_triwulan, $array_where_in, array("td.id_item"=>$id_item));

                $data["list_data_pembelian"] = $this->rp->get_pembelian_item_triwulan($th_triwulan, $array_where_in, array("td.id_item"=>$id_item));

                $where_main = ["td.id_item"=>$id_item,
                                "YEAR(th.tgl_transaksi_tr_header)="=>$th_triwulan];

                $data["list_data_retur_penjualan"] = $this->rp->get_retur_penjualan_detail_triwulan($array_where_in, $where_main);
                $data["list_data_retur_penjualan_p"] = $this->rp->get_retur_penjualan_detail_p_triwulan($array_where_in, $where_main);

                $data["list_data_retur_pembalian"] = $this->rp->get_retur_pembelian_detail_triwulan($array_where_in, $where_main);
                $data["list_data_retur_pembelian_p"] = $this->rp->get_retur_pembelian_detail_p_triwulan($array_where_in, $where_main);

                $data["list_data_sample"] = $this->rp->get_sample_detail_triwulan($array_where_in, $where_main);
                $data["list_data_retur_sample_p"] = $this->rp->get_retur_sample_detail_p_triwulan($array_where_in, $where_main);

                $data["check_data"] = $check_data;
            }
        }
        
        // print_r($data["list_data_retur_penjualan"]);
        $this->load->view('index', $data);
    }

    public function get_penjualan_item_th($th_start = "0", $th_finish = "0", $id_item = "0"){
        $data["page"] = "report_penjualan_item";
        $data["str_periode"] = "";
        $data["item"] = $this->mm->get_data_all_where("item", array("is_del_item"=>"0"));

        $data["list_data"] = array();
        if($th_start != "0" && $th_finish != "0"){
            // $data["list_data"] = $this->rp->get_penjualan_item_th($th_start, $th_finish, array("td.id_item"=>$id_item));
            $data["str_periode"] = "Periode ".$th_start." - ". $th_finish;

            $check_data = $this->rp->get_first_time([
                                                        "YEAR(periode_record_item) >="=>$th_start,
                                                        "YEAR(periode_record_item) <="=>$th_finish,
                                                        "id_item"=>$id_item
                                                        ]
                                                    );
            if($check_data){
                $periode_fisrt = $check_data[0]->periode_record_item;
            
                $where_in = array(
                        "periode_record_item"=>$periode_fisrt,
                        "id_item"=>$id_item
                    );

                $check_data = $this->mm->get_data_all_where("record_item", $where_in);

                $data["list_data"] = $this->rp->get_penjualan_item_th($th_start, $th_finish, array("td.id_item"=>$id_item));

                $data["list_data_pembelian"] = $this->rp->get_pembelian_item_th($th_start, $th_finish, array("td.id_item"=>$id_item));

                $where_main = [
                            "YEAR(th.tgl_transaksi_tr_header)>="=>$th_start,
                            "YEAR(th.tgl_transaksi_tr_header)<="=>$th_finish,
                            "td.id_item"=>$id_item
                        ];

                $data["list_data_retur_penjualan"] = $this->rp->get_retur_penjualan_detail($where_main);
                $data["list_data_retur_penjualan_p"] = $this->rp->get_retur_penjualan_detail_p($where_main);

                $data["list_data_retur_pembalian"] = $this->rp->get_retur_pembelian_detail($where_main);
                $data["list_data_retur_pembelian_p"] = $this->rp->get_retur_pembelian_detail_p($where_main);

                $data["list_data_sample"] = $this->rp->get_sample_detail($where_main);
                $data["list_data_retur_sample_p"] = $this->rp->get_retur_sample_detail_p($where_main);

                $data["check_data"] = $check_data;
            }
        }

        // print_r($data);
        $this->load->view('index', $data);
    }

    public function get_penjualan_item_bulan($bulan = "0", $th = "0", $id_item = "0"){
        $data["page"] = "report_penjualan_item";
        $data["str_periode"] = "";
        $data["item"] = $this->mm->get_data_all_where("item", array("is_del_item"=>"0"));

        $data["list_data"] = array();
        if($bulan != "0" && $th != "0"){
            // $data["list_data"] = $this->rp->get_penjualan_item_bulan($bulan, $th, array("td.id_item"=>$id_item));
            $data["str_periode"] = "Periode ".$this->array_of_month[(int)$bulan]." ". $th;

            $check_data = $this->rp->get_first_time([
                                                        "MONTH(periode_record_item) ="=>$bulan,
                                                        "YEAR(periode_record_item) ="=>$th,
                                                        "id_item"=>$id_item
                                                        ]
                                                    );
            if($check_data){
                $periode_fisrt = $check_data[0]->periode_record_item;
            
                $where_in = array(
                        "periode_record_item"=>$periode_fisrt,
                        "id_item"=>$id_item
                    );

                $check_data = $this->mm->get_data_all_where("record_item", $where_in);

                $data["list_data"] = $this->rp->get_penjualan_item_bulan($bulan, $th, array("td.id_item"=>$id_item));

                $data["list_data_pembelian"] = $this->rp->get_pembelian_item_bulan($bulan, $th, array("td.id_item"=>$id_item));

                $where_main = [
                            "MONTH(th.tgl_transaksi_tr_header)="=>$bulan,
                            "YEAR(th.tgl_transaksi_tr_header)="=>$th,
                            "td.id_item"=>$id_item
                        ];

                $data["list_data_retur_penjualan"] = $this->rp->get_retur_penjualan_detail($where_main);
                $data["list_data_retur_penjualan_p"] = $this->rp->get_retur_penjualan_detail_p($where_main);

                $data["list_data_retur_pembalian"] = $this->rp->get_retur_pembelian_detail($where_main);
                $data["list_data_retur_pembelian_p"] = $this->rp->get_retur_pembelian_detail_p($where_main);

                $data["list_data_sample"] = $this->rp->get_sample_detail($where_main);
                $data["list_data_retur_sample_p"] = $this->rp->get_retur_sample_detail_p($where_main);

                $data["check_data"] = $check_data;
            }
        }

        // print_r("<pre>");
        // print_r($data["check_data"]);
        $this->load->view('index', $data);
    }


#------------------------------show---------------------------------#


#------------------------------main---------------------------------#
    public function main_get_penjualan_item_tgl($tgl_start = "0", $tgl_finish = "0", $id_item = "0"){
        $data["page"] = "report_penjualan_item";
        $data["str_periode"] = "";
        $data["item"] = $this->mm->get_data_all_where("item", array("is_del_item"=>"0"));

        $data["list_data"] = array();
        if($tgl_start != "0" && $tgl_finish != "0"){
            $array_start = explode("-", $tgl_start);
            $m_start = $this->array_of_month[(int)$array_start[1]];

            $array_finish = explode("-", $tgl_finish);
            $m_finish = $this->array_of_month[(int)$array_finish[1]];

            $data["str_periode"] = "Periode ".$array_start[2]." ".$m_start." ".$array_start[0]." - "
            .$array_finish[2]." ".$m_finish." ".$array_finish[0];


            $check_data = $this->mm->get_data_all_where("record_item", array(
                                                                        "periode_record_item >="=>$tgl_start,
                                                                        "periode_record_item <="=>$tgl_finish,
                                                                        "id_item"=>$id_item
                                                                    ));

            if($check_data){
                $periode_fisrt = $check_data[0]->periode_record_item;

                $where_in = array(
                        "periode_record_item"=>$periode_fisrt,
                        "id_item"=>$id_item
                    );

                $check_data = $this->mm->get_data_all_where("record_item", $where_in);
                
                $data["list_data"] = $this->rp->get_penjualan_item_tgl($tgl_start, $tgl_finish, array("td.id_item"=>$id_item));

                $data["list_data_pembelian"] = $this->rp->get_pembelian_item_tgl($tgl_start, $tgl_finish, array("td.id_item"=>$id_item));

                $where_main = [
                            "th.tgl_transaksi_tr_header >="=>$tgl_start,
                            "th.tgl_transaksi_tr_header <="=>$tgl_finish,
                            "td.id_item"=>$id_item
                        ];

                $data["list_data_retur_penjualan"] = $this->rp->get_retur_penjualan_detail($where_main);
                $data["list_data_retur_penjualan_p"] = $this->rp->get_retur_penjualan_detail_p($where_main);

                $data["list_data_retur_pembalian"] = $this->rp->get_retur_pembelian_detail($where_main);
                $data["list_data_retur_pembelian_p"] = $this->rp->get_retur_pembelian_detail_p($where_main);

                $data["list_data_sample"] = $this->rp->get_sample_detail($where_main);
                $data["list_data_retur_sample_p"] = $this->rp->get_retur_sample_detail_p($where_main);

                $data["check_data"] = $check_data;
            }
        }
        
        // print_r($data);
        // $this->load->view('index', $data);
        return $data;
    }

    public function main_get_penjualan_item_triwulan($triwulan = "0", $th_triwulan = "0", $id_item = "0"){
        $data["page"] = "report_penjualan_item";
        $data["str_periode"] = "";
        $data["item"] = $this->mm->get_data_all_where("item", array("is_del_item"=>"0"));

        $data["list_data"] = array();
        if($triwulan != "0" && $th_triwulan != "0"){
            $array_periode = explode("-", $triwulan);
            $array_where_in = array();
            for ($i=$array_periode[0]; $i <= $array_periode[1]; $i++) { 
                array_push($array_where_in, $i);
            }

            $array_triwulan = explode("-", $triwulan);

            $data["str_periode"] = "Periode ".$this->array_of_month[(int)$array_triwulan[0]]." - ".$this->array_of_month[(int)$array_triwulan[1]]." ". $th_triwulan;

            $check_data = $this->rp->get_record_item(array("id_item"=>$id_item),$array_where_in);

            if($check_data){
                $periode_fisrt = $check_data[0]->periode_record_item;
            
                $where_in = array(
                        "periode_record_item"=>$periode_fisrt,
                        "id_item"=>$id_item
                    );

                $check_data = $this->mm->get_data_all_where("record_item", $where_in);

                $data["list_data"] = $this->rp->get_penjualan_item_triwulan($th_triwulan, $array_where_in, array("td.id_item"=>$id_item));

                $data["list_data_pembelian"] = $this->rp->get_pembelian_item_triwulan($th_triwulan, $array_where_in, array("td.id_item"=>$id_item));

                $where_main = ["td.id_item"=>$id_item,
                                "YEAR(th.tgl_transaksi_tr_header)="=>$th_triwulan];

                $data["list_data_retur_penjualan"] = $this->rp->get_retur_penjualan_detail_triwulan($array_where_in, $where_main);
                $data["list_data_retur_penjualan_p"] = $this->rp->get_retur_penjualan_detail_p_triwulan($array_where_in, $where_main);

                $data["list_data_retur_pembalian"] = $this->rp->get_retur_pembelian_detail_triwulan($array_where_in, $where_main);
                $data["list_data_retur_pembelian_p"] = $this->rp->get_retur_pembelian_detail_p_triwulan($array_where_in, $where_main);

                $data["list_data_sample"] = $this->rp->get_sample_detail_triwulan($array_where_in, $where_main);
                $data["list_data_retur_sample_p"] = $this->rp->get_retur_sample_detail_p_triwulan($array_where_in, $where_main);

                $data["check_data"] = $check_data;
            }
        }


        
        // print_r($data);
        // $this->load->view('index', $data);
        return $data;
    }

    public function main_get_penjualan_item_th($th_start = "0", $th_finish = "0", $id_item = "0"){
        $data["page"] = "report_penjualan_item";
        $data["str_periode"] = "";
        $data["item"] = $this->mm->get_data_all_where("item", array("is_del_item"=>"0"));

        $data["list_data"] = array();
        if($th_start != "0" && $th_finish != "0"){
            $data["str_periode"] = "Periode ".$th_start." - ". $th_finish;

            $check_data = $this->rp->get_first_time([
                                                        "YEAR(periode_record_item) >="=>$th_start,
                                                        "YEAR(periode_record_item) <="=>$th_finish,
                                                        "id_item"=>$id_item
                                                        ]
                                                    );
            if($check_data){
                $periode_fisrt = $check_data[0]->periode_record_item;
            
                $where_in = array(
                        "periode_record_item"=>$periode_fisrt,
                        "id_item"=>$id_item
                    );

                $check_data = $this->mm->get_data_all_where("record_item", $where_in);

                $data["list_data"] = $this->rp->get_penjualan_item_th($th_start, $th_finish, array("td.id_item"=>$id_item));

                $data["list_data_pembelian"] = $this->rp->get_pembelian_item_th($th_start, $th_finish, array("td.id_item"=>$id_item));

                $where_main = [
                            "YEAR(th.tgl_transaksi_tr_header)>="=>$th_start,
                            "YEAR(th.tgl_transaksi_tr_header)<="=>$th_finish,
                            "td.id_item"=>$id_item
                        ];

                $data["list_data_retur_penjualan"] = $this->rp->get_retur_penjualan_detail($where_main);
                $data["list_data_retur_penjualan_p"] = $this->rp->get_retur_penjualan_detail_p($where_main);

                $data["list_data_retur_pembalian"] = $this->rp->get_retur_pembelian_detail($where_main);
                $data["list_data_retur_pembelian_p"] = $this->rp->get_retur_pembelian_detail_p($where_main);

                $data["list_data_sample"] = $this->rp->get_sample_detail($where_main);
                $data["list_data_retur_sample_p"] = $this->rp->get_retur_sample_detail_p($where_main);

                $data["check_data"] = $check_data;
            }
        }

        // print_r($data);
        // $this->load->view('index', $data);
        return $data;
    }

    public function main_get_penjualan_item_bulan($bulan = "0", $th = "0", $id_item = "0"){
        $data["page"] = "report_penjualan_item";
        $data["str_periode"] = "";
        $data["item"] = $this->mm->get_data_all_where("item", array("is_del_item"=>"0"));

        $data["list_data"] = array();
        if($bulan != "0" && $th != "0"){
            $data["str_periode"] = "Periode ".$this->array_of_month[(int)$bulan]." ". $th;

            $check_data = $this->rp->get_first_time([
                                                        "MONTH(periode_record_item) ="=>$bulan,
                                                        "YEAR(periode_record_item) ="=>$th,
                                                        "id_item"=>$id_item
                                                        ]
                                                    );
            if($check_data){
                $periode_fisrt = $check_data[0]->periode_record_item;
            
                $where_in = array(
                        "periode_record_item"=>$periode_fisrt,
                        "id_item"=>$id_item
                    );

                $check_data = $this->mm->get_data_all_where("record_item", $where_in);

                $data["list_data"] = $this->rp->get_penjualan_item_bulan($bulan, $th, array("td.id_item"=>$id_item));

                $data["list_data_pembelian"] = $this->rp->get_pembelian_item_bulan($bulan, $th, array("td.id_item"=>$id_item));

                $where_main = [
                            "MONTH(th.tgl_transaksi_tr_header)="=>$bulan,
                            "YEAR(th.tgl_transaksi_tr_header)="=>$th,
                            "td.id_item"=>$id_item
                        ];

                $data["list_data_retur_penjualan"] = $this->rp->get_retur_penjualan_detail($where_main);
                $data["list_data_retur_penjualan_p"] = $this->rp->get_retur_penjualan_detail_p($where_main);

                $data["list_data_retur_pembalian"] = $this->rp->get_retur_pembelian_detail($where_main);
                $data["list_data_retur_pembelian_p"] = $this->rp->get_retur_pembelian_detail_p($where_main);

                $data["list_data_sample"] = $this->rp->get_sample_detail($where_main);
                $data["list_data_retur_sample_p"] = $this->rp->get_retur_sample_detail_p($where_main);

                $data["check_data"] = $check_data;
            }
        }

        // print_r($data);
        // $this->load->view('index', $data);
        return $data;
    }
#------------------------------main---------------------------------#


#------------------------------print---------------------------------#
    public function print_get_penjualan_item_tgl($tgl_start = "0", $tgl_finish = "0", $id_item = "0"){
        $data = array();
        if($tgl_start != "0" && $tgl_finish != "0"){
            $data = $this->main_get_penjualan_item_tgl($tgl_start, $tgl_finish, $id_item);
        }
        
        // print_r($data);
        $this->load->view('print/print_penjualan_item', $data);
    }

    public function print_get_penjualan_item_triwulan($triwulan = "0", $th_triwulan = "0", $id_item = "0"){
        $data = array();
        if($triwulan != "0" && $th_triwulan != "0"){
            $data = $this->main_get_penjualan_item_triwulan($triwulan, $th_triwulan, $id_item);
        }

        // print_r($data);
        $this->load->view('print/print_penjualan_item', $data);
    }

    public function print_get_penjualan_item_th($th_start = "0", $th_finish = "0", $id_item = "0"){
        $data = array();
        if($th_start != "0" && $th_finish != "0"){
            $data = $this->main_get_penjualan_item_th($th_start, $th_finish, $id_item);
        }

        // print_r($data);
        $this->load->view('print/print_penjualan_item', $data);
    }

    public function print_get_penjualan_item_bulan($bulan = "0", $th = "0", $id_item = "0"){
        $data = array();
        if($bulan != "0" && $th != "0"){
            $data = $this->main_get_penjualan_item_bulan($bulan, $th, $id_item);
        }

        // print_r($data);
        $this->load->view('print/print_penjualan_item', $data);
    }
#------------------------------print---------------------------------#


#------------------------------excel---------------------------------#
    public function excel_get_penjualan_item_tgl($tgl_start = "0", $tgl_finish = "0", $id_item = "0"){
        $data = array();
        if($tgl_start != "0" && $tgl_finish != "0"){
            $data = $this->main_get_penjualan_item_tgl($tgl_start, $tgl_finish, $id_item);
            $this->convert_excel($data);
        }
        
        // excel_r($data);
        // $this->load->view('excel/excel_penjualan_item', $data);
    }

    public function excel_get_penjualan_item_triwulan($triwulan = "0", $th_triwulan = "0", $id_item = "0"){
        $data = array();
        if($triwulan != "0" && $th_triwulan != "0"){
            $data = $this->main_get_penjualan_item_triwulan($triwulan, $th_triwulan, $id_item);
            $this->convert_excel($data);
        }

        // excel_r($data);
        // $this->load->view('excel/excel_penjualan_item', $data);
    }

    public function excel_get_penjualan_item_th($th_start = "0", $th_finish = "0", $id_item = "0"){
        $data = array();
        if($th_start != "0" && $th_finish != "0"){
            $data = $this->main_get_penjualan_item_th($th_start, $th_finish, $id_item);
            $this->convert_excel($data);
        }

        // excel_r($data);
        // $this->load->view('excel/excel_penjualan_item', $data);
    }

    public function excel_get_penjualan_item_bulan($bulan = "0", $th = "0", $id_item = "0"){
        $data = array();
        if($bulan != "0" && $th != "0"){
            $data = $this->main_get_penjualan_item_bulan($bulan, $th, $id_item);
            $this->convert_excel($data);
        }

        // excel_r($data);
        // $this->load->view('excel/excel_penjualan_item', $data);
    }
#------------------------------excel---------------------------------#




    public function convert_excel($data){
        /** Error reporting */
        error_reporting(E_ALL);
        require_once APPPATH.'third_party/PHPExcel.php';

        $objPHPExcel = new PHPExcel();


       

        // Set document properties
        $objPHPExcel->getProperties()->setCreator("Filosofi_code")
                                     ->setLastModifiedBy("Filosofi_code Application")
                                     ->setTitle("Laporan Transaksi Penjualan")
                                     ->setSubject("Office 2007 XLSX Laporan Transaksi Penjualan")
                                     ->setDescription("Laporan Transaksi Penjualan for Office 2007 XLSX")
                                     ->setKeywords("office 2007 openxml php")
                                     ->setCategory("Laporan Transaksi Penjualan");


        // Add some data
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'LAPORAN TRANSAKSI PENJUALAN PRODUK')
                    ->setCellValue('A2', $data["str_periode"])
                    ->setCellValue('A3', '')
                    ->setCellValue('A4', '')
                    ->setCellValue('A5', '');

        // Pembelian
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A6', 'Daftar Masuk (Pemebelian)')
                        ->setCellValue('A7', '');

            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A8', 'No')
                        ->setCellValue('B8', 'Tanggal')
                        ->setCellValue('C8', 'No Faktur')
                        ->setCellValue('D8', 'Suplier')
                        ->setCellValue('E8', 'Masuk')
                        ->setCellValue('F8', 'RS')
                        ->setCellValue('G8', 'PT')
                        ->setCellValue('H8', 'APT')
                        ->setCellValue('I8', 'Lainnya')
                        ->setCellValue('J8', 'Sisa');

            $nama_suplier = "";
            $first_val = 0;
            
            $val_sisa = 0;

            if(isset($check_data)){
                $item_fine = $check_data[0]->stok_record_item;
                $item_broken = $check_data[0]->rusak_record_item;

                $first_val = $item_fine+$item_broken;
            }

            $val_sisa +=$first_val;

            $no_row = 9;
            $no = 1;

            if(isset($data["list_data_pembelian"]) && isset($data["check_data"])){
                foreach ($data["list_data_pembelian"] as $key => $value) {
                    $val_sisa +=$first_val;

                    $val_rs = "";
                    $val_pt = "";
                    $val_apt = "";
                    $val_lain = "";

                    $nama_suplier = $value->nama_suplier;   

                    $val_sisa += $value->jml_item_tr_detail;
                    // print_r($value);
                    $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('A'.$no_row, $no)
                            ->setCellValue('B'.$no_row, $value->tgl_transaksi_tr_header)
                            ->setCellValue('C'.$no_row, $value->id_tr_header)
                            ->setCellValue('D'.$no_row, $value->nama_suplier)
                            ->setCellValue('E'.$no_row, $value->jml_item_tr_detail)
                            ->setCellValue('F'.$no_row, $val_rs)
                            ->setCellValue('G'.$no_row, $val_pt)
                            ->setCellValue('H'.$no_row, $val_apt)
                            ->setCellValue('I'.$no_row, $val_lain)
                            ->setCellValue('J'.$no_row, $val_sisa);

                    
                    $no_row++;
                    $no++;
                }
            }
        // Pembelian


        // Retur Penjualan
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.($no_row+2), 'Daftar Masuk (Retur Penjualan)')
                        ->setCellValue('A'.($no_row+3), '');

            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.($no_row+4), 'No')
                        ->setCellValue('B'.($no_row+4), 'Tanggal')
                        ->setCellValue('C'.($no_row+4), 'No Faktur')
                        ->setCellValue('D'.($no_row+4), 'Pelanggan')

                        ->setCellValue('E'.($no_row+4), 'Masuk')
                        ->setCellValue('F'.($no_row+4), 'RS')
                        ->setCellValue('G'.($no_row+4), 'PT')
                        ->setCellValue('H'.($no_row+4), 'APT')
                        ->setCellValue('I'.($no_row+4), 'Lainnya')
                        ->setCellValue('J'.($no_row+4), 'Sisa');

            $no_row_next = $no_row+5;
            $no = 1;
            foreach ($data["list_data_retur_penjualan"] as $key => $value) {

                $val_rs = "";
                $val_pt = "";
                $val_apt = "";
                $val_lain = "";

                switch ($value->jenis_rekanan) {
                    case 'rs':
                        $val_rs = $value->jml_item_tr_detail; 
                        break;

                    case 'apotik':
                        $val_apt = $value->jml_item_tr_detail;
                        break;

                    case 'pbf':
                        $val_pt = $value->jml_item_tr_detail;
                        break;
                    
                    default:
                        $val_lain = $value->jml_item_tr_detail;
                        break;
                }

                $val_sisa += $value->jml_item_tr_detail;

                $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.$no_row_next, $no)
                        ->setCellValue('B'.$no_row_next, $value->tgl_transaksi_tr_header)
                        ->setCellValue('C'.$no_row_next, $value->id_tr_header_p)
                        ->setCellValue('D'.$no_row_next, $value->nama_rekanan)
                        ->setCellValue('E'.$no_row_next, "")
                        ->setCellValue('F'.$no_row_next, $val_rs)
                        ->setCellValue('G'.$no_row_next, $val_pt)
                        ->setCellValue('H'.$no_row_next, $val_apt)
                        ->setCellValue('I'.$no_row_next, $val_lain)
                        ->setCellValue('J'.$no_row_next, $val_sisa);
                
                $no_row_next++;
                $no++;
            }
        // Retur Penjualan


        // Retur Pengganti Retur Pembelian
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.($no_row_next+2), 'Daftar Masuk (Pengganti Retur Pembelian)')
                        ->setCellValue('A'.($no_row_next+3), '');

            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.($no_row_next+4), 'No')
                        ->setCellValue('B'.($no_row_next+4), 'Tanggal')
                        ->setCellValue('C'.($no_row_next+4), 'No Faktur')
                        ->setCellValue('D'.($no_row_next+4), 'Suplier')

                        ->setCellValue('E'.($no_row_next+4), 'Masuk')
                        ->setCellValue('F'.($no_row_next+4), 'RS')
                        ->setCellValue('G'.($no_row_next+4), 'PT')
                        ->setCellValue('H'.($no_row_next+4), 'APT')
                        ->setCellValue('I'.($no_row_next+4), 'Lainnya')
                        ->setCellValue('J'.($no_row_next+4), 'Sisa');

            $no_row_next = $no_row_next+5;
            $no = 1;
            foreach ($data["list_data_retur_pembelian_p"] as $key => $value) {

                $val_rs = "";
                $val_pt = "";
                $val_apt = "";
                $val_lain = "";

                $nama_suplier = $value->nama_suplier; 

                $val_sisa += $value->jml_item_tr_detail;

                $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.$no_row_next, $no)
                        ->setCellValue('B'.$no_row_next, $value->tgl_transaksi_tr_header)
                        ->setCellValue('C'.$no_row_next, $value->id_tr_header_p)
                        ->setCellValue('D'.$no_row_next, $nama_suplier)
                        ->setCellValue('E'.$no_row_next, $value->jml_item_tr_detail)
                        ->setCellValue('F'.$no_row_next, $val_rs)
                        ->setCellValue('G'.$no_row_next, $val_pt)
                        ->setCellValue('H'.$no_row_next, $val_apt)
                        ->setCellValue('I'.$no_row_next, $val_lain)
                        ->setCellValue('J'.$no_row_next, $val_sisa);
                
                $no_row_next++;
                $no++;
            }
        // Retur Pengganti Retur Pembelian


        // Retur Pengganti Retur Sample
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.($no_row_next+2), 'Daftar Masuk (Pengganti Sample)')
                        ->setCellValue('A'.($no_row_next+3), '');

            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.($no_row_next+4), 'No')
                        ->setCellValue('B'.($no_row_next+4), 'Tanggal')
                        ->setCellValue('C'.($no_row_next+4), 'No Faktur')
                        ->setCellValue('D'.($no_row_next+4), 'Pelanggan')

                        ->setCellValue('E'.($no_row_next+4), 'Masuk')
                        ->setCellValue('F'.($no_row_next+4), 'RS')
                        ->setCellValue('G'.($no_row_next+4), 'PT')
                        ->setCellValue('H'.($no_row_next+4), 'APT')
                        ->setCellValue('I'.($no_row_next+4), 'Lainnya')
                        ->setCellValue('J'.($no_row_next+4), 'Sisa');

            $no_row_next = $no_row_next+5;
            $no = 1;
            foreach ($data["list_data_retur_sample_p"] as $key => $value) {

                $val_rs = "";
                $val_pt = "";
                $val_apt = "";
                $val_lain = "";

                $val_sisa += $value->jml_item_tr_detail;

                $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.$no_row_next, $no)
                        ->setCellValue('B'.$no_row_next, $value->tgl_transaksi_tr_header)
                        ->setCellValue('C'.$no_row_next, $value->id_tr_header_p)
                        ->setCellValue('D'.$no_row_next, $nama_suplier)
                        ->setCellValue('E'.$no_row_next, $value->jml_item_tr_detail)
                        ->setCellValue('F'.$no_row_next, $val_rs)
                        ->setCellValue('G'.$no_row_next, $val_pt)
                        ->setCellValue('H'.$no_row_next, $val_apt)
                        ->setCellValue('I'.$no_row_next, $val_lain)
                        ->setCellValue('J'.$no_row_next, $val_sisa);
                
                $no_row_next++;
                $no++;
            }
        // Retur Pengganti Retur Sample




        // Penjualan
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.($no_row_next+2), 'Daftar Keluar (Penjualan)')
                        ->setCellValue('A'.($no_row_next+3), '');

            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.($no_row_next+4), 'No')
                        ->setCellValue('B'.($no_row_next+4), 'Tanggal')
                        ->setCellValue('C'.($no_row_next+4), 'No Faktur')
                        ->setCellValue('D'.($no_row_next+4), 'Pelanggan')

                        ->setCellValue('E'.($no_row_next+4), 'Masuk')
                        ->setCellValue('F'.($no_row_next+4), 'RS')
                        ->setCellValue('G'.($no_row_next+4), 'PT')
                        ->setCellValue('H'.($no_row_next+4), 'APT')
                        ->setCellValue('I'.($no_row_next+4), 'Lainnya')
                        ->setCellValue('J'.($no_row_next+4), 'Sisa');

            $no_row_next = $no_row_next+5;
            $no = 1;
            foreach ($data["list_data"] as $key => $value) {

                $val_rs = "";
                $val_pt = "";
                $val_apt = "";
                $val_lain = "";

                switch ($value->jenis_rekanan) {
                    case 'rs':
                        $val_rs = $value->jml_item_tr_detail; 
                        break;

                    case 'apotik':
                        $val_apt = $value->jml_item_tr_detail;
                        break;

                    case 'pbf':
                        $val_pt = $value->jml_item_tr_detail;
                        break;
                    
                    default:
                        $val_lain = $value->jml_item_tr_detail;
                        break;
                }

                $val_sisa -= $value->jml_item_tr_detail;

                $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.$no_row_next, $no)
                        ->setCellValue('B'.$no_row_next, $value->tgl_transaksi_tr_header)
                        ->setCellValue('C'.$no_row_next, $value->id_tr_header)
                        ->setCellValue('D'.$no_row_next, $value->nama_rekanan)
                        ->setCellValue('E'.$no_row_next, "")
                        ->setCellValue('F'.$no_row_next, $val_rs)
                        ->setCellValue('G'.$no_row_next, $val_pt)
                        ->setCellValue('H'.$no_row_next, $val_apt)
                        ->setCellValue('I'.$no_row_next, $val_lain)
                        ->setCellValue('J'.$no_row_next, $val_sisa);
                
                $no_row_next++;
                $no++;
            }
        // Penjualan


        // Pengganti Retur Penjualan
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.($no_row_next+2), 'Daftar Keluar (Pengganti Retur Penjualan)')
                        ->setCellValue('A'.($no_row_next+3), '');

            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.($no_row_next+4), 'No')
                        ->setCellValue('B'.($no_row_next+4), 'Tanggal')
                        ->setCellValue('C'.($no_row_next+4), 'No Faktur')
                        ->setCellValue('D'.($no_row_next+4), 'Pelanggan')

                        ->setCellValue('E'.($no_row_next+4), 'Masuk')
                        ->setCellValue('F'.($no_row_next+4), 'RS')
                        ->setCellValue('G'.($no_row_next+4), 'PT')
                        ->setCellValue('H'.($no_row_next+4), 'APT')
                        ->setCellValue('I'.($no_row_next+4), 'Lainnya')
                        ->setCellValue('J'.($no_row_next+4), 'Sisa');

            $no_row_next = $no_row_next+5;
            $no = 1;
            foreach ($data["list_data_retur_penjualan_p"] as $key => $value) {

                $val_rs = "";
                $val_pt = "";
                $val_apt = "";
                $val_lain = "";

                switch ($value->jenis_rekanan) {
                    case 'rs':
                        $val_rs = $value->jml_item_tr_detail; 
                        break;

                    case 'apotik':
                        $val_apt = $value->jml_item_tr_detail;
                        break;

                    case 'pbf':
                        $val_pt = $value->jml_item_tr_detail;
                        break;
                    
                    default:
                        $val_lain = $value->jml_item_tr_detail;
                        break;
                }

                $val_sisa -= $value->jml_item_tr_detail;

                $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.$no_row_next, $no)
                        ->setCellValue('B'.$no_row_next, $value->tgl_transaksi_tr_header)
                        ->setCellValue('C'.$no_row_next, $value->id_tr_header_p)
                        ->setCellValue('D'.$no_row_next, $value->nama_rekanan)
                        ->setCellValue('E'.$no_row_next, "")
                        ->setCellValue('F'.$no_row_next, $val_rs)
                        ->setCellValue('G'.$no_row_next, $val_pt)
                        ->setCellValue('H'.$no_row_next, $val_apt)
                        ->setCellValue('I'.$no_row_next, $val_lain)
                        ->setCellValue('J'.$no_row_next, $val_sisa);
                
                $no_row_next++;
                $no++;
            }
        // Pengganti Retur Penjualan


        // Retur Pembelian
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.($no_row_next+2), 'Daftar Keluar (Retur Pembelian)')
                        ->setCellValue('A'.($no_row_next+3), '');

            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.($no_row_next+4), 'No')
                        ->setCellValue('B'.($no_row_next+4), 'Tanggal')
                        ->setCellValue('C'.($no_row_next+4), 'No Faktur')
                        ->setCellValue('D'.($no_row_next+4), 'Suplier')

                        ->setCellValue('E'.($no_row_next+4), 'Masuk')
                        ->setCellValue('F'.($no_row_next+4), 'RS')
                        ->setCellValue('G'.($no_row_next+4), 'PT')
                        ->setCellValue('H'.($no_row_next+4), 'APT')
                        ->setCellValue('I'.($no_row_next+4), 'Lainnya')
                        ->setCellValue('J'.($no_row_next+4), 'Sisa');

            $no_row_next = $no_row_next+5;
            $no = 1;
            foreach ($data["list_data_retur_pembalian"] as $key => $value) {

                $val_rs = "";
                $val_pt = "";
                $val_apt = "";
                $val_lain = "";

                $val_pt = $value->jml_item_tr_detail;

                $val_sisa -= $value->jml_item_tr_detail;

                $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.$no_row_next, $no)
                        ->setCellValue('B'.$no_row_next, $value->tgl_transaksi_tr_header)
                        ->setCellValue('C'.$no_row_next, $value->id_tr_header_p)
                        ->setCellValue('D'.$no_row_next, $nama_suplier)
                        ->setCellValue('E'.$no_row_next, "")
                        ->setCellValue('F'.$no_row_next, $val_rs)
                        ->setCellValue('G'.$no_row_next, $val_pt)
                        ->setCellValue('H'.$no_row_next, $val_apt)
                        ->setCellValue('I'.$no_row_next, $val_lain)
                        ->setCellValue('J'.$no_row_next, $val_sisa);
                
                $no_row_next++;
                $no++;
            }
        // Retur Pembelian


        // Pengganti Sample
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.($no_row_next+2), 'Daftar Keluar (Sample)')
                        ->setCellValue('A'.($no_row_next+3), '');

            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.($no_row_next+4), 'No')
                        ->setCellValue('B'.($no_row_next+4), 'Tanggal')
                        ->setCellValue('C'.($no_row_next+4), 'No Faktur')
                        ->setCellValue('D'.($no_row_next+4), 'Pelanggan')

                        ->setCellValue('E'.($no_row_next+4), 'Masuk')
                        ->setCellValue('F'.($no_row_next+4), 'RS')
                        ->setCellValue('G'.($no_row_next+4), 'PT')
                        ->setCellValue('H'.($no_row_next+4), 'APT')
                        ->setCellValue('I'.($no_row_next+4), 'Lainnya')
                        ->setCellValue('J'.($no_row_next+4), 'Sisa');

            $no_row_next = $no_row_next+5;
            $no = 1;
            foreach ($data["list_data_sample"] as $key => $value) {

                $val_rs = "";
                $val_pt = "";
                $val_apt = "";
                $val_lain = "";

                switch ($value->jenis_rekanan) {
                    case 'rs':
                        $val_rs = $value->jml_item_tr_detail; 
                        break;

                    case 'apotik':
                        $val_apt = $value->jml_item_tr_detail;
                        break;

                    case 'pbf':
                        $val_pt = $value->jml_item_tr_detail;
                        break;
                    
                    default:
                        $val_lain = $value->jml_item_tr_detail;
                        break;
                }

                $val_sisa -= $value->jml_item_tr_detail;

                $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.$no_row_next, $no)
                        ->setCellValue('B'.$no_row_next, $value->tgl_transaksi_tr_header)
                        ->setCellValue('C'.$no_row_next, $value->id_tr_header)
                        ->setCellValue('D'.$no_row_next, $value->nama_rekanan)
                        ->setCellValue('E'.$no_row_next, "")
                        ->setCellValue('F'.$no_row_next, $val_rs)
                        ->setCellValue('G'.$no_row_next, $val_pt)
                        ->setCellValue('H'.$no_row_next, $val_apt)
                        ->setCellValue('I'.$no_row_next, $val_lain)
                        ->setCellValue('J'.$no_row_next, $val_sisa);
                
                $no_row_next++;
                $no++;
            }
        // Pengganti Sample
        
        

        // Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle('Laporan_retur');


        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);


        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel); 
        $objWriter->save('./Laporan_retur.xlsx'); 

        $this->load->helper('download');
        force_download('./Laporan_retur.xlsx', NULL);
    }
}
