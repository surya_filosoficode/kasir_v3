<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Laporan Penjualan Obat Berdasarkan Produk</h3>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->

<?php
    $str_jenis = "";
    switch ($this->uri->segment(3)) {
        case 'get_penjualan_item_tgl':
            $str_jenis = "per_tgl";
            break;
        
        case 'get_penjualan_item_bulan':
            $str_jenis = "per_bulan";
            break;

        case 'get_penjualan_item_triwulan':
            $str_jenis = "tiga_bulan";
            break;

        case 'get_penjualan_item_th':
            $str_jenis = "pertahun";
            break;

        default:
            $str_jenis = "per_tgl";
            break;
    }

    $str_param1 = $this->uri->segment(4);
    $str_param2 = $this->uri->segment(5);
    $str_param3 = $this->uri->segment(6);

    if($str_param1 == ""){
        $str_param1 = date("Y-m-d");
    }

    if($str_param2 == ""){
        $str_param2 = date("Y-m-d");
    }
?>

<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- Row -->
    <div class="row">
        <div class="col-12 m-t-30">
            <!-- Card -->
            <div class="card">
                <div class="card-header">
                    <!-- <h4 class="m-b-0 text-white">Laporan Penjualan Obat Berdasarkan Produk</h4> -->
                    <div class="row">
                        <div class="col-md-9">
                            <h4 class="m-b-0 text-black">Filter Laporan Penjualan Obat Berdasarkan Produk</h4>
                        </div>
                        <div class="col-md-3">
                            <select class="custom-select col-12" id="jenis_filter" name="jenis_filter">
                                <option value="per_tgl">Per Tanggal</option>
                                <option value="per_bulan">Per Bulan</option>
                                <option value="tiga_bulan">Tiga Bulanan</option>
                                <option value="pertahun">Per Tahun</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row" id="out_param_filter">
                        <div class="col-md-12" id="row_per_tgl">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label for="example-search-input" class="col-md-3 col-form-label">Tanggal Mulai</label>
                                        <div class="col-md-9">
                                            <input class="form-control" type="date" id="tgl_start" name="tgl_start">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label for="example-search-input" class="col-md-3 col-form-label">Tanggal Akhir</label>
                                        <div class="col-md-9">
                                            <input class="form-control" type="date" id="tgl_finish" name="tgl_finish">
                                        </div>
                                    </div>
                                </div>
                            </div>       
                        </div>

                        <div class="col-md-12" id="row_per_bulan">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label for="example-search-input" class="col-md-3 col-form-label">Bulan</label>
                                        <div class="col-md-9">
                                            <select class="custom-select col-12" id="bulan" name="bulan">
                                                <option value="1">Januari</option>
                                                <option value="2">Februari</option>
                                                <option value="3">Maret</option>
                                                <option value="4">April</option>
                                                <option value="5">Mei</option>
                                                <option value="6">Juni</option>
                                                <option value="7">Juli</option>
                                                <option value="8">Agustus</option>
                                                <option value="9">September</option>
                                                <option value="10">Oktober</option>
                                                <option value="11">November</option>
                                                <option value="12">Desember</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label for="example-search-input" class="col-md-3 col-form-label">Tahun</label>
                                        <div class="col-md-9">
                                            <input class="form-control" type="number" id="th" name="th">
                                        </div>
                                    </div>
                                </div>
                            </div>       
                        </div>

                        <div class="col-md-12" id="row_tiga_bulan">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label for="example-search-input" class="col-md-3 col-form-label">Periode</label>
                                        <div class="col-md-9">
                                            <select class="custom-select col-12" id="triwulan" name="triwulan">
                                                <option value="1-3">Triwulan I</option>
                                                <option value="4-6">Triwulan II</option>
                                                <option value="7-9">Triwulan III</option>
                                                <option value="10-12">Triwulan IV</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label for="example-search-input" class="col-md-3 col-form-label">Tahun</label>
                                        <div class="col-md-9">
                                            <input class="form-control" type="number" id="th_triwulan" name="th_triwulan">
                                        </div>
                                    </div>
                                </div>
                            </div>       
                        </div>

                        <div class="col-md-12" id="row_pertahun">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label for="example-search-input" class="col-md-3 col-form-label">Tahun Awal</label>
                                        <div class="col-md-9">
                                            <input class="form-control" type="number" id="th_start" name="th_start">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label for="example-search-input" class="col-md-3 col-form-label">Tahun Akhir</label>
                                        <div class="col-md-9">
                                            <input class="form-control" type="number" id="th_finish" name="th_finish">
                                        </div>
                                    </div>
                                </div>
                            </div>       
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label for="example-search-input" class="col-md-3 col-form-label">Nama Produk</label>
                                        <div class="col-md-9">
                                            <select class="select2 form-control custom-select" id="item" name="item">
                                                <?php
                                                if(isset($item)){
                                                    foreach ($item as $key => $value) {
                                                        print_r("<option value=\"".$value->id_item."\">(".$value->id_item.") ".$value->nama_item." - ".$value->kode_produksi_item."</option>");
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 text-right">
                                    <button type="button" id="lanjut" name="lanjut" class="btn waves-effect waves-light btn-rounded btn-info">Terapkan</button>
                                </div>
                            </div>       
                        </div>
                    </div>
                </div>
            </div>
            <!-- Card -->
        </div>
    </div>
    <!-- End Row -->

    <!-- Row -->
    <div class="row">
        <div class="col-12 m-t-30">
            <!-- Card -->
            <div class="card">
                <div class="card-header">
                    <!-- <h4 class="m-b-0 text-white">Laporan Penjualan Obat Berdasarkan Produk</h4> -->
                    <div class="row">

                        <div class="col-md-9">
                            <h4 class="m-b-0 text-black">Tabel Laporan Penjualan Obat Berdasarkan Produk</h4>
                        </div>
                        <div class="col-md-3 text-right">
                            <button class="btn btn-info" id="btn_print" name="btn_print"><i class="fa fa-print"></i> PRINT</button> 
                            <button class="btn btn-info" id="btn_excel" name="btn_excel"><i class="fa fa-file-excel-o"></i> EXCEL</button>
                        </div>                        
                    </div>
                </div>
                <div class="card-body">
                    <div class="row" id="report">
                        <div class="col-md-12">
                            <div class="col-12 text-center">
                                <h3>LAPORAN TRANSAKSI PENJUALAN OBAT BERDASARKAN Produk</h3>
                                <h4><?php print_r($str_periode);?></h4>
                                 <!-- <span>Per-Tanggal ......</span> -->
                            </div>

                    <!-- Pembelian -->
                            <div class="col-md-12">
                                <br>
                                <label>Daftar Masuk (Pemebelian)</label>
                                <!-- <hr> -->
                            </div>
                            <div class="col-md-12" style="font-size: 11px; font-weight: 300;">
                                <table id="myTable" class="table table-bordered table-striped" style="width:100%" >
                                    <thead>
                                        <tr>
                                            <th style="font-weight: bold;">No.</th>
                                            <th style="font-weight: bold;">Tanggal</th>
                                            <th style="font-weight: bold;">No Faktur</th>
                                            <th style="font-weight: bold;">Suplier</th>
                                            <!-- <th style="font-weight: bold;">Id Produk</th>
                                            <th style="font-weight: bold;">Nama Produk</th> -->
                                            <th style="font-weight: bold;">Masuk</th>
                                            <th style="font-weight: bold;">RS</th>
                                            <th style="font-weight: bold;">PT</th>
                                            <th style="font-weight: bold;">APT</th>
                                            <th style="font-weight: bold;">Lainnya</th>
                                            <th style="font-weight: bold;">Sisa</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    // print_r($list_data);
                                        $nama_suplier = "";
                                        $first_val = 0;
                                        
                                        $val_sisa = 0;
                                        $no = 1;

                                        if(isset($check_data)){
                                            $item_fine = $check_data[0]->stok_record_item;
                                            $item_broken = $check_data[0]->rusak_record_item;

                                            $first_val = $item_fine+$item_broken;
                                        }

                                        $val_sisa +=$first_val;

                                        if(isset($list_data_pembelian) && isset($check_data)){
                                            
                                            foreach ($list_data_pembelian as $key => $value) {
                                                // $first_val = 0;
                                                if($key == 0){
                                                    // $item_fine = $check_data[0]->stok_record_item;
                                                    // $item_broken = $check_data[0]->rusak_record_item;

                                                    // $first_val = $item_fine+$item_broken;
                                                    
                                                }

                                                $val_sisa +=$first_val;

                                                $val_rs = "";
                                                $val_pt = "";
                                                $val_apt = "";
                                                $val_lain = "";

                                                $nama_suplier = $value->nama_suplier;   

                                                $val_sisa += $value->jml_item_tr_detail;

                                                print_r("
                                                        <tr>
                                                            <td>".$no."</td>
                                                            <td>".$value->tgl_transaksi_tr_header."</td>
                                                            <td>".$value->id_tr_header."</td>
                                                            <td>".$value->nama_suplier."</td>

                                                            <td align=\"right\">".$value->jml_item_tr_detail."</td>
                                                            <td align=\"right\">".$val_rs."</td>
                                                            <td align=\"right\">".$val_pt."</td>
                                                            <td align=\"right\">".$val_apt."</td>
                                                            <td align=\"right\">".$val_lain."</td>
                                                            <td align=\"right\">".$val_sisa."</td>
                                                        </tr>
                                                    ");

                                                $no++;
                                            }
                                        }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                    <!-- Pembelian -->

                    <!-- Retur Penjualan -->
                            <div class="col-md-12">
                                <br>
                                <label>Daftar Masuk (Retur Penjualan)</label>
                                <!-- <hr> -->
                            </div>
                            <div class="col-md-12" style="font-size: 11px; font-weight: 300;">
                                <table id="myTable" class="table table-bordered table-striped" style="width:100%" >
                                    <thead>
                                        <tr>
                                            <th style="font-weight: bold;">No.</th>
                                            <th style="font-weight: bold;">Tanggal</th>
                                            <th style="font-weight: bold;">No Faktur</th>
                                            <th style="font-weight: bold;">Pelanggan</th>
                                            <!-- <th style="font-weight: bold;">Id Produk</th>
                                            <th style="font-weight: bold;">Nama Produk</th> -->
                                            <th style="font-weight: bold;">Masuk</th>
                                            <th style="font-weight: bold;">RS</th>
                                            <th style="font-weight: bold;">PT</th>
                                            <th style="font-weight: bold;">APT</th>
                                            <th style="font-weight: bold;">Lainnya</th>
                                            <th style="font-weight: bold;">Sisa</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php

                                        if(isset($list_data_retur_penjualan) && isset($check_data)){
                                            // $val_sisa = 0;
                                            foreach ($list_data_retur_penjualan as $key => $value) {
                                                // $first_val = 0;
                                                // if($key == 0){
                                                //     $item_fine = $check_data[0]->stok_record_item;
                                                //     $item_broken = $check_data[0]->rusak_record_item;

                                                //     $first_val = $item_fine+$item_broken;
                                                //     $val_sisa +=$first_val;
                                                // }

                                                $val_rs = "";
                                                $val_pt = "";
                                                $val_apt = "";
                                                $val_lain = "";

                                                switch ($value->jenis_rekanan) {
                                                    case 'rs':
                                                        $val_rs = $value->jml_item_tr_detail; 
                                                        break;

                                                    case 'apotik':
                                                        $val_apt = $value->jml_item_tr_detail;
                                                        break;

                                                    case 'pbf':
                                                        $val_pt = $value->jml_item_tr_detail;
                                                        break;
                                                    
                                                    default:
                                                        $val_lain = $value->jml_item_tr_detail;
                                                        break;
                                                }

                                                $val_sisa += $value->jml_item_tr_detail;

                                                print_r("
                                                        <tr>
                                                            <td align=\"right\">".$no."</td>
                                                            <td align=\"right\">".$value->tgl_transaksi_tr_header."</td>
                                                            <td align=\"right\">".$value->id_tr_header_p."</td>
                                                            <td align=\"right\">".$value->nama_rekanan."</td>

                                                            <td align=\"right\"></td>
                                                            <td align=\"right\">".$val_rs."</td>
                                                            <td align=\"right\">".$val_pt."</td>
                                                            <td align=\"right\">".$val_apt."</td>
                                                            <td align=\"right\">".$val_lain."</td>
                                                            <td align=\"right\">".$val_sisa."</td>
                                                        </tr>
                                                    ");

                                                $no++;
                                            }
                                        }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                    <!-- Retur Penjualan -->

                    <!-- Retur Pengganti Retur Pembelian -->
                            <div class="col-md-12">
                                <br>
                                <label>Daftar Masuk (Pengganti Retur Pembelian)</label>
                                <!-- <hr> -->
                            </div>
                            <div class="col-md-12" style="font-size: 11px; font-weight: 300;">
                                <table id="myTable" class="table table-bordered table-striped" style="width:100%" >
                                    <thead>
                                        <tr>
                                            <th style="font-weight: bold;">No.</th>
                                            <th style="font-weight: bold;">Tanggal</th>
                                            <th style="font-weight: bold;">No Faktur</th>
                                            <th style="font-weight: bold;">Suplier</th>
                                            <!-- <th style="font-weight: bold;">Id Produk</th>
                                            <th style="font-weight: bold;">Nama Produk</th> -->
                                            <th style="font-weight: bold;">Masuk</th>
                                            <th style="font-weight: bold;">RS</th>
                                            <th style="font-weight: bold;">PT</th>
                                            <th style="font-weight: bold;">APT</th>
                                            <th style="font-weight: bold;">Lainnya</th>
                                            <th style="font-weight: bold;">Sisa</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        if(isset($list_data_retur_pembelian_p) && isset($check_data)){
                                            // $val_sisa = 0;
                                            foreach ($list_data_retur_pembelian_p as $key => $value) {
                                                // $first_val = 0;
                                                // if($key == 0){
                                                //     $item_fine = $check_data[0]->stok_record_item;
                                                //     $item_broken = $check_data[0]->rusak_record_item;

                                                //     $first_val = $item_fine+$item_broken;
                                                //     $val_sisa +=$first_val;
                                                // }

                                                $val_rs = "";
                                                $val_pt = "";
                                                $val_apt = "";
                                                $val_lain = "";

                                                $nama_suplier = $value->nama_suplier; 

                                                // switch ($value->jenis_rekanan) {
                                                //     case 'rs':
                                                //         $val_rs = $value->jml_item_tr_detail; 
                                                //         break;

                                                //     case 'apotik':
                                                //         $val_apt = $value->jml_item_tr_detail;
                                                //         break;

                                                //     case 'pbf':
                                                //         $val_pt = $value->jml_item_tr_detail;
                                                //         break;
                                                    
                                                //     default:
                                                //         $val_lain = $value->jml_item_tr_detail;
                                                //         break;
                                                // }

                                                $val_sisa += $value->jml_item_tr_detail;

                                                print_r("
                                                        <tr>
                                                            <td align=\"right\">".$no."</td>
                                                            <td align=\"right\">".$value->tgl_transaksi_tr_header."</td>
                                                            <td align=\"right\">".$value->id_tr_header_p."</td>
                                                            <td align=\"right\">".$value->nama_suplier."</td>

                                                            <td align=\"right\">".$value->jml_item_tr_detail."</td>
                                                            <td align=\"right\">".$val_rs."</td>
                                                            <td align=\"right\">".$val_pt."</td>
                                                            <td align=\"right\">".$val_apt."</td>
                                                            <td align=\"right\">".$val_lain."</td>
                                                            <td align=\"right\">".$val_sisa."</td>
                                                        </tr>
                                                    ");

                                                $no++;
                                            }
                                        }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                    <!-- Retur Pengganti Retur Pembelian -->

                    <!-- Retur Pengganti Retur Sample -->
                            <div class="col-md-12">
                                <br>
                                <label>Daftar Masuk (Pengganti Sample)</label>
                                <!-- <hr> -->
                            </div>
                            <div class="col-md-12" style="font-size: 11px; font-weight: 300;">
                                <table id="myTable" class="table table-bordered table-striped" style="width:100%" >
                                    <thead>
                                        <tr>
                                            <th style="font-weight: bold;">No.</th>
                                            <th style="font-weight: bold;">Tanggal</th>
                                            <th style="font-weight: bold;">No Faktur</th>
                                            <th style="font-weight: bold;">Pelanggan</th>
                                            <!-- <th style="font-weight: bold;">Id Produk</th>
                                            <th style="font-weight: bold;">Nama Produk</th> -->
                                            <th style="font-weight: bold;">Masuk</th>
                                            <th style="font-weight: bold;">RS</th>
                                            <th style="font-weight: bold;">PT</th>
                                            <th style="font-weight: bold;">APT</th>
                                            <th style="font-weight: bold;">Lainnya</th>
                                            <th style="font-weight: bold;">Sisa</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php

                                        if(isset($list_data_retur_sample_p) && isset($check_data)){
                                            // $val_sisa = 0;
                                            foreach ($list_data_retur_sample_p as $key => $value) {
                                                $val_rs = "";
                                                $val_pt = "";
                                                $val_apt = "";
                                                $val_lain = "";

                                                $val_sisa += $value->jml_item_tr_detail;

                                                print_r("
                                                        <tr>
                                                            <td align=\"right\">".$no."</td>
                                                            <td align=\"right\">".$value->tgl_transaksi_tr_header."</td>
                                                            <td align=\"right\">".$value->id_tr_header_p."</td>
                                                            <td align=\"right\">".$nama_suplier."</td>

                                                            <td align=\"right\">".$value->jml_item_tr_detail."</td>
                                                            <td align=\"right\">".$val_rs."</td>
                                                            <td align=\"right\">".$val_pt."</td>
                                                            <td align=\"right\">".$val_apt."</td>
                                                            <td align=\"right\">".$val_lain."</td>
                                                            <td align=\"right\">".$val_sisa."</td>
                                                        </tr>
                                                    ");

                                                $no++;
                                            }
                                        }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                    <!-- Retur Pengganti Retur Sample -->




                    <!-- Penjualan -->
                            <div class="col-md-12">
                                <br>
                                <label>Daftar Keluar (Penjualan)</label>
                                <!-- <hr> -->
                            </div>
                            <div class="col-md-12" style="font-size: 11px; font-weight: 300;">
                                <table id="myTable" class="table table-bordered table-striped" style="width:100%" >
                                    <thead>
                                        <tr>
                                            <th style="font-weight: bold;">No.</th>
                                            <th style="font-weight: bold;">Tanggal</th>
                                            <th style="font-weight: bold;">No Faktur</th>
                                            <th style="font-weight: bold;">Pelanggan</th>
                                            <!-- <th style="font-weight: bold;">Id Produk</th>
                                            <th style="font-weight: bold;">Nama Produk</th> -->
                                            <th style="font-weight: bold;">Masuk</th>
                                            <th style="font-weight: bold;">RS</th>
                                            <th style="font-weight: bold;">PT</th>
                                            <th style="font-weight: bold;">APT</th>
                                            <th style="font-weight: bold;">Lainnya</th>
                                            <th style="font-weight: bold;">Sisa</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        if(isset($list_data) && isset($check_data)){
                                            // $val_sisa = 0;
                                            foreach ($list_data as $key => $value) {
                                                // $first_val = 0;
                                                // if($key == 0){
                                                //     $item_fine = $check_data[0]->stok_record_item;
                                                //     $item_broken = $check_data[0]->rusak_record_item;

                                                //     $first_val = $item_fine+$item_broken;
                                                //     $val_sisa +=$first_val;
                                                // }

                                                $val_rs = "";
                                                $val_pt = "";
                                                $val_apt = "";
                                                $val_lain = "";

                                                switch ($value->jenis_rekanan) {
                                                    case 'rs':
                                                        $val_rs = $value->jml_item_tr_detail; 
                                                        break;

                                                    case 'apotik':
                                                        $val_apt = $value->jml_item_tr_detail;
                                                        break;

                                                    case 'pbf':
                                                        $val_pt = $value->jml_item_tr_detail;
                                                        break;
                                                    
                                                    default:
                                                        $val_lain = $value->jml_item_tr_detail;
                                                        break;
                                                }

                                                $val_sisa -= $value->jml_item_tr_detail;

                                                print_r("
                                                        <tr>
                                                            <td>".$no."</td>
                                                            <td>".$value->tgl_transaksi_tr_header."</td>
                                                            <td>".$value->id_tr_header."</td>
                                                            <td>".$value->nama_rekanan."</td>

                                                            <td align=\"right\"></td>
                                                            <td align=\"right\">".$val_rs."</td>
                                                            <td align=\"right\">".$val_pt."</td>
                                                            <td align=\"right\">".$val_apt."</td>
                                                            <td align=\"right\">".$val_lain."</td>
                                                            <td align=\"right\">".$val_sisa."</td>
                                                        </tr>
                                                    ");

                                                $no++;
                                            }
                                        }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                    <!-- Penjualan -->

                    <!-- Pengganti Retur Penjualan -->
                            <div class="col-md-12">
                                <br>
                                <label>Daftar Keluar (Pengganti Retur Penjualan)</label>
                                <!-- <hr> -->
                            </div>
                            <div class="col-md-12" style="font-size: 11px; font-weight: 300;">
                                <table id="myTable" class="table table-bordered table-striped" style="width:100%" >
                                    <thead>
                                        <tr>
                                            <th style="font-weight: bold;">No.</th>
                                            <th style="font-weight: bold;">Tanggal</th>
                                            <th style="font-weight: bold;">No Faktur</th>
                                            <th style="font-weight: bold;">Pelanggan</th>
                                            <!-- <th style="font-weight: bold;">Id Produk</th>
                                            <th style="font-weight: bold;">Nama Produk</th> -->
                                            <th style="font-weight: bold;">Masuk</th>
                                            <th style="font-weight: bold;">RS</th>
                                            <th style="font-weight: bold;">PT</th>
                                            <th style="font-weight: bold;">APT</th>
                                            <th style="font-weight: bold;">Lainnya</th>
                                            <th style="font-weight: bold;">Sisa</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        if(isset($list_data_retur_penjualan_p) && isset($check_data)){
                                            // $val_sisa = 0;
                                            foreach ($list_data_retur_penjualan_p as $key => $value) {
                                                // $first_val = 0;
                                                // if($key == 0){
                                                //     $item_fine = $check_data[0]->stok_record_item;
                                                //     $item_broken = $check_data[0]->rusak_record_item;

                                                //     $first_val = $item_fine+$item_broken;
                                                //     $val_sisa +=$first_val;
                                                // }

                                                $val_rs = "";
                                                $val_pt = "";
                                                $val_apt = "";
                                                $val_lain = "";

                                                switch ($value->jenis_rekanan) {
                                                    case 'rs':
                                                        $val_rs = $value->jml_item_tr_detail; 
                                                        break;

                                                    case 'apotik':
                                                        $val_apt = $value->jml_item_tr_detail;
                                                        break;

                                                    case 'pbf':
                                                        $val_pt = $value->jml_item_tr_detail;
                                                        break;
                                                    
                                                    default:
                                                        $val_lain = $value->jml_item_tr_detail;
                                                        break;
                                                }

                                                $val_sisa -= $value->jml_item_tr_detail;

                                                print_r("
                                                        <tr>
                                                            <td>".$no."</td>
                                                            <td>".$value->tgl_transaksi_tr_header."</td>
                                                            <td>".$value->id_tr_header_p."</td>
                                                            <td>".$value->nama_rekanan."</td>

                                                            <td align=\"right\"></td>
                                                            <td align=\"right\">".$val_rs."</td>
                                                            <td align=\"right\">".$val_pt."</td>
                                                            <td align=\"right\">".$val_apt."</td>
                                                            <td align=\"right\">".$val_lain."</td>
                                                            <td align=\"right\">".$val_sisa."</td>
                                                        </tr>
                                                    ");

                                                $no++;
                                            }
                                        }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                    <!-- Pengganti Retur Penjualan -->

                    <!-- Retur Pembelian -->
                            <div class="col-md-12">
                                <br>
                                <label>Daftar Keluar (Retur Pembelian)</label>
                                <!-- <hr> -->
                            </div>
                            <div class="col-md-12" style="font-size: 11px; font-weight: 300;">
                                <table id="myTable" class="table table-bordered table-striped" style="width:100%" >
                                    <thead>
                                        <tr>
                                            <th style="font-weight: bold;">No.</th>
                                            <th style="font-weight: bold;">Tanggal</th>
                                            <th style="font-weight: bold;">No Faktur</th>
                                            <th style="font-weight: bold;">Suplier</th>
                                            <!-- <th style="font-weight: bold;">Id Produk</th>
                                            <th style="font-weight: bold;">Nama Produk</th> -->
                                            <th style="font-weight: bold;">Masuk</th>
                                            <th style="font-weight: bold;">RS</th>
                                            <th style="font-weight: bold;">PT</th>
                                            <th style="font-weight: bold;">APT</th>
                                            <th style="font-weight: bold;">Lainnya</th>
                                            <th style="font-weight: bold;">Sisa</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        if(isset($list_data_retur_pembalian) && isset($check_data)){
                                            // $val_sisa = 0;
                                            foreach ($list_data_retur_pembalian as $key => $value) {
                                                // $first_val = 0;
                                                // if($key == 0){
                                                //     $item_fine = $check_data[0]->stok_record_item;
                                                //     $item_broken = $check_data[0]->rusak_record_item;

                                                //     $first_val = $item_fine+$item_broken;
                                                //     $val_sisa +=$first_val;
                                                // }

                                                $val_rs = "";
                                                $val_pt = "";
                                                $val_apt = "";
                                                $val_lain = "";

                                                // switch ($value->jenis_rekanan) {
                                                //     case 'rs':
                                                //         $val_rs = $value->jml_item_tr_detail; 
                                                //         break;

                                                //     case 'apotik':
                                                //         $val_apt = $value->jml_item_tr_detail;
                                                //         break;

                                                //     case 'pbf':
                                                        $val_pt = $value->jml_item_tr_detail;
                                                        // break;
                                                    
                                                //     default:
                                                //         $val_lain = $value->jml_item_tr_detail;
                                                //         break;
                                                // }

                                                $val_sisa -= $value->jml_item_tr_detail;

                                                print_r("
                                                        <tr>
                                                            <td>".$no."</td>
                                                            <td>".$value->tgl_transaksi_tr_header."</td>
                                                            <td>".$value->id_tr_header_p."</td>
                                                            <td>".$value->nama_suplier."</td>

                                                            <td align=\"right\"></td>
                                                            <td align=\"right\">".$val_rs."</td>
                                                            <td align=\"right\">".$val_pt."</td>
                                                            <td align=\"right\">".$val_apt."</td>
                                                            <td align=\"right\">".$val_lain."</td>
                                                            <td align=\"right\">".$val_sisa."</td>
                                                        </tr>
                                                    ");

                                                $no++;
                                            }
                                        }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                    <!-- Retur Pembelian -->

                    <!-- Pengganti Sample -->
                            <div class="col-md-12">
                                <br>
                                <label>Daftar Keluar (Sample)</label>
                                <!-- <hr> -->
                            </div>
                            <div class="col-md-12" style="font-size: 11px; font-weight: 300;">
                                <table id="myTable" class="table table-bordered table-striped" style="width:100%" >
                                    <thead>
                                        <tr>
                                            <th style="font-weight: bold;">No.</th>
                                            <th style="font-weight: bold;">Tanggal</th>
                                            <th style="font-weight: bold;">No Faktur</th>
                                            <th style="font-weight: bold;">Pelanggan</th>
                                            <!-- <th style="font-weight: bold;">Id Produk</th>
                                            <th style="font-weight: bold;">Nama Produk</th> -->
                                            <th style="font-weight: bold;">Masuk</th>
                                            <th style="font-weight: bold;">RS</th>
                                            <th style="font-weight: bold;">PT</th>
                                            <th style="font-weight: bold;">APT</th>
                                            <th style="font-weight: bold;">Lainnya</th>
                                            <th style="font-weight: bold;">Sisa</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        if(isset($list_data_sample) && isset($check_data)){
                                            // $val_sisa = 0;
                                            foreach ($list_data_sample as $key => $value) {
                                                // $first_val = 0;
                                                // if($key == 0){
                                                //     $item_fine = $check_data[0]->stok_record_item;
                                                //     $item_broken = $check_data[0]->rusak_record_item;

                                                //     $first_val = $item_fine+$item_broken;
                                                //     $val_sisa +=$first_val;
                                                // }

                                                $val_rs = "";
                                                $val_pt = "";
                                                $val_apt = "";
                                                $val_lain = "";

                                                switch ($value->jenis_rekanan) {
                                                    case 'rs':
                                                        $val_rs = $value->jml_item_tr_detail; 
                                                        break;

                                                    case 'apotik':
                                                        $val_apt = $value->jml_item_tr_detail;
                                                        break;

                                                    case 'pbf':
                                                        $val_pt = $value->jml_item_tr_detail;
                                                        break;
                                                    
                                                    default:
                                                        $val_lain = $value->jml_item_tr_detail;
                                                        break;
                                                }

                                                $val_sisa -= $value->jml_item_tr_detail;

                                                print_r("
                                                        <tr>
                                                            <td>".$no."</td>
                                                            <td>".$value->tgl_transaksi_tr_header."</td>
                                                            <td>".$value->id_tr_header."</td>
                                                            <td>".$value->nama_rekanan."</td>

                                                            <td align=\"right\"></td>
                                                            <td align=\"right\">".$val_rs."</td>
                                                            <td align=\"right\">".$val_pt."</td>
                                                            <td align=\"right\">".$val_apt."</td>
                                                            <td align=\"right\">".$val_lain."</td>
                                                            <td align=\"right\">".$val_sisa."</td>
                                                        </tr>
                                                    ");

                                                $no++;
                                            }
                                        }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                    <!-- Pengganti Sample -->


                        </div>
                    </div>
                </div>
            </div>
            <!-- Card -->
        </div>
    </div>
    <!-- End Row -->
</div>
<!-- ============================================================== -->
<!-- --------------------------End Container fluid----------------  -->
<!-- ============================================================== -->

<script type="text/javascript">
    $(document).ready(function(){
        set_default_value();
        set_default();
        filter_show();

        console.log("");
    });

    function set_default(){
        $("#jenis_filter").val("<?php print_r($str_jenis);?>");
        $("#item").val("<?php print_r($str_param3);?>");

        var jenis_filter = $("#jenis_filter").val();
        switch(jenis_filter) {
          case "per_tgl":
                $("#tgl_start").val("<?php print_r($str_param1);?>");
                $("#tgl_finish").val("<?php print_r($str_param2);?>");
            break;
          case "per_bulan":
                $("#bulan").val("<?php print_r($str_param1);?>");
                $("#th").val("<?php print_r($str_param2);?>"); 
            break;
          case "tiga_bulan":
                $("#triwulan").val("<?php print_r($str_param1);?>");
                $("#th_triwulan").val("<?php print_r($str_param2);?>"); 
            break;
          case "pertahun":
                $("#th_start").val("<?php print_r($str_param1);?>");
                $("#th_finish").val("<?php print_r($str_param2);?>"); 
            break;
          default:
            // code block
        }      
    }

    function set_default_value(){
        $("#tgl_start").val("<?php print_r(date("Y-m-d"))?>");
        $("#tgl_finish").val("<?php print_r(date("Y-m-d"))?>");

        $("#triwulan").val("1-3");
        $("#th_triwulan").val("<?php print_r(date("Y"))?>");

        $("#bulan").val("1");
        $("#th").val("<?php print_r(date("Y"))?>");

        $("#th_start").val("<?php print_r(date("Y"))?>");
        $("#th_finish").val("<?php print_r(date("Y"))?>");
    }

    $("#jenis_filter").change(function(){
        var jenis_filter = $(this).val();
        filter_show();        
    });

    function filter_show(){
        var jenis_filter = $("#jenis_filter").val();
        switch(jenis_filter) {
          case "per_tgl":
                $("#row_per_tgl").removeAttr("hidden", true);

                $("#row_per_bulan").attr("hidden", true);
                $("#row_tiga_bulan").attr("hidden", true);
                $("#row_pertahun").attr("hidden", true);  
            break;
          case "per_bulan":
                $("#row_per_tgl").attr("hidden", true);

                $("#row_per_bulan").removeAttr("hidden", true);
                
                $("#row_tiga_bulan").attr("hidden", true);
                $("#row_pertahun").attr("hidden", true);  
            break;
          case "tiga_bulan":
                $("#row_per_tgl").attr("hidden", true);
                $("#row_per_bulan").attr("hidden", true);

                $("#row_tiga_bulan").removeAttr("hidden", true);
                
                $("#row_pertahun").attr("hidden", true);  
            break;
          case "pertahun":
                $("#row_per_tgl").attr("hidden", true);
                $("#row_per_bulan").attr("hidden", true);
                $("#row_tiga_bulan").attr("hidden", true);

                $("#row_pertahun").removeAttr("hidden", true);  
            break;
          default:
            // code block
        }
    }

    $("#lanjut").click(function(){
        var jenis_filter = $("#jenis_filter").val();
        switch(jenis_filter) {
          case "per_tgl":
                send_per_tgl();
            break;
          case "per_bulan":
                send_per_bulan(); 
            break;
          case "tiga_bulan":
                send_tiga_bulan();  
            break;
          case "pertahun":
                send_pertahun();  
            break;
          default:
            // code block
        }
    });

    function send_per_tgl(){
        var tgl_start = $("#tgl_start").val();
        var tgl_finish = $("#tgl_finish").val();
        var item = $("#item").val();

        var str_tgl_start = new Date();
        var str_tgl_finish = new Date();


        // console.log(str_tgl_finish);

        window.location.href = "<?php print_r(base_url())?>report/reportpenjualanitem/get_penjualan_item_tgl/"+tgl_start+"/"+tgl_finish+"/"+item;
    }

    function send_tiga_bulan(){
        var triwulan = $("#triwulan").val();
        var th_triwulan = $("#th_triwulan").val();
        var item = $("#item").val();

        window.location.href = "<?php print_r(base_url())?>report/reportpenjualanitem/get_penjualan_item_triwulan/"+triwulan+"/"+th_triwulan+"/"+item;
    }

    function send_per_bulan(){
        var bulan = $("#bulan").val();
        var th = $("#th").val();
        var item = $("#item").val();

        window.location.href = "<?php print_r(base_url())?>report/reportpenjualanitem/get_penjualan_item_bulan/"+bulan+"/"+th+"/"+item;
    }

    function send_pertahun(){
        var th_start = $("#th_start").val();
        var th_finish = $("#th_finish").val();
        var item = $("#item").val();

        window.location.href = "<?php print_r(base_url())?>report/reportpenjualanitem/get_penjualan_item_th/"+th_start+"/"+th_finish+"/"+item;
    }




    $("#btn_print").click(function(){
        var jenis_filter = $("#jenis_filter").val();
        switch(jenis_filter) {
          case "per_tgl":
                print_send_per_tgl();
            break;
          case "per_bulan":
                print_send_per_bulan(); 
            break;
          case "tiga_bulan":
                print_send_tiga_bulan();  
            break;
          case "pertahun":
                print_send_pertahun();  
            break;
          default:
            // code block
        }
    });

    function print_send_per_tgl(){
        var tgl_start = $("#tgl_start").val();
        var tgl_finish = $("#tgl_finish").val();
        var item = $("#item").val();

        var str_tgl_start = new Date();
        var str_tgl_finish = new Date();


        // console.log(str_tgl_finish);

        window.open("<?php print_r(base_url())?>report/reportpenjualanitem/print_get_penjualan_item_tgl/"+tgl_start+"/"+tgl_finish+"/"+item, "_blank");
    }

    function print_send_tiga_bulan(){
        var triwulan = $("#triwulan").val();
        var th_triwulan = $("#th_triwulan").val();
        var item = $("#item").val();

        window.open("<?php print_r(base_url())?>report/reportpenjualanitem/print_get_penjualan_item_triwulan/"+triwulan+"/"+th_triwulan+"/"+item, "_blank");
    }

    function print_send_per_bulan(){
        var bulan = $("#bulan").val();
        var th = $("#th").val();
        var item = $("#item").val();

        window.open("<?php print_r(base_url())?>report/reportpenjualanitem/print_get_penjualan_item_bulan/"+bulan+"/"+th+"/"+item, "_blank");
    }

    function print_send_pertahun(){
        var th_start = $("#th_start").val();
        var th_finish = $("#th_finish").val();
        var item = $("#item").val();

        window.open("<?php print_r(base_url())?>report/reportpenjualanitem/print_get_penjualan_item_th/"+th_start+"/"+th_finish+"/"+item, "_blank");
    }




    $("#btn_excel").click(function(){
        var jenis_filter = $("#jenis_filter").val();
        switch(jenis_filter) {
          case "per_tgl":
                excel_send_per_tgl();
            break;
          case "per_bulan":
                excel_send_per_bulan(); 
            break;
          case "tiga_bulan":
                excel_send_tiga_bulan();  
            break;
          case "pertahun":
                excel_send_pertahun();  
            break;
          default:
            // code block
        }
    });

    function excel_send_per_tgl(){
        var tgl_start = $("#tgl_start").val();
        var tgl_finish = $("#tgl_finish").val();
        var item = $("#item").val();

        var str_tgl_start = new Date();
        var str_tgl_finish = new Date();


        // console.log(str_tgl_finish);

        window.open("<?php print_r(base_url())?>report/reportpenjualanitem/excel_get_penjualan_item_tgl/"+tgl_start+"/"+tgl_finish+"/"+item, "_blank");
    }

    function excel_send_tiga_bulan(){
        var triwulan = $("#triwulan").val();
        var th_triwulan = $("#th_triwulan").val();
        var item = $("#item").val();

        window.open("<?php print_r(base_url())?>report/reportpenjualanitem/excel_get_penjualan_item_triwulan/"+triwulan+"/"+th_triwulan+"/"+item, "_blank");
    }

    function excel_send_per_bulan(){
        var bulan = $("#bulan").val();
        var th = $("#th").val();
        var item = $("#item").val();

        window.open("<?php print_r(base_url())?>report/reportpenjualanitem/excel_get_penjualan_item_bulan/"+bulan+"/"+th+"/"+item, "_blank");
    }

    function excel_send_pertahun(){
        var th_start = $("#th_start").val();
        var th_finish = $("#th_finish").val();
        var item = $("#item").val();

        window.open("<?php print_r(base_url())?>report/reportpenjualanitem/excel_get_penjualan_item_th/"+th_start+"/"+th_finish+"/"+item, "_blank");
    }
    
</script>