<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Laporan bpom</h3>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->

<?php
    $str_jenis = "";
    switch ($this->uri->segment(3)) {
        case 'get_bpom_tgl':
            $str_jenis = "per_tgl";
            break;
        
        case 'get_bpom_bulan':
            $str_jenis = "per_bulan";
            break;

        case 'get_bpom_triwulan':
            $str_jenis = "tiga_bulan";
            break;

        case 'get_bpom_th':
            $str_jenis = "pertahun";
            break;

        default:
            $str_jenis = "per_bulan";
            break;
    }

    $str_param1 = $this->uri->segment(4);
    $str_param2 = $this->uri->segment(5);

    if($str_param1 == ""){
        $str_param1 = date("Y-m-d");
    }

    if($str_param2 == ""){
        $str_param2 = date("Y-m-d");
    }


    $no_siup = "";
        $no_npwp = "";
        if(isset($setting)){
            $array_npwp = array();
            $array_siup = array();
            foreach ($setting as $key => $value) {
                if($value->jenis_setting == "npwp"){
                    $array_npwp = $value;
                    $no_npwp = $value->keterangan_setting;
                }elseif ($value->jenis_setting == "siup") {
                    $array_siup = $value;
                    $no_siup = $value->keterangan_setting;
                }
            }
        }

    $periode = "";
    if(isset($str_periode)){
        $periode = $str_periode;
    }
?>


<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- Row -->
    <div class="row">
        <div class="col-12 m-t-30">
            <!-- Card -->
            <div class="card">
                <div class="card-header">
                    <!-- <h4 class="m-b-0 text-white">Laporan bpom</h4> -->
                    <div class="row">
                        <div class="col-md-9">
                            <h4 class="m-b-0 text-black">Filter Laporan bpom</h4>
                        </div>
                        
                    </div>
                </div>
                <div class="card-body">
                    
                    <div class="row">
                        <div class="col-md-8">
                            <select class="custom-select col-12" id="jenis_filter" name="jenis_filter">
                                <option value="per_tgl">Per Tanggal Sekarang</option>
                                <option value="per_bulan">Per Bulan Sekarang</option>
                                <option value="tiga_bulan">Tiga Bulanan Sekarang</option>
                                <option value="pertahun">Per Tahun Sekarang</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <button type="button" id="lanjut" name="lanjut" class="btn waves-effect waves-light btn-rounded btn-info">Terapkan</button>
                                </div>
                            </div>       
                        </div>
                    </div>
                </div>
            </div>
            <!-- Card -->
        </div>
    </div>
    <!-- End Row -->

    <!-- Row -->
    <div class="row">
        <div class="col-12 m-t-30">
            <!-- Card -->
            <div class="card">
                <div class="card-header">
                    <!-- <h4 class="m-b-0 text-white">Laporan bpom</h4> -->
                    <div class="row">

                        <div class="col-md-9">
                            <h4 class="m-b-0 text-black">Tabel Laporan bpom</h4>
                        </div>
                        <div class="col-md-3 text-right">
                            <button class="btn btn-info" id="btn_print" name="btn_print"><i class="fa fa-print"></i> PRINT</button> 
                            <button class="btn btn-info" id="btn_excel" name="btn_excel"><i class="fa fa-file-excel-o"></i> EXCEL</button>
                        </div>                        
                    </div>
                </div>
                <div class="card-body">
                    <div class="row" id="report">
                        <div class="col-md-12">
                            <div class="col-12 text-center" style="padding-bottom: 25px;">
                                <h3>REKAPITULASI LAPORAN OBAT</h3>
                                <h4><?php print_r($str_periode);?></h4>
                                 <!-- <span>Per-Tanggal ......</span> -->
                            </div>
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-sm-1">Nama</div>
                                    <div class="col-sm-11">: BLESSINDO FARMA (Pusat)</div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-1">No. Izin</div>
                                    <div class="col-sm-11">: <?php print_r($no_siup);?></div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-1">Alamat</div>
                                    <div class="col-sm-11">: JL. MAYJEN SUNGKONO KOMPLEKS WONOKRITI INDAH S-33 SURABAYA, Kota Surabaya - Jawa Timur </div>
                                </div>
                            </div>
                            <div class="col-md-12" style="font-size: 8px; font-weight: 300;">
                                <br>
                                <table border="1" class="table table-bordered table-striped" style="width:100%;"  >
                                    <thead>
                                        <tr>
                                            <th valign="top" style="font-weight: bold;" rowspan="2">NO.</th>
                                            <th valign="top" style="font-weight: bold;" rowspan="2">PERIODE</th>
                                            <th valign="top" style="font-weight: bold;" rowspan="2">KODE OBAT(NIE)</th>
                                            <th valign="top" style="font-weight: bold;" rowspan="2">NAMA OBAT</th>
                                            <th valign="top" style="font-weight: bold;" rowspan="2">STOK AWAL</th>
                                            <th style="font-weight: bold; text-align: center;" colspan="4">PEMASUKAN</th>
                                            <th style="font-weight: bold; text-align: center;" colspan="9">PENGELUARAN</th>
                                            <th valign="top" style="font-weight: bold;" rowspan="2">STOK AKHIR</th>
                                        </tr>
                                        <tr>
                                            <th>PABRIK</th>
                                            <th>PBF</th>
                                            <th>RETUR</th>
                                            <th>LAINNYA</th>
                                            <th>RUMAH SAKIT</th>
                                            <th>APOTIK</th>
                                            <th>PBF</th>
                                            <th>SARANA PEMERINTAH</th>
                                            <th>PUSKESMAS</th>
                                            <th>KLINIK</th>
                                            <th>TOKO OBAT</th>
                                            <th>RETUR</th>
                                            <th>LAINNYA</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php
                                        if(isset($list_data)){

                                            $no = 1;
                                            foreach ($list_data as $key => $value) {
                                                // print_r("<pre>");
                                                // print_r($value);
                                                

                                                print_r("<tr>
                                                            <td>".$no."</td>
                                                            <td>".$periode."</td>
                                                            <td>".$value["item_now"]->id_item."</td>
                                                            <td>".$value["item_now"]->nama_item.", ".$value["item_now"]->satuan." (".$value["item_now"]->kode_produksi_item.")</td>
                                                            <td align=\"right\">".$value["stok_before"]."</td>

                                                            <td align=\"right\">".$value["pemasukan"]["pabrik"]."</td>
                                                            <td align=\"right\">".$value["pemasukan"]["pbf"]."</td>
                                                            <td align=\"right\">".$value["pemasukan"]["retur"]."</td>
                                                            <td align=\"right\">".$value["pemasukan"]["lainnya"]."</td>
                                                            
                                                            <td align=\"right\">".$value["pengeluaran"]["rs"]."</td>
                                                            <td align=\"right\">".$value["pengeluaran"]["apotik"]."</td>
                                                            <td align=\"right\">".$value["pengeluaran"]["pbf"]."</td>
                                                            <td align=\"right\">".$value["pengeluaran"]["sp"]."</td>
                                                            <td align=\"right\">".$value["pengeluaran"]["pm"]."</td>
                                                            <td align=\"right\">".$value["pengeluaran"]["kl"]."</td>
                                                            <td align=\"right\">".$value["pengeluaran"]["to"]."</td>
                                                            <td align=\"right\">".$value["pengeluaran"]["retur"]."</td>
                                                            <td align=\"right\">".$value["pengeluaran"]["ln"]."</td>

                                                            <td align=\"right\">".$value["stok"]."</td>
                                                        </tr>");
                                                $no++;
                                            }
                                        }
                                        ?>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Card -->
        </div>
    </div>
    <!-- End Row -->
</div>
<!-- ============================================================== -->
<!-- --------------------------End Container fluid----------------  -->
<!-- ============================================================== -->

<script type="text/javascript">
    $(document).ready(function(){
        set_default();
    });

    function set_default(){
        $("#jenis_filter").val("<?php print_r($str_jenis);?>");

        var jenis_filter = $("#jenis_filter").val();
        switch(jenis_filter) {
          case "per_tgl":
                
            break;
          case "per_bulan":
                
            break;
          case "tiga_bulan":
                
            break;
          case "pertahun":
                 
            break;
          default:
            // code block
        }      
    }

    $("#jenis_filter").change(function(){
        var jenis_filter = $(this).val();
    });


    $("#lanjut").click(function(){
        var jenis_filter = $("#jenis_filter").val();
        switch(jenis_filter) {
          case "per_tgl":
                send_per_tgl();
            break;
          case "per_bulan":
                send_per_bulan(); 
            break;
          case "tiga_bulan":
                send_tiga_bulan();  
            break;
          case "pertahun":
                send_pertahun();  
            break;
          default:
            // code block
        }
    });

    function send_per_tgl(){
        window.location.href = "<?php print_r(base_url())?>report/reportbpom_new/get_bpom_tgl";
    }

    function send_tiga_bulan(){
        window.location.href = "<?php print_r(base_url())?>report/reportbpom_new/get_bpom_triwulan";
    }

    function send_per_bulan(){
        window.location.href = "<?php print_r(base_url())?>report/reportbpom_new/get_bpom_bulan";
    }

    function send_pertahun(){
        window.location.href = "<?php print_r(base_url())?>report/reportbpom_new/get_bpom_th";
    }



    $("#btn_print").click(function(){
        var jenis_filter = $("#jenis_filter").val();
        switch(jenis_filter) {
          case "per_tgl":
                print_send_per_tgl();
            break;
          case "per_bulan":
                print_send_per_bulan(); 
            break;
          case "tiga_bulan":
                print_send_tiga_bulan();  
            break;
          case "pertahun":
                print_send_pertahun();  
            break;
          default:
            // code block
        }
    });

    function print_send_per_tgl(){
        window.open("<?php print_r(base_url())?>report/reportbpom_new/print_get_bpom_tgl", "_blank");
    }

    function print_send_tiga_bulan(){
        window.open("<?php print_r(base_url())?>report/reportbpom_new/print_get_bpom_triwulan", "_blank");
    }

    function print_send_per_bulan(){
        window.open("<?php print_r(base_url())?>report/reportbpom_new/print_get_bpom_bulan", "_blank");
    }

    function print_send_pertahun(){
        window.open("<?php print_r(base_url())?>report/reportbpom_new/print_get_bpom_th", "_blank");
    }


    $("#btn_excel").click(function(){
        var jenis_filter = $("#jenis_filter").val();
        switch(jenis_filter) {
          case "per_tgl":
                excel_send_per_tgl();
            break;
          case "per_bulan":
                excel_send_per_bulan(); 
            break;
          case "tiga_bulan":
                excel_send_tiga_bulan();  
            break;
          case "pertahun":
                excel_send_pertahun();  
            break;
          default:
            // code block
        }
    });

    function excel_send_per_tgl(){
        window.open("<?php print_r(base_url())?>report/reportbpom_new/excel_get_bpom_tgl", "_blank");
    }

    function excel_send_tiga_bulan(){
        window.open("<?php print_r(base_url())?>report/reportbpom_new/excel_get_bpom_triwulan", "_blank");
    }

    function excel_send_per_bulan(){
        window.open("<?php print_r(base_url())?>report/reportbpom_new/excel_get_bpom_bulan", "_blank");
    }

    function excel_send_pertahun(){
        window.open("<?php print_r(base_url())?>report/reportbpom_new/excel_get_bpom_th", "_blank");
    }
    
</script>