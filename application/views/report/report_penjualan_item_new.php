<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Laporan Penjualan Obat Berdasarkan Produk</h3>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->

<?php
    $str_jenis = "";
    switch ($this->uri->segment(3)) {
        case 'get_penjualan_item_tgl':
            $str_jenis = "per_tgl";
            break;
        
        case 'get_penjualan_item_bulan':
            $str_jenis = "per_bulan";
            break;

        case 'get_penjualan_item_triwulan':
            $str_jenis = "tiga_bulan";
            break;

        case 'get_penjualan_item_th':
            $str_jenis = "pertahun";
            break;

        default:
            $str_jenis = "per_tgl";
            break;
    }

    $str_param1 = $this->uri->segment(4);
    
?>

<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- Row -->
    <div class="row">
        <div class="col-12 m-t-30">
            <!-- Card -->
            <div class="card">
                <div class="card-header">
                    <!-- <h4 class="m-b-0 text-white">Laporan Penjualan Obat Berdasarkan Produk</h4> -->
                    <div class="row">
                        <div class="col-md-9">
                            <h4 class="m-b-0 text-black">Filter Laporan Penjualan Obat Berdasarkan Produk</h4>
                        </div>
                        <div class="col-md-3">
                            <select class="custom-select col-12" id="jenis_filter" name="jenis_filter">
                                <option value="per_tgl">Per Tanggal Sekarang</option>
                                <option value="per_bulan">Per Bulan Sekarang</option>
                                <option value="tiga_bulan">Tiga Bulanan Sekarang</option>
                                <option value="pertahun">Per Tahun Sekarang</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-9">
                                    <div class="form-group row">
                                        <label for="example-search-input" class="col-md-3 col-form-label">Nama Produk</label>
                                        <div class="col-md-9">
                                            <select class="select2 form-control custom-select" id="item" name="item">
                                                <?php
                                                if(isset($item)){
                                                    foreach ($item as $key => $value) {
                                                        print_r("<option value=\"".$value->id_item."\">(".$value->id_item.") ".$value->nama_item." - ".$value->kode_produksi_item."</option>");
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 text-right">
                                    <button type="button" id="lanjut" name="lanjut" class="btn waves-effect waves-light btn-rounded btn-info">Terapkan</button>
                                </div>
                            </div>       
                        </div>
                    </div>
                </div>
            </div>
            <!-- Card -->
        </div>
    </div>
    <!-- End Row -->

    <!-- Row -->
    <div class="row">
        <div class="col-12 m-t-30">
            <!-- Card -->
            <div class="card">
                <div class="card-header">
                    <!-- <h4 class="m-b-0 text-white">Laporan Penjualan Obat Berdasarkan Produk</h4> -->
                    <div class="row">

                        <div class="col-md-9">
                            <h4 class="m-b-0 text-black">Tabel Laporan Penjualan Obat Berdasarkan Produk</h4>
                        </div>
                        <div class="col-md-3 text-right">
                            <button class="btn btn-info" id="btn_print" name="btn_print"><i class="fa fa-print"></i> PRINT</button> 
                            <button class="btn btn-info" id="btn_excel" name="btn_excel"><i class="fa fa-file-excel-o"></i> EXCEL</button>
                        </div>                        
                    </div>
                </div>
                <div class="card-body">
                    <div class="row" id="report">
                        <div class="col-md-12">
                            <div class="col-12 text-center">
                                <h3>LAPORAN TRANSAKSI PENJUALAN OBAT BERDASARKAN PRODUK</h3>
                                <h4><?php print_r($str_periode);?></h4>
                                 <!-- <span>Per-Tanggal ......</span> -->
                            </div>

                    <!-- Pembelian -->
                    <!-- Barang Masuk -->
                            <div class="col-md-12">
                                <br>
                                <label>Daftar Masuk (Pemebelian)</label>
                                <!-- <hr> -->
                            </div>
                            <div class="col-md-12" style="font-size: 11px; font-weight: 300;">
                                <table class="table table-bordered table-striped" style="width:100%" >
                                    <thead>
                                        <tr>
                                            <th style="font-weight: bold;">No.</th>
                                            <th style="font-weight: bold;">Tanggal</th>
                                            <th style="font-weight: bold;">No Faktur</th>
                                            <th style="font-weight: bold;">Suplier</th>
                                            <!-- <th style="font-weight: bold;">Id Produk</th>
                                            <th style="font-weight: bold;">Nama Produk</th> -->
                                            <th style="font-weight: bold;">Masuk</th>
                                            <th style="font-weight: bold;">RS</th>
                                            <th style="font-weight: bold;">PT</th>
                                            <th style="font-weight: bold;">APT</th>
                                            <th style="font-weight: bold;">Lainnya</th>
                                            <th style="font-weight: bold;">Total</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                    <?php
                                        $t_masuk = 0;
                                    // print_r($list_data);
                                        $nama_suplier = "";
                                        $first_val = 0;
                                        
                                        $val_sisa = 0;
                                        $no = 1;

                                        // if(isset($check_data)){
                                        //     $item_fine = $check_data[0]->stok_record_item;
                                        //     $item_broken = $check_data[0]->rusak_record_item;

                                        //     $first_val = $item_fine+$item_broken;
                                        // }

                                        // $val_sisa +=$first_val;


                                        if(isset($data_pembelian)){
                                            
                                            $t_pembelian = 0;
                                            foreach ($data_pembelian as $key => $value) {
                                                $val_rs = "";
                                                $val_pt = "";
                                                $val_apt = "";
                                                $val_lain = "";

                                                $nama_suplier = $value->nama_suplier;   

                                                $t_pembelian += $value->jml_item_tr_detail;

                                                $val_sisa += $value->jml_item_tr_detail;

                                                print_r("
                                                        <tr>
                                                            <td align=\"right\">".$no."</td>
                                                            <td align=\"right\">".$value->tgl_transaksi_tr_header."</td>
                                                            <td align=\"right\">".$value->id_tr_header."</td>
                                                            <td align=\"right\">".$value->nama_suplier."</td>

                                                            <td align=\"right\">".$value->jml_item_tr_detail."</td>
                                                            <td align=\"right\">".$val_rs."</td>
                                                            <td align=\"right\">".$val_pt."</td>
                                                            <td align=\"right\">".$val_apt."</td>
                                                            <td align=\"right\">".$val_lain."</td>
                                                            <td align=\"right\">".$t_pembelian."</td>
                                                        </tr>
                                                    ");

                                                $no++;
                                            }

                                            print_r("
                                                        <tr>
                                                            <td>#</td>
                                                            <td colspan=\"8\"><b>Total Pembelian</b></td>
                                                            <td align=\"right\">".$t_pembelian."</td>
                                                        </tr>
                                                    ");

                                            $t_masuk += $t_pembelian;
                                        }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                    <!-- Pembelian -->

                    <!-- Retur Penjualan -->
                    <!-- Barang Masuk -->
                            <div class="col-md-12">
                                <br>
                                <label>Daftar Masuk (Retur Penjualan)</label>
                                <!-- <hr> -->
                            </div>
                            <div class="col-md-12" style="font-size: 11px; font-weight: 300;">
                                <table id="myTable" class="table table-bordered table-striped" style="width:100%" >
                                    <thead>
                                        <tr>
                                            <th style="font-weight: bold;">No.</th>
                                            <th style="font-weight: bold;">Tanggal</th>
                                            <th style="font-weight: bold;">No Faktur</th>
                                            <th style="font-weight: bold;">Pelanggan</th>
                                            <!-- <th style="font-weight: bold;">Id Produk</th>
                                            <th style="font-weight: bold;">Nama Produk</th> -->
                                            <th style="font-weight: bold;">Masuk</th>
                                            <th style="font-weight: bold;">RS</th>
                                            <th style="font-weight: bold;">PT</th>
                                            <th style="font-weight: bold;">APT</th>
                                            <th style="font-weight: bold;">Lainnya</th>
                                            <th style="font-weight: bold;">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php

                                        if(isset($data_retur_penjualan)){
                                            $t_retur_penjualan = 0;
                                            foreach ($data_retur_penjualan as $key => $value) {
                                                $val_rs = "";
                                                $val_pt = "";
                                                $val_apt = "";
                                                $val_lain = "";

                                                switch ($value->jenis_rekanan) {
                                                    case 'rs':
                                                        $val_rs = $value->jml_item_tr_detail; 
                                                        break;

                                                    case 'apotik':
                                                        $val_apt = $value->jml_item_tr_detail;
                                                        break;

                                                    case 'pbf':
                                                        $val_pt = $value->jml_item_tr_detail;
                                                        break;
                                                    
                                                    default:
                                                        $val_lain = $value->jml_item_tr_detail;
                                                        break;
                                                }

                                                $t_retur_penjualan += $value->jml_item_tr_detail;

                                                $val_sisa += $value->jml_item_tr_detail;

                                                print_r("
                                                        <tr>
                                                            <td align=\"right\">".$no."</td>
                                                            <td align=\"right\">".$value->tgl_transaksi_tr_header."</td>
                                                            <td align=\"right\">".$value->id_tr_header_p."</td>
                                                            <td align=\"right\">".$value->nama_rekanan."</td>

                                                            <td align=\"right\"></td>
                                                            <td align=\"right\">".$val_rs."</td>
                                                            <td align=\"right\">".$val_pt."</td>
                                                            <td align=\"right\">".$val_apt."</td>
                                                            <td align=\"right\">".$val_lain."</td>
                                                            <td align=\"right\">".$t_retur_penjualan."</td>
                                                        </tr>
                                                    ");

                                                $no++;
                                            }

                                            print_r("
                                                        <tr>
                                                            <td>#</td>
                                                            <td colspan=\"8\"><b>Total Retur Penjualan</b></td>
                                                            <td align=\"right\">".$t_retur_penjualan."</td>
                                                        </tr>
                                                    ");

                                            $t_masuk += $t_retur_penjualan;
                                        }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                    <!-- Retur Penjualan -->

                    <!-- Retur Pengganti Retur Pembelian -->
                    <!-- Barang Masuk -->
                            <div class="col-md-12">
                                <br>
                                <label>Daftar Masuk (Pengganti Retur Pembelian)</label>
                                <!-- <hr> -->
                            </div>
                            <div class="col-md-12" style="font-size: 11px; font-weight: 300;">
                                <table id="myTable" class="table table-bordered table-striped" style="width:100%" >
                                    <thead>
                                        <tr>
                                            <th style="font-weight: bold;">No.</th>
                                            <th style="font-weight: bold;">Tanggal</th>
                                            <th style="font-weight: bold;">No Faktur</th>
                                            <th style="font-weight: bold;">Suplier</th>
                                            <!-- <th style="font-weight: bold;">Id Produk</th>
                                            <th style="font-weight: bold;">Nama Produk</th> -->
                                            <th style="font-weight: bold;">Masuk</th>
                                            <th style="font-weight: bold;">RS</th>
                                            <th style="font-weight: bold;">PT</th>
                                            <th style="font-weight: bold;">APT</th>
                                            <th style="font-weight: bold;">Lainnya</th>
                                            <th style="font-weight: bold;">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        if(isset($data_retur_pembelian_p)){
                                            // $val_sisa = 0;
                                            $t_retur_pembelian_p = 0;
                                            foreach ($data_retur_pembelian_p as $key => $value) {
                                                $val_rs = "";
                                                $val_pt = "";
                                                $val_apt = "";
                                                $val_lain = "";

                                                $nama_suplier = $value->nama_suplier;

                                                $t_retur_pembelian_p += $value->jml_item_tr_detail;

                                                // $val_sisa += $value->jml_item_tr_detail;

                                                print_r("
                                                        <tr>
                                                            <td align=\"right\">".$no."</td>
                                                            <td align=\"right\">".$value->tgl_transaksi_tr_header."</td>
                                                            <td align=\"right\">".$value->id_tr_header_p."</td>
                                                            <td align=\"right\">".$value->nama_suplier."</td>

                                                            <td align=\"right\">".$value->jml_item_tr_detail."</td>
                                                            <td align=\"right\">".$val_rs."</td>
                                                            <td align=\"right\">".$val_pt."</td>
                                                            <td align=\"right\">".$val_apt."</td>
                                                            <td align=\"right\">".$val_lain."</td>
                                                            <td align=\"right\">".$t_retur_pembelian_p."</td>
                                                        </tr>
                                                    ");

                                                $no++;
                                            }
                                            print_r("
                                                        <tr>
                                                            <td>#</td>
                                                            <td colspan=\"8\"><b>Total Barang Pengganti Retur Pembelian</b></td>
                                                            <td align=\"right\">".$t_retur_pembelian_p."</td>
                                                        </tr>
                                                    ");

                                            $t_masuk += $t_retur_pembelian_p;
                                        }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                    <!-- Retur Pengganti Retur Pembelian -->

                    <!-- Retur Pengganti Retur Sample -->
                    <!-- Barang Masuk -->
                            <div class="col-md-12">
                                <br>
                                <label>Daftar Masuk (Pengganti Sample)</label>
                                <!-- <hr> -->
                            </div>
                            <div class="col-md-12" style="font-size: 11px; font-weight: 300;">
                                <table id="myTable" class="table table-bordered table-striped" style="width:100%" >
                                    <thead>
                                        <tr>
                                            <th style="font-weight: bold;">No.</th>
                                            <th style="font-weight: bold;">Tanggal</th>
                                            <th style="font-weight: bold;">No Faktur</th>
                                            <th style="font-weight: bold;">Pelanggan</th>
                                            <!-- <th style="font-weight: bold;">Id Produk</th>
                                            <th style="font-weight: bold;">Nama Produk</th> -->
                                            <th style="font-weight: bold;">Masuk</th>
                                            <th style="font-weight: bold;">RS</th>
                                            <th style="font-weight: bold;">PT</th>
                                            <th style="font-weight: bold;">APT</th>
                                            <th style="font-weight: bold;">Lainnya</th>
                                            <th style="font-weight: bold;">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        

                                        if(isset($data_retur_sample_p)){
                                            $t_retur_sample_p = 0;
                                            // $val_sisa = 0;
                                            foreach ($data_retur_sample_p as $key => $value) {
                                                $val_rs = "";
                                                $val_pt = "";
                                                $val_apt = "";
                                                $val_lain = "";

                                                $val_sisa += $value->jml_item_tr_detail;

                                                $t_retur_sample_p += $value->jml_item_tr_detail;

                                                print_r("
                                                        <tr>
                                                            <td align=\"right\">".$no."</td>
                                                            <td align=\"right\">".$value->tgl_transaksi_tr_header."</td>
                                                            <td align=\"right\">".$value->id_tr_header_p."</td>
                                                            <td align=\"right\">".$nama_suplier."</td>

                                                            <td align=\"right\">".$value->jml_item_tr_detail."</td>
                                                            <td align=\"right\">".$val_rs."</td>
                                                            <td align=\"right\">".$val_pt."</td>
                                                            <td align=\"right\">".$val_apt."</td>
                                                            <td align=\"right\">".$val_lain."</td>
                                                            <td align=\"right\">".$t_retur_sample_p."</td>
                                                        </tr>
                                                    ");

                                                $no++;
                                            }

                                            print_r("
                                                        <tr>
                                                            <td>#</td>
                                                            <td colspan=\"8\"><b>Total Barang Pengganti Sample</b></td>
                                                            <td align=\"right\">".$t_retur_sample_p."</td>
                                                        </tr>
                                                    ");

                                            $t_masuk += $t_retur_sample_p;
                                        }


                                    ?>
                                    </tbody>
                                </table>
                            </div>
                    <!-- Retur Pengganti Retur Sample -->




                    <!-- Penjualan -->
                    <!-- Barang Keluar -->
                            <div class="col-md-12">
                                <br>
                                <label>Daftar Keluar (Penjualan)</label>
                                <!-- <hr> -->
                            </div>
                            <div class="col-md-12" style="font-size: 11px; font-weight: 300;">
                                <table id="myTable" class="table table-bordered table-striped" style="width:100%" >
                                    <thead>
                                        <tr>
                                            <th style="font-weight: bold;">No.</th>
                                            <th style="font-weight: bold;">Tanggal</th>
                                            <th style="font-weight: bold;">No Faktur</th>
                                            <th style="font-weight: bold;">Pelanggan</th>
                                            <!-- <th style="font-weight: bold;">Id Produk</th>
                                            <th style="font-weight: bold;">Nama Produk</th> -->
                                            <th style="font-weight: bold;">Masuk</th>
                                            <th style="font-weight: bold;">RS</th>
                                            <th style="font-weight: bold;">PT</th>
                                            <th style="font-weight: bold;">APT</th>
                                            <th style="font-weight: bold;">Lainnya</th>
                                            <th style="font-weight: bold;">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        $t_keluar = 0;
                                        if(isset($data_penjualan)){
                                            // $val_sisa = 0;
                                            $t_penjualan = 0;
                                            foreach ($data_penjualan as $key => $value) {
                                                // $first_val = 0;
                                                // if($key == 0){
                                                //     $item_fine = $check_data[0]->stok_record_item;
                                                //     $item_broken = $check_data[0]->rusak_record_item;

                                                //     $first_val = $item_fine+$item_broken;
                                                //     $val_sisa +=$first_val;
                                                // }

                                                $val_rs = "";
                                                $val_pt = "";
                                                $val_apt = "";
                                                $val_lain = "";

                                                switch ($value->jenis_rekanan) {
                                                    case 'rs':
                                                        $val_rs = $value->jml_item_tr_detail; 
                                                        break;

                                                    case 'apotik':
                                                        $val_apt = $value->jml_item_tr_detail;
                                                        break;

                                                    case 'pbf':
                                                        $val_pt = $value->jml_item_tr_detail;
                                                        break;
                                                    
                                                    default:
                                                        $val_lain = $value->jml_item_tr_detail;
                                                        break;
                                                }

                                                $t_penjualan += $value->jml_item_tr_detail;
                                                $val_sisa -= $value->jml_item_tr_detail;

                                                print_r("
                                                        <tr>
                                                            <td>".$no."</td>
                                                            <td>".$value->tgl_transaksi_tr_header."</td>
                                                            <td>".$value->id_tr_header."</td>
                                                            <td>".$value->nama_rekanan."</td>

                                                            <td align=\"right\"></td>
                                                            <td align=\"right\">".$val_rs."</td>
                                                            <td align=\"right\">".$val_pt."</td>
                                                            <td align=\"right\">".$val_apt."</td>
                                                            <td align=\"right\">".$val_lain."</td>
                                                            <td align=\"right\">".$t_penjualan."</td>
                                                        </tr>
                                                    ");

                                                $no++;
                                            }
                                            print_r("
                                                        <tr>
                                                            <td>#</td>
                                                            <td colspan=\"8\"><b>Total Penjualan</b></td>
                                                            <td align=\"right\">".$t_penjualan."</td>
                                                        </tr>
                                                    ");

                                            $t_keluar += $t_penjualan;
                                        }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                    <!-- Penjualan -->

                    <!-- Pengganti Retur Penjualan -->
                    <!-- Barang Keluar -->
                            <div class="col-md-12">
                                <br>
                                <label>Daftar Keluar (Pengganti Retur Penjualan)</label>
                                <!-- <hr> -->
                            </div>
                            <div class="col-md-12" style="font-size: 11px; font-weight: 300;">
                                <table id="myTable" class="table table-bordered table-striped" style="width:100%" >
                                    <thead>
                                        <tr>
                                            <th style="font-weight: bold;">No.</th>
                                            <th style="font-weight: bold;">Tanggal</th>
                                            <th style="font-weight: bold;">No Faktur</th>
                                            <th style="font-weight: bold;">Pelanggan</th>
                                            <!-- <th style="font-weight: bold;">Id Produk</th>
                                            <th style="font-weight: bold;">Nama Produk</th> -->
                                            <th style="font-weight: bold;">Masuk</th>
                                            <th style="font-weight: bold;">RS</th>
                                            <th style="font-weight: bold;">PT</th>
                                            <th style="font-weight: bold;">APT</th>
                                            <th style="font-weight: bold;">Lainnya</th>
                                            <th style="font-weight: bold;">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        if(isset($data_retur_penjualan_p)){
                                            $t_retur_penjualan_p = 0;
                                            // $val_sisa = 0;
                                            foreach ($data_retur_penjualan_p as $key => $value) {
                                                // $first_val = 0;
                                                // if($key == 0){
                                                //     $item_fine = $check_data[0]->stok_record_item;
                                                //     $item_broken = $check_data[0]->rusak_record_item;

                                                //     $first_val = $item_fine+$item_broken;
                                                //     $val_sisa +=$first_val;
                                                // }

                                                $val_rs = "";
                                                $val_pt = "";
                                                $val_apt = "";
                                                $val_lain = "";

                                                switch ($value->jenis_rekanan) {
                                                    case 'rs':
                                                        $val_rs = $value->jml_item_tr_detail; 
                                                        break;

                                                    case 'apotik':
                                                        $val_apt = $value->jml_item_tr_detail;
                                                        break;

                                                    case 'pbf':
                                                        $val_pt = $value->jml_item_tr_detail;
                                                        break;
                                                    
                                                    default:
                                                        $val_lain = $value->jml_item_tr_detail;
                                                        break;
                                                }

                                                $t_retur_penjualan_p += $value->jml_item_tr_detail;
                                                $val_sisa -= $value->jml_item_tr_detail;

                                                print_r("
                                                        <tr>
                                                            <td>".$no."</td>
                                                            <td>".$value->tgl_transaksi_tr_header."</td>
                                                            <td>".$value->id_tr_header_p."</td>
                                                            <td>".$value->nama_rekanan."</td>

                                                            <td align=\"right\"></td>
                                                            <td align=\"right\">".$val_rs."</td>
                                                            <td align=\"right\">".$val_pt."</td>
                                                            <td align=\"right\">".$val_apt."</td>
                                                            <td align=\"right\">".$val_lain."</td>
                                                            <td align=\"right\">".$t_retur_penjualan_p."</td>
                                                        </tr>
                                                    ");

                                                $no++;
                                            }
                                            print_r("
                                                        <tr>
                                                            <td>#</td>
                                                            <td colspan=\"8\"><b>Total Pengganti Retur Penjualan</b></td>
                                                            <td align=\"right\">".$t_retur_penjualan_p."</td>
                                                        </tr>
                                                    ");

                                            $t_keluar += $t_retur_penjualan_p;
                                        }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                    <!-- Pengganti Retur Penjualan -->

                    <!-- Retur Pembelian -->
                    <!-- Barang Keluar -->
                            <div class="col-md-12">
                                <br>
                                <label>Daftar Keluar (Retur Pembelian)</label>
                                <!-- <hr> -->
                            </div>
                            <div class="col-md-12" style="font-size: 11px; font-weight: 300;">
                                <table id="myTable" class="table table-bordered table-striped" style="width:100%" >
                                    <thead>
                                        <tr>
                                            <th style="font-weight: bold;">No.</th>
                                            <th style="font-weight: bold;">Tanggal</th>
                                            <th style="font-weight: bold;">No Faktur</th>
                                            <th style="font-weight: bold;">Suplier</th>
                                            <!-- <th style="font-weight: bold;">Id Produk</th>
                                            <th style="font-weight: bold;">Nama Produk</th> -->
                                            <th style="font-weight: bold;">Masuk</th>
                                            <th style="font-weight: bold;">RS</th>
                                            <th style="font-weight: bold;">PT</th>
                                            <th style="font-weight: bold;">APT</th>
                                            <th style="font-weight: bold;">Lainnya</th>
                                            <th style="font-weight: bold;">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        if(isset($data_retur_pembelian)){
                                            // $val_sisa = 0;
                                            $t_retur_pembelian = 0;
                                            foreach ($data_retur_pembelian as $key => $value) {
                                                // $first_val = 0;
                                                // if($key == 0){
                                                //     $item_fine = $check_data[0]->stok_record_item;
                                                //     $item_broken = $check_data[0]->rusak_record_item;

                                                //     $first_val = $item_fine+$item_broken;
                                                //     $val_sisa +=$first_val;
                                                // }

                                                $val_rs = "";
                                                $val_pt = "";
                                                $val_apt = "";
                                                $val_lain = "";

                                                // switch ($value->jenis_rekanan) {
                                                //     case 'rs':
                                                //         $val_rs = $value->jml_item_tr_detail; 
                                                //         break;

                                                //     case 'apotik':
                                                //         $val_apt = $value->jml_item_tr_detail;
                                                //         break;

                                                //     case 'pbf':
                                                        $val_pt = $value->jml_item_tr_detail;
                                                        // break;
                                                    
                                                //     default:
                                                //         $val_lain = $value->jml_item_tr_detail;
                                                //         break;
                                                // }

                                                $t_retur_pembelian += $value->jml_item_tr_detail;
                                                $val_sisa -= $value->jml_item_tr_detail;

                                                print_r("
                                                        <tr>
                                                            <td>".$no."</td>
                                                            <td>".$value->tgl_transaksi_tr_header."</td>
                                                            <td>".$value->id_tr_header_p."</td>
                                                            <td>".$value->nama_suplier."</td>

                                                            <td align=\"right\"></td>
                                                            <td align=\"right\">".$val_rs."</td>
                                                            <td align=\"right\">".$val_pt."</td>
                                                            <td align=\"right\">".$val_apt."</td>
                                                            <td align=\"right\">".$val_lain."</td>
                                                            <td align=\"right\">".$t_retur_pembelian."</td>
                                                        </tr>
                                                    ");

                                                $no++;
                                            }

                                            print_r("
                                                        <tr>
                                                            <td>#</td>
                                                            <td colspan=\"8\"><b>Total Retur Pembelian</b></td>
                                                            <td align=\"right\">".$t_retur_pembelian."</td>
                                                        </tr>
                                                    ");
                                            $t_keluar += $t_retur_pembelian;
                                        }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                    <!-- Retur Pembelian -->

                    <!-- Sample -->
                    <!-- Barang Keluar -->
                            <div class="col-md-12">
                                <br>
                                <label>Daftar Keluar (Sample)</label>
                                <!-- <hr> -->
                            </div>
                            <div class="col-md-12" style="font-size: 11px; font-weight: 300;">
                                <table id="myTable" class="table table-bordered table-striped" style="width:100%" >
                                    <thead>
                                        <tr>
                                            <th style="font-weight: bold;">No.</th>
                                            <th style="font-weight: bold;">Tanggal</th>
                                            <th style="font-weight: bold;">No Faktur</th>
                                            <th style="font-weight: bold;">Pelanggan</th>
                                            <!-- <th style="font-weight: bold;">Id Produk</th>
                                            <th style="font-weight: bold;">Nama Produk</th> -->
                                            <th style="font-weight: bold;">Masuk</th>
                                            <th style="font-weight: bold;">RS</th>
                                            <th style="font-weight: bold;">PT</th>
                                            <th style="font-weight: bold;">APT</th>
                                            <th style="font-weight: bold;">Lainnya</th>
                                            <th style="font-weight: bold;">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        if(isset($data_sample)){
                                            // $val_sisa = 0;
                                            $t_sample = 0;
                                            foreach ($data_sample as $key => $value) {
                                                // $first_val = 0;
                                                // if($key == 0){
                                                //     $item_fine = $check_data[0]->stok_record_item;
                                                //     $item_broken = $check_data[0]->rusak_record_item;

                                                //     $first_val = $item_fine+$item_broken;
                                                //     $val_sisa +=$first_val;
                                                // }

                                                $val_rs = "";
                                                $val_pt = "";
                                                $val_apt = "";
                                                $val_lain = "";

                                                switch ($value->jenis_rekanan) {
                                                    case 'rs':
                                                        $val_rs = $value->jml_item_tr_detail; 
                                                        break;

                                                    case 'apotik':
                                                        $val_apt = $value->jml_item_tr_detail;
                                                        break;

                                                    case 'pbf':
                                                        $val_pt = $value->jml_item_tr_detail;
                                                        break;
                                                    
                                                    default:
                                                        $val_lain = $value->jml_item_tr_detail;
                                                        break;
                                                }

                                                $t_sample += $value->jml_item_tr_detail;
                                                $val_sisa -= $value->jml_item_tr_detail;

                                                print_r("
                                                        <tr>
                                                            <td>".$no."</td>
                                                            <td>".$value->tgl_transaksi_tr_header."</td>
                                                            <td>".$value->id_tr_header."</td>
                                                            <td>".$value->nama_rekanan."</td>

                                                            <td align=\"right\"></td>
                                                            <td align=\"right\">".$val_rs."</td>
                                                            <td align=\"right\">".$val_pt."</td>
                                                            <td align=\"right\">".$val_apt."</td>
                                                            <td align=\"right\">".$val_lain."</td>
                                                            <td align=\"right\">".$t_sample."</td>
                                                        </tr>
                                                    ");

                                                $no++;
                                            }

                                            print_r("
                                                        <tr>
                                                            <td>#</td>
                                                            <td colspan=\"8\"><b>Total Sample</b></td>
                                                            <td align=\"right\">".$t_sample."</td>
                                                        </tr>
                                                    ");

                                            $t_keluar += $t_sample;
                                        }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                    <!-- Pengganti Sample -->


                    <!-- All -->
                    <!-- All -->
                            <?php
                            // print_r($item_v);
                                $id_item            = "";
                                $nama_item          = "";
                                $kode_produksi_item = "";
                                $stok               = 0;

                                if(isset($item_v)){
                                    if($item_v){
                                        $id_item            = $item_v->id_item;
                                        $nama_item          = $item_v->nama_item;
                                        $kode_produksi_item = $item_v->kode_produksi_item;
                                        $stok               = $item_v->stok;

                                        
                                    }
                                }
                                
                                $stok_awal = (int)$stok -(int)$t_masuk +(int)$t_keluar;
                                
                            ?>
                            <div class="col-md-12">
                                <br>
                                <label>Estimasi Stok Akhir</label>
                                <!-- <hr> -->
                            </div>
                            <div class="col-md-12" style="font-size: 11px; font-weight: 300;">
                                <table id="myTable" class="table table-bordered table-striped" style="width:100%" >
                                    <thead>
                                        <tr>
                                            <th style="font-weight: bold;">Kode Item</th>
                                            <th style="font-weight: bold;">Nama Item</th>
                                            <th style="font-weight: bold;">Kode Produksi Item</th>
                                            <th style="font-weight: bold;">Estimasi Stok Awal</th>
                                            <th style="font-weight: bold;">Total Barang Masuk</th>
                                            <th style="font-weight: bold;">Total Barang Keluar</th>
                                            <th style="font-weight: bold;">Stok Real</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><?php print_r($id_item); ?></td>
                                            <td><?php print_r($nama_item); ?></td>
                                            <td><?php print_r($kode_produksi_item); ?></td>
                                            <td><?php print_r($stok_awal); ?></td>
                                            <td><?php print_r($t_masuk); ?></td>
                                            <td><?php print_r($t_keluar); ?></td>
                                            <td><?php print_r($stok); ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                    <!-- All -->


                        </div>
                    </div>
                </div>
            </div>
            <!-- Card -->
        </div>
    </div>
    <!-- End Row -->
</div>
<!-- ============================================================== -->
<!-- --------------------------End Container fluid----------------  -->
<!-- ============================================================== -->

<script type="text/javascript">
    $(document).ready(function(){
        
        set_default();
        filter_show();

        console.log("");
    });

    function set_default(){
        $("#jenis_filter").val("<?php print_r($str_jenis);?>");
        $("#item").val("<?php print_r($str_param1);?>");

        var jenis_filter = $("#jenis_filter").val();
        switch(jenis_filter) {
          case "per_tgl":
                
            break;
          case "per_bulan":
                
            break;
          case "tiga_bulan":
                 
            break;
          case "pertahun":

            break;
          default:
            // code block
        }      
    }

    

    $("#jenis_filter").change(function(){
        var jenis_filter = $(this).val();
        filter_show();        
    });

    function filter_show(){
        var jenis_filter = $("#jenis_filter").val();
        switch(jenis_filter) {
          case "per_tgl":
                $("#row_per_tgl").removeAttr("hidden", true);

                $("#row_per_bulan").attr("hidden", true);
                $("#row_tiga_bulan").attr("hidden", true);
                $("#row_pertahun").attr("hidden", true);  
            break;
          case "per_bulan":
                $("#row_per_tgl").attr("hidden", true);

                $("#row_per_bulan").removeAttr("hidden", true);
                
                $("#row_tiga_bulan").attr("hidden", true);
                $("#row_pertahun").attr("hidden", true);  
            break;
          case "tiga_bulan":
                $("#row_per_tgl").attr("hidden", true);
                $("#row_per_bulan").attr("hidden", true);

                $("#row_tiga_bulan").removeAttr("hidden", true);
                
                $("#row_pertahun").attr("hidden", true);  
            break;
          case "pertahun":
                $("#row_per_tgl").attr("hidden", true);
                $("#row_per_bulan").attr("hidden", true);
                $("#row_tiga_bulan").attr("hidden", true);

                $("#row_pertahun").removeAttr("hidden", true);  
            break;
          default:
            // code block
        }
    }

    $("#lanjut").click(function(){
        var jenis_filter = $("#jenis_filter").val();
        switch(jenis_filter) {
          case "per_tgl":
                send_per_tgl();
            break;
          case "per_bulan":
                send_per_bulan(); 
            break;
          case "tiga_bulan":
                send_tiga_bulan();  
            break;
          case "pertahun":
                send_pertahun();  
            break;
          default:
            // code block
        }
    });

    function send_per_tgl(){
        var item = $("#item").val();

        window.location.href = "<?php print_r(base_url())?>report/reportpenjualanitem_new/get_penjualan_item_tgl/"+item;
    }

    function send_tiga_bulan(){
        var item = $("#item").val();

        window.location.href = "<?php print_r(base_url())?>report/reportpenjualanitem_new/get_penjualan_item_triwulan/"+item;
    }

    function send_per_bulan(){
        var item = $("#item").val();

        window.location.href = "<?php print_r(base_url())?>report/reportpenjualanitem_new/get_penjualan_item_bulan/"+item;
    }

    function send_pertahun(){
        var item = $("#item").val();

        window.location.href = "<?php print_r(base_url())?>report/reportpenjualanitem_new/get_penjualan_item_th/"+item;
    }




    $("#btn_print").click(function(){
        var jenis_filter = $("#jenis_filter").val();
        switch(jenis_filter) {
          case "per_tgl":
                print_send_per_tgl();
            break;
          case "per_bulan":
                print_send_per_bulan(); 
            break;
          case "tiga_bulan":
                print_send_tiga_bulan();  
            break;
          case "pertahun":
                print_send_pertahun();  
            break;
          default:
            // code block
        }
    });

    function print_send_per_tgl(){
        var item = $("#item").val();

        window.open("<?php print_r(base_url())?>report/reportpenjualanitem_new/print_get_penjualan_item_tgl/"+item, "_blank");
    }

    function print_send_tiga_bulan(){
        var item = $("#item").val();

        window.open("<?php print_r(base_url())?>report/reportpenjualanitem_new/print_get_penjualan_item_triwulan/"+item, "_blank");
    }

    function print_send_per_bulan(){
        var item = $("#item").val();

        window.open("<?php print_r(base_url())?>report/reportpenjualanitem_new/print_get_penjualan_item_bulan/"+item, "_blank");
    }

    function print_send_pertahun(){
        var item = $("#item").val();

        window.open("<?php print_r(base_url())?>report/reportpenjualanitem_new/print_get_penjualan_item_th/"+item, "_blank");
    }




    $("#btn_excel").click(function(){
        var jenis_filter = $("#jenis_filter").val();
        switch(jenis_filter) {
          case "per_tgl":
                excel_send_per_tgl();
            break;
          case "per_bulan":
                excel_send_per_bulan(); 
            break;
          case "tiga_bulan":
                excel_send_tiga_bulan();  
            break;
          case "pertahun":
                excel_send_pertahun();  
            break;
          default:
            // code block
        }
    });

    function excel_send_per_tgl(){
        var item = $("#item").val();

        window.open("<?php print_r(base_url())?>report/reportpenjualanitem_new/excel_get_penjualan_item_tgl/"+item, "_blank");
    }

    function excel_send_tiga_bulan(){
        var item = $("#item").val();

        window.open("<?php print_r(base_url())?>report/reportpenjualanitem_new/excel_get_penjualan_item_triwulan/"+item, "_blank");
    }

    function excel_send_per_bulan(){
        var item = $("#item").val();

        window.open("<?php print_r(base_url())?>report/reportpenjualanitem_new/excel_get_penjualan_item_bulan/"+item, "_blank");
    }

    function excel_send_pertahun(){
        var item = $("#item").val();

        window.open("<?php print_r(base_url())?>report/reportpenjualanitem_new/excel_get_penjualan_item_th/"+item, "_blank");
    }
    
</script>