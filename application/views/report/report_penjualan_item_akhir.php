<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Laporan Stok Produk Akhir</h3>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->

<?php
    $str_jenis = "";
    switch ($this->uri->segment(3)) {
        case 'get_penjualan_item_tgl':
            $str_jenis = "per_tgl";
            break;
        
        case 'get_penjualan_item_bulan':
            $str_jenis = "per_bulan";
            break;

        case 'get_penjualan_item_triwulan':
            $str_jenis = "tiga_bulan";
            break;

        case 'get_penjualan_item_th':
            $str_jenis = "pertahun";
            break;

        default:
            $str_jenis = "per_tgl";
            break;
    }

    $str_param1 = $this->uri->segment(4);
    $str_param2 = $this->uri->segment(5);
    $str_param3 = $this->uri->segment(6);

    if($str_param1 == ""){
        $str_param1 = date("Y-m-d");
    }

    if($str_param2 == ""){
        $str_param2 = date("Y-m-d");
    }


?>

<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- Row -->
    <div class="row">
        <div class="col-12 m-t-30">
            <!-- Card -->
            <div class="card">
                <div class="card-header">
                    <!-- <h4 class="m-b-0 text-white">Laporan Penjualan Obat Berdasarkan Produk</h4> -->
                    <div class="row">

                        <div class="col-md-9">
                            <h4 class="m-b-0 text-black">Tabel Laporan Stok Akhir Produk</h4>
                        </div>
                        <div class="col-md-3 text-right">
                            <button class="btn btn-info" id="btn_print" name="btn_print"><i class="fa fa-print"></i> PRINT</button> 
                            <button class="btn btn-info" id="btn_excel" name="btn_excel"><i class="fa fa-file-excel-o"></i> EXCEL</button>
                        </div>                        
                    </div>
                </div>
                <div class="card-body">
                    <div class="row" id="report">
                        <div class="col-md-12">
                            <div class="col-12 text-center">
                                <h3>LAPORAN STOK PRODUK AKHIR</h3>
                                <h4><?php print_r($str_periode);?></h4>
                                 <!-- <span>Per-Tanggal ......</span> -->
                            </div>

                            <div class="col-md-12" style="font-size: 11px; font-weight: 300;">
                                <table id="myTable" class="table table-bordered table-striped" style="width:100%; font-size: 11px;" >
                                    <thead>
                                        <tr>
                                            <th style="font-weight: bold;" width="5%">No.</th>
                                            <th style="font-weight: bold;" width="15%">Kode Produk</th>
                                            <th style="font-weight: bold;" width="20%">Nama Produk</th>
                                            <th style="font-weight: bold;" width="7%">Satuan</th>
                                            <!-- <th style="font-weight: bold;">Id Produk</th>
                                            <th style="font-weight: bold;">Nama Produk</th> -->
                                            <th style="font-weight: bold;" width="13%">Kode Produksi</th>
                                            <th style="font-weight: bold;" width="15%">Harga Satuan</th>
                                            <th style="font-weight: bold;" width="15%">Total Harga</th>
                                            <th style="font-weight: bold;" width="15%">Stok Produk</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    
                                        if(isset($array_data_main)){
                                            $no = 1;
                                            $t_harga_all = 0;
                                            foreach ($array_data_main as $key => $value) {
                                                $val_sisa = $value->stok;
                                                $val_harga_satuan = $value->harga_bruto;

                                                $val_t_harga = (float)$val_sisa * (float)$val_harga_satuan;

                                                $t_harga_all += $val_t_harga;

                                                $str_harga_satuan = number_format($val_harga_satuan, 0, ",", ".");
                                                $str_t_harga = number_format($val_t_harga, 0, ",", ".");
                                                print_r("
                                                        <tr>
                                                            <td>".$no."</td>
                                                            <td align=\"left\">".$value->id_item."</td>
                                                            <td align=\"left\">".$value->nama_item."</td>
                                                            <td align=\"left\">".$value->satuan."</td>

                                                            <td align=\"left\">".$value->kode_produksi_item."</td>

                                                            <td align=\"right\">".$str_harga_satuan."</td>
                                                            <td align=\"right\">".$str_t_harga."</td>
                                                            <td align=\"right\">".$val_sisa."</td>
                                                        </tr>
                                                    ");

                                                $no++;
                                            }

                                            $str_t_harga_all = number_format($t_harga_all, 0, ",", ".");

                                            print_r("
                                                        <tr>
                                                            <td colspan=\"6\" align=\"center\">Total</td>
                                                            <td align=\"right\">".$str_t_harga_all."</td>
                                                            <td align=\"right\">-</td>
                                                        </tr>
                                                    ");
                                        }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Card -->
        </div>
    </div>
    <!-- End Row -->
</div>
<!-- ============================================================== -->
<!-- --------------------------End Container fluid----------------  -->
<!-- ============================================================== -->


<script type="text/javascript">
    $("#btn_print").click(function(){
        print_send_main();
    });

    function print_send_main(){
        window.open("<?php print_r(base_url())?>report/reportpenjualanitemakhir/print_get_item_main", "_blank");
    }

    $("#btn_excel").click(function(){
        excel_send_main();
    });

    function excel_send_main(){
        window.open("<?php print_r(base_url())?>report/reportpenjualanitemakhir/excel_get_item_main/", "_blank");
    }

    
</script>