<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Update Stok Produk</h3>
    </div>
    
    <div>
        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->


<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
     <!-- Row -->
    <div class="row">
        <div class="col-12 m-t-30">
            <!-- Card -->
            <div class="card card-outline-info">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">Data Produk</h4>

                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12" id="out">
                            <div class="table-responsive m-t-40">
                                <table id="myTable" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th width="15%">(Kode Produk) Nama Produk</th>
                                            <th width="10%">Kode Produksi</th>
                                            <th width="10%">Tgl. Kadaluarsa</th>
                                            <th width="10%">Stok (Satuan)</th>
                                            <th width="10%">Stok Rusak(Satuan)</th>

                                            <th width="10%">Harga Beli</th>
                                            <!-- <th width="11%">Harga Netto</th> -->
                                            <th width="11%">Harga Jual</th>
                                            <th width="13%">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody id="main_table_content">
                                        <?php
                                        if($list_data){
                                            foreach ($list_data as $key => $value) {
                                                $str_btn_action = 
                                                "<center>
                                                    <button class=\"btn btn-info\" id=\"up_data\" onclick=\"update_data('".$value->id_item."')\" style=\"width: 40px;\"><i class=\"fa fa-pencil-square-o\" ></i></button>
                                                </center>";
                                                print_r("<tr>
                                                            <td>(".$value->id_item.") ".$value->nama_item."</td>
                                                            <td>".$value->kode_produksi_item."</td>
                                                            <td>".$value->tgl_kadaluarsa_item."</td>
                                                            <td align=\"right\">".$value->stok." (".$value->satuan.")</td>
                                                            <td align=\"right\">".$value->stok_opnam." (".$value->satuan.")</td>
                                                            <td align=\"right\">Rp. ".number_format($value->harga_bruto, 2, ",", ".")."</td>
                                                            <td align=\"right\">Rp. ".number_format($value->harga_jual, 2, ",", ".")."</td>
                                                            <td>".$str_btn_action."</td>
                                                        </tr>");
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="text-right">
                        <label class="form-label">Keterangan Tombol Aksi ==> </label>

                        <a class="btn btn-info" style="width: 40px;"><i class="fa fa-pencil-square-o" style="color: white;"></i></a>
                        <label class="form-label text-info">Update Data</label>
                    </div>
                </div>
            </div>
            <!-- Card -->
        </div>
    </div>
    <!-- End Row -->
</div>

<!-- ============================================================== -->
<!-- --------------------------update_data------------------------ -->
<!-- ============================================================== -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="update_data" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel1">Form Ubah Stok Produk</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="message-text" class="control-label">Nama Item
                                <span style="color: red;">*</span></label>
                            <input type="text" class="form-control" id="_nama_item" name="nama_item" readonly="" required="">
                            <p id="_msg_nama_item" style="color: red;"></p>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="message-text" class="control-label">Kode Produksi
                                <span style="color: red;">*</span></label>
                            <input type="text" class="form-control" id="_kode_produksi" name="kode_produksi" readonly="" required="">
                            <p id="_msg_kode_produksi" style="color: red;"></p>
                        </div>
                    </div>


                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="message-text" class="control-label">Stok
                                <span style="color: red;">*</span></label>
                            <input type="number" class="form-control" id="_stok" name="stok" placeholder="0000" required="">
                            <p id="_msg_stok" style="color: red;"></p>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="message-text" class="control-label">Stok Rusak
                                <span style="color: red;">*</span></label>
                            <input type="number" class="form-control" id="_stok_opnam" name="stok_opnam" placeholder="0000" required="">
                            <p id="_msg_stok_opnam" style="color: red;"></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="col-md-12">
                    <div class="form-group text-right">
                        <button type="submit" id="btn_update_data" class="btn waves-effect waves-light btn-rounded btn-info">Ubah</button>
                        <button type="button" id="btn_update_reset" class="btn waves-effect waves-light btn-rounded btn-danger">Reset</button>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- --------------------------update_data------------------------ -->
<!-- ============================================================== -->



<!-- ============================================================== -->
<!-- --------------------------End Container fluid----------------  -->
<!-- ============================================================== -->

<script type="text/javascript">
    var id_global; 

    $(document).ready(function(){
       clear_form();
    });

    //=========================================================================//
    //-----------------------------------master_function-----------------------//
    //=========================================================================//
        function currency(x){
            return x.toLocaleString('us-EG');
        }

        function clear_form(){
            $("#_stok_opnam").val("");
            $("#_stok").val("");
            $("#_nama_item").val("");
            $("#_kode_produksi").val("");
            
            $("#_msg_stok").html("");
            $("#_msg_stok_opnam").html("");
        }

        function clear_form_update(){
            $("#_stok_opnam").val("");
            $("#_stok").val("");
            $("#_nama_item").val("");
            $("#_kode_produksi").val("");
            
            $("#_msg_stok").html("");
            $("#_msg_stok_opnam").html("");
        }

        function change_currency(val_number){
            var str_return;
            if(val_number != ""){
                str_return = "Rp. "+currency(parseFloat(val_number));
            }else{
                str_return = "Rp. 0";
            }

            return str_return;
        }


        function create_alert(title, msg, status){
            $(function() {
                "use strict";                      
               $.toast({
                heading: title,
                text: msg,
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: status,
                hideAfter: 3500, 
                stack: 6
              });
            });
        }

        function render_data(data){
            $("#out").html(data);
        }
    //=========================================================================//
    //-----------------------------------master_function-----------------------//
    //=========================================================================//

    

    //=========================================================================//
    //-----------------------------------get_update----------------------------//
    //=========================================================================//
        function update_data(id_data){
            var data_main = new FormData();
                data_main.append('id_item', id_data);

            id_global = id_data;

            $.ajax({
                url: "<?php echo base_url()."admin/updateitem/get_data_item";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    
                    
                },
                complete: function(res){
                    console.log(res.responseText);
                    response_set_update(res.responseText);

                    id_global = id_data;
                }
            });
        }

        function response_set_update(res){
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;

            if (main_msg.status) {
                $("#update_data").modal("show");

                $("#_nama_item").val(detail_msg.data.nama_item);
                $("#_kode_produksi").val(detail_msg.data.kode_produksi_item);
                
                $("#_stok").val(detail_msg.data.stok);
                $("#_stok_opnam").val(detail_msg.data.stok_opnam);
            } else {
                clear_form_update();
            }
        }
    //=========================================================================//
    //-----------------------------------get_update----------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------form_update---------------------------//
    //=========================================================================//
        $("#btn_update_data").click(function(){
            var data_main = new FormData();
                data_main.append('id_item'      , id_global);

                data_main.append('stok'         , $("#_stok").val());
                data_main.append('stok_opnam'   , $("#_stok_opnam").val());

            $.ajax({
                url: "<?php echo base_url()."admin/updateitem/update_item";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    
                    
                },
                complete: function(res){
                    console.log(res.responseText);
                    response_update(res.responseText);
                }
            });
        });

        $("#btn_update_reset").click(function(){
            clear_form_update();
        });

        function response_update(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                $("#update_data").modal('toggle');
                create_alert('Proses Berhasil', main_msg.msg, 'success');
                render_data(detail_msg.list_data);
                clear_form_update();
            } else {
                
                $("#_msg_stok").html(detail_msg.stok);
                $("#_msg_stok_opnam").html(detail_msg.stok_opnam);
                
                create_alert('Proses Gagal', main_msg.msg, 'error');
            }
        }
    //=========================================================================//
    //-----------------------------------form_update---------------------------//
    //=========================================================================//  
    
</script>