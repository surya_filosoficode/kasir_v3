<!DOCTYPE html>
<html>
<head>
	<title>Faktur PT. Blesindo</title>
</head>
<body>
<?php
	$array_of_month = ["", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
	$array_of_day = [
					"Sunday"=>"Senin",
					"Monday"=>"Senin",
					"Tuesday"=>"Selasa",
					"Wednesday"=>"Rabu",
					"Thursday"=>"Kamis",
					"Friday"=>"Jumat",
					"Saturday"=>"Sabtu"
				];
	if(isset($data_header) && isset($data_detail)){
		$nama_rekanan 				= $data_header[0]->nama_rekanan;
		$id_customer 				= $data_header[0]->id_customer;
		$tgl_transaksi_tr_header 	= $data_header[0]->tgl_transaksi_tr_header;
		$id_tr_header 				= $data_header[0]->id_tr_header;
		$alamat_ktr_rekanan			= $data_header[0]->alamat_ktr_rekanan;


		$nama_sales 				= $data_header[0]->nama_sales;
		$tempo_tr_header			= $data_header[0]->tempo_tr_header;


		$total_pembayaran_tr_header 	 = $data_header[0]->total_pembayaran_tr_header;
		$disc_all_tr_header				 = $data_header[0]->disc_all_tr_header;
		$total_pembayaran_disc_tr_header = $data_header[0]->total_pembayaran_disc_tr_header;
		$ppn_tr_header					 = $data_header[0]->ppn_tr_header;
		$total_pembayaran_pnn_tr_header	 = $data_header[0]->total_pembayaran_pnn_tr_header;

		$tempo_tr_header 				 = $data_header[0]->tempo_tr_header;


		$str_total_pembayaran_tr_header = floor($total_pembayaran_tr_header);

		$t_nominal_disc = floor(floor($str_total_pembayaran_tr_header) * ($disc_all_tr_header / 100));
		$t_nominal_ppn = floor(floor(($str_total_pembayaran_tr_header - floor($t_nominal_disc))) * ($ppn_tr_header / 100));

		$str_total_after_ppn = floor($str_total_pembayaran_tr_header - $t_nominal_disc + $t_nominal_ppn);


		$timestamp_tr = strtotime($tgl_transaksi_tr_header);
		$str_timestamp_tr = date("l", $timestamp_tr);
		$hari_tr = $array_of_day[$str_timestamp_tr];
		$tgl_tr = date("d", $timestamp_tr);
		$bln_tr = $array_of_month[(int)date("m", $timestamp_tr)];
		$th_tr = date("Y", $timestamp_tr);

		$str_tr = $hari_tr.", ".$tgl_tr." ".$bln_tr." ".$th_tr;

		$datetime_tr = new DateTime($tgl_transaksi_tr_header);
		$datetime_tempo = new DateTime($tempo_tr_header);
		

		$diff_tempo = "0";
		$str_tgl_tempo = "&nbsp;&nbsp;/&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;&nbsp;";


		$timestamp_tempo = strtotime($tempo_tr_header);
		$str_timestamp_tempo = date("l", $timestamp_tempo);
		
		$tgl_tempo = date("d", $timestamp_tempo);
		$bln_tempo = date("m", $timestamp_tempo);
		$th_tempo  = date("Y", $timestamp_tempo);

		if($data_header[0]->cara_pembayaran_tr_header == "1"){
			$diff_tempo = $datetime_tr->diff($datetime_tempo)->days;
			$str_tgl_tempo = $tgl_tempo."/".$bln_tempo."/".$th_tempo;
		}
		
		$no = 1;
		$str_tbl = "";
		foreach ($data_detail as $key => $value) {
			$th = "";
			$m = "";
			$d = "";

			$str_date = "";
			$array_date = explode("-", $value->tgl_kadaluarsa_item);
			if($array_date){
				$th = $array_date[0];
				$m = $array_date[1];
				$d = $array_date[2];

				$str_date =$m."/".$th;
				
			}

			$str_tbl .= "<tr>
						<td align=\"center\" style=\"margin: 0px\">".$no."</td>
						<td style=\"margin: 0px\">
							<table cellspacing='0' border=\"0\" style=\"width:100%; border-collapse: collapse; margin: 0px;\">
								<tr>
									<td width=\"65%\" style=\"margin: 0px\">".$value->id_item." - ".ucwords($value->nama_item)."</td>
									<td style=\"text-align: right; margin: 0px;\">".$value->kode_produksi_item." - ".$str_date."</td>
								</tr>
								
							</table>
						</td>
						<td align=\"center\" style=\"margin: 0px\">".$value->satuan."</td>
						<td align=\"right\" style=\"margin: 0px\">".$value->jml_item_tr_detail."</td>
						<td align=\"right\" style=\"margin: 0px\">".number_format(ceil($value->harga_satuan_tr_detail), 0, '.', ',')."</td>
						<td align=\"right\" style=\"margin: 0px\">".$value->disc_item_tr_detail."</td>
						<td style='text-align:right; margin: 0px'>".number_format(ceil($value->harga_total_fix_tr_detail), 0, '.', ',')."</td>
					</tr>";
			$no++;
		}

		$str_tbl_next = "";
		for ($i=$no; $i <= 14 ; $i++) { 
			$str_tbl_next .= "<tr>
						<td align=\"center\">&nbsp;</td>
						<td align=\"center\"></td>
						<td align=\"center\"></td>
						<td align=\"right\"></td>
						<td align=\"right\"></td>
						<td align=\"right\"></td>
						<td></td>
					</tr>";
		}


		$no_siup = "";
		$no_npwp = "";
		if(isset($setting)){
			$array_npwp = array();
			$array_siup = array();
			foreach ($setting as $key => $value) {
				if($value->jenis_setting == "npwp"){
					$array_npwp = $value;
					$no_npwp = $value->keterangan_setting;
				}elseif ($value->jenis_setting == "siup") {
					$array_siup = $value;
					$no_siup = $value->keterangan_setting;
				}
			}
		}

?>
	<center>

		<table width="100%" border="0" cellpadding="0" cellspacing="0" style='font-family:arial; border-collapse: collapse; margin-top: 5px'>
			<td align='center'style='vertical-align:top'>	
				<span style='font-size:17pt'><b>PT. BLESSINDO FARMA</b></span>
				</br><span style='font-size:12.5pt'>JL. Mayjend Soengkono Kompleks Wonokriti Indah S-33</span>
				</br><span style='font-size:12.5pt'>Telp. (031)5673150, Fax. (031)5668523</span>
				</br><span style='font-size:12.5pt'>Surabaya</span>	
			</td>
		</table>
		<!-- <table style='width:1200px; font-family:calibri; border-collapse: collapse; margin-bottom: 20px' border = '0' >
			
		</table> -->
		<table width="100%" border="0" cellpadding="0" cellspacing="0" style='font-family:arial; border-collapse: collapse; margin-top: -10px;'>
			<tr>
				<td align="left">
					<span style='font-size:12.5pt'>IZIN PBF No.<?php print_r($no_siup);?></span>
				</td>
				<td align="right">
					<span style='font-size:12.5pt'>NPWP: <?php print_r($no_npwp);?></span>
				</td>
			</tr>
			<!-- <tr style="height: 5px;">
				<td colspan="2">
					<b></b>
				</td>
			</tr> -->
		</table>

		<hr size='2px' color='black' style="margin-top: -2px; margin-bottom: 3px;">

		<!-- <table style='width:60%; font-family:calibri; border-collapse: collapse;' border = '0' >
			
		</table> -->



		<table width="100%" border="0" cellpadding="0" cellspacing="0" style='font-family:arial; border-collapse: collapse; margin-top: 0px;'>
			<tr>
				<td align="left" style="width: 45%; font-size:12.5pt">
					<table border="0" width="100%" cellpadding="0" cellspacing="0" style='border-collapse: collapse;'>
						<tr>
							<td width="25%"><span>Nomor</span></td>
							<td width="3%">:</td>
							<td width="72%"><span><?php print_r($id_tr_header);?></span></td>
						</tr>
						<tr>
							<td><span>Hari, Tanggal</span></td>
							<td>:</td>
							<td><span><?php print_r($str_tr);?></span></td>
						</tr>
						<tr>
							<td><span>Customer</span></td>
							<td>:</td>
							<td><span><?php print_r($id_customer);?> - <?php print_r(ucwords($nama_rekanan));?></span></td>
						</tr>
						<tr>
							<td><span>Alamat</span></td>
							<td>:</td>
							<td><span><?php print_r(ucwords($alamat_ktr_rekanan));?></span></td>
						</tr>
					</table>
				</td>
				<td valign="top" style="font-size: 18pt; width: 15%;text-align: center;">
					<span><b><u>FAKTUR</u></b></span>
				</td>
				
				<td align="right" valign="bottom" style="padding-right:1px; width: 40%; font-size:12.5pt">

					<table border="0" width="100%" cellpadding="0" cellspacing="0" style='border-collapse: collapse; margin-left: 40px;'>
						<tr>
							<td width="35%"><span>Salesman</span></td>
							<td width="3%">:</td>
							<td width="57%"><span><?php print_r(ucwords($nama_sales));?></span></td>
						</tr>
						<tr>
							<td><span>Tanggal Jatuh Tempo</span></td>
							<td>:</td>
							<td><span><?php print_r($diff_tempo);?> Hari, &nbsp;<?php print_r($str_tgl_tempo);?></span></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>

		<!-- <table style='width:60%; font-size:8pt; font-family:calibri; border-collapse: collapse; ' border = '0'>
			
		</table> -->


		<table width="100%" border="1" cellpadding="-1" cellspacing="0" style='font-size:13pt; font-family:arial;  border-collapse: collapse; margin-top: 5px;'>
			<tr align='center'>
				<td width='3%'>No</td>
				<td width='*'>Jenis Barang</td>
				<!-- <td width='15%'>Tgl. Kadaluarsa</td> -->
				<td width='7%'>Sat</td>
				<td width='5%'>Qty</td>
				<td width='10%'>Harga</td>
				<td width='3%'>%</td>
				<td width='12%'>Jumlah</td>
			</tr>
			
			<?php print_r($str_tbl);?>
			<?php print_r($str_tbl_next);?>
		</table>

		<!-- <table cellspacing='0' style='width:60%; font-size:10pt; font-family:calibri;  border-collapse: collapse; margin-top: 20px;' border="1">
			
		</table> -->


		<table width="100%" cellpadding="0" cellspacing="0" border="0" style='font-size:13pt; font-family:arial;  border-collapse: collapse; margin-top: 3px;'>
			<tr>
				<td width="22%" align="left" style="padding-right: 30px;">
					<table>
						<tr >
							<td style='padding-bottom: 65px;'>Penerima / Stempel</td>
						</tr>
						<tr>
							<td><hr></td>
						</tr>
					</table>
				</td>
				<td width="15%" align="left" style="padding-right: 20px;">
					<table>
						<tr >
							<td style='padding-bottom: 65px; padding-right:30px; text-align: center;'>Penjual</td>
						</tr>
						<tr>
							<td><hr></td>
						</tr>
					</table>
				</td>
				<td valign="top" style='border:1px solid black; font-size:12pt; padding:5px; text-align:left; width:30%;'>
					<span><center>PERHATIAN</center></span>
					<span>1. Barang harap diperiksa dengan baik</span><br />
					<span>2. Kekurangan setelah pengiriman diluar tanggung jawab kami</span><br />
					<span>3. Barang yg telah dibeli tak dapat dikembalikan</span>
				</td>
				<td>
					<table align="right" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse;">
						<tr align="right">
							<td>Sub Total</td>
							<td>:</td>
							<td><?php print_r(number_format(floor($str_total_pembayaran_tr_header), 0, '.', ','));?></td>
						</tr>
						<tr align="right">
							<td>Disc 1(<?php print_r($disc_all_tr_header); ?>%)</td>
							<td>:</td>
							<td><?php print_r(number_format($t_nominal_disc, 0, '.', ','));?></td>
						</tr>
						<!-- <tr align="right">
							<td>Disc 2</td>
							<td>:</td>
							<td></td>
						</tr> -->
						<tr>
							<td></td>
							<td colspan="2"><hr></td>
						</tr>
						<tr align="right">
							<td></td>
							<td colspan="2"><?php print_r(number_format(floor($str_total_pembayaran_tr_header-$t_nominal_disc), 0, '.', ','));?></td>
						</tr>
						<tr align="right">
							<td>PPN (<?php print_r($ppn_tr_header); ?>%)</td>
							<td>:</td>
							<td><?php print_r(number_format(floor($t_nominal_ppn), 0, '.', ','));?></td>
						</tr>
						<tr align="right">
							<td>Grand Total</td>
							<td>:</td>
							<td><?php print_r(number_format(floor($str_total_after_ppn), 0, '.', ','));?></td>
						</tr>
					</table>
				</td>	
			</tr>
		</table>

		<!-- <table cellspacing='0' style='width:60%; font-size:10pt; font-family:calibri;  border-collapse: collapse; margin-top: 20px;' border="0">
			
			<tr>
				<td width="15%" align="left" style="padding-right: 20px;">
					<span><hr width="80px;"></span>
				</td>
				<td width="15%" align="left" style="padding-right: 20px;">
					<span><hr width="80px;"></span>
				</td>
			</tr>
		</table> -->
	</center>
<?php		
	}
?>

</body>

<script type="text/javascript">
	window.print();
</script>
</html>