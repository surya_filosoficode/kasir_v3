<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <!-- Bootstrap Core CSS -->
    <link href="<?php print_r(base_url());?>assets/template/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    
    <!-- Custom CSS -->
    <link href="<?php print_r(base_url());?>assets/template/main/css/style.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="<?php print_r(base_url());?>assets/template/main/css/colors/blue.css" id="theme" rel="stylesheet">
</head>

<div class="table-responsive m-t-40">
    <table id="myTable" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th width="10%">No</th>
                <th width="30%">Jenis Setting</th>
                <th width="30%">Nilai Setting</th>
                <th width="20%">Aksi</th>
            </tr>
        </thead>
        <tbody id=\"main_table_content\">
            <?php
                if($list_data){
                    $no = 1;
                    foreach ($list_data as $key => $value) {
                        $jenis_setting = "Nomor NPWP";
                        if($value->jenis_setting == "siup"){
                            $jenis_setting = "Nomor SIUP";
                        }


                        $str_btn_action = 
                        "<center>".
                            "<button class=\"btn btn-info\" id=\"up_data\" onclick=\"update_data('".$value->id_setting."')\" style=\"width: 40px;\"><i class=\"fa fa-pencil-square-o\" ></i></button>&nbsp;&nbsp;".
                            // "<button class=\"btn btn-danger\" id=\"del_data\" onclick=\"delete_data('".$value->id_setting."')\" style=\"width: 40px;\"><i class=\"fa fa-trash-o\"></i></button>".
                        "</center>";
                        print_r("<tr>
                                    <td>".$no."</td>
                                    <td>".$jenis_setting."</td>
                                    <td>".$value->keterangan_setting."</td>
                                    <td>".$str_btn_action."</td>
                                </tr>");
                        $no++;
                    }
                }
            ?>
        </tbody>
    </table>
</div>
    <script>
    $(document).ready(function() {
        $('#myTable').DataTable();
        $(document).ready(function() {
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function(settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function(group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function() {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
        });
    });
    $('#example23').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
    </script>