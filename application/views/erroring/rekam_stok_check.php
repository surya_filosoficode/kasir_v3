<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Periksa Rekam Stok</h3>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->


<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- Row -->
    <div class="row">
        <div class="col-12 m-t-30">
            <!-- Card -->
            <div class="card card-outline-info">
                <div class="card-header">
                    <!-- <h4 class="m-b-0 text-white">List Penjualan</h4> -->
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="m-b-0 text-white">List Stok</h4>
                        </div>
                        <!-- <div class="col-md-6 text-right">
                            <a href="<?php print_r(base_url());?>erroring/checksaveitem/rekam_stok" style="color: #ffff;">
                                <i class="fa fa-plus-square"></i>&nbsp;&nbsp;Periksa Rekam Stok Akhir Hari Ini
                            </a>
                        </div> -->
                    </div>
                </div>
                <div class="card-body">
                    <div class="card">
                        <div class="card-body font_edit font_color">
                            <!-- <div class="table-responsive m-t-40"> -->
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th width="15%">(Kode Produk) Nama Produk</th>
                                            <th width="10%">Kode Produksi</th>
                                            <th width="10%">Periode Record Item</th>

                                            <th width="10%">Stok (Terekam)</th>
                                            <th width="10%">Stok Rusak(Terekam)</th>

                                            <th width="10%">Stok (Real)</th>
                                            <th width="10%">Stok Rusak(Real)</th>

                                            <!-- <th width="10%">Harga Beli</th> -->
                                            <!-- <th width="11%">Harga Netto</th> -->
                                            <!-- <th width="11%">Harga Jual</th> -->
                                            <!-- <th width="13%">Aksi</th> -->
                                        </tr>
                                    </thead>
                                    <tbody id=\"main_table_content\">
                                        <?php
                                            if($list_data){
                                                foreach ($list_data as $key => $value) {
                                                    $str_btn_action = 
                                                    "<center>
                                                        <button class=\"btn btn-info\" id=\"up_data\" onclick=\"update_data('".$value->id_item."')\" style=\"width: 40px;\"><i class=\"fa fa-pencil-square-o\" ></i></button>&nbsp;&nbsp;
                                                    </center>";
                                                    print_r("<tr>
                                                                <td>(".$value->id_item.") ".$value->nama_item."</td>
                                                                <td>".$value->kode_produksi_item."</td>
                                                                <td>".$value->periode_record_item."</td>
                                                                <td align=\"right\">".$value->stok_record_item." (".$value->satuan.")</td>
                                                                <td align=\"right\" style=\"color: red;\">".$value->rusak_record_item." (".$value->satuan.")</td>
                                                                <td align=\"right\">".$value->stok." (".$value->satuan.")</td>
                                                                <td align=\"right\" style=\"color: red;\">".$value->stok_opnam." (".$value->satuan.")</td>
                                                            </tr>");
                                                }
                                            }
                                        ?>
                                    </tbody>
                                </table>
                            <!-- </div> -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- Card -->
        </div>
    </div>
    <!-- End Row -->
</div>
<!-- ============================================================== -->
<!-- --------------------------End Container fluid----------------  -->
<!-- ============================================================== -->

