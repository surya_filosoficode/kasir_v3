<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Rekam Stok</h3>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->


<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- Row -->
    <div class="row">
        <div class="col-12 m-t-30">
            <!-- Card -->
            <div class="card card-outline-info">
                <div class="card-header">
                    <!-- <h4 class="m-b-0 text-white">List Penjualan</h4> -->
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="m-b-0 text-white">List Stok</h4>
                        </div>
                        <div class="col-md-6 text-right">
                            
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="card">
                        <div class="row">
                            <div class="col-md-6">
                                <a href="#" class="btn waves-effect waves-light btn-rounded btn-info" onclick="send_data()">
                                    <i class="fa fa-plus-square"></i>&nbsp;&nbsp;Rekam Stok Akhir Hari Ini
                                </a>
                            </div>
                        </div>
                        <div class="card-body font_edit font_color">
                            <!-- <div class="table-responsive m-t-40"> -->
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th width="15%">(Kode Produk) Nama Produk</th>
                                            <th width="10%">Kode Produksi</th>
                                            <th width="10%">Tgl. Kadaluarsa</th>
                                            <th width="10%">Stok (Satuan)</th>
                                            <th width="10%">Stok Rusak(Satuan)</th>

                                            <!-- <th width="10%">Harga Beli</th> -->
                                            <!-- <th width="11%">Harga Netto</th> -->
                                            <!-- <th width="11%">Harga Jual</th> -->
                                            <!-- <th width="13%">Aksi</th> -->
                                        </tr>
                                    </thead>
                                    <tbody id=\"main_table_content\">
                                        <?php
                                            if($list_data){
                                                foreach ($list_data as $key => $value) {
                                                    $str_btn_action = 
                                                    "<center>
                                                        <button class=\"btn btn-info\" id=\"up_data\" onclick=\"update_data('".$value->id_item."')\" style=\"width: 40px;\"><i class=\"fa fa-pencil-square-o\" ></i></button>&nbsp;&nbsp;
                                                    </center>";
                                                    print_r("<tr>
                                                                <td>(".$value->id_item.") ".$value->nama_item."</td>
                                                                <td>".$value->kode_produksi_item."</td>
                                                                <td>".$value->tgl_kadaluarsa_item."</td>
                                                                <td align=\"right\">".$value->stok." (".$value->satuan.")</td>
                                                                <td align=\"right\">".$value->stok_opnam." (".$value->satuan.")</td>
                                                            </tr>");
                                                }
                                            }
                                        ?>
                                    </tbody>
                                </table>
                            <!-- </div> -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- Card -->
        </div>
    </div>
    <!-- End Row -->
</div>
<!-- ============================================================== -->
<!-- --------------------------End Container fluid----------------  -->
<!-- ============================================================== -->

<script type="text/javascript">
    $(document).ready(function(){
        
    });

    function send_data(){
        var data_main = new FormData();
        data_main.append('id'        , "");

        $.ajax({
            url: "<?php print_r(base_url());?>erroring/checksaveitem/rekam_stok",
            dataType: 'html', // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,
            type: 'post',
            success: function(res) {
                // console.log(res);
                response_req(res);
            }
        });
        
    }

    function response_req(res){
        var res_data = JSON.parse(res);
        // console.log(res_data);
            var msg_main = res_data.msg_main;
            var msg_detail = res_data.msg_detail;

        if(msg_main.status){
            create_sweet_alert("Proses Berhasil", msg_main.msg, "success");
        }else {
            create_sweet_alert("Proses Gagal", msg_main.msg, "warning");
        }
    }

    function create_sweet_alert(title, msg, status) {
        ! function($) {
            "use strict";
            // var next = swal.close();
            var SweetAlert = function() {};
            if(status == "success"){
                SweetAlert.prototype.init = function() {
                    swal({   
                        title: title,   
                        text: msg,   
                        type: status,   
                        showCancelButton: false,   
                        confirmButtonColor: "#DD6B55",   
                        confirmButtonText: "Lanjutkan",   
                        cancelButtonText: "Perbaiki",   
                        closeOnConfirm: false,   
                        closeOnCancel: false 
                    }, function(isConfirm){   
                        if (isConfirm) {

                            window.location.href = "<?php print_r(base_url());?>checking/rekam_stok";  
                        } 
                    });
                },
                $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            }else {
                SweetAlert.prototype.init = function() {
                    swal({   
                        title: title,   
                        text: msg,   
                        type: status,   
                        showCancelButton: false,   
                        confirmButtonColor: "#DD6B55",   
                        confirmButtonText: "Perbaiki",   
                        cancelButtonText: "Perbaiki",   
                        closeOnConfirm: false,   
                        closeOnCancel: false 
                    }, function(isConfirm){   
                        if (isConfirm) {
                            swal.close();
                        } 
                    });
                },
                $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            }
            
            
            //init
            
        }(window.jQuery),

        function($) {
            "use strict";
            $.SweetAlert.init()
        }(window.jQuery);
    }

    
</script>