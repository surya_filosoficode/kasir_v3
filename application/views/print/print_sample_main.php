<!DOCTYPE html>
<html>
<head>
    <title>Laporan Pengembalian Sample PT. Blesindo Farma</title>
</head>
<body>
    <center>
        <table style='font-family:calibri; border-collapse: collapse; margin-bottom: 20px' border = '0' >
            <td align='center' style='vertical-align:top'>  
                <span style='font-size:24pt'><b>LAPORAN PENGEMBALIAN SAMPLE PT. BLESSINDO FARMA</b></span>
                <h4><?php print_r($str_periode);?></h4>  
            </td>
        </table>
        <div class="col-md-12">
            <label>Daftar Produk Sample</label>
            <br>
        </div>

        <!-- <div class="col-md-12" style="font-size: 11px; font-weight: 300;"> -->
            <table id="myTable" class="table table-bordered table-striped" border="1" style="width:100%; border-collapse: collapse;" >
                <thead>
                    <tr>
                        <th style="font-weight: bold;">No</th>
                        <th style="font-weight: bold;">Nama Obat</th>
                        <th style="font-weight: bold;">Exp Date</th>
                        <th style="font-weight: bold;">Kode Produksi</th>
                        <th style="font-weight: bold;">Sample</th>
                        <th style="font-weight: bold;">Total</th>
                        <th style="font-weight: bold;">Harga Satuan</th>
                        <th style="font-weight: bold;">Jumlah Uang</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        if(isset($list_sample)){
                            $no = 1;

                            $t_harga = 0;
                            foreach ($list_sample as $key => $value) {
                                print_r("
                                    <tr>
                                        <td>".$no."</td>
                                        <td>".$value["detail"]["nama_item"]."</td>
                                        <td>".$value["detail"]["tgl_kadaluarsa_item"]."</td>
                                        <td>".$value["detail"]["kode_produksi_item"]."</td>
                                        <td>".$value["t_item"]."</td>
                                        <td>".$value["t_item"]."</td>
                                        <td align=\"right\">Rp. ".number_format($value["detail"]["harga_bruto"], 2, ',', '.')."</td>
                                        <td align=\"right\">Rp. ".number_format($value["t_harga"], 2, ',', '.')."</td>
                                    </tr>");

                                $t_harga += $value["t_harga"];

                                $no++;
                            }
                        }
                    ?>
                    
                    <!-- <tr v-if="!haveData"><td colspan="12" align="center"><i>* Tidak ada data transaksi untuk periode tersebut *</i></td></tr> -->
                    <tr style="font-weight:bold;">
                      <td colspan="7">Grand Total</td> 
                      <td align="right">Rp. <?php print_r(number_format($t_harga, 2, ',', '.'));?></td>
                    </tr>
                </tbody>
            </table>
        <!-- </div> -->

        <div class="col-md-12">
            <br>
            <label>Daftar Produk Pengganti</label>
            <br>
            <!-- <hr> -->
        </div>

        <!-- <div class="col-md-12" style="font-size: 11px; font-weight: 300;"> -->
            <table id="myTable" class="table table-bordered table-striped" border="1" style="width:100%; border-collapse: collapse;" >
                <thead>
                    <tr>
                        <th style="font-weight: bold;">No</th>
                        <th style="font-weight: bold;">Nama Obat</th>
                        <th style="font-weight: bold;">Exp Date</th>
                        <th style="font-weight: bold;">Kode Produksi</th>
                        <th style="font-weight: bold;">Sample</th>
                        <th style="font-weight: bold;">Total</th>
                        <th style="font-weight: bold;">Harga Satuan</th>
                        <th style="font-weight: bold;">Jumlah Uang</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        if(isset($list_pengganti)){
                            $no = 1;

                            $t_harga_p = 0;
                            foreach ($list_pengganti as $key => $value) {
                                print_r("
                                    <tr>
                                        <td>".$no."</td>
                                        <td>".$value["detail"]["nama_item"]."</td>
                                        <td>".$value["detail"]["tgl_kadaluarsa_item"]."</td>
                                        <td>".$value["detail"]["kode_produksi_item"]."</td>
                                        <td>".$value["t_item"]."</td>
                                        <td>3</td>
                                        <td align=\"right\">Rp. ".number_format($value["detail"]["harga_bruto"], 2, ',', '.')."</td>
                                        <td align=\"right\">Rp. ".number_format($value["t_harga"], 2, ',', '.')."</td>
                                    </tr>");

                                $t_harga_p += $value["t_harga"];

                                $no++;
                            }

                            $t_harga_all = $t_harga_p - $t_harga;
                        }
                    ?>
                    <!-- <tr v-if="!haveData"><td colspan="12" align="center"><i>* Tidak ada data transaksi untuk periode tersebut *</i></td></tr> -->
                    <tr style="font-weight:bold;">
                      <td colspan="7">Grand Total</td> 
                      <td align="right">Rp. <?php print_r(number_format($t_harga_p, 2, ',', '.'));?></td>
                    </tr>
                    <tr style="font-weight:bold;">
                      <td colspan="7">Selisih Harga Sample dan Pengganti</td> 
                      <td align="right">Rp. <?php print_r(number_format($t_harga_all, 2, ',', '.'));?></td>
                    </tr>
                    <!-- <tr style="font-weight:bold;">
                      <td colspan="7">Jumlah Cendo Xetrol Sebagai Pengganti</td> 
                      <td align="right"> 50 botol(satuan: misal botol)</td>
                    </tr> -->
                </tbody>
            </table>
        <!-- </div> -->
        <table align="right" cellspacing='0' style='width:60%; font-size:10pt; font-family:calibri;  border-collapse: collapse; margin-top: 20px;' border="0">
            <tr >
                <td style='padding-right:30px; text-align: center;'>Surabaya, 15 Oktober 2019</td>
            </tr>
            <tr >
                <td style='padding-bottom: 65px; padding-right:30px; text-align: center;'>PT. BLESSINDO FARMA</td>
            </tr>
            <tr>
                <td style="text-decoration: underline; padding-right:30px; text-align: center;">Yuliani Lemantara, Ssi, Apt.</td>
            </tr>
            <tr>
                <td style='padding-right:30px; text-align: center;'>19760310/SIKA-35.78/2016/2219</td>
            </tr>
        </table>
    </center>
</body>
<script type="text/javascript">window.print();</script>
</html>

