<!DOCTYPE html>
<html>
<head>
    <title>Rekapitulasi Laporan Obat PT. Blesindo Farma</title>
</head>
<?php
    $no_siup = "";
    $no_npwp = "";
    if(isset($setting)){
        $array_npwp = array();
        $array_siup = array();
        foreach ($setting as $key => $value) {
            if($value->jenis_setting == "npwp"){
                $array_npwp = $value;
                $no_npwp = $value->keterangan_setting;
            }elseif ($value->jenis_setting == "siup") {
                $array_siup = $value;
                $no_siup = $value->keterangan_setting;
            }
        }
    }

    $periode = "";
    if(isset($str_periode)){
        $periode = $str_periode;
    }
?>
<body>
    <center>
        <table style='font-family:calibri; border-collapse: collapse; margin-bottom: 20px' border = '0' >
            <td align='center' style='vertical-align:top'>  
                <span style='font-size:24pt'><b>REKAPITULASI LAPORAN OBAT PT. BLESSINDO FARMA</b></span>
                <h4><?php print_r($str_periode);?></h4>  
            </td>
        </table>

        <table style='font-family:calibri; border-collapse: collapse; margin-bottom: 20px' width="100%" border = '0' >
            <tr>
                <td width="15%">Nama</td>
                <td width="3%">:</td>
                <td width="*">BLESSINDO FARMA (Pusat)</td>
            </tr>
            <tr>
                <td>No. Izin</td>
                <td>:</td>
                <td><?php print_r($no_siup);?></td>
            </tr>
            <tr>
                <td>Alamat</td>
                <td>:</td>
                <td>JL. MAYJEN SUNGKONO KOMPLEKS WONOKRITI INDAH S-33 SURABAYA, Kota Surabaya - Jawa Timur</td>
            </tr>
        </table>
        <table cellspacing='0' style='width:100%; font-size:10pt; font-family:calibri;  border-collapse: collapse; margin-top: 20px;' border="1">
            <thead>
                <tr>
                    <th valign="top" style="font-weight: bold;" rowspan="2">NO.</th>
                    <th valign="top" style="font-weight: bold;" rowspan="2">PERIODE</th>
                    <th valign="top" style="font-weight: bold;" rowspan="2">KODE OBAT(NIE)</th>
                    <th valign="top" style="font-weight: bold;" rowspan="2">NAMA OBAT</th>
                    <th valign="top" style="font-weight: bold;" rowspan="2">STOK AWAL</th>
                    <th style="font-weight: bold; text-align: center;" colspan="4">PEMASUKAN</th>
                    <th style="font-weight: bold; text-align: center;" colspan="9">PENGELUARAN</th>
                    <th valign="top" style="font-weight: bold;" rowspan="2">STOK AKHIR</th>
                </tr>
                <tr>
                    <th>PABRIK</th>
                    <th>PBF</th>
                    <th>RETUR</th>
                    <th>LAINNYA</th>
                    <th>RUMAH SAKIT</th>
                    <th>APOTIK</th>
                    <th>PBF</th>
                    <th>SARANA PEMERINTAH</th>
                    <th>PUSKESMAS</th>
                    <th>KLINIK</th>
                    <th>TOKO OBAT</th>
                    <th>RETUR</th>
                    <th>LAINNYA</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if(isset($list_data)){

                    $no = 1;
                    foreach ($list_data as $key => $value) {
                        // print_r("<pre>");
                        // print_r($value);
                        

                        print_r("<tr>
                                    <td>".$no."</td>
                                    <td>".$periode."</td>
                                    <td>".$value["item_now"]->id_item."</td>
                                    <td>".$value["item_now"]->nama_item.", ".$value["item_now"]->satuan." (".$value["item_now"]->kode_produksi_item.")</td>
                                    <td align=\"right\">".$value["stok_before"]."</td>

                                    <td align=\"right\">".$value["pemasukan"]["pabrik"]."</td>
                                    <td align=\"right\">".$value["pemasukan"]["pbf"]."</td>
                                    <td align=\"right\">".$value["pemasukan"]["retur"]."</td>
                                    <td align=\"right\">".$value["pemasukan"]["lainnya"]."</td>
                                    
                                    <td align=\"right\">".$value["pengeluaran"]["rs"]."</td>
                                    <td align=\"right\">".$value["pengeluaran"]["apotik"]."</td>
                                    <td align=\"right\">".$value["pengeluaran"]["pbf"]."</td>
                                    <td align=\"right\">".$value["pengeluaran"]["sp"]."</td>
                                    <td align=\"right\">".$value["pengeluaran"]["pm"]."</td>
                                    <td align=\"right\">".$value["pengeluaran"]["kl"]."</td>
                                    <td align=\"right\">".$value["pengeluaran"]["to"]."</td>
                                    <td align=\"right\">".$value["pengeluaran"]["retur"]."</td>
                                    <td align=\"right\">".$value["pengeluaran"]["ln"]."</td>

                                    <td align=\"right\">".$value["stok"]."</td>
                                </tr>");
                        $no++;
                    }
                }
                ?>
            </tbody>
        </table>
        <table align="right" cellspacing='0' style='width:40%; font-size:10pt; font-family:calibri;  border-collapse: collapse; margin-top: 20px;' border="0">
            <tr >
                <td style='padding-right:30px; text-align: center;'>Surabaya, 15 Oktober 2019</td>
            </tr>
            <tr >
                <td style='padding-bottom: 65px; padding-right:30px; text-align: center;'>PT. BLESSINDO FARMA</td>
            </tr>
            <tr>
                <td style="text-decoration: underline; padding-right:30px; text-align: center;">Yuliani Lemantara, Ssi, Apt.</td>
            </tr>
            <tr>
                <td style='padding-right:30px; text-align: center;'>19760310/SIKA-35.78/2016/2219</td>
            </tr>
        </table>
    </center>
</body>
<script type="text/javascript">window.print();</script>
</html>

