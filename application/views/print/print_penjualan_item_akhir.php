<!DOCTYPE html>
<html>
<head>
    <title>Laporan Stok Akhir Produk PT. Blesindo Farma</title>
</head>
<body>
    <center>
        <table style='font-family:calibri; border-collapse: collapse; margin-bottom: 20px' border = '0' >
            <td align='center' style='vertical-align:top'>  
                <span style='font-size:24pt'><b>LAPORAN STOK AKHIR PRODUK PT. BLESSINDO FARMA</b></span>
                <h4><?php print_r($str_periode);?></h4>  
            </td>
        </table>
    <!-- Pembelian -->
        <table cellspacing='0' style='width:100%; font-size:10pt; font-family:calibri;  border-collapse: collapse; margin-top: 20px;' border="1">
            <thead>
                <tr>
                    <th style="font-weight: bold;" width="5%">No.</th>
                    <th style="font-weight: bold;" width="15%">Kode Produk</th>
                    <th style="font-weight: bold;" width="20%">Nama Produk</th>
                    <th style="font-weight: bold;" width="7%">Satuan</th>
                    <!-- <th style="font-weight: bold;">Id Produk</th>
                    <th style="font-weight: bold;">Nama Produk</th> -->
                    <th style="font-weight: bold;" width="13%">Kode Produksi</th>
                    <th style="font-weight: bold;" width="15%">Harga Satuan</th>
                    <th style="font-weight: bold;" width="15%">Total Harga</th>
                    <th style="font-weight: bold;" width="15%">Stok Produk</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    if(isset($array_data_main)){
                        $no = 1;
                        $t_harga_all = 0;
                        foreach ($array_data_main as $key => $value) {
                            $val_sisa = $value->stok;
                            $val_harga_satuan = $value->harga_bruto;

                            $val_t_harga = (float)$val_sisa * (float)$val_harga_satuan;

                            $t_harga_all += $val_t_harga;

                            $str_harga_satuan = number_format($val_harga_satuan, 0, ",", ".");
                            $str_t_harga = number_format($val_t_harga, 0, ",", ".");
                            print_r("
                                    <tr>
                                        <td>".$no."</td>
                                        <td align=\"left\">".$value->id_item."</td>
                                        <td align=\"left\">".$value->nama_item."</td>
                                        <td align=\"left\">".$value->satuan."</td>

                                        <td align=\"left\">".$value->kode_produksi_item."</td>

                                        <td align=\"right\">".$str_harga_satuan."</td>
                                        <td align=\"right\">".$str_t_harga."</td>
                                        <td align=\"right\">".$val_sisa."</td>
                                    </tr>
                                ");

                            $no++;
                        }

                        $str_t_harga_all = number_format($t_harga_all, 0, ",", ".");

                        print_r("
                                    <tr>
                                        <td colspan=\"6\" align=\"center\">Total</td>
                                        <td align=\"right\">".$str_t_harga_all."</td>
                                        <td align=\"right\">-</td>
                                    </tr>
                                ");
                    }
                ?>
            </tbody>
        </table>
    <!-- Pembelian -->
        <br><br>
        <table align="right" cellspacing='0' style='width:60%; font-size:10pt; font-family:calibri;  border-collapse: collapse; margin-top: 20px;' border="0">
            <tr >
                <td style='padding-right:30px; text-align: center;'>Surabaya, 15 Oktober 2019</td>
            </tr>
            <tr >
                <td style='padding-bottom: 65px; padding-right:30px; text-align: center;'>PT. BLESSINDO FARMA</td>
            </tr>
            <tr>
                <td style="text-decoration: underline; padding-right:30px; text-align: center;">Yuliani Lemantara, Ssi, Apt.</td>
            </tr>
            <tr>
                <td style='padding-right:30px; text-align: center;'>19760310/SIKA-35.78/2016/2219</td>
            </tr>
        </table>



    </center>
</body>
<script type="text/javascript">window.print();</script>
</html>

