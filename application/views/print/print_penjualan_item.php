<!DOCTYPE html>
<html>
<head>
    <title>Laporan Penjualan PT. Blesindo Farma</title>
</head>
<body>
    <center>
        <table style='font-family:calibri; border-collapse: collapse; margin-bottom: 20px' border = '0' >
            <td align='center' style='vertical-align:top'>  
                <span style='font-size:24pt'><b>LAPORAN TRANSAKSI PENJUALAN PT. BLESSINDO FARMA</b></span>
                <h4><?php print_r($str_periode);?></h4>  
            </td>
        </table>
    <!-- Pembelian -->
        <table style='font-family:calibri; border-collapse: collapse; margin-bottom: 20px' border = '0' >
            <tr>
                <td>&nbsp;</td>    
            </tr>
            <tr>
                <td align="left"><label>Daftar Masuk (Pemebelian)</label></td>
                <!-- <hr> -->
            </tr>
        </table>
        <table cellspacing='0' style='width:80%; font-size:10pt; font-family:calibri;  border-collapse: collapse; margin-top: 20px;' border="1">
            <thead>
                <tr>
                    <th style="font-weight: bold;" width="5%">No.</th>
                    <th style="font-weight: bold;" width="10%">Tanggal</th>
                    <th style="font-weight: bold;" width="15%">No Faktur</th>
                    <th style="font-weight: bold;" width="20%">Suplier</th>
                    <!-- <th style="font-weight: bold;">Id Produk</th>
                    <th style="font-weight: bold;">Nama Produk</th> -->
                    <th style="font-weight: bold;" width="10%">Masuk</th>
                    <th style="font-weight: bold;" width="6%">RS</th>
                    <th style="font-weight: bold;" width="6%">PT</th>
                    <th style="font-weight: bold;" width="6%">APT</th>
                    <th style="font-weight: bold;" width="6%">Lainnya</th>
                    <th style="font-weight: bold;" width="10%">Sisa</th>
                </tr>
            </thead>
            <tbody>
                <?php
                // print_r($list_data);
                    $nama_suplier = "";
                    $first_val = 0;
                    
                    $val_sisa = 0;
                    $no = 1;

                    if(isset($check_data)){
                        $item_fine = $check_data[0]->stok_record_item;
                        $item_broken = $check_data[0]->rusak_record_item;

                        $first_val = $item_fine+$item_broken;
                    }

                    $val_sisa +=$first_val;

                    if(isset($list_data_pembelian) && isset($check_data)){
                        
                        foreach ($list_data_pembelian as $key => $value) {
                            // $first_val = 0;
                            if($key == 0){
                                // $item_fine = $check_data[0]->stok_record_item;
                                // $item_broken = $check_data[0]->rusak_record_item;

                                // $first_val = $item_fine+$item_broken;
                                
                            }

                            $val_sisa +=$first_val;

                            $val_rs = "";
                            $val_pt = "";
                            $val_apt = "";
                            $val_lain = "";

                            $nama_suplier = $value->nama_suplier;   

                            $val_sisa += $value->jml_item_tr_detail;

                            print_r("
                                    <tr>
                                        <td align=\"left\">".$no."</td>
                                        <td align=\"left\">".$value->tgl_transaksi_tr_header."</td>
                                        <td align=\"left\">".$value->id_tr_header."</td>
                                        <td align=\"left\">".$value->nama_suplier."</td>

                                        <td align=\"right\">".$value->jml_item_tr_detail."</td>
                                        <td align=\"right\">".$val_rs."</td>
                                        <td align=\"right\">".$val_pt."</td>
                                        <td align=\"right\">".$val_apt."</td>
                                        <td align=\"right\">".$val_lain."</td>
                                        <td align=\"right\">".$val_sisa."</td>
                                    </tr>
                                ");

                            $no++;
                        }
                    }
                ?>
            </tbody>
        </table>
    <!-- Pembelian -->

    <!-- Retur Penjualan -->
        <table style='font-family:calibri; border-collapse: collapse; margin-bottom: 20px' border = '0' >
            <tr>
                <td>&nbsp;</td>    
            </tr>
            <tr>
                <td align="left"><label>Daftar Masuk (Retur Penjualan)</label></td>
                <!-- <hr> -->
            </tr>
        </table>
        <table cellspacing='0' style='width:80%; font-size:10pt; font-family:calibri;  border-collapse: collapse; margin-top: 20px;' border="1">
            <thead>
                <tr>
                    <th style="font-weight: bold;" width="5%">No.</th>
                    <th style="font-weight: bold;" width="10%">Tanggal</th>
                    <th style="font-weight: bold;" width="15%">No Faktur</th>
                    <th style="font-weight: bold;" width="20%">Pelanggan</th>
                    <!-- <th style="font-weight: bold;">Id Produk</th>
                    <th style="font-weight: bold;">Nama Produk</th> -->
                    <th style="font-weight: bold;" width="10%">Masuk</th>
                    <th style="font-weight: bold;" width="6%">RS</th>
                    <th style="font-weight: bold;" width="6%">PT</th>
                    <th style="font-weight: bold;" width="6%">APT</th>
                    <th style="font-weight: bold;" width="6%">Lainnya</th>
                    <th style="font-weight: bold;" width="10%">Sisa</th>
                </tr>
            </thead>
            <tbody>
                <?php

                    if(isset($list_data_retur_penjualan) && isset($check_data)){
                        // $val_sisa = 0;
                        foreach ($list_data_retur_penjualan as $key => $value) {
                            // $first_val = 0;
                            // if($key == 0){
                            //     $item_fine = $check_data[0]->stok_record_item;
                            //     $item_broken = $check_data[0]->rusak_record_item;

                            //     $first_val = $item_fine+$item_broken;
                            //     $val_sisa +=$first_val;
                            // }

                            $val_rs = "";
                            $val_pt = "";
                            $val_apt = "";
                            $val_lain = "";

                            switch ($value->jenis_rekanan) {
                                case 'rs':
                                    $val_rs = $value->jml_item_tr_detail; 
                                    break;

                                case 'apotik':
                                    $val_apt = $value->jml_item_tr_detail;
                                    break;

                                case 'pbf':
                                    $val_pt = $value->jml_item_tr_detail;
                                    break;
                                
                                default:
                                    $val_lain = $value->jml_item_tr_detail;
                                    break;
                            }

                            $val_sisa += $value->jml_item_tr_detail;

                            print_r("
                                    <tr>
                                        <td align=\"left\">".$no."</td>
                                        <td align=\"left\">".$value->tgl_transaksi_tr_header."</td>
                                        <td align=\"left\">".$value->id_tr_header_p."</td>
                                        <td align=\"left\">".$value->nama_rekanan."</td>

                                        <td align=\"right\"></td>
                                        <td align=\"right\">".$val_rs."</td>
                                        <td align=\"right\">".$val_pt."</td>
                                        <td align=\"right\">".$val_apt."</td>
                                        <td align=\"right\">".$val_lain."</td>
                                        <td align=\"right\">".$val_sisa."</td>
                                    </tr>
                                ");

                            $no++;
                        }
                    }
                ?>
            </tbody>
        </table>
    <!-- Retur Penjualan -->

    <!-- Retur Pengganti Retur Pembelian -->
        <table style='font-family:calibri; border-collapse: collapse; margin-bottom: 20px' border = '0' >
            <tr>
                <td>&nbsp;</td>    
            </tr>
            <tr>
                <td align="left"><label>Daftar Masuk (Pengganti Retur Pembelian)</label></td>
                <!-- <hr> -->
            </tr>
        </table>
        <table cellspacing='0' style='width:80%; font-size:10pt; font-family:calibri;  border-collapse: collapse; margin-top: 20px;' border="1">
            <thead>
                <tr>
                    <th style="font-weight: bold;" width="5%">No.</th>
                    <th style="font-weight: bold;" width="10%">Tanggal</th>
                    <th style="font-weight: bold;" width="15%">No Faktur</th>
                    <th style="font-weight: bold;" width="20%">Suplier</th>
                    <!-- <th style="font-weight: bold;">Id Produk</th>
                    <th style="font-weight: bold;">Nama Produk</th> -->
                    <th style="font-weight: bold;" width="10%">Masuk</th>
                    <th style="font-weight: bold;" width="6%">RS</th>
                    <th style="font-weight: bold;" width="6%">PT</th>
                    <th style="font-weight: bold;" width="6%">APT</th>
                    <th style="font-weight: bold;" width="6%">Lainnya</th>
                    <th style="font-weight: bold;" width="10%">Sisa</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    if(isset($list_data_retur_pembelian_p) && isset($check_data)){
                        foreach ($list_data_retur_pembelian_p as $key => $value) {
                            $val_rs = "";
                            $val_pt = "";
                            $val_apt = "";
                            $val_lain = "";

                            $nama_suplier = $value->nama_suplier; 

                            $val_sisa += $value->jml_item_tr_detail;

                            print_r("
                                    <tr>
                                        <td align=\"left\">".$no."</td>
                                        <td align=\"left\">".$value->tgl_transaksi_tr_header."</td>
                                        <td align=\"left\">".$value->id_tr_header_p."</td>
                                        <td align=\"left\">".$value->nama_suplier."</td>

                                        <td align=\"right\">".$value->jml_item_tr_detail."</td>
                                        <td align=\"right\">".$val_rs."</td>
                                        <td align=\"right\">".$val_pt."</td>
                                        <td align=\"right\">".$val_apt."</td>
                                        <td align=\"right\">".$val_lain."</td>
                                        <td align=\"right\">".$val_sisa."</td>
                                    </tr>
                                ");

                            $no++;
                        }
                    }
                ?>
            </tbody>
        </table>
    <!-- Retur Pengganti Retur Pembelian -->

    <!-- Retur Pengganti Retur Sample -->
        <table style='font-family:calibri; border-collapse: collapse; margin-bottom: 20px' border = '0' >
            <tr>
                <td>&nbsp;</td>    
            </tr>
            <tr>
                <td align="left"><label>Daftar Masuk (Pengganti Sample)</label></td>
                <!-- <hr> -->
            </tr>
        </table>
        <table cellspacing='0' style='width:80%; font-size:10pt; font-family:calibri;  border-collapse: collapse; margin-top: 20px;' border="1">
            <thead>
                <tr>
                    <th style="font-weight: bold;" width="5%">No.</th>
                    <th style="font-weight: bold;" width="10%">Tanggal</th>
                    <th style="font-weight: bold;" width="15%">No Faktur</th>
                    <th style="font-weight: bold;" width="20%">Pelanggan</th>
                    <!-- <th style="font-weight: bold;">Id Produk</th>
                    <th style="font-weight: bold;">Nama Produk</th> -->
                    <th style="font-weight: bold;" width="10%">Masuk</th>
                    <th style="font-weight: bold;" width="6%">RS</th>
                    <th style="font-weight: bold;" width="6%">PT</th>
                    <th style="font-weight: bold;" width="6%">APT</th>
                    <th style="font-weight: bold;" width="6%">Lainnya</th>
                    <th style="font-weight: bold;" width="10%">Sisa</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    if(isset($list_data_retur_sample_p) && isset($check_data)){
                        // $val_sisa = 0;
                        foreach ($list_data_retur_sample_p as $key => $value) {
                            $val_rs = "";
                            $val_pt = "";
                            $val_apt = "";
                            $val_lain = "";

                            $val_sisa += $value->jml_item_tr_detail;

                            print_r("
                                    <tr>
                                        <td align=\"left\">".$no."</td>
                                        <td align=\"left\">".$value->tgl_transaksi_tr_header."</td>
                                        <td align=\"left\">".$value->id_tr_header_p."</td>
                                        <td align=\"left\">".$nama_suplier."</td>

                                        <td align=\"right\">".$value->jml_item_tr_detail."</td>
                                        <td align=\"right\">".$val_rs."</td>
                                        <td align=\"right\">".$val_pt."</td>
                                        <td align=\"right\">".$val_apt."</td>
                                        <td align=\"right\">".$val_lain."</td>
                                        <td align=\"right\">".$val_sisa."</td>
                                    </tr>
                                ");

                            $no++;
                        }
                    }
                ?>
            </tbody>
        </table>
    <!-- Retur Pengganti Retur Sample -->



    <!-- Penjualan -->
        <table style='font-family:calibri; border-collapse: collapse; margin-bottom: 20px' border = '0' >
            <tr>
                <td>&nbsp;</td>    
            </tr>
            <tr>
                <td align="left"><label>Daftar Keluar (Penjualan)</label></td>
                <!-- <hr> -->
            </tr>
        </table>
        <table cellspacing='0' style='width:80%; font-size:10pt; font-family:calibri;  border-collapse: collapse; margin-top: 20px;' border="1">
            <thead>
                <tr>
                    <th style="font-weight: bold;" width="5%">No.</th>
                    <th style="font-weight: bold;" width="10%">Tanggal</th>
                    <th style="font-weight: bold;" width="15%">No Faktur</th>
                    <th style="font-weight: bold;" width="20%">Pelanggan</th>
                    <!-- <th style="font-weight: bold;">Id Produk</th>
                    <th style="font-weight: bold;">Nama Produk</th> -->
                    <th style="font-weight: bold;" width="10%">Masuk</th>
                    <th style="font-weight: bold;" width="6%">RS</th>
                    <th style="font-weight: bold;" width="6%">PT</th>
                    <th style="font-weight: bold;" width="6%">APT</th>
                    <th style="font-weight: bold;" width="6%">Lainnya</th>
                    <th style="font-weight: bold;" width="10%">Sisa</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    if(isset($list_data) && isset($check_data)){
                        // $val_sisa = 0;
                        foreach ($list_data as $key => $value) {
                            // $first_val = 0;
                            // if($key == 0){
                            //     $item_fine = $check_data[0]->stok_record_item;
                            //     $item_broken = $check_data[0]->rusak_record_item;

                            //     $first_val = $item_fine+$item_broken;
                            //     $val_sisa +=$first_val;
                            // }

                            $val_rs = "";
                            $val_pt = "";
                            $val_apt = "";
                            $val_lain = "";

                            switch ($value->jenis_rekanan) {
                                case 'rs':
                                    $val_rs = $value->jml_item_tr_detail; 
                                    break;

                                case 'apotik':
                                    $val_apt = $value->jml_item_tr_detail;
                                    break;

                                case 'pbf':
                                    $val_pt = $value->jml_item_tr_detail;
                                    break;
                                
                                default:
                                    $val_lain = $value->jml_item_tr_detail;
                                    break;
                            }

                            $val_sisa -= $value->jml_item_tr_detail;

                            print_r("
                                    <tr>
                                        <td>".$no."</td>
                                        <td align=\"left\">".$value->tgl_transaksi_tr_header."</td>
                                        <td align=\"left\">".$value->id_tr_header."</td>
                                        <td align=\"left\">".$value->nama_rekanan."</td>

                                        <td align=\"right\"></td>
                                        <td align=\"right\">".$val_rs."</td>
                                        <td align=\"right\">".$val_pt."</td>
                                        <td align=\"right\">".$val_apt."</td>
                                        <td align=\"right\">".$val_lain."</td>
                                        <td align=\"right\">".$val_sisa."</td>
                                    </tr>
                                ");

                            $no++;
                        }
                    }
                ?>
            </tbody>
        </table>
    <!-- Penjualan -->

    <!-- Pengganti Retur Penjualan -->
        <table style='font-family:calibri; border-collapse: collapse; margin-bottom: 20px' border = '0' >
            <tr>
                <td>&nbsp;</td>    
            </tr>
            <tr>
                <td align="left"><label>Daftar Keluar (Pengganti Retur Penjualan)</label></td>
                <!-- <hr> -->
            </tr>
        </table>
        <table cellspacing='0' style='width:80%; font-size:10pt; font-family:calibri;  border-collapse: collapse; margin-top: 20px;' border="1">
            <thead>
                <tr>
                    <th style="font-weight: bold;" width="5%">No.</th>
                    <th style="font-weight: bold;" width="10%">Tanggal</th>
                    <th style="font-weight: bold;" width="15%">No Faktur</th>
                    <th style="font-weight: bold;" width="20%">Pelanggan</th>
                    <!-- <th style="font-weight: bold;">Id Produk</th>
                    <th style="font-weight: bold;">Nama Produk</th> -->
                    <th style="font-weight: bold;" width="10%">Masuk</th>
                    <th style="font-weight: bold;" width="6%">RS</th>
                    <th style="font-weight: bold;" width="6%">PT</th>
                    <th style="font-weight: bold;" width="6%">APT</th>
                    <th style="font-weight: bold;" width="6%">Lainnya</th>
                    <th style="font-weight: bold;" width="10%">Sisa</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    if(isset($list_data_retur_penjualan_p) && isset($check_data)){
                    
                        foreach ($list_data_retur_penjualan_p as $key => $value) {

                            $val_rs = "";
                            $val_pt = "";
                            $val_apt = "";
                            $val_lain = "";

                            switch ($value->jenis_rekanan) {
                                case 'rs':
                                    $val_rs = $value->jml_item_tr_detail; 
                                    break;

                                case 'apotik':
                                    $val_apt = $value->jml_item_tr_detail;
                                    break;

                                case 'pbf':
                                    $val_pt = $value->jml_item_tr_detail;
                                    break;
                                
                                default:
                                    $val_lain = $value->jml_item_tr_detail;
                                    break;
                            }

                            $val_sisa -= $value->jml_item_tr_detail;

                            print_r("
                                    <tr>
                                        <td>".$no."</td>
                                        <td align=\"left\">".$value->tgl_transaksi_tr_header."</td>
                                        <td align=\"left\">".$value->id_tr_header_p."</td>
                                        <td align=\"left\">".$value->nama_rekanan."</td>

                                        <td align=\"right\"></td>
                                        <td align=\"right\">".$val_rs."</td>
                                        <td align=\"right\">".$val_pt."</td>
                                        <td align=\"right\">".$val_apt."</td>
                                        <td align=\"right\">".$val_lain."</td>
                                        <td align=\"right\">".$val_sisa."</td>
                                    </tr>
                                ");

                            $no++;
                        }
                    }
                ?>
            </tbody>
        </table>
    <!-- Pengganti Retur Penjualan -->

    <!-- Retur Pembelian -->
        <table style='font-family:calibri; border-collapse: collapse; margin-bottom: 20px' border = '0' >
            <tr>
                <td>&nbsp;</td>    
            </tr>
            <tr>
                <td align="left"><label>Daftar Keluar (Retur Pembelian)</label></td>
                <!-- <hr> -->
            </tr>
        </table>
        <table cellspacing='0' style='width:80%; font-size:10pt; font-family:calibri;  border-collapse: collapse; margin-top: 20px;' border="1">
            <thead>
                <tr>
                    <th style="font-weight: bold;" width="5%">No.</th>
                    <th style="font-weight: bold;" width="10%">Tanggal</th>
                    <th style="font-weight: bold;" width="15%">No Faktur</th>
                    <th style="font-weight: bold;" width="20%">Suplier</th>
                    <!-- <th style="font-weight: bold;">Id Produk</th>
                    <th style="font-weight: bold;">Nama Produk</th> -->
                    <th style="font-weight: bold;" width="10%">Masuk</th>
                    <th style="font-weight: bold;" width="6%">RS</th>
                    <th style="font-weight: bold;" width="6%">PT</th>
                    <th style="font-weight: bold;" width="6%">APT</th>
                    <th style="font-weight: bold;" width="6%">Lainnya</th>
                    <th style="font-weight: bold;" width="10%">Sisa</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    if(isset($list_data_retur_pembalian) && isset($check_data)){
                        foreach ($list_data_retur_pembalian as $key => $value) {

                            $val_rs = "";
                            $val_pt = "";
                            $val_apt = "";
                            $val_lain = "";

                            $val_pt = $value->jml_item_tr_detail;

                            $val_sisa -= $value->jml_item_tr_detail;

                            print_r("
                                    <tr>
                                        <td>".$no."</td>
                                        <td align=\"left\">".$value->tgl_transaksi_tr_header."</td>
                                        <td align=\"left\">".$value->id_tr_header_p."</td>
                                        <td align=\"left\">".$value->nama_suplier."</td>

                                        <td align=\"right\"></td>
                                        <td align=\"right\">".$val_rs."</td>
                                        <td align=\"right\">".$val_pt."</td>
                                        <td align=\"right\">".$val_apt."</td>
                                        <td align=\"right\">".$val_lain."</td>
                                        <td align=\"right\">".$val_sisa."</td>
                                    </tr>
                                ");

                            $no++;
                        }
                    }
                ?>
            </tbody>
        </table>
    <!-- Retur Pembelian -->

    <!-- Pengganti Sample -->
        <table style='font-family:calibri; border-collapse: collapse; margin-bottom: 20px' border = '0' >
            <tr>
                <td>&nbsp;</td>    
            </tr>
            <tr>
                <td align="left"><label>Daftar Keluar (Sample)</label></td>
                <!-- <hr> -->
            </tr>
        </table>
        <table cellspacing='0' style='width:80%; font-size:10pt; font-family:calibri;  border-collapse: collapse; margin-top: 20px;' border="1">
            <thead>
                <tr>
                    <th style="font-weight: bold;" width="5%">No.</th>
                    <th style="font-weight: bold;" width="10%">Tanggal</th>
                    <th style="font-weight: bold;" width="15%">No Faktur</th>
                    <th style="font-weight: bold;" width="20%">Pelanggan</th>
                    <!-- <th style="font-weight: bold;">Id Produk</th>
                    <th style="font-weight: bold;">Nama Produk</th> -->
                    <th style="font-weight: bold;" width="10%">Masuk</th>
                    <th style="font-weight: bold;" width="6%">RS</th>
                    <th style="font-weight: bold;" width="6%">PT</th>
                    <th style="font-weight: bold;" width="6%">APT</th>
                    <th style="font-weight: bold;" width="6%">Lainnya</th>
                    <th style="font-weight: bold;" width="10%">Sisa</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    if(isset($list_data_sample) && isset($check_data)){
                        // $val_sisa = 0;
                        foreach ($list_data_sample as $key => $value) {
                            // $first_val = 0;
                            // if($key == 0){
                            //     $item_fine = $check_data[0]->stok_record_item;
                            //     $item_broken = $check_data[0]->rusak_record_item;

                            //     $first_val = $item_fine+$item_broken;
                            //     $val_sisa +=$first_val;
                            // }

                            $val_rs = "";
                            $val_pt = "";
                            $val_apt = "";
                            $val_lain = "";

                            switch ($value->jenis_rekanan) {
                                case 'rs':
                                    $val_rs = $value->jml_item_tr_detail; 
                                    break;

                                case 'apotik':
                                    $val_apt = $value->jml_item_tr_detail;
                                    break;

                                case 'pbf':
                                    $val_pt = $value->jml_item_tr_detail;
                                    break;
                                
                                default:
                                    $val_lain = $value->jml_item_tr_detail;
                                    break;
                            }

                            $val_sisa -= $value->jml_item_tr_detail;

                            print_r("
                                    <tr>
                                        <td>".$no."</td>
                                        <td align=\"left\">".$value->tgl_transaksi_tr_header."</td>
                                        <td align=\"left\">".$value->id_tr_header."</td>
                                        <td align=\"left\">".$value->nama_rekanan."</td>

                                        <td align=\"right\"></td>
                                        <td align=\"right\">".$val_rs."</td>
                                        <td align=\"right\">".$val_pt."</td>
                                        <td align=\"right\">".$val_apt."</td>
                                        <td align=\"right\">".$val_lain."</td>
                                        <td align=\"right\">".$val_sisa."</td>
                                    </tr>
                                ");

                            $no++;
                        }
                    }
                ?>
            </tbody>
        </table>
    <!-- Pengganti Sample -->

        <br><br>
        <table align="right" cellspacing='0' style='width:60%; font-size:10pt; font-family:calibri;  border-collapse: collapse; margin-top: 20px;' border="0">
            <tr >
                <td style='padding-right:30px; text-align: center;'>Surabaya, 15 Oktober 2019</td>
            </tr>
            <tr >
                <td style='padding-bottom: 65px; padding-right:30px; text-align: center;'>PT. BLESSINDO FARMA</td>
            </tr>
            <tr>
                <td style="text-decoration: underline; padding-right:30px; text-align: center;">Yuliani Lemantara, Ssi, Apt.</td>
            </tr>
            <tr>
                <td style='padding-right:30px; text-align: center;'>19760310/SIKA-35.78/2016/2219</td>
            </tr>
        </table>



    </center>
</body>
<script type="text/javascript">window.print();</script>
</html>

