<!DOCTYPE html>
<html>
<head>
    <title>Laporan Penjualan PT. Blesindo Farma</title>
</head>
<body>
    <center>
        <table style='font-family:calibri; border-collapse: collapse; margin-bottom: 20px' border = '0' >
            <td align='center' style='vertical-align:top'>  
                <span style='font-size:24pt'><b>LAPORAN TRANSAKSI PENJUALAN PT. BLESSINDO FARMA</b></span>
                <h4><?php print_r($str_periode);?></h4>  
            </td>
        </table>
    <!-- Pembelian -->
        <table style='font-family:calibri; border-collapse: collapse; margin-bottom: 20px' border = '0' >
            <tr>
                <td>&nbsp;</td>    
            </tr>
            <tr>
                <td align="left"><label>Daftar Masuk (Pemebelian)</label></td>
                <!-- <hr> -->
            </tr>
        </table>
        <table cellspacing='0' style='width:80%; font-size:10pt; font-family:calibri;  border-collapse: collapse; margin-top: 20px;' border="1">
            <thead>
                <tr>
                    <th style="font-weight: bold;">No.</th>
                    <th style="font-weight: bold;">Tanggal</th>
                    <th style="font-weight: bold;">No Faktur</th>
                    <th style="font-weight: bold;">Suplier</th>
                    <!-- <th style="font-weight: bold;">Id Produk</th>
                    <th style="font-weight: bold;">Nama Produk</th> -->
                    <th style="font-weight: bold;">Masuk</th>
                    <th style="font-weight: bold;">RS</th>
                    <th style="font-weight: bold;">PT</th>
                    <th style="font-weight: bold;">APT</th>
                    <th style="font-weight: bold;">Lainnya</th>
                    <th style="font-weight: bold;">Total</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $t_masuk = 0;
                    // print_r($list_data);
                    $nama_suplier = "";
                    $first_val = 0;
                    
                    $val_sisa = 0;
                    $no = 1;

                    // if(isset($check_data)){
                    //     $item_fine = $check_data[0]->stok_record_item;
                    //     $item_broken = $check_data[0]->rusak_record_item;

                    //     $first_val = $item_fine+$item_broken;
                    // }

                    // $val_sisa +=$first_val;


                    if(isset($data_pembelian)){
                        
                        $t_pembelian = 0;
                        foreach ($data_pembelian as $key => $value) {
                            $val_rs = "";
                            $val_pt = "";
                            $val_apt = "";
                            $val_lain = "";

                            $nama_suplier = $value->nama_suplier;   

                            $t_pembelian += $value->jml_item_tr_detail;

                            $val_sisa += $value->jml_item_tr_detail;

                            print_r("
                                    <tr>
                                        <td align=\"right\">".$no."</td>
                                        <td align=\"right\">".$value->tgl_transaksi_tr_header."</td>
                                        <td align=\"right\">".$value->id_tr_header."</td>
                                        <td align=\"right\">".$value->nama_suplier."</td>

                                        <td align=\"right\">".$value->jml_item_tr_detail."</td>
                                        <td align=\"right\">".$val_rs."</td>
                                        <td align=\"right\">".$val_pt."</td>
                                        <td align=\"right\">".$val_apt."</td>
                                        <td align=\"right\">".$val_lain."</td>
                                        <td align=\"right\">".$t_pembelian."</td>
                                    </tr>
                                ");

                            $no++;
                        }

                        print_r("
                                    <tr>
                                        <td>#</td>
                                        <td colspan=\"8\"><b>Total Pembelian</b></td>
                                        <td align=\"right\">".$t_pembelian."</td>
                                    </tr>
                                ");

                        $t_masuk += $t_pembelian;
                    }
                ?>
            </tbody>
        </table>
    <!-- Pembelian -->

    <!-- Retur Penjualan -->
        <table style='font-family:calibri; border-collapse: collapse; margin-bottom: 20px' border = '0' >
            <tr>
                <td>&nbsp;</td>    
            </tr>
            <tr>
                <td align="left"><label>Daftar Masuk (Retur Penjualan)</label></td>
                <!-- <hr> -->
            </tr>
        </table>
        <table cellspacing='0' style='width:80%; font-size:10pt; font-family:calibri;  border-collapse: collapse; margin-top: 20px;' border="1">
            <thead>
                <tr>
                    <th style="font-weight: bold;">No.</th>
                    <th style="font-weight: bold;">Tanggal</th>
                    <th style="font-weight: bold;">No Faktur</th>
                    <th style="font-weight: bold;">Pelanggan</th>
                    <!-- <th style="font-weight: bold;">Id Produk</th>
                    <th style="font-weight: bold;">Nama Produk</th> -->
                    <th style="font-weight: bold;">Masuk</th>
                    <th style="font-weight: bold;">RS</th>
                    <th style="font-weight: bold;">PT</th>
                    <th style="font-weight: bold;">APT</th>
                    <th style="font-weight: bold;">Lainnya</th>
                    <th style="font-weight: bold;">Total</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    if(isset($data_retur_penjualan)){
                        $t_retur_penjualan = 0;
                        foreach ($data_retur_penjualan as $key => $value) {
                            $val_rs = "";
                            $val_pt = "";
                            $val_apt = "";
                            $val_lain = "";

                            switch ($value->jenis_rekanan) {
                                case 'rs':
                                    $val_rs = $value->jml_item_tr_detail; 
                                    break;

                                case 'apotik':
                                    $val_apt = $value->jml_item_tr_detail;
                                    break;

                                case 'pbf':
                                    $val_pt = $value->jml_item_tr_detail;
                                    break;
                                
                                default:
                                    $val_lain = $value->jml_item_tr_detail;
                                    break;
                            }

                            $t_retur_penjualan += $value->jml_item_tr_detail;

                            $val_sisa += $value->jml_item_tr_detail;

                            print_r("
                                    <tr>
                                        <td align=\"right\">".$no."</td>
                                        <td align=\"right\">".$value->tgl_transaksi_tr_header."</td>
                                        <td align=\"right\">".$value->id_tr_header_p."</td>
                                        <td align=\"right\">".$value->nama_rekanan."</td>

                                        <td align=\"right\"></td>
                                        <td align=\"right\">".$val_rs."</td>
                                        <td align=\"right\">".$val_pt."</td>
                                        <td align=\"right\">".$val_apt."</td>
                                        <td align=\"right\">".$val_lain."</td>
                                        <td align=\"right\">".$t_retur_penjualan."</td>
                                    </tr>
                                ");

                            $no++;
                        }

                        print_r("
                                    <tr>
                                        <td>#</td>
                                        <td colspan=\"8\"><b>Total Retur Penjualan</b></td>
                                        <td align=\"right\">".$t_retur_penjualan."</td>
                                    </tr>
                                ");

                        $t_masuk += $t_retur_penjualan;
                    }
                ?>
            </tbody>
        </table>
    <!-- Retur Penjualan -->

    <!-- Retur Pengganti Retur Pembelian -->
        <table style='font-family:calibri; border-collapse: collapse; margin-bottom: 20px' border = '0' >
            <tr>
                <td>&nbsp;</td>    
            </tr>
            <tr>
                <td align="left"><label>Daftar Masuk (Pengganti Retur Pembelian)</label></td>
                <!-- <hr> -->
            </tr>
        </table>
        <table cellspacing='0' style='width:80%; font-size:10pt; font-family:calibri;  border-collapse: collapse; margin-top: 20px;' border="1">
            <thead>
                <tr>
                    <th style="font-weight: bold;">No.</th>
                    <th style="font-weight: bold;">Tanggal</th>
                    <th style="font-weight: bold;">No Faktur</th>
                    <th style="font-weight: bold;">Suplier</th>
                    <!-- <th style="font-weight: bold;">Id Produk</th>
                    <th style="font-weight: bold;">Nama Produk</th> -->
                    <th style="font-weight: bold;">Masuk</th>
                    <th style="font-weight: bold;">RS</th>
                    <th style="font-weight: bold;">PT</th>
                    <th style="font-weight: bold;">APT</th>
                    <th style="font-weight: bold;">Lainnya</th>
                    <th style="font-weight: bold;">Total</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    if(isset($data_retur_pembelian_p)){
                        // $val_sisa = 0;
                        $t_retur_pembelian_p = 0;
                        foreach ($data_retur_pembelian_p as $key => $value) {
                            $val_rs = "";
                            $val_pt = "";
                            $val_apt = "";
                            $val_lain = "";

                            $nama_suplier = $value->nama_suplier;

                            $t_retur_pembelian_p += $value->jml_item_tr_detail;

                            // $val_sisa += $value->jml_item_tr_detail;

                            print_r("
                                    <tr>
                                        <td align=\"right\">".$no."</td>
                                        <td align=\"right\">".$value->tgl_transaksi_tr_header."</td>
                                        <td align=\"right\">".$value->id_tr_header_p."</td>
                                        <td align=\"right\">".$value->nama_suplier."</td>

                                        <td align=\"right\">".$value->jml_item_tr_detail."</td>
                                        <td align=\"right\">".$val_rs."</td>
                                        <td align=\"right\">".$val_pt."</td>
                                        <td align=\"right\">".$val_apt."</td>
                                        <td align=\"right\">".$val_lain."</td>
                                        <td align=\"right\">".$t_retur_pembelian_p."</td>
                                    </tr>
                                ");

                            $no++;
                        }
                        print_r("
                                    <tr>
                                        <td>#</td>
                                        <td colspan=\"8\"><b>Total Barang Pengganti Retur Pembelian</b></td>
                                        <td align=\"right\">".$t_retur_pembelian_p."</td>
                                    </tr>
                                ");

                        $t_masuk += $t_retur_pembelian_p;
                    }
                ?>
            </tbody>
        </table>
    <!-- Retur Pengganti Retur Pembelian -->

    <!-- Retur Pengganti Retur Sample -->
        <table style='font-family:calibri; border-collapse: collapse; margin-bottom: 20px' border = '0' >
            <tr>
                <td>&nbsp;</td>    
            </tr>
            <tr>
                <td align="left"><label>Daftar Masuk (Pengganti Sample)</label></td>
                <!-- <hr> -->
            </tr>
        </table>
        <table cellspacing='0' style='width:80%; font-size:10pt; font-family:calibri;  border-collapse: collapse; margin-top: 20px;' border="1">
            <thead>
                <tr>
                    <th style="font-weight: bold;">No.</th>
                    <th style="font-weight: bold;">Tanggal</th>
                    <th style="font-weight: bold;">No Faktur</th>
                    <th style="font-weight: bold;">Pelanggan</th>
                    <!-- <th style="font-weight: bold;">Id Produk</th>
                    <th style="font-weight: bold;">Nama Produk</th> -->
                    <th style="font-weight: bold;">Masuk</th>
                    <th style="font-weight: bold;">RS</th>
                    <th style="font-weight: bold;">PT</th>
                    <th style="font-weight: bold;">APT</th>
                    <th style="font-weight: bold;">Lainnya</th>
                    <th style="font-weight: bold;">Total</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    if(isset($data_retur_sample_p)){
                        $t_retur_sample_p = 0;
                        // $val_sisa = 0;
                        foreach ($data_retur_sample_p as $key => $value) {
                            $val_rs = "";
                            $val_pt = "";
                            $val_apt = "";
                            $val_lain = "";

                            $val_sisa += $value->jml_item_tr_detail;

                            $t_retur_sample_p += $value->jml_item_tr_detail;

                            print_r("
                                    <tr>
                                        <td align=\"right\">".$no."</td>
                                        <td align=\"right\">".$value->tgl_transaksi_tr_header."</td>
                                        <td align=\"right\">".$value->id_tr_header_p."</td>
                                        <td align=\"right\">".$nama_suplier."</td>

                                        <td align=\"right\">".$value->jml_item_tr_detail."</td>
                                        <td align=\"right\">".$val_rs."</td>
                                        <td align=\"right\">".$val_pt."</td>
                                        <td align=\"right\">".$val_apt."</td>
                                        <td align=\"right\">".$val_lain."</td>
                                        <td align=\"right\">".$t_retur_sample_p."</td>
                                    </tr>
                                ");

                            $no++;
                        }

                        print_r("
                                    <tr>
                                        <td>#</td>
                                        <td colspan=\"8\"><b>Total Barang Pengganti Sample</b></td>
                                        <td align=\"right\">".$t_retur_sample_p."</td>
                                    </tr>
                                ");

                        $t_masuk += $t_retur_sample_p;
                    }
                ?>
            </tbody>
        </table>
    <!-- Retur Pengganti Retur Sample -->



    <!-- Penjualan -->
        <table style='font-family:calibri; border-collapse: collapse; margin-bottom: 20px' border = '0' >
            <tr>
                <td>&nbsp;</td>    
            </tr>
            <tr>
                <td align="left"><label>Daftar Keluar (Penjualan)</label></td>
                <!-- <hr> -->
            </tr>
        </table>
        <table cellspacing='0' style='width:80%; font-size:10pt; font-family:calibri;  border-collapse: collapse; margin-top: 20px;' border="1">
            <thead>
                <tr>
                    <th style="font-weight: bold;">No.</th>
                    <th style="font-weight: bold;">Tanggal</th>
                    <th style="font-weight: bold;">No Faktur</th>
                    <th style="font-weight: bold;">Pelanggan</th>
                    <!-- <th style="font-weight: bold;">Id Produk</th>
                    <th style="font-weight: bold;">Nama Produk</th> -->
                    <th style="font-weight: bold;">Masuk</th>
                    <th style="font-weight: bold;">RS</th>
                    <th style="font-weight: bold;">PT</th>
                    <th style="font-weight: bold;">APT</th>
                    <th style="font-weight: bold;">Lainnya</th>
                    <th style="font-weight: bold;">Total</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $t_keluar = 0;
                    if(isset($data_penjualan)){
                        // $val_sisa = 0;
                        $t_penjualan = 0;
                        foreach ($data_penjualan as $key => $value) {
                            // $first_val = 0;
                            // if($key == 0){
                            //     $item_fine = $check_data[0]->stok_record_item;
                            //     $item_broken = $check_data[0]->rusak_record_item;

                            //     $first_val = $item_fine+$item_broken;
                            //     $val_sisa +=$first_val;
                            // }

                            $val_rs = "";
                            $val_pt = "";
                            $val_apt = "";
                            $val_lain = "";

                            switch ($value->jenis_rekanan) {
                                case 'rs':
                                    $val_rs = $value->jml_item_tr_detail; 
                                    break;

                                case 'apotik':
                                    $val_apt = $value->jml_item_tr_detail;
                                    break;

                                case 'pbf':
                                    $val_pt = $value->jml_item_tr_detail;
                                    break;
                                
                                default:
                                    $val_lain = $value->jml_item_tr_detail;
                                    break;
                            }

                            $t_penjualan += $value->jml_item_tr_detail;
                            $val_sisa -= $value->jml_item_tr_detail;

                            print_r("
                                    <tr>
                                        <td>".$no."</td>
                                        <td>".$value->tgl_transaksi_tr_header."</td>
                                        <td>".$value->id_tr_header."</td>
                                        <td>".$value->nama_rekanan."</td>

                                        <td align=\"right\"></td>
                                        <td align=\"right\">".$val_rs."</td>
                                        <td align=\"right\">".$val_pt."</td>
                                        <td align=\"right\">".$val_apt."</td>
                                        <td align=\"right\">".$val_lain."</td>
                                        <td align=\"right\">".$t_penjualan."</td>
                                    </tr>
                                ");

                            $no++;
                        }
                        print_r("
                                    <tr>
                                        <td>#</td>
                                        <td colspan=\"8\"><b>Total Penjualan</b></td>
                                        <td align=\"right\">".$t_penjualan."</td>
                                    </tr>
                                ");

                        $t_keluar += $t_penjualan;
                    }
                ?>
            </tbody>
        </table>
    <!-- Penjualan -->

    <!-- Pengganti Retur Penjualan -->
        <table style='font-family:calibri; border-collapse: collapse; margin-bottom: 20px' border = '0' >
            <tr>
                <td>&nbsp;</td>    
            </tr>
            <tr>
                <td align="left"><label>Daftar Keluar (Pengganti Retur Penjualan)</label></td>
                <!-- <hr> -->
            </tr>
        </table>
        <table cellspacing='0' style='width:80%; font-size:10pt; font-family:calibri;  border-collapse: collapse; margin-top: 20px;' border="1">
            <thead>
                <tr>
                    <th style="font-weight: bold;">No.</th>
                    <th style="font-weight: bold;">Tanggal</th>
                    <th style="font-weight: bold;">No Faktur</th>
                    <th style="font-weight: bold;">Pelanggan</th>
                    <!-- <th style="font-weight: bold;">Id Produk</th>
                    <th style="font-weight: bold;">Nama Produk</th> -->
                    <th style="font-weight: bold;">Masuk</th>
                    <th style="font-weight: bold;">RS</th>
                    <th style="font-weight: bold;">PT</th>
                    <th style="font-weight: bold;">APT</th>
                    <th style="font-weight: bold;">Lainnya</th>
                    <th style="font-weight: bold;">Total</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    if(isset($data_retur_penjualan_p)){
                        $t_retur_penjualan_p = 0;
                        // $val_sisa = 0;
                        foreach ($data_retur_penjualan_p as $key => $value) {
                            // $first_val = 0;
                            // if($key == 0){
                            //     $item_fine = $check_data[0]->stok_record_item;
                            //     $item_broken = $check_data[0]->rusak_record_item;

                            //     $first_val = $item_fine+$item_broken;
                            //     $val_sisa +=$first_val;
                            // }

                            $val_rs = "";
                            $val_pt = "";
                            $val_apt = "";
                            $val_lain = "";

                            switch ($value->jenis_rekanan) {
                                case 'rs':
                                    $val_rs = $value->jml_item_tr_detail; 
                                    break;

                                case 'apotik':
                                    $val_apt = $value->jml_item_tr_detail;
                                    break;

                                case 'pbf':
                                    $val_pt = $value->jml_item_tr_detail;
                                    break;
                                
                                default:
                                    $val_lain = $value->jml_item_tr_detail;
                                    break;
                            }

                            $t_retur_penjualan_p += $value->jml_item_tr_detail;
                            $val_sisa -= $value->jml_item_tr_detail;

                            print_r("
                                    <tr>
                                        <td>".$no."</td>
                                        <td>".$value->tgl_transaksi_tr_header."</td>
                                        <td>".$value->id_tr_header_p."</td>
                                        <td>".$value->nama_rekanan."</td>

                                        <td align=\"right\"></td>
                                        <td align=\"right\">".$val_rs."</td>
                                        <td align=\"right\">".$val_pt."</td>
                                        <td align=\"right\">".$val_apt."</td>
                                        <td align=\"right\">".$val_lain."</td>
                                        <td align=\"right\">".$t_retur_penjualan_p."</td>
                                    </tr>
                                ");

                            $no++;
                        }
                        print_r("
                                    <tr>
                                        <td>#</td>
                                        <td colspan=\"8\"><b>Total Pengganti Retur Penjualan</b></td>
                                        <td align=\"right\">".$t_retur_penjualan_p."</td>
                                    </tr>
                                ");

                        $t_keluar += $t_retur_penjualan_p;
                    }
                ?>
            </tbody>
        </table>
    <!-- Pengganti Retur Penjualan -->

    <!-- Retur Pembelian -->
        <table style='font-family:calibri; border-collapse: collapse; margin-bottom: 20px' border = '0' >
            <tr>
                <td>&nbsp;</td>    
            </tr>
            <tr>
                <td align="left"><label>Daftar Keluar (Retur Pembelian)</label></td>
                <!-- <hr> -->
            </tr>
        </table>
        <table cellspacing='0' style='width:80%; font-size:10pt; font-family:calibri;  border-collapse: collapse; margin-top: 20px;' border="1">
            <thead>
                <tr>
                    <th style="font-weight: bold;">No.</th>
                    <th style="font-weight: bold;">Tanggal</th>
                    <th style="font-weight: bold;">No Faktur</th>
                    <th style="font-weight: bold;">Suplier</th>
                    <!-- <th style="font-weight: bold;">Id Produk</th>
                    <th style="font-weight: bold;">Nama Produk</th> -->
                    <th style="font-weight: bold;">Masuk</th>
                    <th style="font-weight: bold;">RS</th>
                    <th style="font-weight: bold;">PT</th>
                    <th style="font-weight: bold;">APT</th>
                    <th style="font-weight: bold;">Lainnya</th>
                    <th style="font-weight: bold;">Total</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    if(isset($data_retur_pembelian)){
                        // $val_sisa = 0;
                        $t_retur_pembelian = 0;
                        foreach ($data_retur_pembelian as $key => $value) {
                            // $first_val = 0;
                            // if($key == 0){
                            //     $item_fine = $check_data[0]->stok_record_item;
                            //     $item_broken = $check_data[0]->rusak_record_item;

                            //     $first_val = $item_fine+$item_broken;
                            //     $val_sisa +=$first_val;
                            // }

                            $val_rs = "";
                            $val_pt = "";
                            $val_apt = "";
                            $val_lain = "";

                            // switch ($value->jenis_rekanan) {
                            //     case 'rs':
                            //         $val_rs = $value->jml_item_tr_detail; 
                            //         break;

                            //     case 'apotik':
                            //         $val_apt = $value->jml_item_tr_detail;
                            //         break;

                            //     case 'pbf':
                                    $val_pt = $value->jml_item_tr_detail;
                                    // break;
                                
                            //     default:
                            //         $val_lain = $value->jml_item_tr_detail;
                            //         break;
                            // }

                            $t_retur_pembelian += $value->jml_item_tr_detail;
                            $val_sisa -= $value->jml_item_tr_detail;

                            print_r("
                                    <tr>
                                        <td>".$no."</td>
                                        <td>".$value->tgl_transaksi_tr_header."</td>
                                        <td>".$value->id_tr_header_p."</td>
                                        <td>".$value->nama_suplier."</td>

                                        <td align=\"right\"></td>
                                        <td align=\"right\">".$val_rs."</td>
                                        <td align=\"right\">".$val_pt."</td>
                                        <td align=\"right\">".$val_apt."</td>
                                        <td align=\"right\">".$val_lain."</td>
                                        <td align=\"right\">".$t_retur_pembelian."</td>
                                    </tr>
                                ");

                            $no++;
                        }

                        print_r("
                                    <tr>
                                        <td>#</td>
                                        <td colspan=\"8\"><b>Total Retur Pembelian</b></td>
                                        <td align=\"right\">".$t_retur_pembelian."</td>
                                    </tr>
                                ");
                        $t_keluar += $t_retur_pembelian;
                    }
                ?>
            </tbody>
        </table>
    <!-- Retur Pembelian -->

    <!-- Pengganti Sample -->
        <table style='font-family:calibri; border-collapse: collapse; margin-bottom: 20px' border = '0' >
            <tr>
                <td>&nbsp;</td>    
            </tr>
            <tr>
                <td align="left"><label>Daftar Keluar (Sample)</label></td>
                <!-- <hr> -->
            </tr>
        </table>
        <table cellspacing='0' style='width:80%; font-size:10pt; font-family:calibri;  border-collapse: collapse; margin-top: 20px;' border="1">
            <thead>
                <tr>
                    <th style="font-weight: bold;">No.</th>
                    <th style="font-weight: bold;">Tanggal</th>
                    <th style="font-weight: bold;">No Faktur</th>
                    <th style="font-weight: bold;">Pelanggan</th>
                    <!-- <th style="font-weight: bold;">Id Produk</th>
                    <th style="font-weight: bold;">Nama Produk</th> -->
                    <th style="font-weight: bold;">Masuk</th>
                    <th style="font-weight: bold;">RS</th>
                    <th style="font-weight: bold;">PT</th>
                    <th style="font-weight: bold;">APT</th>
                    <th style="font-weight: bold;">Lainnya</th>
                    <th style="font-weight: bold;">Total</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    if(isset($data_sample)){
                        // $val_sisa = 0;
                        $t_sample = 0;
                        foreach ($data_sample as $key => $value) {
                            // $first_val = 0;
                            // if($key == 0){
                            //     $item_fine = $check_data[0]->stok_record_item;
                            //     $item_broken = $check_data[0]->rusak_record_item;

                            //     $first_val = $item_fine+$item_broken;
                            //     $val_sisa +=$first_val;
                            // }

                            $val_rs = "";
                            $val_pt = "";
                            $val_apt = "";
                            $val_lain = "";

                            switch ($value->jenis_rekanan) {
                                case 'rs':
                                    $val_rs = $value->jml_item_tr_detail; 
                                    break;

                                case 'apotik':
                                    $val_apt = $value->jml_item_tr_detail;
                                    break;

                                case 'pbf':
                                    $val_pt = $value->jml_item_tr_detail;
                                    break;
                                
                                default:
                                    $val_lain = $value->jml_item_tr_detail;
                                    break;
                            }

                            $t_sample += $value->jml_item_tr_detail;
                            $val_sisa -= $value->jml_item_tr_detail;

                            print_r("
                                    <tr>
                                        <td>".$no."</td>
                                        <td>".$value->tgl_transaksi_tr_header."</td>
                                        <td>".$value->id_tr_header."</td>
                                        <td>".$value->nama_rekanan."</td>

                                        <td align=\"right\"></td>
                                        <td align=\"right\">".$val_rs."</td>
                                        <td align=\"right\">".$val_pt."</td>
                                        <td align=\"right\">".$val_apt."</td>
                                        <td align=\"right\">".$val_lain."</td>
                                        <td align=\"right\">".$t_sample."</td>
                                    </tr>
                                ");

                            $no++;
                        }

                        print_r("
                                    <tr>
                                        <td>#</td>
                                        <td colspan=\"8\"><b>Total Sample</b></td>
                                        <td align=\"right\">".$t_sample."</td>
                                    </tr>
                                ");

                        $t_keluar += $t_sample;
                    }
                ?>
            </tbody>
        </table>
    <!-- Pengganti Sample -->


    <!-- Estimasi Stok Akhir -->
        <?php
        // print_r($item_v);
            $id_item            = "";
            $nama_item          = "";
            $kode_produksi_item = "";
            $stok               = 0;

            if(isset($item_v)){
                if($item_v){
                    $id_item            = $item_v->id_item;
                    $nama_item          = $item_v->nama_item;
                    $kode_produksi_item = $item_v->kode_produksi_item;
                    $stok               = $item_v->stok;

                    
                }
            }
            
            $stok_awal = (int)$stok -(int)$t_masuk +(int)$t_keluar;
            
        ?>
        <table style='font-family:calibri; border-collapse: collapse; margin-bottom: 20px' border = '0' >
            <tr>
                <td>&nbsp;</td>    
            </tr>
            <tr>
                <td align="left"><label>Estimasi Stok Akhir</label></td>
                <!-- <hr> -->
            </tr>
        </table>
        <table cellspacing='0' style='width:80%; font-size:10pt; font-family:calibri;  border-collapse: collapse; margin-top: 20px;' border="1">
            <thead>
                <tr>
                    <th style="font-weight: bold;">Kode Item</th>
                    <th style="font-weight: bold;">Nama Item</th>
                    <th style="font-weight: bold;">Kode Produksi Item</th>
                    <th style="font-weight: bold;">Estimasi Stok Awal</th>
                    <th style="font-weight: bold;">Total Barang Masuk</th>
                    <th style="font-weight: bold;">Total Barang Keluar</th>
                    <th style="font-weight: bold;">Stok Real</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><?php print_r($id_item); ?></td>
                    <td><?php print_r($nama_item); ?></td>
                    <td><?php print_r($kode_produksi_item); ?></td>
                    <td><?php print_r($stok_awal); ?></td>
                    <td><?php print_r($t_masuk); ?></td>
                    <td><?php print_r($t_keluar); ?></td>
                    <td><?php print_r($stok); ?></td>
                </tr>
            </tbody>
        </table>
    <!-- Estimasi Stok Akhir -->

        <br><br>
        <table align="right" cellspacing='0' style='width:60%; font-size:10pt; font-family:calibri;  border-collapse: collapse; margin-top: 20px;' border="0">
            <tr >
                <td style='padding-right:30px; text-align: center;'>Surabaya, 15 Oktober 2019</td>
            </tr>
            <tr >
                <td style='padding-bottom: 65px; padding-right:30px; text-align: center;'>PT. BLESSINDO FARMA</td>
            </tr>
            <tr>
                <td style="text-decoration: underline; padding-right:30px; text-align: center;">Yuliani Lemantara, Ssi, Apt.</td>
            </tr>
            <tr>
                <td style='padding-right:30px; text-align: center;'>19760310/SIKA-35.78/2016/2219</td>
            </tr>
        </table>



    </center>
</body>
<script type="text/javascript">window.print();</script>
</html>

