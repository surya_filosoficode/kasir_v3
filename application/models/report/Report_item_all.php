<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Report_item_all extends CI_Model{


// penjualan item
    public function get_penjualan_item_tgl($tgl_start, $tgl_finish, $where){
        $this->db->join("tr_header th", "th.id_tr_header = td.id_tr_header");
        $this->db->join("rekanan rk", "rk.id_rekanan = th.id_customer");
        // $this->db->join("sales sl", "sl.id_sales = th.id_sales");
        $this->db->join("item it", "td.id_item = it.id_item");

        $this->db->where('tgl_transaksi_tr_header >=', $tgl_start);
        $this->db->where('tgl_transaksi_tr_header <=', $tgl_finish);

        $this->db->or_where('tgl_transaksi_tr_header =', $tgl_finish);
        $this->db->or_where('tgl_transaksi_tr_header =', $tgl_finish);

        $this->db->order_by("tgl_transaksi_tr_header", "asc");
        
        $data = $this->db->get_where("tr_detail td", $where)->result();

        return $data;
    }


    public function get_penjualan_item_th($th_start, $th_finish, $where){
        $this->db->join("tr_header th", "th.id_tr_header = td.id_tr_header");
        $this->db->join("rekanan rk", "rk.id_rekanan = th.id_customer");
        // $this->db->join("sales sl", "sl.id_sales = th.id_sales");

        $this->db->join("item it", "td.id_item = it.id_item");

        $this->db->where('YEAR(th.tgl_transaksi_tr_header) >=', $th_start);
        $this->db->where('YEAR(th.tgl_transaksi_tr_header) <=', $th_finish);

        $this->db->or_where('YEAR(th.tgl_transaksi_tr_header) =', $th_start);
        $this->db->or_where('YEAR(th.tgl_transaksi_tr_header) =', $th_finish);

        $this->db->order_by("tgl_transaksi_tr_header", "asc");
        
        $data = $this->db->get_where("tr_detail td", $where)->result();

        return $data;
    }


    public function get_penjualan_item_bulan($bulan, $th, $where){
        $this->db->join("tr_header th", "th.id_tr_header = td.id_tr_header");
        $this->db->join("rekanan rk", "rk.id_rekanan = th.id_customer");
        // $this->db->join("sales sl", "sl.id_sales = th.id_sales");
        $this->db->join("item it", "td.id_item = it.id_item");

        $this->db->where('MONTH(th.tgl_transaksi_tr_header) = ', $bulan);
        $this->db->where('YEAR(th.tgl_transaksi_tr_header) =', $th);

        
        $data = $this->db->get_where("tr_detail td", $where)->result();

        return $data;
    }


    public function get_penjualan_item_triwulan($th, $where_in, $where){
        $this->db->join("tr_header th", "th.id_tr_header = td.id_tr_header");
        $this->db->join("rekanan rk", "rk.id_rekanan = th.id_customer");
        
        $this->db->join("item it", "td.id_item = it.id_item");

        $this->db->where('YEAR(th.tgl_transaksi_tr_header) =', $th);
        $this->db->where_in('MONTH(th.tgl_transaksi_tr_header)', $where_in);
        
        $data = $this->db->get_where("tr_detail td", $where)->result();

        return $data;
    }


// pembelian item
    public function get_pembelian_item_tgl($tgl_start, $tgl_finish, $where){
        $this->db->join("tr_pb_header th", "th.id_tr_header = td.id_tr_header");
        $this->db->join("suplier rk", "rk.id_suplier = th.id_suplier");
        // $this->db->join("sales sl", "sl.id_sales = th.id_sales");
        $this->db->join("item it", "td.id_item = it.id_item");

        $this->db->where('tgl_transaksi_tr_header >=', $tgl_start);
        $this->db->where('tgl_transaksi_tr_header <=', $tgl_finish);

        $this->db->or_where('tgl_transaksi_tr_header =', $tgl_start);
        $this->db->or_where('tgl_transaksi_tr_header =', $tgl_finish);
        
        $data = $this->db->get_where("tr_pb_detail td", $where)->result();

        return $data;
    }


    public function get_pembelian_item_th($th_start, $th_finish, $where){
        $this->db->join("tr_pb_header th", "th.id_tr_header = td.id_tr_header");
        $this->db->join("suplier rk", "rk.id_suplier = th.id_suplier");
        // $this->db->join("sales sl", "sl.id_sales = th.id_sales");

        $this->db->join("item it", "td.id_item = it.id_item");

        $this->db->where('YEAR(th.tgl_transaksi_tr_header) >=', $th_start);
        $this->db->where('YEAR(th.tgl_transaksi_tr_header) <=', $th_finish);

        $this->db->or_where('YEAR(th.tgl_transaksi_tr_header) =', $th_start);
        $this->db->or_where('YEAR(th.tgl_transaksi_tr_header) =', $th_finish);
        
        $data = $this->db->get_where("tr_pb_detail td", $where)->result();

        return $data;
    }


    public function get_pembelian_item_bulan($bulan, $th, $where){
        $this->db->join("tr_pb_header th", "th.id_tr_header = td.id_tr_header");
        $this->db->join("suplier rk", "rk.id_suplier = th.id_suplier");
        // $this->db->join("sales sl", "sl.id_sales = th.id_sales");
        $this->db->join("item it", "td.id_item = it.id_item");

        $this->db->where('MONTH(th.tgl_transaksi_tr_header) = ', $bulan);
        $this->db->where('YEAR(th.tgl_transaksi_tr_header) =', $th);
        
        $data = $this->db->get_where("tr_pb_detail td", $where)->result();

        return $data;
    }


    public function get_pembelian_item_triwulan($th, $where_in, $where){
        $this->db->join("tr_pb_header th", "th.id_tr_header = td.id_tr_header");
        $this->db->join("suplier rk", "rk.id_suplier = th.id_suplier");
        
        $this->db->join("item it", "td.id_item = it.id_item");

        $this->db->where('YEAR(th.tgl_transaksi_tr_header) =', $th);
        $this->db->where_in('MONTH(th.tgl_transaksi_tr_header)', $where_in);
        
        $data = $this->db->get_where("tr_pb_detail td", $where)->result();

        return $data;
    }


// retur_penjualan
    public function get_retur_penjualan_detail($where){
        $this->db->join("item i", "i.id_item = td.id_item");
        $this->db->join("tr_rt_pj_header th", "td.id_tr_header = th.id_tr_header");
        $this->db->join("rekanan rk", "rk.id_rekanan = th.id_customer");
        
        $data = $this->db->get_where("tr_rt_pj_detail td", $where)->result();

        return $data;
    }

    public function get_retur_penjualan_detail_p($where){
        $this->db->join("item i", "i.id_item = td.id_item");
        $this->db->join("tr_rt_pj_header_p th", "td.id_tr_header_p = th.id_tr_header_p");
        $this->db->join("rekanan rk", "rk.id_rekanan = th.id_customer");

        $data = $this->db->get_where("tr_rt_pj_detail_p td", $where)->result();

        return $data;
    }


    public function get_retur_penjualan_detail_triwulan($where_in, $where){
        $this->db->join("item i", "i.id_item = td.id_item");
        $this->db->join("tr_rt_pj_header th", "td.id_tr_header = th.id_tr_header");
        $this->db->join("rekanan rk", "rk.id_rekanan = th.id_customer");
        
        $data = $this->db->get_where("tr_rt_pj_detail td", $where)->result();

        return $data;
    }

    public function get_retur_penjualan_detail_p_triwulan($where_in, $where){
        $this->db->join("item i", "i.id_item = td.id_item");
        $this->db->join("tr_rt_pj_header_p th", "td.id_tr_header_p = th.id_tr_header_p");
        $this->db->join("rekanan rk", "rk.id_rekanan = th.id_customer");

        $data = $this->db->get_where("tr_rt_pj_detail_p td", $where)->result();

        return $data;
    }


// retur_pembelian
    public function get_retur_pembelian_detail($where){
        $this->db->join("item i", "i.id_item = td.id_item");
        $this->db->join("tr_rt_pb_header th", "td.id_tr_header = th.id_tr_header");
        $this->db->join("suplier rk", "rk.id_suplier = th.id_suplier");
        
        $data = $this->db->get_where("tr_rt_pb_detail td", $where)->result();

        return $data;
    }

    public function get_retur_pembelian_detail_p($where){
        $this->db->join("item i", "i.id_item = td.id_item");
        $this->db->join("tr_rt_pb_header_p th", "td.id_tr_header_p = th.id_tr_header_p");
        $this->db->join("suplier rk", "rk.id_suplier = th.id_suplier");

        $data = $this->db->get_where("tr_rt_pb_detail_p td", $where)->result();

        return $data;
    }


    public function get_retur_pembelian_detail_triwulan($where_in, $where){
        $this->db->join("item i", "i.id_item = td.id_item");
        $this->db->join("tr_rt_pb_header th", "td.id_tr_header = th.id_tr_header");
        $this->db->join("suplier rk", "rk.id_suplier = th.id_suplier");
        
        $data = $this->db->get_where("tr_rt_pb_detail td", $where)->result();

        return $data;
    }

    public function get_retur_pembelian_detail_p_triwulan($where_in, $where){
        $this->db->join("item i", "i.id_item = td.id_item");
        $this->db->join("tr_rt_pb_header_p th", "td.id_tr_header_p = th.id_tr_header_p");
        $this->db->join("suplier rk", "rk.id_suplier = th.id_suplier");

        $data = $this->db->get_where("tr_rt_pb_detail_p td", $where)->result();

        return $data;
    }


// retur_sample
    public function get_sample_detail($where){
        $this->db->join("item i", "i.id_item = td.id_item");
        $this->db->join("tr_sm_header th", "td.id_tr_header = th.id_tr_header");
        $this->db->join("rekanan rk", "rk.id_rekanan = th.id_customer");
        
        $data = $this->db->get_where("tr_sm_detail td", $where)->result();

        return $data;
    }

    public function get_retur_sample_detail_p($where){
        $this->db->join("item i", "i.id_item = td.id_item");
        $this->db->join("tr_sm_rt_header_p th", "td.id_tr_header_p = th.id_tr_header_p");

        $data = $this->db->get_where("tr_sm_rt_detail_p td", $where)->result();

        return $data;
    }


    public function get_sample_detail_triwulan($where_in, $where){
        $this->db->join("item i", "i.id_item = td.id_item");
        $this->db->join("tr_sm_header th", "td.id_tr_header = th.id_tr_header");
        $this->db->join("rekanan rk", "rk.id_rekanan = th.id_customer");
        
        $data = $this->db->get_where("tr_sm_detail td", $where)->result();

        return $data;
    }

    public function get_retur_sample_detail_p_triwulan($where_in, $where){
        $this->db->join("item i", "i.id_item = td.id_item");
        $this->db->join("tr_sm_rt_header_p th", "td.id_tr_header_p = th.id_tr_header_p");

        $data = $this->db->get_where("tr_sm_rt_detail_p td", $where)->result();

        return $data;
    }


// check first item
    public function get_first_time($where){
        $this->db->order_by("periode_record_item", "asc");
        $data = $this->db->get_where("record_item", $where)->result();

        return $data;
    }


    public function get_record_item($where, $where_in){
        $this->db->where_in("MONTH(periode_record_item)", $where_in);

        $this->db->order_by("periode_record_item", "asc");
        $data = $this->db->get_where("record_item", $where)->result();

        return $data;
    }

}
?>