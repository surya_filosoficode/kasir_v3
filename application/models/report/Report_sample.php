<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Report_sample extends CI_Model{

    public function get_sample_header_tgl($tgl_start, $tgl_finish, $where){
        $this->db->where('tgl_transaksi_tr_header >=', $tgl_start);
        $this->db->where('tgl_transaksi_tr_header <=', $tgl_finish);
        
        $data = $this->db->get_where("tr_sm_rt_header_p th", $where)->result();

        return $data;
    }

    public function get_sample_th($th_start, $th_finish, $where){
        $this->db->where('YEAR(th.tgl_transaksi_tr_header) >=', $th_start);
        $this->db->where('YEAR(th.tgl_transaksi_tr_header) <=', $th_finish);
        
        $data = $this->db->get_where("tr_sm_rt_header_p th", $where)->result();

        return $data;
    }


    public function get_sample_bulan($bulan, $th, $where){
        $this->db->where('MONTH(th.tgl_transaksi_tr_header) = ', $bulan);
        $this->db->where('YEAR(th.tgl_transaksi_tr_header) =', $th);
        
        $data = $this->db->get_where("tr_sm_rt_header_p th", $where)->result();

        return $data;
    }


    public function get_sample_triwulan($th, $where_in, $where){
        $this->db->where('YEAR(th.tgl_transaksi_tr_header) =', $th);
        $this->db->where_in('MONTH(th.tgl_transaksi_tr_header)', $where_in);
        
        $data = $this->db->get_where("tr_sm_rt_header_p th", $where)->result();

        return $data;
    }


    public function get_sample_detail_p($where){
        $this->db->join("item i", "i.id_item = th.id_item");
        
        $data = $this->db->get_where("tr_sm_rt_detail_p th", $where)->result();

        return $data;
    }

    public function get_sample_detail($where){
        $this->db->join("item i", "i.id_item = th.id_item");
        
        $data = $this->db->get_where("tr_sm_detail th", $where)->result();

        return $data;
    }
}
?>