<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Report_item_akhir extends CI_Model{

    public function get_all_item_short($where, $param_short = "nama_item", $type_short = "ASC"){
        $this->db->order_by($param_short, $type_short);
        $data = $this->db->get_where("item", $where)->result();

        return $data;
    }

    
}
?>