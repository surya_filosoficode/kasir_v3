<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Item_main extends CI_Model{

	public function get_data_produk_where($where){
        // $this->db->select("id_brand, lpad(id_brand, 3, 0) as id_brand_txt, nama_brand, keterangan_brand");
        $this->db->join("brand b", "i.id_brand = b.id_brand");
    	$data = $this->db->get_where("item i", $where);
    	return $data->result();
    }

    public function get_distinct_nama_produk($where){
        $this->db->distinct();
        $this->db->select('nama_item');
        $data = $this->db->get_where("item", $where);
        return $data->result();
    }

    public function get_data_produk_where_each($where){
        // $this->db->select("id_brand, lpad(id_brand, 3, 0) as id_brand_txt, nama_brand, keterangan_brand");
        $this->db->join("brand b", "i.id_brand = b.id_brand");
    	$data = $this->db->get_where("item i", $where);
    	return $data->row_array();
    }
    
    public function get_data_brand_where($where){
        $this->db->select("id_brand, lpad(id_brand, 3, 0) as id_brand_txt, nama_brand, keterangan_brand");
    	$data = $this->db->get_where("brand", $where);
    	return $data->result();
    }


    public function insert_record_item($periode_record_item, $id_item, $stok_record_item, $rusak_record_item){
        $insert = $this->db->query("SELECT insert_record_item('".$periode_record_item."', '".$id_item."', '".$stok_record_item."', '".$rusak_record_item."') AS id_record_item");
        return $insert->row_array();
    }


#------------------------------show---------------------------------#

    public function get_active_record_item($where){
        $this->db->join("item it", "ri.id_item = it.id_item");
        $this->db->where("it.is_del_item", "0");
        $this->db->order_by('it.nama_item', 'ASC');
        $data = $this->db->get_where("record_item ri", $where)->result();

        return $data;
    }
}
?>