    function currency(x){
        return x.toLocaleString('us-EG');
    }

    function create_alert(title, msg, status){
        $(function() {
            "use strict";                      
           $.toast({
            heading: title,
            text: msg,
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: status,
            hideAfter: 3500, 
            stack: 6
          });
        });
    }

    function create_sweet_alert(title, msg, status, url_seccess) {
        ! function($) {
            "use strict";
            // var next = swal.close();
            var SweetAlert = function() {};
            if(status == "success"){
                SweetAlert.prototype.init = function() {
                    swal({   
                        title: title,   
                        text: msg,   
                        type: status,   
                        showCancelButton: false,   
                        confirmButtonColor: "#DD6B55",   
                        confirmButtonText: "Lanjutkan",   
                        cancelButtonText: "Perbaiki",   
                        closeOnConfirm: false,   
                        closeOnCancel: false 
                    }, function(isConfirm){   
                        if (isConfirm) {
                            window.location.href = url_seccess;  
                        } 
                    });
                },
                $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            }else {
                SweetAlert.prototype.init = function() {
                    swal({   
                        title: title,   
                        text: msg,   
                        type: status,   
                        showCancelButton: false,   
                        confirmButtonColor: "#DD6B55",   
                        confirmButtonText: "Perbaiki",   
                        cancelButtonText: "Perbaiki",   
                        closeOnConfirm: false,   
                        closeOnCancel: false 
                    }, function(isConfirm){   
                        if (isConfirm) {
                            swal.close();
                        } 
                    });
                },
                $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            }
        }(window.jQuery),

        function($) {
            "use strict";
            $.SweetAlert.init()
        }(window.jQuery);
    }