-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 20, 2020 at 05:57 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.1.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kasir`
--

DELIMITER $$
--
-- Functions
--
CREATE DEFINER=`root`@`localhost` FUNCTION `insert_admin` (`id_tipe_admin` VARCHAR(3), `email` TEXT, `username` VARCHAR(15), `password` TEXT, `status_active` ENUM('0','1','2'), `nama_admin` VARCHAR(64), `nip_admin` VARCHAR(64)) RETURNS VARCHAR(14) CHARSET latin1 BEGIN
  declare last_key_user varchar(20);
  declare count_row_user int;
  declare fix_key_user varchar(12);
  
  select count(*) into count_row_user from admin 
  	where substr(id_admin,3,6) = left(NOW()+0, 6);
        
  select id_admin into last_key_user from admin
  	where substr(id_admin,3,6) = left(NOW()+0, 6)
  	order by id_admin desc limit 1;
    
  if(count_row_user <1) then
  	set fix_key_user = concat("AD",left(NOW()+0, 6),"0001");
  else
      	set fix_key_user = concat("AD",substr(last_key_user,3,10)+1);
      
  END IF;
  
  
  insert into admin values(fix_key_user, id_tipe_admin, email, username, password, status_active, nama_admin, nip_admin, '0');
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_item` (`id_brand_in` INT, `nama_item` TEXT, `satuan` VARCHAR(32), `kode_produksi_item` TEXT, `tgl_kadaluarsa_item` DATE, `harga_bruto` VARCHAR(64), `harga_netto` VARCHAR(64), `harga_jual` VARCHAR(64), `admin_create_item` VARCHAR(12), `time_up_item` DATETIME, `is_del_item` ENUM('0','1')) RETURNS VARCHAR(14) CHARSET latin1 BEGIN
  declare last_key_user varchar(14);
  declare count_row_user int;
  declare fix_key_user varchar(14);
   
  select count(*) into count_row_user from item WHERE id_brand = id_brand_in;
        
  select id_item into last_key_user from item
  	WHERE id_brand = id_brand_in
    order by id_item desc limit 1;
    
  if(count_row_user <1) then
  	set fix_key_user = concat(LPAD(id_brand_in, 3, '0'), "00001");
  else
    set fix_key_user = concat(LPAD(id_brand_in, 3, '0'), LPAD(RIGHT(last_key_user,5)+1, 5, '0'));
      
  END IF;
  
  
  insert into item values(fix_key_user, id_brand_in, nama_item, kode_produksi_item, tgl_kadaluarsa_item, '0', '0', satuan, harga_bruto, harga_netto, harga_jual, admin_create_item, time_up_item, is_del_item);
    
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_pb_tr_detail` (`id_tr_header_in` VARCHAR(15), `id_item` VARCHAR(10), `harga_satuan_tr_detail` VARCHAR(32), `jml_item_tr_detail` VARCHAR(8), `harga_total_tr_detail` VARCHAR(32), `disc_item_tr_detail` VARCHAR(8), `harga_total_fix_tr_detail` VARCHAR(32), `admin_create_tr_detail` VARCHAR(12), `time_up_tr_detail` DATETIME) RETURNS VARCHAR(19) CHARSET latin1 BEGIN
  declare last_key_user varchar(19);
  declare count_row_user int;
  declare fix_key_user varchar(19);
   
  select count(*) into count_row_user from tr_pb_detail where substr(id_tr_detail,1,15) = id_tr_header_in;
        
  select id_tr_detail into last_key_user from tr_pb_detail where substr(id_tr_detail,1,15) = id_tr_header_in order by id_tr_detail desc limit 1;
  if(count_row_user <1) then
  	set fix_key_user = concat(id_tr_header_in, "-001");
  else
    set fix_key_user = concat(id_tr_header_in, "-", LPAD(RIGHT(last_key_user, 3)+1, 3, '0'));  
  END IF;
  
  insert into tr_pb_detail values(fix_key_user, id_tr_header_in, id_item, harga_satuan_tr_detail, jml_item_tr_detail, harga_total_tr_detail, disc_item_tr_detail, harga_total_fix_tr_detail, admin_create_tr_detail, time_up_tr_detail, "0");
    
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_pb_tr_header` (`id_suplier` VARCHAR(13), `tgl_transaksi_tr_header` DATE, `cara_pembayaran_tr_header` ENUM('0','1'), `tempo_tr_header` DATE, `status_hutang` ENUM('0','1'), `total_pembayaran_tr_header` VARCHAR(32), `disc_all_tr_header` VARCHAR(8), `total_pembayaran_disc_tr_header` VARCHAR(32), `ppn_tr_header` VARCHAR(8), `total_pembayaran_pnn_tr_header` VARCHAR(32), `admin_create_tr_header` VARCHAR(12), `time_up_tr_header` DATETIME) RETURNS VARCHAR(15) CHARSET latin1 BEGIN
  declare last_key_user varchar(15);
  declare count_row_user int;
  declare fix_key_user varchar(15);
   
  select count(*) into count_row_user from tr_pb_header where substr(id_tr_header,4,4) = left(NOW()+0, 4);
        
  select id_tr_header into last_key_user from tr_pb_header where 	substr(id_tr_header,4,4) = left(NOW()+0, 4) order by id_tr_header desc limit 1;
  if(count_row_user <1) then
  	set fix_key_user = concat("TBH", left(NOW()+0, 4), "-", "0000001");
  else
    set fix_key_user = concat("TBH", left(NOW()+0, 4), "-", LPAD(RIGHT(last_key_user,7)+1, 7, '0'));  
  END IF;
  
  insert into tr_pb_header values(fix_key_user, id_suplier,  tgl_transaksi_tr_header, cara_pembayaran_tr_header, tempo_tr_header, status_hutang, total_pembayaran_tr_header, disc_all_tr_header, total_pembayaran_disc_tr_header, ppn_tr_header, total_pembayaran_pnn_tr_header, admin_create_tr_header, time_up_tr_header, "0");
    
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_record_item` (`periode_record_item` DATE, `id_item` VARCHAR(10), `stok_record_item` VARCHAR(16), `rusak_record_item` VARCHAR(16)) RETURNS VARCHAR(17) CHARSET latin1 BEGIN
  declare last_key_user varchar(17);
  declare count_row_user int;
  declare fix_key_user varchar(17);
   
  select count(*) into count_row_user from record_item where substr(id_record_item,1,8) = left(NOW()+0, 8);
        
  select id_record_item into last_key_user from record_item where 	substr(id_record_item,1,8) = left(NOW()+0, 8) order by id_record_item desc limit 1;
  if(count_row_user <1) then
  	set fix_key_user = concat(left(NOW()+0, 8), "-", "0000001");
  else
    set fix_key_user = concat(left(NOW()+0, 8), "-", LPAD(RIGHT(last_key_user,7)+1, 7, '0'));  
  END IF;
  
  insert into record_item values(fix_key_user, periode_record_item, id_item, stok_record_item,  rusak_record_item);
    
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_record_stok` (`id_tr_detail` VARCHAR(19), `id_item` VARCHAR(10), `tgl_insert` DATE, `keterangan_record_stok` TEXT, `jenis_record_stok` VARCHAR(64), `status_record_stok` VARCHAR(64), `stok_awal_record_stok` INT(11), `stok_tr_record_stok_before` INT(11), `stok_tr_record_stok` INT(11), `stok_akhir_record_stok` INT(11), `admin_create_record_stok` VARCHAR(12), `time_up_record_stok` DATETIME) RETURNS VARCHAR(15) CHARSET latin1 BEGIN
  declare last_key_user varchar(15);
  declare count_row_user int;
  declare fix_key_user varchar(15);
   
  select count(*) into count_row_user from record_stok where substr(id_record,4,8) = left(NOW()+0, 8);
        
  select id_record into last_key_user from record_stok where 	substr(id_record,4,8) = left(NOW()+0, 8) order by id_record desc limit 1;
  if(count_row_user <1) then
  	set fix_key_user = concat("RCI", left(NOW()+0, 8), "0001");
  else
    set fix_key_user = concat("RCI", left(NOW()+0, 8), LPAD(RIGHT(last_key_user,4)+1, 4, '0'));  
  END IF;
  
  insert into record_stok values(fix_key_user, id_tr_detail, id_item, tgl_insert, keterangan_record_stok, jenis_record_stok, status_record_stok, stok_awal_record_stok, stok_tr_record_stok_before, stok_tr_record_stok, stok_akhir_record_stok, admin_create_record_stok, time_up_record_stok, "0");
    
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_record_stok_opnam` (`id_tr_detail` VARCHAR(19), `id_item` VARCHAR(10), `tgl_insert` DATE, `keterangan_record_stok` TEXT, `jenis_record_stok` VARCHAR(64), `status_record_stok` VARCHAR(64), `stok_awal_record_stok` INT(11), `stok_tr_record_stok_before` INT(11), `stok_tr_record_stok` INT(11), `stok_akhir_record_stok` INT(11), `admin_create_record_stok` VARCHAR(12), `time_up_record_stok` DATETIME) RETURNS VARCHAR(15) CHARSET latin1 BEGIN
  declare last_key_user varchar(15);
  declare count_row_user int;
  declare fix_key_user varchar(15);
   
  select count(*) into count_row_user from record_stok_opnam where substr(id_record,4,8) = left(NOW()+0, 8);
        
  select id_record into last_key_user from record_stok_opnam where 	substr(id_record,4,8) = left(NOW()+0, 8) order by id_record desc limit 1;
  if(count_row_user <1) then
  	set fix_key_user = concat("RCO", left(NOW()+0, 8), "0001");
  else
    set fix_key_user = concat("RCO", left(NOW()+0, 8), LPAD(RIGHT(last_key_user,4)+1, 4, '0'));  
  END IF;
  
  insert into record_stok_opnam values(fix_key_user, id_tr_detail, id_item, tgl_insert, keterangan_record_stok, jenis_record_stok, status_record_stok, stok_awal_record_stok, stok_tr_record_stok_before, stok_tr_record_stok, stok_akhir_record_stok, admin_create_record_stok, time_up_record_stok, "0");
    
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_rekanan` (`nama_rekanan` TEXT, `jenis_rekanan` TEXT, `email_rekanan` TEXT, `tlp_rekanan` VARCHAR(13), `alamat_ktr_rekanan` TEXT, `alamat_krm_rekanan` TEXT, `website_rekanan` TEXT, `is_delete` ENUM('0','1'), `time_update` DATETIME, `id_admin` VARCHAR(12)) RETURNS VARCHAR(14) CHARSET latin1 BEGIN
  declare last_key_user varchar(14);
  declare count_row_user int;
  declare fix_key_user varchar(14);
   
  select count(*) into count_row_user from rekanan where substr(id_rekanan,4,6) = left(NOW()+0, 6);
        
  select id_rekanan into last_key_user from rekanan
  	where substr(id_rekanan,4,6) = left(NOW()+0, 6)
  	order by id_rekanan desc limit 1;
    
  if(count_row_user <1) then
  	set fix_key_user = concat("RKA", left(NOW()+0, 6),"0001");
  else
      	set fix_key_user = concat("RKA", left(NOW()+0, 6), LPAD(RIGHT(last_key_user,4)+1, 4, '0'));
      
  END IF;
  
  
  insert into rekanan values(fix_key_user, nama_rekanan, jenis_rekanan,  email_rekanan,  tlp_rekanan, alamat_ktr_rekanan, alamat_krm_rekanan, website_rekanan, is_delete, time_update, id_admin);
    
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_rt_pb_detail` (`id_tr_header_in` VARCHAR(15), `id_item` VARCHAR(10), `kode_produksi_tr_detail` TEXT, `tgl_kadaluarsa_tr_detail` DATE, `harga_satuan_tr_detail` VARCHAR(32), `jml_item_tr_detail` VARCHAR(8), `harga_total_tr_detail` VARCHAR(32), `status_rt_tr_detail` ENUM('0','1'), `admin_create_tr_detail` VARCHAR(12), `time_up_tr_detail` DATETIME) RETURNS VARCHAR(19) CHARSET latin1 BEGIN
  declare last_key_user varchar(19);
  declare count_row_user int;
  declare fix_key_user varchar(19);
   
  select count(*) into count_row_user from tr_rt_pb_detail where substr(id_tr_detail,1,15) = id_tr_header_in;
        
  select id_tr_detail into last_key_user from tr_rt_pb_detail where substr(id_tr_detail,1,15) = id_tr_header_in order by id_tr_detail desc limit 1;
  if(count_row_user <1) then
  	set fix_key_user = concat(id_tr_header_in, "-001");
  else
    set fix_key_user = concat(id_tr_header_in, "-", LPAD(RIGHT(last_key_user, 3)+1, 3, '0'));  
  END IF;
  
  insert into tr_rt_pb_detail values(fix_key_user, id_tr_header_in, id_item, kode_produksi_tr_detail, tgl_kadaluarsa_tr_detail, harga_satuan_tr_detail, jml_item_tr_detail, harga_total_tr_detail, status_rt_tr_detail,  admin_create_tr_detail, time_up_tr_detail, "0");
    
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_rt_pb_detail_p` (`id_tr_header_in` VARCHAR(15), `id_item` VARCHAR(10), `kode_produksi_tr_detail` TEXT, `tgl_kadaluarsa_tr_detail` DATE, `harga_satuan_tr_detail` VARCHAR(32), `jml_item_tr_detail` VARCHAR(8), `harga_total_tr_detail` VARCHAR(32), `admin_create_tr_detail` VARCHAR(12), `time_up_tr_detail` DATETIME) RETURNS VARCHAR(19) CHARSET latin1 BEGIN
  declare last_key_user varchar(19);
  declare count_row_user int;
  declare fix_key_user varchar(19);
   
  select count(*) into count_row_user from tr_rt_pb_detail_p where substr(id_tr_detail,1,15) = id_tr_header_in;
        
  select id_tr_detail into last_key_user from tr_rt_pb_detail_p where substr(id_tr_detail,1,15) = id_tr_header_in order by id_tr_detail desc limit 1;
  if(count_row_user <1) then
  	set fix_key_user = concat(id_tr_header_in, "-001");
  else
    set fix_key_user = concat(id_tr_header_in, "-", LPAD(RIGHT(last_key_user, 3)+1, 3, '0'));  
  END IF;
  
  insert into tr_rt_pb_detail_p values(fix_key_user, id_tr_header_in, id_item, kode_produksi_tr_detail, tgl_kadaluarsa_tr_detail, harga_satuan_tr_detail, jml_item_tr_detail, harga_total_tr_detail,  admin_create_tr_detail, time_up_tr_detail, "0");
    
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_rt_pb_header` (`id_tr_header_p` VARCHAR(15), `status_retur` ENUM('0','1'), `selisih` VARCHAR(64), `id_suplier` VARCHAR(13), `tgl_transaksi_tr_header` DATE, `total_pembayaran_tr_header` VARCHAR(32), `admin_create_tr_header` VARCHAR(12), `time_up_tr_header` DATETIME) RETURNS VARCHAR(15) CHARSET latin1 BEGIN
  declare last_key_user varchar(15);
  declare count_row_user int;
  declare fix_key_user varchar(15);
   
  select count(*) into count_row_user from tr_rt_pb_header where substr(id_tr_header,4,4) = left(NOW()+0, 4);
        
  select id_tr_header into last_key_user from tr_rt_pb_header where 	substr(id_tr_header,4,4) = left(NOW()+0, 4) order by id_tr_header desc limit 1;
  if(count_row_user <1) then
  	set fix_key_user = concat("RBM", left(NOW()+0, 4), "-", "0000001");
  else
    set fix_key_user = concat("RBM", left(NOW()+0, 4), "-", LPAD(RIGHT(last_key_user,7)+1, 7, '0'));  
  END IF;
  
  insert into tr_rt_pb_header values(fix_key_user, id_tr_header_p, status_retur, selisih, id_suplier, tgl_transaksi_tr_header, total_pembayaran_tr_header, admin_create_tr_header, time_up_tr_header, "0");
    
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_rt_pb_header_p` (`id_suplier` VARCHAR(13), `tgl_transaksi_tr_header` DATE, `total_pembayaran_tr_header` VARCHAR(32), `admin_create_tr_header` VARCHAR(12), `time_up_tr_header` DATETIME) RETURNS VARCHAR(15) CHARSET latin1 BEGIN
  declare last_key_user varchar(15);
  declare count_row_user int;
  declare fix_key_user varchar(15);
   
  select count(*) into count_row_user from tr_rt_pb_header_p where substr(id_tr_header_p,4,4) = left(NOW()+0, 4);
        
  select id_tr_header_p into last_key_user from tr_rt_pb_header_p where 	substr(id_tr_header_p,4,4) = left(NOW()+0, 4) order by id_tr_header_p desc limit 1;
  if(count_row_user <1) then
  	set fix_key_user = concat("RBP", left(NOW()+0, 4), "-", "0000001");
  else
    set fix_key_user = concat("RBP", left(NOW()+0, 4), "-", LPAD(RIGHT(last_key_user,7)+1, 7, '0'));  
  END IF;
  
  insert into tr_rt_pb_header_p values(fix_key_user, id_suplier, tgl_transaksi_tr_header, total_pembayaran_tr_header, admin_create_tr_header, time_up_tr_header, "0");
    
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_rt_pj_detail` (`id_tr_header_in` VARCHAR(15), `id_item` VARCHAR(10), `kode_produksi_tr_detail` TEXT, `tgl_kadaluarsa_tr_detail` DATE, `harga_satuan_tr_detail` VARCHAR(32), `jml_item_tr_detail` VARCHAR(8), `harga_total_tr_detail` VARCHAR(32), `status_rt_tr_detail` ENUM('0','1'), `admin_create_tr_detail` VARCHAR(12), `time_up_tr_detail` DATETIME) RETURNS VARCHAR(19) CHARSET latin1 BEGIN
  declare last_key_user varchar(19);
  declare count_row_user int;
  declare fix_key_user varchar(19);
   
  select count(*) into count_row_user from tr_rt_pj_detail where substr(id_tr_detail,1,15) = id_tr_header_in;
        
  select id_tr_detail into last_key_user from tr_rt_pj_detail where substr(id_tr_detail,1,15) = id_tr_header_in order by id_tr_detail desc limit 1;
  if(count_row_user <1) then
  	set fix_key_user = concat(id_tr_header_in, "-001");
  else
    set fix_key_user = concat(id_tr_header_in, "-", LPAD(RIGHT(last_key_user, 3)+1, 3, '0'));  
  END IF;
  
  insert into tr_rt_pj_detail values(fix_key_user, id_tr_header_in, id_item, kode_produksi_tr_detail, tgl_kadaluarsa_tr_detail, harga_satuan_tr_detail, jml_item_tr_detail, harga_total_tr_detail, status_rt_tr_detail,  admin_create_tr_detail, time_up_tr_detail, "0");
    
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_rt_pj_detail_p` (`id_tr_header_in` VARCHAR(15), `id_item` VARCHAR(10), `kode_produksi_tr_detail` TEXT, `tgl_kadaluarsa_tr_detail` DATE, `harga_satuan_tr_detail` VARCHAR(32), `jml_item_tr_detail` VARCHAR(8), `harga_total_tr_detail` VARCHAR(32), `admin_create_tr_detail` VARCHAR(12), `time_up_tr_detail` DATETIME) RETURNS VARCHAR(19) CHARSET latin1 BEGIN
  declare last_key_user varchar(19);
  declare count_row_user int;
  declare fix_key_user varchar(19);
   
  select count(*) into count_row_user from tr_rt_pj_detail_p where substr(id_tr_detail,1,15) = id_tr_header_in;
        
  select id_tr_detail into last_key_user from tr_rt_pj_detail_p where substr(id_tr_detail,1,15) = id_tr_header_in order by id_tr_detail desc limit 1;
  if(count_row_user <1) then
  	set fix_key_user = concat(id_tr_header_in, "-001");
  else
    set fix_key_user = concat(id_tr_header_in, "-", LPAD(RIGHT(last_key_user, 3)+1, 3, '0'));  
  END IF;
  
  insert into tr_rt_pj_detail_p values(fix_key_user, id_tr_header_in, id_item, kode_produksi_tr_detail, tgl_kadaluarsa_tr_detail, harga_satuan_tr_detail, jml_item_tr_detail, harga_total_tr_detail,  admin_create_tr_detail, time_up_tr_detail, "0");
    
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_rt_pj_header` (`id_tr_header_p` VARCHAR(15), `selisih` VARCHAR(64), `id_customer` VARCHAR(13), `tgl_transaksi_tr_header` DATE, `total_pembayaran_tr_header` VARCHAR(32), `admin_create_tr_header` VARCHAR(12), `time_up_tr_header` DATETIME) RETURNS VARCHAR(15) CHARSET latin1 BEGIN
  declare last_key_user varchar(15);
  declare count_row_user int;
  declare fix_key_user varchar(15);
   
  select count(*) into count_row_user from tr_rt_pj_header where substr(id_tr_header,4,4) = left(NOW()+0, 4);
        
  select id_tr_header into last_key_user from tr_rt_pj_header where 	substr(id_tr_header,4,4) = left(NOW()+0, 4) order by id_tr_header desc limit 1;
  if(count_row_user <1) then
  	set fix_key_user = concat("RPM", left(NOW()+0, 4), "-", "0000001");
  else
    set fix_key_user = concat("RPM", left(NOW()+0, 4), "-", LPAD(RIGHT(last_key_user,7)+1, 7, '0'));  
  END IF;
  
  insert into tr_rt_pj_header values(fix_key_user, id_tr_header_p, selisih, id_customer, tgl_transaksi_tr_header, total_pembayaran_tr_header, admin_create_tr_header, time_up_tr_header, "0");
    
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_rt_pj_header_p` (`id_customer` VARCHAR(13), `tgl_transaksi_tr_header` DATE, `total_pembayaran_tr_header` VARCHAR(32), `admin_create_tr_header` VARCHAR(12), `time_up_tr_header` DATETIME) RETURNS VARCHAR(15) CHARSET latin1 BEGIN
  declare last_key_user varchar(15);
  declare count_row_user int;
  declare fix_key_user varchar(15);
   
  select count(*) into count_row_user from tr_rt_pj_header_p where substr(id_tr_header_p,4,4) = left(NOW()+0, 4);
        
  select id_tr_header_p into last_key_user from tr_rt_pj_header_p where 	substr(id_tr_header_p,4,4) = left(NOW()+0, 4) order by id_tr_header_p desc limit 1;
  if(count_row_user <1) then
  	set fix_key_user = concat("RPP", left(NOW()+0, 4), "-", "0000001");
  else
    set fix_key_user = concat("RPP", left(NOW()+0, 4), "-", LPAD(RIGHT(last_key_user,7)+1, 7, '0'));  
  END IF;
  
  insert into tr_rt_pj_header_p values(fix_key_user, id_customer, tgl_transaksi_tr_header, total_pembayaran_tr_header, admin_create_tr_header, time_up_tr_header, "0");
    
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_sales` (`nama_sales` VARCHAR(64), `jk_sales` ENUM('0','1'), `alamat_sales` TEXT, `tlp_sales` VARCHAR(13), `nik_sales` VARCHAR(16), `is_delete` ENUM('0','1'), `time_update` DATETIME, `id_admin` VARCHAR(12)) RETURNS VARCHAR(11) CHARSET latin1 BEGIN
  declare last_key_user varchar(14);
  declare count_row_user int;
  declare fix_key_user varchar(14);
   
  select count(*) into count_row_user from sales where substr(id_sales,3,4) = left(NOW()+0, 4);
        
  select id_sales into last_key_user from sales
  	where substr(id_sales,3,4) = left(NOW()+0, 4)
  	order by id_sales desc limit 1;
    
  if(count_row_user <1) then
  	set fix_key_user = concat("SL", left(NOW()+0, 4),"0001");
  else
      	set fix_key_user = concat("SL", left(NOW()+0, 4), LPAD(RIGHT(last_key_user,4)+1, 4, '0'));
      
  END IF;
  
  
  insert into sales values(fix_key_user, nama_sales, jk_sales,  alamat_sales, tlp_sales, nik_sales, is_delete, time_update, id_admin);
    
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_sm_rt_detail_p` (`id_tr_header_in` VARCHAR(15), `id_item` VARCHAR(10), `kode_produksi_tr_detail` TEXT, `tgl_kadaluarsa_tr_detail` DATE, `harga_satuan_tr_detail` VARCHAR(32), `jml_item_tr_detail` VARCHAR(8), `harga_total_tr_detail` VARCHAR(32), `admin_create_tr_detail` VARCHAR(12), `time_up_tr_detail` DATETIME) RETURNS VARCHAR(19) CHARSET latin1 BEGIN
  declare last_key_user varchar(19);
  declare count_row_user int;
  declare fix_key_user varchar(19);
   
  select count(*) into count_row_user from tr_sm_rt_detail_p where substr(id_tr_detail,1,15) = id_tr_header_in;
        
  select id_tr_detail into last_key_user from tr_sm_rt_detail_p where substr(id_tr_detail,1,15) = id_tr_header_in order by id_tr_detail desc limit 1;
  if(count_row_user <1) then
  	set fix_key_user = concat(id_tr_header_in, "-001");
  else
    set fix_key_user = concat(id_tr_header_in, "-", LPAD(RIGHT(last_key_user, 3)+1, 3, '0'));  
  END IF;
  
  insert into tr_sm_rt_detail_p values(fix_key_user, id_tr_header_in, id_item, kode_produksi_tr_detail, tgl_kadaluarsa_tr_detail, harga_satuan_tr_detail, jml_item_tr_detail, harga_total_tr_detail,  admin_create_tr_detail, time_up_tr_detail, "0");
    
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_sm_rt_header_p` (`tgl_transaksi_tr_header` DATE, `total_pembayaran_tr_header` VARCHAR(32), `admin_create_tr_header` VARCHAR(12), `time_up_tr_header` DATETIME) RETURNS VARCHAR(15) CHARSET latin1 BEGIN
  declare last_key_user varchar(15);
  declare count_row_user int;
  declare fix_key_user varchar(15);
   
  select count(*) into count_row_user from tr_sm_rt_header_p where substr(id_tr_header_p,4,4) = left(NOW()+0, 4);
        
  select id_tr_header_p into last_key_user from tr_sm_rt_header_p where 	substr(id_tr_header_p,4,4) = left(NOW()+0, 4) order by id_tr_header_p desc limit 1;
  if(count_row_user <1) then
  	set fix_key_user = concat("RSP", left(NOW()+0, 4), "-", "0000001");
  else
    set fix_key_user = concat("RSP", left(NOW()+0, 4), "-", LPAD(RIGHT(last_key_user,7)+1, 7, '0'));  
  END IF;
  
  insert into tr_sm_rt_header_p values(fix_key_user, tgl_transaksi_tr_header, total_pembayaran_tr_header, admin_create_tr_header, time_up_tr_header, "0");
    
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_sm_tr_detail` (`id_tr_header_in` VARCHAR(15), `id_item` VARCHAR(10), `kode_produksi_tr_detail` TEXT, `tgl_kadaluarsa_tr_detail` DATE, `harga_satuan_tr_detail` VARCHAR(32), `jml_item_tr_detail` VARCHAR(8), `harga_total_tr_detail` VARCHAR(32), `admin_create_tr_detail` VARCHAR(12), `time_up_tr_detail` DATETIME) RETURNS VARCHAR(19) CHARSET latin1 BEGIN
  declare last_key_user varchar(19);
  declare count_row_user int;
  declare fix_key_user varchar(19);
   
  select count(*) into count_row_user from tr_sm_detail where substr(id_tr_detail,1,15) = id_tr_header_in;
        
  select id_tr_detail into last_key_user from tr_sm_detail where substr(id_tr_detail,1,15) = id_tr_header_in order by id_tr_detail desc limit 1;
  if(count_row_user <1) then
  	set fix_key_user = concat(id_tr_header_in, "-001");
  else
    set fix_key_user = concat(id_tr_header_in, "-", LPAD(RIGHT(last_key_user, 3)+1, 3, '0'));  
  END IF;
  
  insert into tr_sm_detail values(fix_key_user, id_tr_header_in, id_item, kode_produksi_tr_detail, tgl_kadaluarsa_tr_detail, harga_satuan_tr_detail, jml_item_tr_detail, harga_total_tr_detail, admin_create_tr_detail, time_up_tr_detail, "0");
    
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_sm_tr_header` (`id_customer` VARCHAR(13), `atas_nama` TEXT, `tgl_transaksi_tr_header` DATE, `total_pembayaran_tr_header` VARCHAR(32), `admin_create_tr_header` VARCHAR(12), `time_up_tr_header` DATETIME) RETURNS VARCHAR(15) CHARSET latin1 BEGIN
  declare last_key_user varchar(15);
  declare count_row_user int;
  declare fix_key_user varchar(15);
   
  select count(*) into count_row_user from tr_sm_header where substr(id_tr_header,4,4) = left(NOW()+0, 4);
        
  select id_tr_header into last_key_user from tr_sm_header where 	substr(id_tr_header,4,4) = left(NOW()+0, 4) order by id_tr_header desc limit 1;
  if(count_row_user <1) then
  	set fix_key_user = concat("TSH", left(NOW()+0, 4), "-", "0000001");
  else
    set fix_key_user = concat("TSH", left(NOW()+0, 4), "-",  LPAD(RIGHT(last_key_user,7)+1, 7, '0'));  
  END IF;
  
  insert into tr_sm_header values(fix_key_user, "0", "0", id_customer, atas_nama, tgl_transaksi_tr_header, total_pembayaran_tr_header, "0", admin_create_tr_header, time_up_tr_header, "0");
    
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_suplier` (`nama_suplier` TEXT, `jenis_suplier` TEXT, `email_suplier` TEXT, `tlp_suplier` VARCHAR(13), `alamat_ktr_suplier` TEXT, `alamat_krm_suplier` TEXT, `website` TEXT, `is_delete` ENUM('0','1'), `time_update` DATETIME, `id_admin` VARCHAR(12)) RETURNS VARCHAR(14) CHARSET latin1 BEGIN
  declare last_key_user varchar(14);
  declare count_row_user int;
  declare fix_key_user varchar(14);
   
  select count(*) into count_row_user from suplier where substr(id_suplier,4,6) = left(NOW()+0, 6);
        
  select id_suplier into last_key_user from suplier
  	where substr(id_suplier,4,6) = left(NOW()+0, 6)
  	order by id_suplier desc limit 1;
    
  if(count_row_user <1) then
  	set fix_key_user = concat("SUP", left(NOW()+0, 6),"0001");
  else
      	set fix_key_user = concat("SUP", left(NOW()+0, 6), LPAD(RIGHT(last_key_user,4)+1, 4, '0'));
      
  END IF;
  
  
  insert into suplier values(fix_key_user, nama_suplier, jenis_suplier, email_suplier,  tlp_suplier, alamat_ktr_suplier, alamat_krm_suplier, website, is_delete, time_update, id_admin);
    
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_tr_detail` (`id_tr_header_in` VARCHAR(15), `id_item` VARCHAR(10), `harga_satuan_tr_detail` VARCHAR(32), `jml_item_tr_detail` VARCHAR(8), `harga_total_tr_detail` VARCHAR(32), `disc_item_tr_detail` VARCHAR(8), `harga_total_fix_tr_detail` VARCHAR(32), `admin_create_tr_detail` VARCHAR(12), `time_up_tr_detail` DATETIME) RETURNS VARCHAR(19) CHARSET latin1 BEGIN
  declare last_key_user varchar(19);
  declare count_row_user int;
  declare fix_key_user varchar(19);
   
  select count(*) into count_row_user from tr_detail where substr(id_tr_detail,1,15) = id_tr_header_in;
        
  select id_tr_detail into last_key_user from tr_detail where substr(id_tr_detail,1,15) = id_tr_header_in order by id_tr_detail desc limit 1;
  if(count_row_user <1) then
  	set fix_key_user = concat(id_tr_header_in, "-001");
  else
    set fix_key_user = concat(id_tr_header_in, "-", LPAD(RIGHT(last_key_user, 3)+1, 3, '0'));  
  END IF;
  
  insert into tr_detail values(fix_key_user, id_tr_header_in, id_item, harga_satuan_tr_detail, jml_item_tr_detail, harga_total_tr_detail, disc_item_tr_detail, harga_total_fix_tr_detail, admin_create_tr_detail, time_up_tr_detail, "0");
    
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_tr_header` (`id_customer` VARCHAR(13), `id_sales` VARCHAR(10), `tgl_transaksi_tr_header` DATE, `cara_pembayaran_tr_header` ENUM('0','1'), `tempo_tr_header` DATE, `status_hutang` ENUM('0','1','2'), `total_pembayaran_tr_header` VARCHAR(32), `disc_all_tr_header` VARCHAR(8), `total_pembayaran_disc_tr_header` VARCHAR(32), `ppn_tr_header` VARCHAR(8), `total_pembayaran_pnn_tr_header` VARCHAR(32), `admin_create_tr_header` VARCHAR(12), `time_up_tr_header` DATETIME) RETURNS VARCHAR(15) CHARSET latin1 BEGIN
  declare last_key_user varchar(15);
  declare count_row_user int;
  declare fix_key_user varchar(15);
   
  select count(*) into count_row_user from tr_header where substr(id_tr_header,4,4) = left(NOW()+0, 4);
        
  select id_tr_header into last_key_user from tr_header where 	substr(id_tr_header,4,4) = left(NOW()+0, 4) order by id_tr_header desc limit 1;
  if(count_row_user <1) then
  	set fix_key_user = concat("TRH", left(NOW()+0, 4), "-", "0000001");
  else
    set fix_key_user = concat("TRH", left(NOW()+0, 4), "-", LPAD(RIGHT(last_key_user,7)+1, 7, '0'));  
  END IF;
  
  insert into tr_header values(fix_key_user, id_customer, id_sales,  tgl_transaksi_tr_header, cara_pembayaran_tr_header, tempo_tr_header, status_hutang, total_pembayaran_tr_header, disc_all_tr_header, total_pembayaran_disc_tr_header, ppn_tr_header, total_pembayaran_pnn_tr_header, admin_create_tr_header, time_up_tr_header, "0");
    
  
  return fix_key_user;
  
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id_admin` char(12) NOT NULL,
  `id_tipe_admin` varchar(3) NOT NULL,
  `email` text NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(256) NOT NULL,
  `status_active` enum('0','1','2') NOT NULL,
  `nama_admin` varchar(100) NOT NULL,
  `nip_admin` varchar(64) NOT NULL,
  `is_delete` enum('0','1') NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id_admin`, `id_tipe_admin`, `email`, `username`, `password`, `status_active`, `nama_admin`, `nip_admin`, `is_delete`) VALUES
('AD2019110001', '0', 'suryahanggara@gmail.com', 'admin', '21232f297a57a5a743894a0e4a801fc3', '1', 'admin', '20190001', '0'),
('AD2019110002', '0', 'sudirman@gmail.com', 'yuli', '21232f297a57a5a743894a0e4a801fc3', '1', 'yuli', '7838372', '0'),
('AD2019110003', '0', 'dimas@gmail.comx', 'ara', '636bfa0fb2716ff876f5e33854cc9648', '1', 'ara', 'a', '1'),
('AD2019120001', '0', 'vasa@gmail.com', 'hela', '21232f297a57a5a743894a0e4a801fc3', '1', 'hela', 'a', '0'),
('AD2019120002', '0', 'didi@gmail.com', 'didi', '21232f297a57a5a743894a0e4a801fc3', '1', 'didi', 'a', '0');

-- --------------------------------------------------------

--
-- Table structure for table `brand`
--

CREATE TABLE `brand` (
  `id_brand` int(11) NOT NULL,
  `nama_brand` text NOT NULL,
  `keterangan_brand` text NOT NULL,
  `admin_create_brand` varchar(12) NOT NULL,
  `time_up_brand` datetime NOT NULL,
  `is_del_brand` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `brand`
--

INSERT INTO `brand` (`id_brand`, `nama_brand`, `keterangan_brand`, `admin_create_brand`, `time_up_brand`, `is_del_brand`) VALUES
(1, 'Cendo', '-', 'AD2019110001', '2019-12-31 11:01:01', '0');

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

CREATE TABLE `item` (
  `id_item` varchar(10) NOT NULL,
  `id_brand` int(11) NOT NULL,
  `nama_item` text NOT NULL,
  `kode_produksi_item` text NOT NULL,
  `tgl_kadaluarsa_item` date NOT NULL,
  `stok` int(11) NOT NULL,
  `stok_opnam` int(11) NOT NULL,
  `satuan` varchar(32) NOT NULL,
  `harga_bruto` varchar(64) NOT NULL,
  `harga_netto` varchar(64) NOT NULL,
  `harga_jual` varchar(64) NOT NULL,
  `admin_create_item` varchar(12) NOT NULL,
  `time_up_item` datetime NOT NULL,
  `is_del_item` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item`
--

INSERT INTO `item` (`id_item`, `id_brand`, `nama_item`, `kode_produksi_item`, `tgl_kadaluarsa_item`, `stok`, `stok_opnam`, `satuan`, `harga_bruto`, `harga_netto`, `harga_jual`, `admin_create_item`, `time_up_item`, `is_del_item`) VALUES
('00100002', 1, 'ALBUVIT 15% 5 ML', '9A50916', '2022-02-09', 4700, 0, 'fls', '11250', '11250', '11250', 'AD2019110001', '2020-01-22 16:13:16', '0'),
('00100004', 1, 'C.M.C 5 ML', '7CC1005', '2020-10-28', 5100, 0, 'fls', '15125', '15125', '15125', 'AD2019110001', '2020-02-02 16:14:50', '0'),
('00100122', 1, 'MYDRIATIL 1% 5 ML INH', '8M0206', '2021-02-28', 500, 100, 'fls', '37091', '37091', '37091', 'AD2019110001', '2020-02-02 14:06:50', '0'),
('00100126', 1, 'NONCORT MDS INH', '9N60524', '2022-05-28', 900, 0, 'stp', '40182', '40182', '40182', 'AD2019110001', '2020-01-23 14:55:22', '0'),
('00100127', 1, 'POSOP MDS INH', '9PO60125', '2022-05-28', 100, 0, 'stp', '49545', '49545', '49545', 'AD2019110001', '2020-01-23 14:55:34', '0'),
('00100128', 1, 'P-PRED MDS INH', '9PP60524', '2022-05-28', 0, 0, 'stp', '36909', '36909', '36909', 'AD2019110001', '2020-01-23 14:56:02', '0'),
('00100129', 1, 'PROTAGENTA MDS INH', '9PR60152', '2022-05-28', 0, 0, 'stp', '33636', '33636', '33636', 'AD2019110001', '2020-01-23 14:56:21', '0'),
('00100130', 1, 'TOBRO MDS INH', '9TO60154', '2022-05-28', 0, 0, 'stp', '18545', '18545', '18545', 'AD2019110001', '2020-01-23 14:56:35', '0'),
('00100131', 1, 'TONOR 0,5% MDS INH', '9TR60524', '2022-05-28', 0, 0, 'stp', '23409', '23409', '23409', 'AD2019110001', '2020-01-23 14:56:54', '0'),
('00100132', 1, 'VOSAMA MDS INH', '9V60154', '2022-05-28', 0, 0, 'stp', '17184', '17184', '17184', 'AD2019110001', '2020-01-23 14:57:11', '0'),
('00100133', 1, 'LYTEERS 15 ML E-KATALOG', '9L1001', '2022-10-28', 500, 0, 'fls', '17386', '17386', '17386', 'AD2019110001', '2020-02-10 14:09:58', '0'),
('00100134', 1, 'ALBUVIT 10% 5 ML', '9A11102', '2021-11-28', 62, 0, 'fls', '10175', '10', '10175', 'AD2019110001', '2020-02-02 12:28:42', '0'),
('00100135', 1, 'C.M.C 5 ML', '8CC0410', '2021-04-28', 40, 0, 'FLS', '15125', '10', '15125', 'AD2019110001', '2020-02-02 12:30:04', '0'),
('00100136', 1, 'CARPINE 1% 5 ML', '9C10313', '2022-03-28', 0, 0, 'fls', '15500', '10', '15500', 'AD2019110001', '2020-02-02 12:30:58', '0'),
('00100137', 1, 'CARPINE 1% 5 ML', '9C10516', '2022-05-28', 20, 0, 'fls', '15500', '10', '15500', 'AD2019110001', '2020-02-02 12:31:21', '0'),
('00100138', 1, 'CARPINE 1% 5 ML', '9C10816', '2022-08-28', 50, 0, 'fls', '15500', '10', '15500', 'AD2019110001', '2020-02-02 12:31:40', '0'),
('00100139', 1, 'CARPINE 2% 5 ML', '9C20109', '2022-01-28', 0, 0, 'fls', '19000', '10', '19000', 'AD2019110001', '2020-02-02 12:32:07', '0'),
('00100140', 1, 'CARPINE 2% 5 ML', '9C20626', '2022-06-28', 15, 0, 'fls', '19000', '10', '19000', 'AD2019110001', '2020-02-02 12:32:26', '0'),
('00100141', 1, 'CARPINE 2% 5 ML', '9C20715', '2022-07-28', 0, 0, 'fls', '19000', '10', '19000', 'AD2019110001', '2020-02-02 12:32:47', '0'),
('00100142', 1, 'CARPINE 2% 5 ML', '9C20923', '2022-09-28', 150, 0, 'fls', '19000', '10', '19000', 'AD2019110001', '2020-02-02 12:33:09', '0'),
('00100143', 1, 'CATARLENT 5 ML', '9CA0924', '2021-09-28', 74, 0, 'FLS', '22625', '10', '22625', 'AD2019110001', '2020-02-02 12:34:31', '0'),
('00100144', 1, 'CATARLENT 5 ML', '9CA1017', '2021-10-28', 2, 0, 'FLS', '22625', '10', '22625', 'AD2019110001', '2020-02-02 12:34:53', '0'),
('00100145', 1, 'CATARLENT 15 ML', '9CN1113', '2021-11-28', 600, 0, 'FLS', '30625', '10', '30625', 'AD2019110001', '2020-02-02 12:35:37', '0'),
('00100146', 1, 'CATARLENT 15 ML', '9CN1125', '2021-11-28', 34, 0, 'FLS', '30625', '10', '30625', 'AD2019110001', '2020-02-02 12:35:58', '0'),
('00100147', 1, 'CATARLENT 15 ML', '9CN1010', '2021-10-28', 0, 0, 'FLS', '30625', '10', '30625', 'AD2019110001', '2020-02-02 12:36:16', '0'),
('00100148', 1, 'CENDRID 5 ML', '9CD0107', '2022-01-28', 25, 0, 'FLS', '15125', '10', '15125', 'AD2019110001', '2020-02-02 12:36:47', '0'),
('00100149', 1, 'CENDRID 5 ML', '8CD0314', '2021-03-29', 7, 0, 'FLS', '15125', '10', '15125', 'AD2019110001', '2020-02-02 12:37:17', '0'),
('00100150', 1, 'CENFRESH 5 ML', '9C1017', '2022-10-28', 0, 0, 'FLS', '34875', '10', '34875', 'AD2019110001', '2020-02-02 12:37:46', '0'),
('00100151', 1, 'CENFRESH 5 ML', '9C1021', '2022-10-28', 0, 0, 'FLS', '34875', '10', '34875', 'AD2019110001', '2020-02-02 12:38:06', '0'),
('00100152', 1, 'ALBUVIT 15% 5 ML', '9A509146', '0022-09-28', 0, 0, 'FLS', '11250', '10', '11250', 'AD2019110001', '2020-02-02 12:38:32', '0'),
('00100153', 1, 'CONAL 5 ML', '9C01106', '2022-11-28', 74, 0, 'FLS', '20250', '10', '20250', 'AD2019110001', '2020-02-02 12:40:49', '0'),
('00100154', 1, 'CONJUCTO 5 ML', '8CJ0709', '2021-01-28', 34, 0, 'FLS', '10375', '10', '10375', 'AD2019110001', '2020-02-02 12:41:15', '0'),
('00100155', 1, 'CONJUCTO 5 ML', '9CJ0227', '2021-08-28', 50, 0, 'FLS', '10375', '10375', '10375', 'AD2019110001', '2020-02-07 11:18:37', '0'),
('00100156', 1, 'CONVER 2% 15 ML', '9CV0902', '2022-09-28', 68, 0, 'FLS', '13000', '10', '13000', 'AD2019110001', '2020-02-02 12:41:57', '0'),
('00100157', 1, 'CONVER 2% 15 ML', '9CV1102', '2022-11-28', 50, 0, 'FLS', '13000', '10', '13000', 'AD2019110001', '2020-02-02 12:42:25', '0'),
('00100158', 1, 'CONVER 2% 15 ML', '9CV1204', '2022-12-28', 50, 0, 'FLS', '13000', '10', '13000', 'AD2019110001', '2020-02-02 12:42:44', '0'),
('00100159', 1, 'CORTHON 5 ML', '7CR1127', '2020-05-28', 50, 0, 'FLS', '22500', '10', '22500', 'AD2019110001', '2020-02-02 12:43:06', '0'),
('00100160', 1, 'CORTHON 5 ML', '9CR0720', '2022-01-28', 33, 0, 'FLS', '22500', '10', '22500', 'AD2019110001', '2020-02-02 12:43:33', '0'),
('00100161', 1, 'CYCLON 5 ML', '9CY0725', '2022-07-28', 50, 0, 'FLS', '17750', '10', '17750', 'AD2019110001', '2020-02-02 12:44:00', '0'),
('00100162', 1, 'CYCLON 5 ML', '9CY0909', '2022-09-28', 50, 0, 'FLS', '17750', '10', '17750', 'AD2019110001', '2020-02-02 12:44:22', '0'),
('00100163', 1, 'DEXATON 5 ML', '9D0703', '2022-01-28', 1, 0, 'FLS', '18150', '10', '18150', 'AD2019110001', '2020-02-02 12:44:46', '0'),
('00100164', 1, 'DEXATON 5 ML', '9D0912', '2022-03-28', 0, 0, 'FLS', '18150', '10', '18150', 'AD2019110001', '2020-02-02 12:45:12', '0'),
('00100165', 1, 'EFRISEL 10% 5 ML', '9E11004', '2021-10-28', 59, 0, 'FLS', '15250', '10', '15250', 'AD2019110001', '2020-02-02 12:45:49', '0'),
('00100166', 1, 'EYEFRESH 5 ML', '9EH0904', '2022-09-28', 0, 0, 'FLS', '28875', '10', '28875', 'AD2019110001', '2020-02-02 12:46:09', '0'),
('00100167', 1, 'EYEFRESH 5 ML', '9EH0918', '2022-09-28', 53, 0, 'FLS', '28875', '10', '28875', 'AD2019110001', '2020-02-02 12:46:27', '0'),
('00100168', 1, 'EYEFRESH 5 ML', '9EH1002', '2022-10-28', 100, 0, 'FLS', '28875', '10', '28875', 'AD2019110001', '2020-02-02 12:47:06', '0'),
('00100169', 1, 'FENICOL 0,25% 5 ML', '9F20711', '2021-07-28', 0, 0, 'FLS', '21875', '10', '21875', 'AD2019110001', '2020-02-02 12:47:30', '0'),
('00100170', 1, 'FENICOL 0,25% 5 ML', '9F20724', '2021-07-28', 48, 0, 'FLS', '21875', '10', '21875', 'AD2019110001', '2020-02-02 12:47:52', '0'),
('00100171', 1, 'FENICOL 0,25% 5 ML', '9F20919', '2021-09-28', 100, 0, 'FLS', '21875', '10', '21875', 'AD2019110001', '2020-02-02 12:48:32', '0'),
('00100172', 1, 'FENICOL 0,5% 5 ML', '9F50710', '2021-07-28', 59, 0, 'FLS', '30625', '10', '30625', 'AD2019110001', '2020-02-02 12:48:51', '0'),
('00100173', 1, 'FENICOL 0,5% 5 ML', '9F50725', '2021-07-28', 100, 0, 'FLS', '30625', '10', '30625', 'AD2019110001', '2020-02-02 12:49:07', '0'),
('00100174', 1, 'FLOXA 5 ML', '9F31025', '2022-04-28', 17, 0, 'FLS', '30525', '30525', '30525', 'AD2019110001', '2020-02-11 12:09:00', '0'),
('00100175', 1, 'FLUORESCEINE 5 ML', '9FL0131', '2021-01-28', 8, 0, 'FLS', '13000', '10', '13000', 'AD2019110001', '2020-02-02 12:50:03', '0'),
('00100176', 1, 'FLUORESCEINE 5 ML', '9FL1008', '2022-10-28', 196, 0, 'FLS', '13000', '10', '13000', 'AD2019110001', '2020-02-02 12:50:28', '0'),
('00100177', 1, 'GENTA 0,3% 5 ML', '9G50924', '2022-09-28', 14, 0, 'FLS', '28375', '10', '28375', 'AD2019110001', '2020-02-02 12:50:54', '0'),
('00100178', 1, 'GENTA 0,3% 5 ML', '9G51202', '2022-12-28', 450, 0, 'FLS', '28375', '10', '28375', 'AD2019110001', '2020-02-02 12:51:13', '0'),
('00100179', 1, 'GENTA 1% 5 ML', '9G10818', '2021-08-28', 64, 0, 'FLS', '23250', '10', '23250', 'AD2019110001', '2020-02-02 12:52:18', '0'),
('00100180', 1, 'GENTA 1% 5 ML', '9G11106', '2021-11-28', 50, 0, 'FLS', '23250', '10', '23250', 'AD2019110001', '2020-02-02 12:52:44', '0'),
('00100181', 1, 'HOMATRO 2% 5 ML', '9H0809', '2022-08-28', 100, 0, 'FLS', '35500', '10', '35500', 'AD2019110001', '2020-02-02 12:53:01', '0'),
('00100182', 1, 'HOMATRO 2% 5 ML', '9H1015', '2022-10-28', 164, 0, 'FLS', '35500', '10', '35500', 'AD2019110001', '2020-02-02 12:53:40', '0'),
('00100183', 1, 'L F X 5 ML', '9LF0918', '2022-03-28', 168, 0, 'FLS', '69250', '10', '69250', 'AD2019110001', '2020-02-02 12:54:02', '0'),
('00100184', 1, 'LUBRICEN 5 ML', '9LU0523', '2022-05-28', 272, 0, 'FLS', '36750', '10', '36750', 'AD2019110001', '2020-02-02 12:54:39', '0'),
('00100185', 1, 'LYTEERS 15 ML', '9L1202', '2022-12-28', 0, 0, 'FLS', '22500', '10', '22500', 'AD2019110001', '2020-02-02 12:55:38', '0'),
('00100186', 1, 'METHASON 5 ML', '9ME0906', '2022-09-28', 20, 0, 'FLS', '11250', '10', '11250', 'AD2019110001', '2020-02-02 12:56:03', '0'),
('00100187', 1, 'METHASON 5 ML', '9ME0907', '2022-10-28', 50, 0, 'FLS', '11250', '10', '11250', 'AD2019110001', '2020-02-02 12:56:45', '0'),
('00100188', 1, 'MYCOS 5 ML', '9MY1005', '2021-04-28', 68, 0, 'FLS', '19875', '10', '19875', 'AD2019110001', '2020-02-02 12:57:18', '0'),
('00100189', 1, 'MYDRIATIL 0,5% 5 ML', '9M50831', '2022-08-28', 0, 0, 'FLS', '28875', '10', '28875', 'AD2019110001', '2020-02-02 12:57:47', '0'),
('00100190', 1, 'MYDRIATIL 0,5% 5 ML', '9M50520', '2022-05-28', 80, 0, 'FLS', '28875', '10', '28875', 'AD2019110001', '2020-02-02 12:58:17', '0'),
('00100191', 1, 'MYDRIATIL 0,5% 5 ML', '9M50814', '2022-08-28', 150, 0, 'FLS', '28875', '10', '28875', 'AD2019110001', '2020-02-02 12:58:43', '0'),
('00100192', 1, 'MYDRIATIL 1% 5 ML', '9M10018', '2022-10-28', 0, 0, 'FLS', '38250', '10', '38250', 'AD2019110001', '2020-02-02 12:59:29', '0'),
('00100193', 1, 'MYDRIATIL 1% 5 ML', '9M1001', '2022-10-28', 0, 0, 'FLS', '38250', '10', '38250', 'AD2019110001', '2020-02-02 12:59:53', '0'),
('00100194', 1, 'CARPINE 2% MDS', '9C260907', '2022-09-28', 40, 0, 'STP', '17625', '10', '17625', 'AD2019110001', '2020-02-02 13:00:50', '0'),
('00100195', 1, 'CARPINE 2% MDS', '9C260612', '2022-06-28', 50, 0, 'STP', '17625', '10', '17625', 'AD2019110001', '2020-02-02 13:01:30', '0'),
('00100196', 1, 'CARPINE 2% MDS', '8C261129', '2021-11-28', 8, 0, 'STP', '17625', '10', '17625', 'AD2019110001', '2020-02-02 13:02:14', '0'),
('00100197', 1, 'CATARLENT MINIDOSE', '9CA61017', '2021-10-28', 142, 0, 'STP', '20000', '10', '20000', 'AD2019110001', '2020-02-02 13:02:37', '0'),
('00100198', 1, 'CATARLENT MINIDOSE', '9CA61129', '2021-11-28', 480, 0, 'STP', '20000', '10', '20000', 'AD2019110001', '2020-02-02 13:02:59', '0'),
('00100199', 1, 'CENDRID MDS', '9CD60525', '2022-05-20', 20, 0, 'STP', '16625', '10', '16625', 'AD2019110001', '2020-02-02 13:03:26', '0'),
('00100200', 1, 'CENDRID MDS', '8CD60521', '2021-05-28', 6, 0, 'STP', '16625', '10', '16625', 'AD2019110001', '2020-02-02 13:03:49', '0'),
('00100201', 1, 'CENFRESH MDS', '9C61003', '2022-10-28', 0, 0, 'STP', '23000', '10', '23000', 'AD2019110001', '2020-02-02 13:04:12', '0'),
('00100202', 1, 'CENFRESH MDS', '9C61010', '2022-10-28', 106, 0, 'STP', '23000', '10', '23000', 'AD2019110001', '2020-02-02 13:04:33', '0'),
('00100203', 1, 'CENFRESH MDS', '9C61015', '2022-10-28', 600, 0, 'STP', '23000', '10', '23000', 'AD2019110001', '2020-02-02 13:04:56', '0'),
('00100204', 1, 'CONVER 2% MDS', '9CV61022', '2022-10-28', 192, 0, 'STP', '16125', '10', '16125', 'AD2019110001', '2020-02-02 13:05:33', '0'),
('00100205', 1, 'DEXATON MDS', '9D60703', '2022-01-28', 40, 0, 'STP', '16125', '10', '16125', 'AD2019110001', '2020-02-02 13:06:16', '0'),
('00100206', 1, 'DEXATON MDS', '9D60328', '2021-09-28', 16, 0, 'STP', '16125', '10', '16125', 'AD2019110001', '2020-02-02 13:06:35', '0'),
('00100207', 1, 'EYE LOTION MDS', '9EL61107', '2022-11-28', 80, 0, 'STP', '31900', '10', '31900', 'AD2019110001', '2020-02-02 13:07:03', '0'),
('00100208', 1, 'EYE LOTION MDS', '9EL60306', '2022-03-28', 39, 0, 'STP', '31900', '10', '31900', 'AD2019110001', '2020-02-02 13:07:34', '0'),
('00100209', 1, 'EYEFRESH MDS', '9EH61007', '2022-10-28', 0, 0, 'STP', '23125', '10', '23125', 'AD2019110001', '2020-02-02 13:07:58', '0'),
('00100210', 1, 'EYEFRESH MDS', '9EH61008', '2022-10-28', 600, 0, 'STP', '23125', '10', '23125', 'AD2019110001', '2020-02-02 13:08:14', '0'),
('00100211', 1, 'EYEFRESH MDS', '9EH61018', '2022-10-28', 0, 0, 'STP', '23125', '10', '23125', 'AD2019110001', '2020-02-02 13:08:35', '0'),
('00100212', 1, 'EYEFRESH MILD MDS', '9EM61008', '2022-10-28', 210, 0, 'STP', '30625', '10', '30625', 'AD2019110001', '2020-02-02 13:08:52', '0'),
('00100213', 1, 'EYEFRESH MILD MDS', '9EM60925', '2022-09-28', 0, 0, 'STP', '30625', '10', '30625', 'AD2019110001', '2020-02-02 13:09:09', '0'),
('00100214', 1, 'EYEFRESH PLUS MDS', '9EP61018', '2022-10-28', 450, 0, 'STP', '34875', '10', '34875', 'AD2019110001', '2020-02-02 13:09:31', '0'),
('00100215', 1, 'EYEFRESH PLUS MDS', '9EP61007', '2022-10-28', 35, 0, 'STP', '34875', '10', '34875', 'AD2019110001', '2020-02-02 13:09:51', '0'),
('00100216', 1, 'FENICOL 0,25% MDS', '9F260526', '2021-05-28', 0, 0, 'STP', '16375', '10', '16375', 'AD2019110001', '2020-02-02 13:10:13', '0'),
('00100217', 1, 'FENICOL 0,5% 5 MDS', '9F560625', '2021-06-28', 52, 0, 'STP', '17500', '17500', '17500', 'AD2019110001', '2020-02-10 14:01:54', '0'),
('00100218', 1, 'FENICOL 0,5% MDS', '9F561206', '2020-12-28', 0, 0, 'STP', '17500', '10', '17500', 'AD2019110001', '2020-02-02 13:11:01', '0'),
('00100219', 1, 'FLOXA MDS', '9F360917', '2022-03-28', 152, 0, 'STP', '26750', '10', '26750', 'AD2019110001', '2020-02-02 13:11:25', '0'),
('00100220', 1, 'FLOXA MDS', '9F361015', '2022-04-28', 600, 0, 'STP', '26750', '10', '26750', 'AD2019110001', '2020-02-02 13:12:01', '0'),
('00100221', 1, 'GENTA 0,3% MDS', '9G60829', '2022-08-28', 0, 0, 'STP', '15875', '10', '15875', 'AD2019110001', '2020-02-02 13:12:27', '0'),
('00100222', 1, 'GENTA 0,3% MDS', '9G560812', '2022-08-28', 0, 0, 'STP', '15875', '10', '15875', 'AD2019110001', '2020-02-02 13:12:53', '0'),
('00100223', 1, 'GENTA 1% MDS', '9G60412', '2021-04-28', 0, 0, 'STP', '16625', '10', '16625', 'AD2019110001', '2020-02-02 13:13:23', '0'),
('00100224', 1, 'GIFLOX MDS', '9GF60911', '2021-09-28', 0, 0, 'STP', '80250', '10', '80250', 'AD2019110001', '2020-02-02 13:13:47', '0'),
('00100225', 1, 'GIFLOX MDS', '9GF60905', '2021-09-28', 32, 0, 'STP', '80250', '10', '80250', 'AD2019110001', '2020-02-02 13:14:06', '0'),
('00100226', 1, 'GIFLOX MDS', '9GF60919', '2021-09-28', 200, 0, 'STP', '80250', '10', '80250', 'AD2019110001', '2020-02-02 13:14:27', '0'),
('00100227', 1, 'GLAOPEN MDS', '9GL61119', '2021-11-28', 0, 0, 'STP', '90250', '10', '90250', 'AD2019110001', '2020-02-02 13:16:31', '0'),
('00100228', 1, 'GLAOPLUS MDS', '9GP61004', '2021-10-28', 20, 0, 'STP', '95875', '10', '95875', 'AD2019110001', '2020-02-02 13:16:57', '0'),
('00100229', 1, 'GLAOPLUS MDS', '9GP61024', '2021-10-28', 98, 0, 'STP', '95875', '10', '95875', 'AD2019110001', '2020-02-02 13:17:21', '0'),
('00100230', 1, 'GLAOPLUS MDS', '9GP61202', '2021-12-28', 400, 0, 'STP', '95875', '10', '95875', 'AD2019110001', '2020-02-02 13:17:42', '0'),
('00100231', 1, 'GLAOPLUS MDS', '9GP61106', '2021-11-28', 160, 0, 'STP', '95875', '10', '95875', 'AD2019110001', '2020-02-02 13:18:11', '0'),
('00100232', 1, 'HYALUB MDS', '9HA61023', '2022-10-28', 0, 0, 'STP', '57000', '10', '57000', 'AD2019110001', '2020-02-02 13:18:31', '0'),
('00100233', 1, 'HYALUB MDS', '9HA61102', '2022-11-28', 20, 0, 'STP', '57000', '10', '57000', 'AD2019110001', '2020-02-02 13:18:54', '0'),
('00100234', 1, 'LENTIKULAR MDS', '9LE61029', '2021-10-28', 1, 0, 'STP', '33250', '10', '33250', 'AD2019110001', '2020-02-02 13:19:13', '0'),
('00100235', 1, 'LENTIKULAR MDS', '9LE61011', '0021-12-28', 330, 0, 'STP', '33250', '10', '33250', 'AD2019110001', '2020-02-02 13:19:32', '0'),
('00100236', 1, 'L F X MDS', '9LF61108', '2022-05-28', 6, 0, 'STP', '69000', '10', '69000', 'AD2019110001', '2020-02-02 13:20:00', '0'),
('00100237', 1, 'L F X MDS', '9LF61030', '2022-04-28', 0, 0, 'STP', '69000', '10', '69000', 'AD2019110001', '2020-02-02 13:20:23', '0'),
('00100238', 1, 'LUBRICEN MDS', '9LU60522', '2022-05-28', 356, 0, 'STP', '40500', '10', '40500', 'AD2019110001', '2020-02-02 13:20:44', '0'),
('00100239', 1, 'NONCORT 5 ML', '9N1019', '2022-10-28', 600, 0, 'FLS', '37500', '10', '37500', 'AD2019110001', '2020-02-02 13:24:09', '0'),
('00100240', 1, 'NONCORT 5 ML', '9N1113', '2022-11-28', 823, 0, 'FLS', '37500', '10', '37500', 'AD2019110001', '2020-02-02 13:24:30', '0'),
('00100241', 1, 'PANTOCAIN 0,5% 5 ML', '9P1115', '2022-11-28', 58, 0, 'FLS', '12125', '10', '12125', 'AD2019110001', '2020-02-02 13:25:05', '0'),
('00100242', 1, 'PANTOCAIN 2% 5 ML', '9P31012', '2022-10-28', 50, 0, 'FLS', '13250', '10', '13250', 'AD2019110001', '2020-02-02 13:25:32', '0'),
('00100243', 1, 'POLYDEX 5 ML', '9PX1128', '2022-05-28', 490, 0, 'FLS', '35750', '35750', '35750', 'AD2019110001', '2020-02-02 16:14:03', '0'),
('00100244', 1, 'POLYDEX 5 ML', '9PX0320', '2021-09-28', 2, 0, 'FLS', '35750', '10', '35750', 'AD2019110001', '2020-02-02 13:26:25', '0'),
('00100245', 1, 'POLYDEX 5 ML', '9PX0927', '2022-03-28', 2, 0, 'FLS', '35750', '10', '35750', 'AD2019110001', '2020-02-02 13:26:44', '0'),
('00100246', 1, 'POLYGRAN 5 ML', '9PG0831', '2022-02-28', 374, 0, 'FLS', '31625', '10', '31625', 'AD2019110001', '2020-02-02 13:29:09', '0'),
('00100247', 1, 'POLYGRAN 5 ML', '9PG0726', '2022-01-28', 0, 0, 'FLS', '31625', '10', '31625', 'AD2019110001', '2020-02-02 13:29:36', '0'),
('00100248', 1, 'POLYNEL 5 ML', '9PL1120', '2022-05-28', 200, 0, 'FLS', '26875', '10', '26875', 'AD2019110001', '2020-02-02 13:30:06', '0'),
('00100249', 1, 'POLYNEL 5 ML', '9PL1115', '2022-05-28', 60, 0, 'FLS', '26875', '10', '26875', 'AD2019110001', '2020-02-02 13:30:24', '0'),
('00100250', 1, 'P-PRED 5 ML', '9PP0629', '2021-06-28', 0, 0, 'FLS', '55500', '10', '55500', 'AD2019110001', '2020-02-02 13:30:40', '0'),
('00100251', 1, 'P-PRED 5 ML', '9PP0717', '2021-07-28', 74, 0, 'FLS', '55500', '10', '55500', 'AD2019110001', '2020-02-02 13:31:07', '0'),
('00100252', 1, 'SILOXAN 5 ML', '9SI1018', '2022-10-28', 233, 0, 'FLS', '52750', '10', '52750', 'AD2019110001', '2020-02-02 13:31:25', '0'),
('00100253', 1, 'STATROL 5 ML', '9S0907', '2022-03-28', 257, 0, 'FLS', '18250', '10', '18250', 'AD2019110001', '2020-02-02 13:32:04', '0'),
('00100254', 1, 'TIMOL 0,25% 5 ML', '9TI0821', '2022-08-28', 100, 0, 'FLS', '30500', '10', '30500', 'AD2019110001', '2020-02-02 13:32:24', '0'),
('00100255', 1, 'TIMOL 0,25% 5 ML', '9TI0626', '2022-06-28', 95, 0, 'FLS', '30500', '10', '30500', 'AD2019110001', '2020-02-02 13:32:44', '0'),
('00100256', 1, 'TIMOL 0,5% 5 ML', '9TM1111', '2022-11-28', 1, 0, 'FLS', '47750', '10', '47750', 'AD2019110001', '2020-02-02 13:33:12', '0'),
('00100257', 1, 'TIMOL 0,5% 5 ML', '9TM1108', '2022-11-28', 0, 0, 'FLS', '47750', '10', '47750', 'AD2019110001', '2020-02-02 13:33:28', '0'),
('00100258', 1, 'TOBRO 5 ML', '9TO0830', '2022-08-28', 565, 0, 'FLS', '20750', '10', '20750', 'AD2019110001', '2020-02-02 13:33:46', '0'),
('00100259', 1, 'TOBROSON 5 ML', '9TS1120', '2022-05-28', 199, 0, 'FLS', '46250', '10', '46250', 'AD2019110001', '2020-02-02 13:34:03', '0'),
('00100260', 1, 'TONOR 0,5% 5 ML', '9TR0807', '2022-08-28', 218, 0, 'FLS', '50000', '10', '50000', 'AD2019110001', '2020-02-02 13:34:19', '0'),
('00100261', 1, 'TROPINE 0,5% 5 ML', '9T50725', '2022-07-28', 100, 0, 'FLS', '11250', '10', '11250', 'AD2019110001', '2020-02-02 13:34:41', '0'),
('00100262', 1, 'TROPINE 0,5% 5 ML', '9T50706', '2022-07-28', 72, 0, 'FLS', '11250', '10', '11250', 'AD2019110001', '2020-02-02 13:34:55', '0'),
('00100263', 1, 'TROPINE 1% 5 ML', '9T11002', '2022-10-28', 50, 0, 'FLS', '12500', '10', '12500', 'AD2019110001', '2020-02-02 13:35:26', '0'),
('00100264', 1, 'TROPINE 1% 5 ML', '9T10831', '2022-08-28', 95, 0, 'FLS', '12500', '10', '12500', 'AD2019110001', '2020-02-02 13:35:48', '0'),
('00100265', 1, 'VASACON 15 ML', '9VN0731', '2022-07-28', 177, 0, 'FLS', '16250', '10', '16250', 'AD2019110001', '2020-02-02 13:36:09', '0'),
('00100266', 1, 'VASACON 15 ML', '9VN0912', '2022-09-28', 250, 0, 'FLS', '16250', '10', '16250', 'AD2019110001', '2020-02-02 13:36:29', '0'),
('00100267', 1, 'VASACON A 15 ML', '9VA1008', '2021-10-28', 113, 0, 'FLS', '22375', '10', '22375', 'AD2019110001', '2020-02-02 13:36:46', '0'),
('00100268', 1, 'VASACON A 15 ML', '9VA1023', '2021-10-28', 200, 0, 'FLS', '22375', '10', '22375', 'AD2019110001', '2020-02-02 13:37:03', '0'),
('00100269', 1, 'VERNACEL 5 ML', '9VE0426', '2022-04-28', 131, 0, 'FLS', '18500', '10', '18500', 'AD2019110001', '2020-02-02 13:37:27', '0'),
('00100270', 1, 'VISION 15 ML', '9VI0904', '2022-09-28', 255, 0, 'FLS', '19875', '10', '19875', 'AD2019110001', '2020-02-02 13:37:52', '0'),
('00100271', 1, 'VITROLENTA 5 ML', '9VL1017', '2022-10-28', 437, 0, 'FLS', '32750', '10', '32750', 'AD2019110001', '2020-02-02 13:38:10', '0'),
('00100272', 1, 'XITROL 5 ML', '9X1007', '2022-10-28', 528, 0, 'FLS', '27250', '10', '27250', 'AD2019110001', '2020-02-02 13:38:30', '0'),
('00100273', 1, 'XITROL 5 ML', '9X1015', '2022-04-28', 0, 0, 'FLS', '27250', '10', '27250', 'AD2019110001', '2020-02-02 13:38:48', '0'),
('00100274', 1, 'XITROL 5 ML', '9X0828', '2022-02-28', 0, 0, 'FLS', '27250', '27250', '27250', 'AD2019110001', '2020-02-10 12:26:31', '0'),
('00100275', 1, 'LYTEERS MDS', '9L61102', '2022-11-28', 1, 0, 'STP', '19250', '10', '19250', 'AD2019110001', '2020-02-02 13:39:34', '0'),
('00100276', 1, 'LYTEERS MDS', '9L60827', '2022-08-28', 0, 0, 'STP', '19250', '10', '19250', 'AD2019110001', '2020-02-02 13:39:47', '0'),
('00100277', 1, 'MYCOS MDS', '9MY60706', '2021-01-28', 36, 0, 'STP', '16625', '10', '16625', 'AD2019110001', '2020-02-02 13:40:04', '0'),
('00100278', 1, 'MYDRIATIL 0,5% MDS', '9M561028', '2022-10-28', 156, 0, 'STP', '26375', '10', '26375', 'AD2019110001', '2020-02-02 13:40:31', '0'),
('00100279', 1, 'MYDRIATIL 0,5% MDS', '9M560520', '2022-05-28', 3, 0, 'STP', '26375', '10', '26375', 'AD2019110001', '2020-02-02 13:40:50', '0'),
('00100280', 1, 'MYDRIATIL 1% MDS', '9M60720', '2022-07-28', 80, 0, 'STP', '29375', '29375', '29375', 'AD2019110001', '2020-02-02 13:41:28', '0'),
('00100281', 1, 'MYDRIATIL 1% MDS', '9M60116', '2022-01-28', 13, 0, 'STP', '29375', '10', '29375', 'AD2019110001', '2020-02-02 13:41:48', '0'),
('00100282', 1, 'MYDRIATIL 1% MDS', '9M60507', '2021-05-28', 1, 0, 'STP', '29375', '10', '29375', 'AD2019110001', '2020-02-02 13:42:09', '0'),
('00100283', 1, 'NATACEN MDS', '9NA60912', '2021-09-28', 320, 0, 'STP', '41625', '10', '41625', 'AD2019110001', '2020-02-02 13:42:43', '0'),
('00100284', 1, 'NONCORT MDS', '9N61101', '2022-11-28', 115, 0, 'STP', '42250', '42250', '42250', 'AD2019110001', '2020-02-05 11:58:42', '0'),
('00100285', 1, 'POLYGRAN MDS', '9PG60523', '2021-11-28', 84, 0, 'STP', '17500', '10', '17500', 'AD2019110001', '2020-02-02 13:44:01', '0'),
('00100286', 1, 'POLYGRAN MDS', '9PG61108', '2022-05-28', 40, 0, 'STP', '17500', '10', '17500', 'AD2019110001', '2020-02-02 13:44:20', '0'),
('00100287', 1, 'POLYNEL MDS', '9PL61104', '2022-05-28', 389, 0, 'STP', '17875', '10', '17875', 'AD2019110001', '2020-02-02 13:44:47', '0'),
('00100288', 1, 'POLYNEL MDS', '9PL61012', '2022-04-28', 0, 0, 'STP', '17875', '10', '17875', 'AD2019110001', '2020-02-02 13:45:12', '0'),
('00100289', 1, 'POLYPRED MDS', '8PD60413', '2020-10-28', 52, 0, 'STP', '17375', '17375', '17375', 'AD2019110001', '2020-02-02 13:46:13', '0'),
('00100290', 1, 'POLYPRED MDS', '8PD60412', '2020-10-28', 2, 0, 'STP', '17375', '10', '17375', 'AD2019110001', '2020-02-02 13:46:49', '0'),
('00100291', 1, 'POSOP MDS', '9PO61120', '2022-05-28', 374, 0, 'STP', '52500', '10', '52500', 'AD2019110001', '2020-02-02 13:47:12', '0'),
('00100292', 1, 'POSOP MDS', '9PO61016', '2022-04-28', 2, 0, 'STP', '52500', '52500', '52500', 'AD2019110001', '2020-02-05 15:36:33', '0'),
('00100293', 1, 'POSOP MDS', '9PO61002', '2022-04-28', 0, 0, 'STP', '52500', '10', '52500', 'AD2019110001', '2020-02-02 13:48:06', '0'),
('00100294', 1, 'P-PRED MDS', '9PP61011', '2021-10-28', 97, 0, 'STP', '38250', '10', '38250', 'AD2019110001', '2020-02-02 13:48:31', '0'),
('00100295', 1, 'P-PRED MDS', '9PP60925', '2021-09-28', 5, 0, 'STP', '38250', '10', '38250', 'AD2019110001', '2020-02-02 13:48:45', '0'),
('00100296', 1, 'PROTAGENTA MDS', '9PR61016', '2022-10-28', 0, 0, 'STP', '35875', '10', '35875', 'AD2019110001', '2020-02-02 13:49:15', '0'),
('00100297', 1, 'PROTAGENTA MDS', '9PR61105', '2022-11-28', 20, 0, 'STP', '35875', '10', '35875', 'AD2019110001', '2020-02-02 13:49:39', '0'),
('00100298', 1, 'STATROL MDS', '9S60907', '2022-03-28', 140, 0, 'STP', '16875', '10', '16875', 'AD2019110001', '2020-02-02 13:49:55', '0'),
('00100299', 1, 'TIMOL 0,25% MDS', '9TI60515', '2022-05-28', 90, 0, 'STP', '17625', '10', '17625', 'AD2019110001', '2020-02-02 13:50:14', '0'),
('00100300', 1, 'TIMOL 0,25% MDS', '9TI60412', '2022-04-28', 1, 0, 'STP', '17625', '10', '17625', 'AD2019110001', '2020-02-02 13:50:29', '0'),
('00100301', 1, 'TIMOL 0,5% MDS', '9TM60824', '2022-08-28', 7, 0, 'STP', '25750', '10', '25750', 'AD2019110001', '2020-02-02 13:50:46', '0'),
('00100302', 1, 'TIMOL 0,5% MDS', '9TM60903', '2022-09-28', 600, 0, 'STP', '25750', '10', '25750', 'AD2019110001', '2020-02-02 13:51:08', '0'),
('00100303', 1, 'TOBRO MDS', '9TO61016', '2022-10-28', 210, 0, 'STP', '19125', '10', '19125', 'AD2019110001', '2020-02-02 13:51:29', '0'),
('00100304', 1, 'TOBROSON MDS', '9TS61015', '2022-04-28', 28, 0, 'STP', '29000', '10', '29000', 'AD2019110001', '2020-02-02 13:52:01', '0'),
('00100305', 1, 'TOBROSON MDS', '9TS60928', '2022-03-28', 5, 0, 'STP', '29000', '10', '29000', 'AD2019110001', '2020-02-02 13:52:21', '0'),
('00100306', 1, 'TONOR 0,5% MDS', '9TR60807', '2022-08-28', 112, 0, 'STP', '24375', '10', '24375', 'AD2019110001', '2020-02-02 13:52:47', '0'),
('00100307', 1, 'TONOR 0,5% MDS', '9TR61120', '2022-11-28', 109, 0, 'STP', '24375', '10', '24375', 'AD2019110001', '2020-02-02 13:53:00', '0'),
('00100308', 1, 'TONOR 0,5% MDS', '9TR60904', '2022-09-28', 140, 0, 'STP', '24375', '10', '24375', 'AD2019110001', '2020-02-02 13:53:16', '0'),
('00100309', 1, 'TROPIN 0,5% MDS', '9T560809', '2022-08-28', 110, 0, 'STP', '15375', '10', '15375', 'AD2019110001', '2020-02-02 13:53:44', '0'),
('00100310', 1, 'TROPIN 0,5% MDS', '9T561221', '2021-12-28', 0, 0, 'STP', '15375', '10', '15375', 'AD2019110001', '2020-02-02 13:54:04', '0'),
('00100311', 1, 'TROPIN 1% MDS', '9T160815', '2022-08-28', 1, 0, 'STP', '16000', '10', '16000', 'AD2019110001', '2020-02-02 13:54:30', '0'),
('00100312', 1, 'TROPIN 1% MDS', '9T160909', '2022-09-28', 51, 0, 'STP', '16000', '10', '16000', 'AD2019110001', '2020-02-02 13:54:46', '0'),
('00100314', 1, 'ULCORI MDS', '9U61121', '2022-05-28', 10, 0, 'STP', '19625', '10', '19625', 'AD2019110001', '2020-02-02 13:55:45', '0'),
('00100315', 1, 'ULCORI MDS', '9U60701', '2022-01-28', 2, 0, 'STP', '19625', '10', '19625', 'AD2019110001', '2020-02-02 13:56:04', '0'),
('00100316', 1, 'VASACON MDS', '9VN60115', '2022-01-28', 1, 0, 'STP', '13875', '10', '13875', 'AD2019110001', '2020-02-02 13:56:28', '0'),
('00100317', 1, 'VASACON MDS', '9VN60927', '2022-09-28', 154, 0, 'STP', '13875', '10', '13875', 'AD2019110001', '2020-02-02 13:56:47', '0'),
('00100318', 1, 'VASACON A MDS', '9VA61023', '2021-10-28', 318, 0, 'STP', '14750', '10', '14750', 'AD2019110001', '2020-02-02 13:57:14', '0'),
('00100319', 1, 'VERNACEL MDS', '9VE60425', '2022-04-28', 192, 0, 'STP', '20750', '10', '20750', 'AD2019110001', '2020-02-02 13:57:38', '0'),
('00100320', 1, 'VERNACEL MDS', '9VE60927', '2022-09-28', 60, 0, 'STP', '20750', '10', '20750', 'AD2019110001', '2020-02-02 13:58:18', '0'),
('00100321', 1, 'VOSAMA MDS', '9V60905', '2022-09-28', 185, 0, 'STP', '17625', '10', '17625', 'AD2019110001', '2020-02-02 13:59:01', '0'),
('00100322', 1, 'VOSAMA MDS', '9V60826', '2022-08-28', 9, 0, 'STP', '17625', '10', '17625', 'AD2019110001', '2020-02-02 13:59:16', '0'),
('00100323', 1, 'XITROL MDS', '9X60910', '2022-03-28', 23, 0, 'STP', '24375', '10', '24375', 'AD2019110001', '2020-02-02 13:59:33', '0'),
('00100324', 1, 'PATACEN MDS', '9PC61108', '2022-11-28', 58, 0, 'STP', '101500', '10', '101500', 'AD2019110001', '2020-02-02 14:01:24', '0'),
('00100325', 1, 'PATACEN 5 ML', '9PC1108', '2022-11-28', 35, 0, 'FLS', '93250', '10', '93250', 'AD2019110001', '2020-02-02 14:04:54', '0'),
('00100326', 1, 'CATARLENT 15 ML INH', '7CN0922', '2020-03-28', 50, 0, 'FLS', '27563', '10', '27563', 'AD2019110001', '2020-02-02 14:05:28', '0'),
('00100327', 1, 'LYTEERS 15 ML INH', '9L1105', '2022-11-28', 5, 0, 'FLS', '21818', '10', '21818', 'AD2019110001', '2020-02-02 14:05:56', '0'),
('00100328', 1, 'MYDRIATIL 1% 5 ML INH', '8M0409', '2021-04-28', 6, 0, 'FLS', '37091', '10', '37091', 'AD2019110001', '2020-02-02 14:07:12', '0'),
('00100329', 1, 'PANTOCAIN 0.5% 5 ML INH', '8P10816', '2021-08-28', 50, 0, 'FLS', '11365', '10', '11365', 'AD2019110001', '2020-02-02 14:07:38', '0'),
('00100330', 1, 'PANTOCAIN 0.5% 5 ML INH', '9P111115', '2022-11-28', 20, 0, 'FLS', '11365', '10', '11365', 'AD2019110001', '2020-02-02 14:07:58', '0'),
('00100331', 1, 'PANTOCAIN 0.5% 5 ML INH', '8P10123', '2021-01-28', 9, 0, 'FLS', '11365', '10', '11365', 'AD2019110001', '2020-02-02 14:08:16', '0'),
('00100332', 1, 'FLOXA MDS INH', '9F360904', '2022-03-28', 0, 0, 'STP', '25455', '10', '25455', 'AD2019110001', '2020-02-02 14:08:52', '0'),
('00100333', 1, 'GLAOPEN MDS INH', '9GL60507', '2021-05-28', 42, 0, 'STP', '85636', '10', '85636', 'AD2019110001', '2020-02-02 14:09:09', '0'),
('00100334', 1, 'GLAOPEN MDS INH', '9GL60121', '2021-01-28', 1, 0, 'STP', '85636', '10', '85636', 'AD2019110001', '2020-02-02 14:09:25', '0'),
('00100335', 1, 'NONCORT MDS INH', '9N60930', '2022-09-28', 16, 0, 'STP', '40182', '10', '40182', 'AD2019110001', '2020-02-02 14:09:38', '0'),
('00100336', 1, 'NONCORT MDS INH', '9N61101', '2022-11-28', 20, 0, 'STP', '40182', '10', '40182', 'AD2019110001', '2020-02-02 14:09:52', '0'),
('00100337', 1, 'NONCORT MDS INH', '7N61106', '2020-11-28', 1, 0, 'STP', '40182', '10', '40182', 'AD2019110001', '2020-02-02 14:10:12', '0'),
('00100338', 1, 'POSOP MDS INH', '9PO61016', '2020-04-28', 20, 0, 'STP', '49545', '10', '49545', 'AD2019110001', '2020-02-02 14:10:28', '0'),
('00100339', 1, 'POSOP MDS INH', '9PO6074', '2022-01-28', 0, 0, 'STP', '49545', '10', '49545', 'AD2019110001', '2020-02-02 14:10:53', '0'),
('00100340', 1, 'POSOP MDS INH', '9PO60724', '2022-01-28', 10, 0, 'STP', '49545', '10', '49545', 'AD2019110001', '2020-02-02 14:11:10', '0'),
('00100341', 1, 'POSOP MDS INH', '8PO60522', '2020-11-28', 2, 0, 'STP', '49545', '10', '49545', 'AD2019110001', '2020-02-02 14:11:34', '0'),
('00100342', 1, 'P-PRED MDS INH', '8PP60828', '2020-06-28', 72, 0, 'STP', '36909', '10', '36909', 'AD2019110001', '2020-02-02 14:11:57', '0'),
('00100343', 1, 'PROTAGENTA MDS INH', '9PR61109', '2022-10-28', 5, 0, 'STP', '33636', '10', '33636', 'AD2019110001', '2020-02-02 14:12:14', '0'),
('00100344', 1, 'TOBRO MDS INH', '9TO60912', '2022-09-28', 20, 0, 'STP', '18545', '10', '18545', 'AD2019110001', '2020-02-02 14:13:37', '0'),
('00100345', 1, 'TOBRO MDS INH', '9TO60830', '2022-08-28', 15, 0, 'STP', '18545', '10', '18545', 'AD2019110001', '2020-02-02 14:13:56', '0'),
('00100346', 1, 'TONOR 0,5% MDS INH', '8TR61008', '2021-10-28', 3, 0, 'STP', '23409', '10', '23409', 'AD2019110001', '2020-02-02 14:14:17', '0'),
('00100347', 1, 'TONOR 0,5% MDS INH', '9TR60222', '2022-02-28', 20, 0, 'STP', '23409', '10', '23409', 'AD2019110001', '2020-02-02 14:14:41', '0'),
('00100348', 1, 'TOBRO MDS INH', '8TO60810', '2021-08-28', 1, 0, 'STP', '18545', '10', '18545', 'AD2019110001', '2020-02-02 14:15:40', '0'),
('00100349', 1, 'VOSAMA MDS INH', '8V61004', '2021-10-28', 4, 0, 'STP', '17184', '10', '17184', 'AD2019110001', '2020-02-02 14:15:57', '0'),
('00100350', 1, 'VOSAMA MDS INH', '8V61130', '2022-11-28', 20, 0, 'STP', '17184', '10', '17184', 'AD2019110001', '2020-02-02 14:16:15', '0'),
('00100351', 1, 'VOSAMA MDS INH', '9V60807', '2022-08-28', 20, 0, 'STP', '17184', '10', '17184', 'AD2019110001', '2020-02-02 14:16:41', '0'),
('00100352', 1, 'PANTOCAIN 0.5% 5 ML INH', '710809', '2020-08-28', 0, 0, 'FLS', '11365', '10', '11365', 'AD2019110001', '2020-02-02 14:17:21', '0'),
('00100353', 1, 'PANTOCAIN 0.5% 5 ML INH', '8P10411', '2020-04-28', 22, 0, 'FLS', '11365', '10', '11365', 'AD2019110001', '2020-02-02 14:17:35', '0'),
('00100354', 1, 'PANTOCAIN 0.5% 5 ML INH', '8P10118', '2021-01-28', 1, 0, 'FLS', '11365', '10', '11365', 'AD2019110001', '2020-02-02 14:17:52', '0'),
('00100355', 1, 'MYDRIATIL 1% 5 ML', '9M1028', '2022-10-28', 172, 0, 'FLS', '38250', '10', '38250', 'AD2019110001', '2020-02-02 14:45:04', '0'),
('00100356', 1, 'FENICOL 3,5 GR', '901008', '2019-10-28', 50, 0, 'TBS', '21875', '21875', '21875', 'AD2019110001', '2020-02-07 15:52:46', '0'),
('00100357', 1, 'FENICOL 3,5 GR', '900810', '2022-08-28', 60, 0, 'TBS', '21875', '21875', '21875', 'AD2019110001', '2020-02-11 12:11:29', '0'),
('00100358', 1, 'GENTA 0,3% MDS', '9G560829', '2022-08-28', 140, 0, 'STP', '15875', '10', '15875', 'AD2019110001', '2020-02-02 15:10:41', '0'),
('00100359', 1, 'FENICOL 3,5 GR', '800717', '2022-07-28', 0, 0, 'TBS', '21875', '10', '21875', 'AD2019110001', '2020-02-02 15:11:16', '0'),
('00100360', 1, 'GENTAMYCINE 3,5 GR', '921022', '2022-10-28', 178, 0, 'TBS', '36375', '10', '36375', 'AD2019110001', '2020-02-02 15:11:55', '0'),
('00100361', 1, 'GENTAMYCINE 3,5 GR', '920925', '2022-09-28', 0, 0, 'TBS', '36375', '10', '36375', 'AD2019110001', '2020-02-02 15:12:28', '0'),
('00100362', 1, 'GENTA 1% MDS', '9G160412', '2021-04-28', 33, 0, 'STP', '16625', '10', '16625', 'AD2019110001', '2020-02-02 15:12:45', '0'),
('00100363', 1, 'HERVIS 3,5 GR', '930628', '2022-06-28', 137, 0, 'TBS', '36750', '10', '36750', 'AD2019110001', '2020-02-02 15:12:56', '0'),
('00100365', 1, 'MYCETINE 3,5 GR', '940806', '2022-08-28', 59, 0, 'TBS', '12750', '10', '12750', 'AD2019110001', '2020-02-02 15:14:07', '0'),
('00100366', 1, 'MYCETINE 3,5 GR', '940619', '2022-08-28', 3, 0, 'TBS', '12750', '10', '12750', 'AD2019110001', '2020-02-02 15:14:33', '0'),
('00100367', 1, 'MYCOS 3,5 GR', '951002', '2022-10-28', 200, 0, 'TBS', '26250', '10', '26250', 'AD2019110001', '2020-02-02 15:15:04', '0'),
('00100368', 1, 'MYCOS 3,5 GR', '950830', '2022-08-28', 64, 0, 'TBS', '26250', '10', '26250', 'AD2019110001', '2020-02-02 15:15:33', '0'),
('00100369', 1, 'OCULENTA VD 5 GR', '9OCG1023', '2022-04-28', 0, 0, 'tbs', '63875', '10', '63875', 'AD2019110001', '2020-02-02 15:16:27', '0'),
('00100370', 1, 'OCULENTA VD 5 GR', '9OCG1024', '2022-04-28', 0, 0, 'TBS', '63875', '10', '63875', 'AD2019110001', '2020-02-02 15:17:02', '0'),
('00100371', 1, 'OCULENTA VD 5 GR', '9OCG1029', '2022-04-28', 60, 0, 'TBS', '63875', '10', '63875', 'AD2019110001', '2020-02-02 15:17:46', '0'),
('00100372', 1, 'POLYGRAN 3,5 GR', '970928', '2022-09-28', 123, 0, 'TBS', '18500', '10', '18500', 'AD2019110001', '2020-02-02 15:18:27', '0'),
('00100373', 1, 'SPECTRON 3,5 GR', '880528', '2021-05-28', 169, 0, 'TBS', '18250', '10', '18250', 'AD2019110001', '2020-02-02 15:18:55', '0'),
('00100374', 1, 'TOBROSON 3,5 GR', '9100411', '2022-04-28', 53, 0, 'TBS', '43250', '10', '43250', 'AD2019110001', '2020-02-02 15:19:29', '0'),
('00100375', 1, 'XITROL 3,5 GR', '991014', '2022-02-21', 22, 0, 'TBS', '35875', '10', '35875', 'AD2019110001', '2020-02-02 15:20:11', '0'),
('00100376', 1, 'ULCORI MDS', '9U61107', '2022-05-28', 65, 0, 'STP', '19625', '10', '19625', 'AD2019110001', '2020-02-02 15:43:07', '0'),
('00100377', 1, 'PANTOCAIN 0.5% 5 ML INH', '7P10809', '2020-08-28', 1, 0, 'FLS', '11365', '10', '11365', 'AD2019110001', '2020-02-02 16:01:04', '0'),
('00100378', 1, 'SILOXAN 5 ML', '9SI1108', '2022-11-28', 1200, 0, 'FLS', '52750', '10', '52750', 'AD2019110001', '2020-02-03 12:20:53', '0'),
('00100379', 1, 'VITROLENTA 5 ML', '9VL1106', '2022-11-28', 0, 0, 'FLS', '32750', '10', '32750', 'AD2019110001', '2020-02-03 12:25:36', '0'),
('00100380', 1, 'PANTOCAIN 0,5% 5 ML', '9P11204', '2022-12-28', 350, 0, 'FLS', '12125', '10', '12125', 'AD2019110001', '2020-02-03 14:15:40', '0'),
('00100381', 1, 'CENDO VITAL', '19VT11111', '2022-11-28', 197, 0, 'KTK/30TAB', '90000', '10', '90000', 'AD2019110001', '2020-02-03 14:32:19', '0'),
('00100382', 1, 'CENDO VITAL', '19VT05231', '2022-05-28', 0, 0, 'KTK/30TAB', '90000', '10', '90000', 'AD2019110001', '2020-02-03 14:32:45', '0'),
('00100383', 1, 'GLAUCON', '18GL12041', '2023-12-28', 0, 0, 'ktk/20tab', '76000', '10', '76000', 'AD2019110001', '2020-02-03 14:33:56', '0'),
('00100384', 1, 'GLAUCON', '18GL12121', '2023-12-28', 140, 0, 'ktk/20tab', '76000', '10', '76000', 'AD2019110001', '2020-02-03 14:34:28', '0'),
('00100385', 1, 'STATROL MDS', '9S60808', '2022-02-28', 0, 0, 'STP', '16875', '10', '16875', 'AD2019110001', '2020-02-03 14:35:23', '0'),
('00100386', 1, 'XITROL MDS', '9X60818', '2022-02-28', 185, 0, 'STP', '24375', '24375', '24375', 'AD2019110001', '2020-02-17 11:23:01', '0'),
('00100387', 1, 'CENDO CHOLINE SYRUP', '19C508201', '2021-08-28', 16, 0, 'KTK/5TUBE', '108750', '108750', '108750', 'AD2019110001', '2020-02-07 11:21:45', '0'),
('00100388', 1, 'CETAPRED 5 ML', '8CT0626', '2020-06-28', 49, 0, 'FLS', '14250', '10', '14250', 'AD2019110001', '2020-02-03 15:15:59', '0'),
('00100389', 1, 'ALBUVIT 10% 5 ML', '9A10916', '2021-09-28', 1, 0, 'FLS', '10175', '10', '10175', 'AD2019110001', '2020-02-04 12:47:14', '0'),
('00100390', 1, 'MYCETINE 3,5 GR', '940808', '2022-08-28', 50, 0, 'TBS', '112750', '10', '12750', 'AD2019110001', '2020-02-04 12:47:48', '0'),
('00100391', 1, 'FENICOL 0,25% MDS', '9F260528', '2021-05-28', 25, 0, 'STP', '16375', '10', '16375', 'AD2019110001', '2020-02-04 12:48:34', '0'),
('00100392', 1, 'FENICOL 0,5% MDS', '8F561206', '2020-12-28', 1, 0, 'STP', '17500', '10', '17500', 'AD2019110001', '2020-02-04 12:49:09', '0'),
('00100393', 1, 'CENDO CHOLINE', '19CT03121', '2022-03-28', 1, 0, 'ktk/30tab', '225000', '10', '225000', 'AD2019110001', '2020-02-04 12:50:31', '0'),
('00100394', 1, 'CENDO CHOLINE', '19CT03141', '2022-03-28', 12, 0, 'ktk/30tab', '225000', '225000', '225000', 'AD2019110001', '2020-02-07 11:21:58', '0'),
('00100395', 1, 'CENDO CHOLINE', '19CT03211', '2022-03-28', 29, 0, 'ktk/30tab', '225000', '10', '225000', 'AD2019110001', '2020-02-04 12:51:13', '0'),
('00100396', 1, 'TOBRO MDS INH', '9TO61016', '2022-10-28', 20, 0, 'STP', '18545', '10', '18545', 'AD2019110001', '2020-02-04 12:51:45', '0'),
('00100397', 1, 'TOBRO MDS INH', '9TO60121', '2022-01-28', 2, 0, 'STP', '18545', '10', '18545', 'AD2019110001', '2020-02-04 12:52:16', '0'),
('00100398', 1, 'MYCOS MDS', '9MY60625', '2021-06-28', 0, 0, 'STP', '16625', '10', '16625', 'AD2019110001', '2020-02-04 15:40:11', '0'),
('00100399', 1, 'LYTEERS 15 ML', '9L1104', '2022-11-28', 0, 0, 'FLS', '22500', '10', '22500', 'AD2019110001', '2020-02-04 15:45:50', '0'),
('00100400', 1, 'GLAOPEN MDS', '9GL61204', '2021-12-28', 123, 0, 'STP', '90250', '10', '90250', 'AD2019110001', '2020-02-04 16:07:49', '0'),
('00100401', 1, 'CATARLENT MINIDOSE', '9CA61129', '2021-11-28', 0, 0, 'STP', '20000', '10', '20000', 'AD2019110001', '2020-02-04 16:31:26', '0'),
('00100402', 1, 'CENFRESH MDS', '9C61025', '2022-10-28', 2660, 0, 'STP', '23000', '10', '23000', 'AD2019110001', '2020-02-04 16:31:47', '0'),
('00100403', 1, 'CONVER 4% MDS', '9CV461220', '0000-00-00', 31, 0, 'STP', '22250', '10', '22250', 'AD2019110001', '2020-02-04 16:32:46', '0'),
('00100404', 1, 'EYE LOTION MDS', '9EL61107', '2022-07-28', 0, 0, 'STP', '31900', '10', '31900', 'AD2019110001', '2020-02-04 16:33:53', '0'),
('00100405', 1, 'EYEFRESH MDS', '9EH61029', '2022-10-28', 653, 0, 'STP', '23125', '10', '23125', 'AD2019110001', '2020-02-04 16:34:13', '0'),
('00100406', 1, 'EYEFRESH MILD MDS', '9EM61009', '2022-10-28', 200, 0, 'STP', '30625', '10', '30625', 'AD2019110001', '2020-02-04 16:34:33', '0'),
('00100407', 1, 'EYEFRESH PLUS MDS', '9EP61021', '2022-10-28', 600, 0, 'STP', '34875', '10', '34875', 'AD2019110001', '2020-02-04 16:34:47', '0'),
('00100408', 1, 'FLOXA MDS', '9F361021', '2022-04-28', 60, 0, 'STP', '26750', '10', '26750', 'AD2019110001', '2020-02-04 16:35:11', '0'),
('00100409', 1, 'GENTA 0,3% MDS', '9G560829', '2022-08-28', 0, 0, 'STP', '15875', '10', '15875', 'AD2019110001', '2020-02-04 16:35:31', '0'),
('00100410', 1, 'GIFLOX MDS', '9GF60919', '2021-09-28', 0, 0, 'STP', '80250', '10', '80250', 'AD2019110001', '2020-02-04 16:35:45', '0'),
('00100411', 1, 'GLAOPLUS MDS', '9GP61106', '2021-11-28', 0, 0, 'STP', '95875', '10', '95875', 'AD2019110001', '2020-02-04 16:36:10', '0'),
('00100412', 1, 'HYALUB MDS', '9HA61104', '2022-11-28', 500, 0, 'STP', '57000', '10', '57000', 'AD2019110001', '2020-02-04 16:36:29', '0'),
('00100413', 1, 'L F X MDS', '9LF61115', '2022-05-28', 676, 0, 'STP', '69000', '10', '69000', 'AD2019110001', '2020-02-04 16:36:47', '0'),
('00100414', 1, 'XITROL MDS', '9X61019', '2022-04-28', 0, 0, 'STP', '24375', '10', '24375', 'AD2019110001', '2020-02-04 16:37:03', '0'),
('00100415', 1, 'LYTEERS MDS', '9L61105', '2022-11-28', 120, 0, 'STP', '19250', '10', '19250', 'AD2019110001', '2020-02-04 16:37:33', '0'),
('00100416', 1, 'MYDRIATIL 0,5% MDS', '9M561028', '2022-10-28', 0, 0, 'STP', '26375', '10', '26375', 'AD2019110001', '2020-02-04 16:37:59', '0'),
('00100417', 1, 'NATACEN MDS', '9NA60912', '2021-09-28', 0, 0, 'STP', '41625', '10', '41625', 'AD2019110001', '2020-02-04 16:38:17', '0'),
('00100418', 1, 'NONCORT MDS', '9N61112', '2022-11-28', 600, 0, 'STP', '42250', '10', '42250', 'AD2019110001', '2020-02-04 16:38:45', '0'),
('00100419', 1, 'POLYDEX MDS', '9PX61130', '2022-05-28', 97, 0, 'STP', '26500', '10', '26500', 'AD2019110001', '2020-02-04 16:39:00', '0'),
('00100420', 1, 'POLYGRAN MDS', '9PG61129', '2022-05-28', 100, 0, 'STP', '17500', '10', '17500', 'AD2019110001', '2020-02-04 16:39:22', '0'),
('00100421', 1, 'POLYNEL MDS', '9PL61104', '2022-05-28', 0, 0, 'STP', '17875', '10', '17875', 'AD2019110001', '2020-02-04 16:39:39', '0'),
('00100422', 1, 'POSOP MDS', '9PO61120', '2022-05-28', 0, 0, 'STP', '52500', '10', '52500', 'AD2019110001', '2020-02-04 16:39:53', '0'),
('00100423', 1, 'P-PRED MDS', '9PP61019', '2021-10-28', 60, 0, 'STP', '38250', '10', '38250', 'AD2019110001', '2020-02-04 16:40:19', '0'),
('00100424', 1, 'PROTAGENTA MDS', '9PR61106', '2022-11-28', 1100, 0, 'STP', '35875', '10', '35875', 'AD2019110001', '2020-02-04 16:40:34', '0'),
('00100425', 1, 'TIMOL 0,5% MDS', '9TM61030', '2022-10-28', 152, 0, 'STP', '25750', '10', '25750', 'AD2019110001', '2020-02-04 16:40:57', '0'),
('00100426', 1, 'TOBRO MDS', '9TO61031', '2022-10-28', 100, 0, 'STP', '19125', '19125', '19125', 'AD2019110001', '2020-02-05 08:36:16', '0'),
('00100427', 1, 'TOBROSON MDS', '9TS61023', '2022-04-28', 1966, 0, 'STP', '29000', '10', '29000', 'AD2019110001', '2020-02-04 16:41:38', '0'),
('00100428', 1, 'TONOR 0,5% MDS', '9TR61206', '2022-12-28', 100, 0, 'STP', '24375', '10', '24375', 'AD2019110001', '2020-02-04 16:42:03', '0'),
('00100429', 1, 'TROPIN 1% MDS', '9T61004', '2022-10-28', 60, 0, 'STP', '16000', '10', '16000', 'AD2019110001', '2020-02-04 16:42:25', '0'),
('00100430', 1, 'ULCORI MDS', '9U61107', '2022-05-28', 0, 0, 'STP', '19625', '19625', '19625', 'AD2019110001', '2020-02-04 16:43:01', '0'),
('00100431', 1, 'VASACON MDS', '9VN61031', '2021-10-28', 40, 0, 'STP', '13875', '10', '13875', 'AD2019110001', '2020-02-04 16:43:31', '0'),
('00100432', 1, 'VASACON A MDS', '9VA61024', '2021-10-28', 98, 0, 'STP', '14750', '10', '14750', 'AD2019110001', '2020-02-04 16:43:50', '0'),
('00100433', 1, 'VISION MDS', '9VI60512', '2022-05-28', 40, 0, 'STP', '13500', '10', '13500', 'AD2019110001', '2020-02-04 16:44:10', '0'),
('00100434', 1, 'VOSAMA MDS', '9V60905', '2022-09-28', 0, 0, 'STP', '17625', '10', '17625', 'AD2019110001', '2020-02-04 16:44:27', '0'),
('00100435', 1, 'GLAOPLUS MDS', '9GP61106', '2021-11-28', 0, 0, 'STP', '95875', '10', '95875', 'AD2019110001', '2020-02-04 16:45:01', '0'),
('00100436', 1, 'EYEFRESH PLUS MDS', '9EP61021', '2022-10-28', 0, 0, 'STP', '34875', '10', '34875', 'AD2019110001', '2020-02-04 16:45:16', '0'),
('00100437', 1, 'POSOP MDS', '9PO61120', '2022-05-28', 0, 0, 'STP', '52500', '10', '52500', 'AD2019110001', '2020-02-04 16:45:38', '0'),
('00100438', 1, 'LUBRICEN MDS', '9LU60522', '2022-05-28', 0, 0, 'STP', '40500', '10', '40500', 'AD2019110001', '2020-02-04 16:47:39', '0'),
('00100439', 1, 'PATACEN MDS', '9PC61108', '2022-11-28', 0, 0, 'STP', '101500', '10', '101500', 'AD2019110001', '2020-02-04 16:47:56', '0'),
('00100440', 1, 'ALBUVIT 15% 5 ML', '9A50916', '2022-09-28', 0, 0, 'fls', '11250', '11250', '11250', 'AD2019110001', '2020-02-04 16:52:00', '0'),
('00100441', 1, 'CARPINE 1% 5 ML', '9C10907', '2022-09-28', 50, 0, 'FLS', '15500', '10', '15500', 'AD2019110001', '2020-02-04 16:53:02', '0'),
('00100442', 1, 'CATARLENT 5 ML', '9CA1028', '2021-10-28', 465, 0, 'FLS', '22625', '10', '22625', 'AD2019110001', '2020-02-04 16:53:25', '0'),
('00100443', 1, 'CATARLENT 15 ML', '9CN1231', '2021-12-28', 200, 0, 'FLS', '30625', '10', '30625', 'AD2019110001', '2020-02-04 16:53:43', '0'),
('00100444', 1, 'CENDRID 5 ML', '9CD0818', '2022-08-28', 50, 0, 'FLS', '15125', '10', '15125', 'AD2019110001', '2020-02-04 16:54:33', '0'),
('00100445', 1, 'CENFRESH 5 ML', '9C1120', '2022-11-28', 455, 0, 'FLS', '34875', '10', '34875', 'AD2019110001', '2020-02-04 16:54:50', '0'),
('00100446', 1, 'CONAL 5 ML', '9C01106', '2022-11-28', 0, 0, 'FLS', '20250', '10', '20250', 'AD2019110001', '2020-02-04 16:55:20', '0'),
('00100447', 1, 'CONVER 2% 15 ML', '9V1205', '2022-12-28', 0, 0, 'FLS', '13000', '10', '13000', 'AD2019110001', '2020-02-04 16:55:43', '0'),
('00100448', 1, 'DEXATON 5 ML', '9D1009', '2022-04-28', 283, 0, 'FLS', '18150', '10', '18150', 'AD2019110001', '2020-02-04 16:56:02', '0'),
('00100449', 1, 'EYEFRESH 5 ML', '9EH1017', '2022-10-28', 300, 0, 'FLS', '28875', '10', '28875', 'AD2019110001', '2020-02-04 16:56:17', '0'),
('00100450', 1, 'FLOXA 5 ML', '9F31205', '2022-06-28', 120, 0, 'FLS', '30525', '10', '30525', 'AD2019110001', '2020-02-04 16:57:38', '0'),
('00100451', 1, 'GENTA 0,3% 5 ML', '9G51202', '2022-12-28', 0, 0, 'FLS', '28375', '10', '28375', 'AD2019110001', '2020-02-04 16:57:57', '0'),
('00100452', 1, 'HOMATRO 2% 5 ML', '9H1015', '2022-10-28', 0, 0, 'FLS', '35500', '10', '35500', 'AD2019110001', '2020-02-04 16:58:12', '0'),
('00100453', 1, 'L F X 5 ML', '9LF1104', '2022-05-28', 50, 0, 'FLS', '69250', '10', '69250', 'AD2019110001', '2020-02-04 16:58:29', '0'),
('00100454', 1, 'LYTEERS 15 ML', '9L1220', '2022-12-28', 924, 0, 'FLS', '22500', '10', '22500', 'AD2019110001', '2020-02-04 17:02:04', '0'),
('00100455', 1, 'MYCOS 5 ML', '9MY1112', '2021-04-28', 50, 0, 'FLS', '19875', '19875', '19875', 'AD2019110001', '2020-02-05 09:03:11', '0'),
('00100456', 1, 'MYDRIATIL 0,5% 5 ML', '9M51128', '2022-11-28', 50, 0, 'FLS', '28875', '10', '28875', 'AD2019110001', '2020-02-04 17:02:45', '0'),
('00100457', 1, 'MYDRIATIL 1% 5 ML', '9M1107', '2022-11-28', 100, 0, 'FLS', '38250', '10', '38250', 'AD2019110001', '2020-02-04 17:03:07', '0'),
('00100458', 1, 'NONCORT 5 ML', '9N1113', '2022-11-28', 0, 0, 'FLS', '37500', '10', '37500', 'AD2019110001', '2020-02-04 17:03:20', '0'),
('00100459', 1, 'PANTOCAIN 0,5% 5 ML', '9P1204', '2022-12-28', 0, 0, 'FLS', '12125', '10', '12125', 'AD2019110001', '2020-02-04 17:05:09', '0'),
('00100460', 1, 'PANTOCAIN 2% 5 ML', '9P31018', '2022-10-28', 200, 0, 'FLS', '13250', '10', '13250', 'AD2019110001', '2020-02-04 17:05:26', '0'),
('00100461', 1, 'POLYDEX 5 ML', '9PX1128', '2022-05-28', 0, 0, 'FLS', '35750', '10', '35750', 'AD2019110001', '2020-02-04 17:05:42', '0'),
('00100462', 1, 'POLYNEL 5 ML', '9PL1120', '2022-05-28', 0, 0, 'FLS', '26875', '10', '26875', 'AD2019110001', '2020-02-04 17:05:58', '0'),
('00100463', 1, 'P-PRED 5 ML', '9PP1105', '2021-11-28', 100, 0, 'FLS', '55500', '10', '55500', 'AD2019110001', '2020-02-04 17:06:12', '0'),
('00100464', 1, 'SILOXAN 5 ML', '9SI1108', '2022-11-28', 0, 0, 'FLS', '52750', '10', '52750', 'AD2019110001', '2020-02-04 17:06:28', '0'),
('00100465', 1, 'STATROL 5 ML', '9S1010', '2022-04-28', 200, 0, 'FLS', '18250', '10', '18250', 'AD2019110001', '2020-02-04 17:06:55', '0'),
('00100466', 1, 'TIMOL 0,5% 5 ML', '9TM1206', '2022-12-28', 1140, 0, 'FLS', '47750', '10', '47750', 'AD2019110001', '2020-02-04 17:07:15', '0'),
('00100467', 1, 'TOBROSON 5 ML', '9TS1125', '2022-05-28', 600, 0, 'FLS', '46250', '10', '46250', 'AD2019110001', '2020-02-04 17:07:37', '0'),
('00100468', 1, 'TROPINE 0,5% 5 ML', '9T51104', '2022-11-28', 100, 0, 'FLS', '11250', '10', '11250', 'AD2019110001', '2020-02-04 17:07:59', '0'),
('00100469', 1, 'TROPINE 1% 5 ML', '9T11114', '2022-11-28', 100, 0, 'FLS', '12500', '10', '12500', 'AD2019110001', '2020-02-04 17:08:14', '0'),
('00100470', 1, 'VASACON 15 ML', '9VN1007', '2022-10-28', 100, 0, 'FLS', '16250', '10', '16250', 'AD2019110001', '2020-02-04 17:08:27', '0'),
('00100471', 1, 'VASACON A 15 ML', '9VA1014', '2021-10-28', 300, 0, 'FLS', '22375', '10', '22375', 'AD2019110001', '2020-02-04 17:08:47', '0'),
('00100472', 1, 'VISION 15 ML', '9VI0904', '2022-09-28', 0, 0, 'FLS', '19875', '10', '19875', 'AD2019110001', '2020-02-04 17:08:59', '0'),
('00100473', 1, 'VITROLENTA 5 ML', '9VL1107', '2022-11-28', 722, 0, 'FLS', '32750', '10', '32750', 'AD2019110001', '2020-02-04 17:09:17', '0'),
('00100474', 1, 'XITROL 5 ML', '9X1102', '2022-05-28', 0, 0, 'FLS', '27250', '10', '27250', 'AD2019110001', '2020-02-04 17:09:37', '0'),
('00100475', 1, 'VERNACEL 5 ML', '9VE0826', '2022-08-28', 100, 0, 'FLS', '18500', '10', '18500', 'AD2019110001', '2020-02-04 17:09:51', '0'),
('00100476', 1, 'GENTAMYCINE 3,5 GR', '921025', '2022-10-28', 1104, 0, 'TBS', '36375', '10', '36375', 'AD2019110001', '2020-02-04 17:10:25', '0'),
('00100477', 1, 'MYCOS 3,5 GR', '951031', '2022-10-28', 100, 0, 'TBS', '26250', '10', '26250', 'AD2019110001', '2020-02-04 17:10:49', '0'),
('00100478', 1, 'OCULENTA VD 5 GR', '9OCG1030', '2022-04-28', 349, 0, 'TBS', '63875', '10', '63875', 'AD2019110001', '2020-02-04 17:11:06', '0');
INSERT INTO `item` (`id_item`, `id_brand`, `nama_item`, `kode_produksi_item`, `tgl_kadaluarsa_item`, `stok`, `stok_opnam`, `satuan`, `harga_bruto`, `harga_netto`, `harga_jual`, `admin_create_item`, `time_up_item`, `is_del_item`) VALUES
('00100479', 1, 'POLYGRAN 3,5 GR', '970928', '2022-09-28', 0, 0, 'TBS', '18500', '10', '18500', 'AD2019110001', '2020-02-04 17:11:54', '0'),
('00100480', 1, 'SPECTRON 3,5 GR', '880528', '2021-05-28', 0, 0, 'TBS', '18250', '10', '18250', 'AD2019110001', '2020-02-04 17:12:07', '0'),
('00100481', 1, 'TOBROSON 3,5 GR', '9101111', '2022-11-28', 100, 0, 'TBS', '43250', '10', '43250', 'AD2019110001', '2020-02-04 17:12:25', '0'),
('00100482', 1, 'XITROL 3,5 GR', '991019', '2022-10-28', 523, 0, 'TBS', '35875', '10', '35875', 'AD2019110001', '2020-02-04 17:12:42', '0'),
('00100483', 1, 'GLAUCON', '18GL12121', '2023-12-28', 0, 0, 'ktk/20tab', '76000', '10', '76000', 'AD2019110001', '2020-02-04 17:13:05', '0'),
('00100484', 1, 'CENDO VITAL', '19VT1111', '2022-11-28', 0, 0, 'ktk/30tab', '90000', '10', '90000', 'AD2019110001', '2020-02-04 17:13:31', '0'),
('00100485', 1, 'LYTEERS 15 ML', '9L1219', '2022-12-28', 0, 0, 'FLS', '22500', '10', '22500', 'AD2019110001', '2020-02-04 17:13:49', '0'),
('00100486', 1, 'HOMATRO 2% 5 ML', '9H1015', '2022-10-28', 0, 0, 'FLS', '35500', '10', '35500', 'AD2019110001', '2020-02-04 17:14:20', '0'),
('00100487', 1, 'FLOXA 5 ML', '9F31205', '2022-06-28', 0, 0, 'FLS', '30525', '10', '30525', 'AD2019110001', '2020-02-04 17:14:48', '0'),
('00100488', 1, 'NONCORT 5 ML', '9N1113', '2022-11-28', 0, 0, 'FLS', '37500', '10', '37500', 'AD2019110001', '2020-02-04 17:15:01', '0'),
('00100490', 1, 'LUBRICEN 5 ML', '9LU0523', '2023-05-28', 0, 0, 'FLS', '36750', '10', '36750', 'AD2019110001', '2020-02-04 17:16:00', '0'),
('00100491', 1, 'LYTEERS 15 ML INH', '9L1219', '2022-12-28', 0, 0, 'FLS', '21818', '10', '21818', 'AD2019110001', '2020-02-04 17:16:23', '0'),
('00100492', 1, 'POSOP MDS INH', '9PO61120', '2022-05-28', 20, 0, 'STP', '49545', '10', '49545', 'AD2019110001', '2020-02-04 17:16:39', '0'),
('00100493', 1, 'PROTAGENTA MDS INH', '9PR61106', '2022-11-28', 20, 0, 'STP', '33636', '10', '33636', 'AD2019110001', '2020-02-04 17:16:57', '0'),
('00100494', 1, 'CONVER 2% 15 ML', '9CV1205', '2022-12-28', 100, 0, 'FLS', '13000', '10', '13000', 'AD2019110001', '2020-02-05 09:05:25', '0'),
('00100495', 1, 'NONCORT MDS', '9N61102', '2022-11-28', 6, 0, 'STP', '42250', '10', '42250', 'AD2019110001', '2020-02-05 12:00:21', '0'),
('00100496', 1, 'XITROL 5 ML', '9X1022', '2022-04-28', 0, 0, 'FLS', '27250', '27250', '27250', 'AD2019110001', '2020-02-11 12:38:20', '0'),
('00100497', 1, 'XITROL 5 ML', '9X1101', '2022-05-28', 370, 0, 'FLS', '27250', '10', '27250', 'AD2019110001', '2020-02-05 14:50:52', '0'),
('00100498', 1, 'TIMOL 0,25% MDS', '8TI60404', '2021-04-28', 7, 0, 'STP', '17625', '10', '17625', 'AD2019110001', '2020-02-06 14:35:45', '0'),
('00100499', 1, 'FENICOL 3,5 GR', '900717', '2022-07-28', 0, 0, 'TBS', '21875', '10', '21875', 'AD2019110001', '2020-02-10 13:52:14', '0'),
('00100500', 1, 'XITROL 5 ML', '9X0924', '2022-03-28', 1, 0, 'FLS', '27250', '10', '27250', 'AD2019110001', '2020-02-11 13:45:52', '0'),
('00100501', 1, 'L F X 5 ML', '9LF0819', '2021-12-28', 0, 0, 'FLS', '69250', '10', '69250', 'AD2019110001', '2020-02-11 16:05:10', '0'),
('00100502', 1, 'GENTAMYCINE 3,5 GR', '92013', '2022-09-28', 0, 0, 'TBS', '36375', '10', '36375', 'AD2019110001', '2020-02-11 16:05:32', '0'),
('00100503', 1, 'NONCORT MDS', '9N60928', '2022-09-28', 0, 0, 'STP', '42250', '10', '42250', 'AD2019110001', '2020-02-11 16:05:58', '0'),
('00100504', 1, 'TIMOL 0,5% MDS', '9TM61113', '2022-11-28', 20, 0, 'stp', '25750', '10', '25750', 'AD2019110001', '2020-02-11 16:38:52', '0'),
('00100505', 1, 'LYTEERS MDS', '9L61121', '2022-11-23', 40, 0, 'STP', '19250', '10', '19250', 'AD2019110001', '2020-02-11 16:40:45', '0'),
('00100506', 1, 'FLOXA MDS', '9F361029', '2022-04-28', 0, 0, 'STP', '26750', '10', '26750', 'AD2019110001', '2020-02-11 16:42:19', '0'),
('00100507', 1, 'CATARLENT MINIDOSE', '0CA60110', '2022-01-28', 10, 0, 'STP', '20000', '20000', '20000', 'AD2019110001', '2020-02-13 10:33:27', '0'),
('00100508', 1, 'AUGENTONIC 5 ML', '0AU0121', '2023-01-28', 560, 0, 'FLS', '25125', '25125', '25125', 'AD2019110001', '2020-02-13 10:42:50', '0'),
('00100509', 1, 'EFRISEL 10% 5 ML', '0E10122', '2022-01-28', 100, 0, 'FLS', '15250', '15250', '15250', 'AD2019110001', '2020-02-13 10:31:25', '0'),
('00100510', 1, 'AUGENTONIC 15 ML', '0AG0121', '2023-01-28', 400, 0, 'FLS', '30625', '10', '30625', 'AD2019110001', '2020-02-13 10:44:07', '0'),
('00100511', 1, 'NONCORT MDS', '7N60224', '2020-02-28', 1, 0, 'STP', '42250', '10', '42250', 'AD2019110001', '2020-02-13 11:54:42', '0'),
('00100512', 1, 'XITROL MDS', '7X60822', '2020-02-28', 1, 0, 'STP', '24375', '10', '24375', 'AD2019110001', '2020-02-13 11:55:29', '0'),
('00100513', 1, 'GLAOPEN MDS', '8GL60213', '2020-02-28', 1, 0, 'STP', '90250', '10', '90250', 'AD2019110001', '2020-02-13 11:56:09', '0'),
('00100514', 1, 'VITROLENTA MDS', '7VL60131', '2020-01-28', 1, 0, 'STP', '28625', '10', '28625', 'AD2019110001', '2020-02-13 11:57:02', '0'),
('00100515', 1, 'CATARLENT MINIDOSE', '7ca61229', '2020-06-28', 2, 0, 'stp', '20000', '10', '20000', 'AD2019110001', '2020-02-13 14:50:28', '0'),
('00100516', 1, 'PROTAGENTA MDS', '7pr60120', '2020-01-28', 10, 0, 'stp', '35875', '10', '35875', 'AD2019110001', '2020-02-13 15:02:06', '0'),
('00100517', 1, 'tetsss', 'r32rrefrgfd111', '2020-02-12', 0, 0, '1021', '1000001', '1000001', '1000001', 'AD2019110001', '2020-02-19 10:58:05', '0'),
('00100519', 1, 'testr', 'dq7367frf', '2020-02-21', 0, 0, 'sats', '10000', '10', '10000', 'AD2019110001', '2020-02-19 10:58:36', '0'),
('00100520', 1, 'dsadsad', '12213dewfs', '2020-02-07', 0, 0, 'dasdsa', '1', '10', '1', 'AD2019110001', '2020-02-19 10:59:00', '0');

-- --------------------------------------------------------

--
-- Table structure for table `record_item`
--

CREATE TABLE `record_item` (
  `id_record_item` varchar(18) NOT NULL,
  `periode_record_item` date NOT NULL,
  `id_item` varchar(10) NOT NULL,
  `stok_record_item` varchar(16) NOT NULL,
  `rusak_record_item` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `record_item`
--

INSERT INTO `record_item` (`id_record_item`, `periode_record_item`, `id_item`, `stok_record_item`, `rusak_record_item`) VALUES
('20200219-0000001', '2020-02-19', '00100002', '3870', '0'),
('20200219-0000002', '2020-02-19', '00100004', '3870', '0'),
('20200219-0000003', '2020-02-19', '00100122', '0', '0'),
('20200219-0000004', '2020-02-19', '00100126', '0', '0'),
('20200219-0000005', '2020-02-19', '00100127', '0', '0'),
('20200219-0000006', '2020-02-19', '00100128', '0', '0'),
('20200219-0000007', '2020-02-19', '00100129', '0', '0'),
('20200219-0000008', '2020-02-19', '00100130', '0', '0'),
('20200219-0000009', '2020-02-19', '00100131', '0', '0'),
('20200219-0000010', '2020-02-19', '00100132', '0', '0'),
('20200219-0000011', '2020-02-19', '00100133', '320', '0'),
('20200219-0000012', '2020-02-19', '00100134', '62', '0'),
('20200219-0000013', '2020-02-19', '00100135', '40', '0'),
('20200219-0000014', '2020-02-19', '00100136', '0', '0'),
('20200219-0000015', '2020-02-19', '00100137', '20', '0'),
('20200219-0000016', '2020-02-19', '00100138', '50', '0'),
('20200219-0000017', '2020-02-19', '00100139', '0', '0'),
('20200219-0000018', '2020-02-19', '00100140', '15', '0'),
('20200219-0000019', '2020-02-19', '00100141', '0', '0'),
('20200219-0000020', '2020-02-19', '00100142', '150', '0'),
('20200219-0000021', '2020-02-19', '00100143', '74', '0'),
('20200219-0000022', '2020-02-19', '00100144', '2', '0'),
('20200219-0000023', '2020-02-19', '00100145', '600', '0'),
('20200219-0000024', '2020-02-19', '00100146', '34', '0'),
('20200219-0000025', '2020-02-19', '00100147', '0', '0'),
('20200219-0000026', '2020-02-19', '00100148', '25', '0'),
('20200219-0000027', '2020-02-19', '00100149', '7', '0'),
('20200219-0000028', '2020-02-19', '00100150', '0', '0'),
('20200219-0000029', '2020-02-19', '00100151', '0', '0'),
('20200219-0000030', '2020-02-19', '00100152', '0', '0'),
('20200219-0000031', '2020-02-19', '00100153', '74', '0'),
('20200219-0000032', '2020-02-19', '00100154', '34', '0'),
('20200219-0000033', '2020-02-19', '00100155', '50', '0'),
('20200219-0000034', '2020-02-19', '00100156', '68', '0'),
('20200219-0000035', '2020-02-19', '00100157', '50', '0'),
('20200219-0000036', '2020-02-19', '00100158', '50', '0'),
('20200219-0000037', '2020-02-19', '00100159', '50', '0'),
('20200219-0000038', '2020-02-19', '00100160', '33', '0'),
('20200219-0000039', '2020-02-19', '00100161', '50', '0'),
('20200219-0000040', '2020-02-19', '00100162', '50', '0'),
('20200219-0000041', '2020-02-19', '00100163', '1', '0'),
('20200219-0000042', '2020-02-19', '00100164', '0', '0'),
('20200219-0000043', '2020-02-19', '00100165', '59', '0'),
('20200219-0000044', '2020-02-19', '00100166', '0', '0'),
('20200219-0000045', '2020-02-19', '00100167', '53', '0'),
('20200219-0000046', '2020-02-19', '00100168', '100', '0'),
('20200219-0000047', '2020-02-19', '00100169', '0', '0'),
('20200219-0000048', '2020-02-19', '00100170', '48', '0'),
('20200219-0000049', '2020-02-19', '00100171', '100', '0'),
('20200219-0000050', '2020-02-19', '00100172', '59', '0'),
('20200219-0000051', '2020-02-19', '00100173', '100', '0'),
('20200219-0000052', '2020-02-19', '00100174', '17', '0'),
('20200219-0000053', '2020-02-19', '00100175', '8', '0'),
('20200219-0000054', '2020-02-19', '00100176', '196', '0'),
('20200219-0000055', '2020-02-19', '00100177', '14', '0'),
('20200219-0000056', '2020-02-19', '00100178', '450', '0'),
('20200219-0000057', '2020-02-19', '00100179', '64', '0'),
('20200219-0000058', '2020-02-19', '00100180', '50', '0'),
('20200219-0000059', '2020-02-19', '00100181', '100', '0'),
('20200219-0000060', '2020-02-19', '00100182', '164', '0'),
('20200219-0000061', '2020-02-19', '00100183', '168', '0'),
('20200219-0000062', '2020-02-19', '00100184', '272', '0'),
('20200219-0000063', '2020-02-19', '00100185', '0', '0'),
('20200219-0000064', '2020-02-19', '00100186', '20', '0'),
('20200219-0000065', '2020-02-19', '00100187', '50', '0'),
('20200219-0000066', '2020-02-19', '00100188', '68', '0'),
('20200219-0000067', '2020-02-19', '00100189', '0', '0'),
('20200219-0000068', '2020-02-19', '00100190', '80', '0'),
('20200219-0000069', '2020-02-19', '00100191', '150', '0'),
('20200219-0000070', '2020-02-19', '00100192', '0', '0'),
('20200219-0000071', '2020-02-19', '00100193', '0', '0'),
('20200219-0000072', '2020-02-19', '00100194', '40', '0'),
('20200219-0000073', '2020-02-19', '00100195', '50', '0'),
('20200219-0000074', '2020-02-19', '00100196', '8', '0'),
('20200219-0000075', '2020-02-19', '00100197', '142', '0'),
('20200219-0000076', '2020-02-19', '00100198', '480', '0'),
('20200219-0000077', '2020-02-19', '00100199', '20', '0'),
('20200219-0000078', '2020-02-19', '00100200', '6', '0'),
('20200219-0000079', '2020-02-19', '00100201', '0', '0'),
('20200219-0000080', '2020-02-19', '00100202', '106', '0'),
('20200219-0000081', '2020-02-19', '00100203', '600', '0'),
('20200219-0000082', '2020-02-19', '00100204', '192', '0'),
('20200219-0000083', '2020-02-19', '00100205', '40', '0'),
('20200219-0000084', '2020-02-19', '00100206', '16', '0'),
('20200219-0000085', '2020-02-19', '00100207', '80', '0'),
('20200219-0000086', '2020-02-19', '00100208', '39', '0'),
('20200219-0000087', '2020-02-19', '00100209', '0', '0'),
('20200219-0000088', '2020-02-19', '00100210', '600', '0'),
('20200219-0000089', '2020-02-19', '00100211', '0', '0'),
('20200219-0000090', '2020-02-19', '00100212', '210', '0'),
('20200219-0000091', '2020-02-19', '00100213', '0', '0'),
('20200219-0000092', '2020-02-19', '00100214', '450', '0'),
('20200219-0000093', '2020-02-19', '00100215', '35', '0'),
('20200219-0000094', '2020-02-19', '00100216', '0', '0'),
('20200219-0000095', '2020-02-19', '00100217', '52', '0'),
('20200219-0000096', '2020-02-19', '00100218', '0', '0'),
('20200219-0000097', '2020-02-19', '00100219', '152', '0'),
('20200219-0000098', '2020-02-19', '00100220', '600', '0'),
('20200219-0000099', '2020-02-19', '00100221', '0', '0'),
('20200219-0000100', '2020-02-19', '00100222', '0', '0'),
('20200219-0000101', '2020-02-19', '00100223', '0', '0'),
('20200219-0000102', '2020-02-19', '00100224', '0', '0'),
('20200219-0000103', '2020-02-19', '00100225', '32', '0'),
('20200219-0000104', '2020-02-19', '00100226', '200', '0'),
('20200219-0000105', '2020-02-19', '00100227', '0', '0'),
('20200219-0000106', '2020-02-19', '00100228', '20', '0'),
('20200219-0000107', '2020-02-19', '00100229', '98', '0'),
('20200219-0000108', '2020-02-19', '00100230', '400', '0'),
('20200219-0000109', '2020-02-19', '00100231', '160', '0'),
('20200219-0000110', '2020-02-19', '00100232', '0', '0'),
('20200219-0000111', '2020-02-19', '00100233', '20', '0'),
('20200219-0000112', '2020-02-19', '00100234', '1', '0'),
('20200219-0000113', '2020-02-19', '00100235', '330', '0'),
('20200219-0000114', '2020-02-19', '00100236', '6', '0'),
('20200219-0000115', '2020-02-19', '00100237', '0', '0'),
('20200219-0000116', '2020-02-19', '00100238', '356', '0'),
('20200219-0000117', '2020-02-19', '00100239', '600', '0'),
('20200219-0000118', '2020-02-19', '00100240', '823', '0'),
('20200219-0000119', '2020-02-19', '00100241', '58', '0'),
('20200219-0000120', '2020-02-19', '00100242', '50', '0'),
('20200219-0000121', '2020-02-19', '00100243', '490', '0'),
('20200219-0000122', '2020-02-19', '00100244', '2', '0'),
('20200219-0000123', '2020-02-19', '00100245', '2', '0'),
('20200219-0000124', '2020-02-19', '00100246', '374', '0'),
('20200219-0000125', '2020-02-19', '00100247', '0', '0'),
('20200219-0000126', '2020-02-19', '00100248', '200', '0'),
('20200219-0000127', '2020-02-19', '00100249', '60', '0'),
('20200219-0000128', '2020-02-19', '00100250', '0', '0'),
('20200219-0000129', '2020-02-19', '00100251', '74', '0'),
('20200219-0000130', '2020-02-19', '00100252', '233', '0'),
('20200219-0000131', '2020-02-19', '00100253', '257', '0'),
('20200219-0000132', '2020-02-19', '00100254', '100', '0'),
('20200219-0000133', '2020-02-19', '00100255', '95', '0'),
('20200219-0000134', '2020-02-19', '00100256', '1', '0'),
('20200219-0000135', '2020-02-19', '00100257', '0', '0'),
('20200219-0000136', '2020-02-19', '00100258', '565', '0'),
('20200219-0000137', '2020-02-19', '00100259', '199', '0'),
('20200219-0000138', '2020-02-19', '00100260', '218', '0'),
('20200219-0000139', '2020-02-19', '00100261', '100', '0'),
('20200219-0000140', '2020-02-19', '00100262', '72', '0'),
('20200219-0000141', '2020-02-19', '00100263', '50', '0'),
('20200219-0000142', '2020-02-19', '00100264', '95', '0'),
('20200219-0000143', '2020-02-19', '00100265', '177', '0'),
('20200219-0000144', '2020-02-19', '00100266', '250', '0'),
('20200219-0000145', '2020-02-19', '00100267', '113', '0'),
('20200219-0000146', '2020-02-19', '00100268', '200', '0'),
('20200219-0000147', '2020-02-19', '00100269', '131', '0'),
('20200219-0000148', '2020-02-19', '00100270', '255', '0'),
('20200219-0000149', '2020-02-19', '00100271', '437', '0'),
('20200219-0000150', '2020-02-19', '00100272', '528', '0'),
('20200219-0000151', '2020-02-19', '00100273', '0', '0'),
('20200219-0000152', '2020-02-19', '00100274', '0', '0'),
('20200219-0000153', '2020-02-19', '00100275', '1', '0'),
('20200219-0000154', '2020-02-19', '00100276', '0', '0'),
('20200219-0000155', '2020-02-19', '00100277', '36', '0'),
('20200219-0000156', '2020-02-19', '00100278', '156', '0'),
('20200219-0000157', '2020-02-19', '00100279', '3', '0'),
('20200219-0000158', '2020-02-19', '00100280', '80', '0'),
('20200219-0000159', '2020-02-19', '00100281', '13', '0'),
('20200219-0000160', '2020-02-19', '00100282', '1', '0'),
('20200219-0000161', '2020-02-19', '00100283', '320', '0'),
('20200219-0000162', '2020-02-19', '00100284', '115', '0'),
('20200219-0000163', '2020-02-19', '00100285', '84', '0'),
('20200219-0000164', '2020-02-19', '00100286', '40', '0'),
('20200219-0000165', '2020-02-19', '00100287', '389', '0'),
('20200219-0000166', '2020-02-19', '00100288', '0', '0'),
('20200219-0000167', '2020-02-19', '00100289', '52', '0'),
('20200219-0000168', '2020-02-19', '00100290', '2', '0'),
('20200219-0000169', '2020-02-19', '00100291', '374', '0'),
('20200219-0000170', '2020-02-19', '00100292', '2', '0'),
('20200219-0000171', '2020-02-19', '00100293', '0', '0'),
('20200219-0000172', '2020-02-19', '00100294', '97', '0'),
('20200219-0000173', '2020-02-19', '00100295', '5', '0'),
('20200219-0000174', '2020-02-19', '00100296', '0', '0'),
('20200219-0000175', '2020-02-19', '00100297', '20', '0'),
('20200219-0000176', '2020-02-19', '00100298', '140', '0'),
('20200219-0000177', '2020-02-19', '00100299', '90', '0'),
('20200219-0000178', '2020-02-19', '00100300', '1', '0'),
('20200219-0000179', '2020-02-19', '00100301', '7', '0'),
('20200219-0000180', '2020-02-19', '00100302', '600', '0'),
('20200219-0000181', '2020-02-19', '00100303', '210', '0'),
('20200219-0000182', '2020-02-19', '00100304', '28', '0'),
('20200219-0000183', '2020-02-19', '00100305', '5', '0'),
('20200219-0000184', '2020-02-19', '00100306', '112', '0'),
('20200219-0000185', '2020-02-19', '00100307', '109', '0'),
('20200219-0000186', '2020-02-19', '00100308', '140', '0'),
('20200219-0000187', '2020-02-19', '00100309', '110', '0'),
('20200219-0000188', '2020-02-19', '00100310', '0', '0'),
('20200219-0000189', '2020-02-19', '00100311', '1', '0'),
('20200219-0000190', '2020-02-19', '00100312', '51', '0'),
('20200219-0000191', '2020-02-19', '00100314', '10', '0'),
('20200219-0000192', '2020-02-19', '00100315', '2', '0'),
('20200219-0000193', '2020-02-19', '00100316', '1', '0'),
('20200219-0000194', '2020-02-19', '00100317', '154', '0'),
('20200219-0000195', '2020-02-19', '00100318', '318', '0'),
('20200219-0000196', '2020-02-19', '00100319', '192', '0'),
('20200219-0000197', '2020-02-19', '00100320', '60', '0'),
('20200219-0000198', '2020-02-19', '00100321', '185', '0'),
('20200219-0000199', '2020-02-19', '00100322', '9', '0'),
('20200219-0000200', '2020-02-19', '00100323', '23', '0'),
('20200219-0000201', '2020-02-19', '00100324', '58', '0'),
('20200219-0000202', '2020-02-19', '00100325', '35', '0'),
('20200219-0000203', '2020-02-19', '00100326', '50', '0'),
('20200219-0000204', '2020-02-19', '00100327', '5', '0'),
('20200219-0000205', '2020-02-19', '00100328', '6', '0'),
('20200219-0000206', '2020-02-19', '00100329', '50', '0'),
('20200219-0000207', '2020-02-19', '00100330', '20', '0'),
('20200219-0000208', '2020-02-19', '00100331', '9', '0'),
('20200219-0000209', '2020-02-19', '00100332', '0', '0'),
('20200219-0000210', '2020-02-19', '00100333', '42', '0'),
('20200219-0000211', '2020-02-19', '00100334', '1', '0'),
('20200219-0000212', '2020-02-19', '00100335', '16', '0'),
('20200219-0000213', '2020-02-19', '00100336', '20', '0'),
('20200219-0000214', '2020-02-19', '00100337', '1', '0'),
('20200219-0000215', '2020-02-19', '00100338', '20', '0'),
('20200219-0000216', '2020-02-19', '00100339', '0', '0'),
('20200219-0000217', '2020-02-19', '00100340', '10', '0'),
('20200219-0000218', '2020-02-19', '00100341', '2', '0'),
('20200219-0000219', '2020-02-19', '00100342', '72', '0'),
('20200219-0000220', '2020-02-19', '00100343', '5', '0'),
('20200219-0000221', '2020-02-19', '00100344', '20', '0'),
('20200219-0000222', '2020-02-19', '00100345', '15', '0'),
('20200219-0000223', '2020-02-19', '00100346', '3', '0'),
('20200219-0000224', '2020-02-19', '00100347', '20', '0'),
('20200219-0000225', '2020-02-19', '00100348', '1', '0'),
('20200219-0000226', '2020-02-19', '00100349', '4', '0'),
('20200219-0000227', '2020-02-19', '00100350', '20', '0'),
('20200219-0000228', '2020-02-19', '00100351', '20', '0'),
('20200219-0000229', '2020-02-19', '00100352', '0', '0'),
('20200219-0000230', '2020-02-19', '00100353', '22', '0'),
('20200219-0000231', '2020-02-19', '00100354', '1', '0'),
('20200219-0000232', '2020-02-19', '00100355', '172', '0'),
('20200219-0000233', '2020-02-19', '00100356', '50', '0'),
('20200219-0000234', '2020-02-19', '00100357', '60', '0'),
('20200219-0000235', '2020-02-19', '00100358', '140', '0'),
('20200219-0000236', '2020-02-19', '00100359', '0', '0'),
('20200219-0000237', '2020-02-19', '00100360', '178', '0'),
('20200219-0000238', '2020-02-19', '00100361', '0', '0'),
('20200219-0000239', '2020-02-19', '00100362', '33', '0'),
('20200219-0000240', '2020-02-19', '00100363', '137', '0'),
('20200219-0000241', '2020-02-19', '00100365', '59', '0'),
('20200219-0000242', '2020-02-19', '00100366', '3', '0'),
('20200219-0000243', '2020-02-19', '00100367', '200', '0'),
('20200219-0000244', '2020-02-19', '00100368', '64', '0'),
('20200219-0000245', '2020-02-19', '00100369', '0', '0'),
('20200219-0000246', '2020-02-19', '00100370', '0', '0'),
('20200219-0000247', '2020-02-19', '00100371', '60', '0'),
('20200219-0000248', '2020-02-19', '00100372', '123', '0'),
('20200219-0000249', '2020-02-19', '00100373', '169', '0'),
('20200219-0000250', '2020-02-19', '00100374', '53', '0'),
('20200219-0000251', '2020-02-19', '00100375', '22', '0'),
('20200219-0000252', '2020-02-19', '00100376', '65', '0'),
('20200219-0000253', '2020-02-19', '00100377', '1', '0'),
('20200219-0000254', '2020-02-19', '00100378', '1200', '0'),
('20200219-0000255', '2020-02-19', '00100379', '0', '0'),
('20200219-0000256', '2020-02-19', '00100380', '350', '0'),
('20200219-0000257', '2020-02-19', '00100381', '197', '0'),
('20200219-0000258', '2020-02-19', '00100382', '0', '0'),
('20200219-0000259', '2020-02-19', '00100383', '0', '0'),
('20200219-0000260', '2020-02-19', '00100384', '140', '0'),
('20200219-0000261', '2020-02-19', '00100385', '0', '0'),
('20200219-0000262', '2020-02-19', '00100386', '185', '0'),
('20200219-0000263', '2020-02-19', '00100387', '16', '0'),
('20200219-0000264', '2020-02-19', '00100388', '49', '0'),
('20200219-0000265', '2020-02-19', '00100389', '1', '0'),
('20200219-0000266', '2020-02-19', '00100390', '50', '0'),
('20200219-0000267', '2020-02-19', '00100391', '25', '0'),
('20200219-0000268', '2020-02-19', '00100392', '1', '0'),
('20200219-0000269', '2020-02-19', '00100393', '1', '0'),
('20200219-0000270', '2020-02-19', '00100394', '12', '0'),
('20200219-0000271', '2020-02-19', '00100395', '29', '0'),
('20200219-0000272', '2020-02-19', '00100396', '20', '0'),
('20200219-0000273', '2020-02-19', '00100397', '2', '0'),
('20200219-0000274', '2020-02-19', '00100398', '0', '0'),
('20200219-0000275', '2020-02-19', '00100399', '0', '0'),
('20200219-0000276', '2020-02-19', '00100400', '123', '0'),
('20200219-0000277', '2020-02-19', '00100401', '0', '0'),
('20200219-0000278', '2020-02-19', '00100402', '2660', '0'),
('20200219-0000279', '2020-02-19', '00100403', '31', '0'),
('20200219-0000280', '2020-02-19', '00100404', '0', '0'),
('20200219-0000281', '2020-02-19', '00100405', '653', '0'),
('20200219-0000282', '2020-02-19', '00100406', '200', '0'),
('20200219-0000283', '2020-02-19', '00100407', '600', '0'),
('20200219-0000284', '2020-02-19', '00100408', '60', '0'),
('20200219-0000285', '2020-02-19', '00100409', '0', '0'),
('20200219-0000286', '2020-02-19', '00100410', '0', '0'),
('20200219-0000287', '2020-02-19', '00100411', '0', '0'),
('20200219-0000288', '2020-02-19', '00100412', '500', '0'),
('20200219-0000289', '2020-02-19', '00100413', '676', '0'),
('20200219-0000290', '2020-02-19', '00100414', '0', '0'),
('20200219-0000291', '2020-02-19', '00100415', '120', '0'),
('20200219-0000292', '2020-02-19', '00100416', '0', '0'),
('20200219-0000293', '2020-02-19', '00100417', '0', '0'),
('20200219-0000294', '2020-02-19', '00100418', '600', '0'),
('20200219-0000295', '2020-02-19', '00100419', '97', '0'),
('20200219-0000296', '2020-02-19', '00100420', '100', '0'),
('20200219-0000297', '2020-02-19', '00100421', '0', '0'),
('20200219-0000298', '2020-02-19', '00100422', '0', '0'),
('20200219-0000299', '2020-02-19', '00100423', '60', '0'),
('20200219-0000300', '2020-02-19', '00100424', '1100', '0'),
('20200219-0000301', '2020-02-19', '00100425', '152', '0'),
('20200219-0000302', '2020-02-19', '00100426', '100', '0'),
('20200219-0000303', '2020-02-19', '00100427', '1966', '0'),
('20200219-0000304', '2020-02-19', '00100428', '100', '0'),
('20200219-0000305', '2020-02-19', '00100429', '60', '0'),
('20200219-0000306', '2020-02-19', '00100430', '0', '0'),
('20200219-0000307', '2020-02-19', '00100431', '40', '0'),
('20200219-0000308', '2020-02-19', '00100432', '98', '0'),
('20200219-0000309', '2020-02-19', '00100433', '40', '0'),
('20200219-0000310', '2020-02-19', '00100434', '0', '0'),
('20200219-0000311', '2020-02-19', '00100435', '0', '0'),
('20200219-0000312', '2020-02-19', '00100436', '0', '0'),
('20200219-0000313', '2020-02-19', '00100437', '0', '0'),
('20200219-0000314', '2020-02-19', '00100438', '0', '0'),
('20200219-0000315', '2020-02-19', '00100439', '0', '0'),
('20200219-0000316', '2020-02-19', '00100440', '0', '0'),
('20200219-0000317', '2020-02-19', '00100441', '50', '0'),
('20200219-0000318', '2020-02-19', '00100442', '465', '0'),
('20200219-0000319', '2020-02-19', '00100443', '200', '0'),
('20200219-0000320', '2020-02-19', '00100444', '50', '0'),
('20200219-0000321', '2020-02-19', '00100445', '455', '0'),
('20200219-0000322', '2020-02-19', '00100446', '0', '0'),
('20200219-0000323', '2020-02-19', '00100447', '0', '0'),
('20200219-0000324', '2020-02-19', '00100448', '283', '0'),
('20200219-0000325', '2020-02-19', '00100449', '300', '0'),
('20200219-0000326', '2020-02-19', '00100450', '120', '0'),
('20200219-0000327', '2020-02-19', '00100451', '0', '0'),
('20200219-0000328', '2020-02-19', '00100452', '0', '0'),
('20200219-0000329', '2020-02-19', '00100453', '50', '0'),
('20200219-0000330', '2020-02-19', '00100454', '924', '0'),
('20200219-0000331', '2020-02-19', '00100455', '50', '0'),
('20200219-0000332', '2020-02-19', '00100456', '50', '0'),
('20200219-0000333', '2020-02-19', '00100457', '100', '0'),
('20200219-0000334', '2020-02-19', '00100458', '0', '0'),
('20200219-0000335', '2020-02-19', '00100459', '0', '0'),
('20200219-0000336', '2020-02-19', '00100460', '200', '0'),
('20200219-0000337', '2020-02-19', '00100461', '0', '0'),
('20200219-0000338', '2020-02-19', '00100462', '0', '0'),
('20200219-0000339', '2020-02-19', '00100463', '100', '0'),
('20200219-0000340', '2020-02-19', '00100464', '0', '0'),
('20200219-0000341', '2020-02-19', '00100465', '200', '0'),
('20200219-0000342', '2020-02-19', '00100466', '1140', '0'),
('20200219-0000343', '2020-02-19', '00100467', '600', '0'),
('20200219-0000344', '2020-02-19', '00100468', '100', '0'),
('20200219-0000345', '2020-02-19', '00100469', '100', '0'),
('20200219-0000346', '2020-02-19', '00100470', '100', '0'),
('20200219-0000347', '2020-02-19', '00100471', '300', '0'),
('20200219-0000348', '2020-02-19', '00100472', '0', '0'),
('20200219-0000349', '2020-02-19', '00100473', '722', '0'),
('20200219-0000350', '2020-02-19', '00100474', '0', '0'),
('20200219-0000351', '2020-02-19', '00100475', '100', '0'),
('20200219-0000352', '2020-02-19', '00100476', '1104', '0'),
('20200219-0000353', '2020-02-19', '00100477', '100', '0'),
('20200219-0000354', '2020-02-19', '00100478', '349', '0'),
('20200219-0000355', '2020-02-19', '00100479', '0', '0'),
('20200219-0000356', '2020-02-19', '00100480', '0', '0'),
('20200219-0000357', '2020-02-19', '00100481', '100', '0'),
('20200219-0000358', '2020-02-19', '00100482', '523', '0'),
('20200219-0000359', '2020-02-19', '00100483', '0', '0'),
('20200219-0000360', '2020-02-19', '00100484', '0', '0'),
('20200219-0000361', '2020-02-19', '00100485', '0', '0'),
('20200219-0000362', '2020-02-19', '00100486', '0', '0'),
('20200219-0000363', '2020-02-19', '00100487', '0', '0'),
('20200219-0000364', '2020-02-19', '00100488', '0', '0'),
('20200219-0000365', '2020-02-19', '00100490', '0', '0'),
('20200219-0000366', '2020-02-19', '00100491', '0', '0'),
('20200219-0000367', '2020-02-19', '00100492', '20', '0'),
('20200219-0000368', '2020-02-19', '00100493', '20', '0'),
('20200219-0000369', '2020-02-19', '00100494', '100', '0'),
('20200219-0000370', '2020-02-19', '00100495', '6', '0'),
('20200219-0000371', '2020-02-19', '00100496', '0', '0'),
('20200219-0000372', '2020-02-19', '00100497', '370', '0'),
('20200219-0000373', '2020-02-19', '00100498', '7', '0'),
('20200219-0000374', '2020-02-19', '00100499', '0', '0'),
('20200219-0000375', '2020-02-19', '00100500', '1', '0'),
('20200219-0000376', '2020-02-19', '00100501', '0', '0'),
('20200219-0000377', '2020-02-19', '00100502', '0', '0'),
('20200219-0000378', '2020-02-19', '00100503', '0', '0'),
('20200219-0000379', '2020-02-19', '00100504', '20', '0'),
('20200219-0000380', '2020-02-19', '00100505', '40', '0'),
('20200219-0000381', '2020-02-19', '00100506', '0', '0'),
('20200219-0000382', '2020-02-19', '00100507', '10', '0'),
('20200219-0000383', '2020-02-19', '00100508', '560', '0'),
('20200219-0000384', '2020-02-19', '00100509', '100', '0'),
('20200219-0000385', '2020-02-19', '00100510', '400', '0'),
('20200219-0000386', '2020-02-19', '00100511', '1', '0'),
('20200219-0000387', '2020-02-19', '00100512', '1', '0'),
('20200219-0000388', '2020-02-19', '00100513', '1', '0'),
('20200219-0000389', '2020-02-19', '00100514', '1', '0'),
('20200219-0000390', '2020-02-19', '00100515', '2', '0'),
('20200219-0000391', '2020-02-19', '00100516', '10', '0'),
('20200219-0000392', '2020-02-19', '00100517', '0', '0'),
('20200219-0000393', '2020-02-19', '00100519', '0', '0'),
('20200219-0000394', '2020-02-19', '00100520', '0', '0'),
('20200220-0000001', '2020-02-20', '00100002', '4850', '0'),
('20200220-0000002', '2020-02-20', '00100004', '4850', '0'),
('20200220-0000003', '2020-02-20', '00100122', '1000', '0'),
('20200220-0000004', '2020-02-20', '00100126', '900', '50'),
('20200220-0000005', '2020-02-20', '00100127', '50', '0'),
('20200220-0000006', '2020-02-20', '00100128', '0', '0'),
('20200220-0000007', '2020-02-20', '00100129', '0', '0'),
('20200220-0000008', '2020-02-20', '00100130', '0', '0'),
('20200220-0000009', '2020-02-20', '00100131', '0', '0'),
('20200220-0000010', '2020-02-20', '00100132', '0', '0'),
('20200220-0000011', '2020-02-20', '00100133', '400', '0'),
('20200220-0000012', '2020-02-20', '00100134', '62', '0'),
('20200220-0000013', '2020-02-20', '00100135', '40', '0'),
('20200220-0000014', '2020-02-20', '00100136', '0', '0'),
('20200220-0000015', '2020-02-20', '00100137', '20', '0'),
('20200220-0000016', '2020-02-20', '00100138', '50', '0'),
('20200220-0000017', '2020-02-20', '00100139', '0', '0'),
('20200220-0000018', '2020-02-20', '00100140', '15', '0'),
('20200220-0000019', '2020-02-20', '00100141', '0', '0'),
('20200220-0000020', '2020-02-20', '00100142', '150', '0'),
('20200220-0000021', '2020-02-20', '00100143', '74', '0'),
('20200220-0000022', '2020-02-20', '00100144', '2', '0'),
('20200220-0000023', '2020-02-20', '00100145', '600', '0'),
('20200220-0000024', '2020-02-20', '00100146', '34', '0'),
('20200220-0000025', '2020-02-20', '00100147', '0', '0'),
('20200220-0000026', '2020-02-20', '00100148', '25', '0'),
('20200220-0000027', '2020-02-20', '00100149', '7', '0'),
('20200220-0000028', '2020-02-20', '00100150', '0', '0'),
('20200220-0000029', '2020-02-20', '00100151', '0', '0'),
('20200220-0000030', '2020-02-20', '00100152', '0', '0'),
('20200220-0000031', '2020-02-20', '00100153', '74', '0'),
('20200220-0000032', '2020-02-20', '00100154', '34', '0'),
('20200220-0000033', '2020-02-20', '00100155', '50', '0'),
('20200220-0000034', '2020-02-20', '00100156', '68', '0'),
('20200220-0000035', '2020-02-20', '00100157', '50', '0'),
('20200220-0000036', '2020-02-20', '00100158', '50', '0'),
('20200220-0000037', '2020-02-20', '00100159', '50', '0'),
('20200220-0000038', '2020-02-20', '00100160', '33', '0'),
('20200220-0000039', '2020-02-20', '00100161', '50', '0'),
('20200220-0000040', '2020-02-20', '00100162', '50', '0'),
('20200220-0000041', '2020-02-20', '00100163', '1', '0'),
('20200220-0000042', '2020-02-20', '00100164', '0', '0'),
('20200220-0000043', '2020-02-20', '00100165', '59', '0'),
('20200220-0000044', '2020-02-20', '00100166', '0', '0'),
('20200220-0000045', '2020-02-20', '00100167', '53', '0'),
('20200220-0000046', '2020-02-20', '00100168', '100', '0'),
('20200220-0000047', '2020-02-20', '00100169', '0', '0'),
('20200220-0000048', '2020-02-20', '00100170', '48', '0'),
('20200220-0000049', '2020-02-20', '00100171', '100', '0'),
('20200220-0000050', '2020-02-20', '00100172', '59', '0'),
('20200220-0000051', '2020-02-20', '00100173', '100', '0'),
('20200220-0000052', '2020-02-20', '00100174', '17', '0'),
('20200220-0000053', '2020-02-20', '00100175', '8', '0'),
('20200220-0000054', '2020-02-20', '00100176', '196', '0'),
('20200220-0000055', '2020-02-20', '00100177', '14', '0'),
('20200220-0000056', '2020-02-20', '00100178', '450', '0'),
('20200220-0000057', '2020-02-20', '00100179', '64', '0'),
('20200220-0000058', '2020-02-20', '00100180', '50', '0'),
('20200220-0000059', '2020-02-20', '00100181', '100', '0'),
('20200220-0000060', '2020-02-20', '00100182', '164', '0'),
('20200220-0000061', '2020-02-20', '00100183', '168', '0'),
('20200220-0000062', '2020-02-20', '00100184', '272', '0'),
('20200220-0000063', '2020-02-20', '00100185', '0', '0'),
('20200220-0000064', '2020-02-20', '00100186', '20', '0'),
('20200220-0000065', '2020-02-20', '00100187', '50', '0'),
('20200220-0000066', '2020-02-20', '00100188', '68', '0'),
('20200220-0000067', '2020-02-20', '00100189', '0', '0'),
('20200220-0000068', '2020-02-20', '00100190', '80', '0'),
('20200220-0000069', '2020-02-20', '00100191', '150', '0'),
('20200220-0000070', '2020-02-20', '00100192', '0', '0'),
('20200220-0000071', '2020-02-20', '00100193', '0', '0'),
('20200220-0000072', '2020-02-20', '00100194', '40', '0'),
('20200220-0000073', '2020-02-20', '00100195', '50', '0'),
('20200220-0000074', '2020-02-20', '00100196', '8', '0'),
('20200220-0000075', '2020-02-20', '00100197', '142', '0'),
('20200220-0000076', '2020-02-20', '00100198', '480', '0'),
('20200220-0000077', '2020-02-20', '00100199', '20', '0'),
('20200220-0000078', '2020-02-20', '00100200', '6', '0'),
('20200220-0000079', '2020-02-20', '00100201', '0', '0'),
('20200220-0000080', '2020-02-20', '00100202', '106', '0'),
('20200220-0000081', '2020-02-20', '00100203', '600', '0'),
('20200220-0000082', '2020-02-20', '00100204', '192', '0'),
('20200220-0000083', '2020-02-20', '00100205', '40', '0'),
('20200220-0000084', '2020-02-20', '00100206', '16', '0'),
('20200220-0000085', '2020-02-20', '00100207', '80', '0'),
('20200220-0000086', '2020-02-20', '00100208', '39', '0'),
('20200220-0000087', '2020-02-20', '00100209', '0', '0'),
('20200220-0000088', '2020-02-20', '00100210', '600', '0'),
('20200220-0000089', '2020-02-20', '00100211', '0', '0'),
('20200220-0000090', '2020-02-20', '00100212', '210', '0'),
('20200220-0000091', '2020-02-20', '00100213', '0', '0'),
('20200220-0000092', '2020-02-20', '00100214', '450', '0'),
('20200220-0000093', '2020-02-20', '00100215', '35', '0'),
('20200220-0000094', '2020-02-20', '00100216', '0', '0'),
('20200220-0000095', '2020-02-20', '00100217', '52', '0'),
('20200220-0000096', '2020-02-20', '00100218', '0', '0'),
('20200220-0000097', '2020-02-20', '00100219', '152', '0'),
('20200220-0000098', '2020-02-20', '00100220', '600', '0'),
('20200220-0000099', '2020-02-20', '00100221', '0', '0'),
('20200220-0000100', '2020-02-20', '00100222', '0', '0'),
('20200220-0000101', '2020-02-20', '00100223', '0', '0'),
('20200220-0000102', '2020-02-20', '00100224', '0', '0'),
('20200220-0000103', '2020-02-20', '00100225', '32', '0'),
('20200220-0000104', '2020-02-20', '00100226', '200', '0'),
('20200220-0000105', '2020-02-20', '00100227', '0', '0'),
('20200220-0000106', '2020-02-20', '00100228', '20', '0'),
('20200220-0000107', '2020-02-20', '00100229', '98', '0'),
('20200220-0000108', '2020-02-20', '00100230', '400', '0'),
('20200220-0000109', '2020-02-20', '00100231', '160', '0'),
('20200220-0000110', '2020-02-20', '00100232', '0', '0'),
('20200220-0000111', '2020-02-20', '00100233', '20', '0'),
('20200220-0000112', '2020-02-20', '00100234', '1', '0'),
('20200220-0000113', '2020-02-20', '00100235', '330', '0'),
('20200220-0000114', '2020-02-20', '00100236', '6', '0'),
('20200220-0000115', '2020-02-20', '00100237', '0', '0'),
('20200220-0000116', '2020-02-20', '00100238', '356', '0'),
('20200220-0000117', '2020-02-20', '00100239', '600', '0'),
('20200220-0000118', '2020-02-20', '00100240', '823', '0'),
('20200220-0000119', '2020-02-20', '00100241', '58', '0'),
('20200220-0000120', '2020-02-20', '00100242', '50', '0'),
('20200220-0000121', '2020-02-20', '00100243', '490', '0'),
('20200220-0000122', '2020-02-20', '00100244', '2', '0'),
('20200220-0000123', '2020-02-20', '00100245', '2', '0'),
('20200220-0000124', '2020-02-20', '00100246', '374', '0'),
('20200220-0000125', '2020-02-20', '00100247', '0', '0'),
('20200220-0000126', '2020-02-20', '00100248', '200', '0'),
('20200220-0000127', '2020-02-20', '00100249', '60', '0'),
('20200220-0000128', '2020-02-20', '00100250', '0', '0'),
('20200220-0000129', '2020-02-20', '00100251', '74', '0'),
('20200220-0000130', '2020-02-20', '00100252', '233', '0'),
('20200220-0000131', '2020-02-20', '00100253', '257', '0'),
('20200220-0000132', '2020-02-20', '00100254', '100', '0'),
('20200220-0000133', '2020-02-20', '00100255', '95', '0'),
('20200220-0000134', '2020-02-20', '00100256', '1', '0'),
('20200220-0000135', '2020-02-20', '00100257', '0', '0'),
('20200220-0000136', '2020-02-20', '00100258', '565', '0'),
('20200220-0000137', '2020-02-20', '00100259', '199', '0'),
('20200220-0000138', '2020-02-20', '00100260', '218', '0'),
('20200220-0000139', '2020-02-20', '00100261', '100', '0'),
('20200220-0000140', '2020-02-20', '00100262', '72', '0'),
('20200220-0000141', '2020-02-20', '00100263', '50', '0'),
('20200220-0000142', '2020-02-20', '00100264', '95', '0'),
('20200220-0000143', '2020-02-20', '00100265', '177', '0'),
('20200220-0000144', '2020-02-20', '00100266', '250', '0'),
('20200220-0000145', '2020-02-20', '00100267', '113', '0'),
('20200220-0000146', '2020-02-20', '00100268', '200', '0'),
('20200220-0000147', '2020-02-20', '00100269', '131', '0'),
('20200220-0000148', '2020-02-20', '00100270', '255', '0'),
('20200220-0000149', '2020-02-20', '00100271', '437', '0'),
('20200220-0000150', '2020-02-20', '00100272', '528', '0'),
('20200220-0000151', '2020-02-20', '00100273', '0', '0'),
('20200220-0000152', '2020-02-20', '00100274', '0', '0'),
('20200220-0000153', '2020-02-20', '00100275', '1', '0'),
('20200220-0000154', '2020-02-20', '00100276', '0', '0'),
('20200220-0000155', '2020-02-20', '00100277', '36', '0'),
('20200220-0000156', '2020-02-20', '00100278', '156', '0'),
('20200220-0000157', '2020-02-20', '00100279', '3', '0'),
('20200220-0000158', '2020-02-20', '00100280', '80', '0'),
('20200220-0000159', '2020-02-20', '00100281', '13', '0'),
('20200220-0000160', '2020-02-20', '00100282', '1', '0'),
('20200220-0000161', '2020-02-20', '00100283', '320', '0'),
('20200220-0000162', '2020-02-20', '00100284', '115', '0'),
('20200220-0000163', '2020-02-20', '00100285', '84', '0'),
('20200220-0000164', '2020-02-20', '00100286', '40', '0'),
('20200220-0000165', '2020-02-20', '00100287', '389', '0'),
('20200220-0000166', '2020-02-20', '00100288', '0', '0'),
('20200220-0000167', '2020-02-20', '00100289', '52', '0'),
('20200220-0000168', '2020-02-20', '00100290', '2', '0'),
('20200220-0000169', '2020-02-20', '00100291', '374', '0'),
('20200220-0000170', '2020-02-20', '00100292', '2', '0'),
('20200220-0000171', '2020-02-20', '00100293', '0', '0'),
('20200220-0000172', '2020-02-20', '00100294', '97', '0'),
('20200220-0000173', '2020-02-20', '00100295', '5', '0'),
('20200220-0000174', '2020-02-20', '00100296', '0', '0'),
('20200220-0000175', '2020-02-20', '00100297', '20', '0'),
('20200220-0000176', '2020-02-20', '00100298', '140', '0'),
('20200220-0000177', '2020-02-20', '00100299', '90', '0'),
('20200220-0000178', '2020-02-20', '00100300', '1', '0'),
('20200220-0000179', '2020-02-20', '00100301', '7', '0'),
('20200220-0000180', '2020-02-20', '00100302', '600', '0'),
('20200220-0000181', '2020-02-20', '00100303', '210', '0'),
('20200220-0000182', '2020-02-20', '00100304', '28', '0'),
('20200220-0000183', '2020-02-20', '00100305', '5', '0'),
('20200220-0000184', '2020-02-20', '00100306', '112', '0'),
('20200220-0000185', '2020-02-20', '00100307', '109', '0'),
('20200220-0000186', '2020-02-20', '00100308', '140', '0'),
('20200220-0000187', '2020-02-20', '00100309', '110', '0'),
('20200220-0000188', '2020-02-20', '00100310', '0', '0'),
('20200220-0000189', '2020-02-20', '00100311', '1', '0'),
('20200220-0000190', '2020-02-20', '00100312', '51', '0'),
('20200220-0000191', '2020-02-20', '00100314', '10', '0'),
('20200220-0000192', '2020-02-20', '00100315', '2', '0'),
('20200220-0000193', '2020-02-20', '00100316', '1', '0'),
('20200220-0000194', '2020-02-20', '00100317', '154', '0'),
('20200220-0000195', '2020-02-20', '00100318', '318', '0'),
('20200220-0000196', '2020-02-20', '00100319', '192', '0'),
('20200220-0000197', '2020-02-20', '00100320', '60', '0'),
('20200220-0000198', '2020-02-20', '00100321', '185', '0'),
('20200220-0000199', '2020-02-20', '00100322', '9', '0'),
('20200220-0000200', '2020-02-20', '00100323', '23', '0'),
('20200220-0000201', '2020-02-20', '00100324', '58', '0'),
('20200220-0000202', '2020-02-20', '00100325', '35', '0'),
('20200220-0000203', '2020-02-20', '00100326', '50', '0'),
('20200220-0000204', '2020-02-20', '00100327', '5', '0'),
('20200220-0000205', '2020-02-20', '00100328', '6', '0'),
('20200220-0000206', '2020-02-20', '00100329', '50', '0'),
('20200220-0000207', '2020-02-20', '00100330', '20', '0'),
('20200220-0000208', '2020-02-20', '00100331', '9', '0'),
('20200220-0000209', '2020-02-20', '00100332', '0', '0'),
('20200220-0000210', '2020-02-20', '00100333', '42', '0'),
('20200220-0000211', '2020-02-20', '00100334', '1', '0'),
('20200220-0000212', '2020-02-20', '00100335', '16', '0'),
('20200220-0000213', '2020-02-20', '00100336', '20', '0'),
('20200220-0000214', '2020-02-20', '00100337', '1', '0'),
('20200220-0000215', '2020-02-20', '00100338', '20', '0'),
('20200220-0000216', '2020-02-20', '00100339', '0', '0'),
('20200220-0000217', '2020-02-20', '00100340', '10', '0'),
('20200220-0000218', '2020-02-20', '00100341', '2', '0'),
('20200220-0000219', '2020-02-20', '00100342', '72', '0'),
('20200220-0000220', '2020-02-20', '00100343', '5', '0'),
('20200220-0000221', '2020-02-20', '00100344', '20', '0'),
('20200220-0000222', '2020-02-20', '00100345', '15', '0'),
('20200220-0000223', '2020-02-20', '00100346', '3', '0'),
('20200220-0000224', '2020-02-20', '00100347', '20', '0'),
('20200220-0000225', '2020-02-20', '00100348', '1', '0'),
('20200220-0000226', '2020-02-20', '00100349', '4', '0'),
('20200220-0000227', '2020-02-20', '00100350', '20', '0'),
('20200220-0000228', '2020-02-20', '00100351', '20', '0'),
('20200220-0000229', '2020-02-20', '00100352', '0', '0'),
('20200220-0000230', '2020-02-20', '00100353', '22', '0'),
('20200220-0000231', '2020-02-20', '00100354', '1', '0'),
('20200220-0000232', '2020-02-20', '00100355', '172', '0'),
('20200220-0000233', '2020-02-20', '00100356', '50', '0'),
('20200220-0000234', '2020-02-20', '00100357', '60', '0'),
('20200220-0000235', '2020-02-20', '00100358', '140', '0'),
('20200220-0000236', '2020-02-20', '00100359', '0', '0'),
('20200220-0000237', '2020-02-20', '00100360', '178', '0'),
('20200220-0000238', '2020-02-20', '00100361', '0', '0'),
('20200220-0000239', '2020-02-20', '00100362', '33', '0'),
('20200220-0000240', '2020-02-20', '00100363', '137', '0'),
('20200220-0000241', '2020-02-20', '00100365', '59', '0'),
('20200220-0000242', '2020-02-20', '00100366', '3', '0'),
('20200220-0000243', '2020-02-20', '00100367', '200', '0'),
('20200220-0000244', '2020-02-20', '00100368', '64', '0'),
('20200220-0000245', '2020-02-20', '00100369', '0', '0'),
('20200220-0000246', '2020-02-20', '00100370', '0', '0'),
('20200220-0000247', '2020-02-20', '00100371', '60', '0'),
('20200220-0000248', '2020-02-20', '00100372', '123', '0'),
('20200220-0000249', '2020-02-20', '00100373', '169', '0'),
('20200220-0000250', '2020-02-20', '00100374', '53', '0'),
('20200220-0000251', '2020-02-20', '00100375', '22', '0'),
('20200220-0000252', '2020-02-20', '00100376', '65', '0'),
('20200220-0000253', '2020-02-20', '00100377', '1', '0'),
('20200220-0000254', '2020-02-20', '00100378', '1200', '0'),
('20200220-0000255', '2020-02-20', '00100379', '0', '0'),
('20200220-0000256', '2020-02-20', '00100380', '350', '0'),
('20200220-0000257', '2020-02-20', '00100381', '197', '0'),
('20200220-0000258', '2020-02-20', '00100382', '0', '0'),
('20200220-0000259', '2020-02-20', '00100383', '0', '0'),
('20200220-0000260', '2020-02-20', '00100384', '140', '0'),
('20200220-0000261', '2020-02-20', '00100385', '0', '0'),
('20200220-0000262', '2020-02-20', '00100386', '185', '0'),
('20200220-0000263', '2020-02-20', '00100387', '16', '0'),
('20200220-0000264', '2020-02-20', '00100388', '49', '0'),
('20200220-0000265', '2020-02-20', '00100389', '1', '0'),
('20200220-0000266', '2020-02-20', '00100390', '50', '0'),
('20200220-0000267', '2020-02-20', '00100391', '25', '0'),
('20200220-0000268', '2020-02-20', '00100392', '1', '0'),
('20200220-0000269', '2020-02-20', '00100393', '1', '0'),
('20200220-0000270', '2020-02-20', '00100394', '12', '0'),
('20200220-0000271', '2020-02-20', '00100395', '29', '0'),
('20200220-0000272', '2020-02-20', '00100396', '20', '0'),
('20200220-0000273', '2020-02-20', '00100397', '2', '0'),
('20200220-0000274', '2020-02-20', '00100398', '0', '0'),
('20200220-0000275', '2020-02-20', '00100399', '0', '0'),
('20200220-0000276', '2020-02-20', '00100400', '123', '0'),
('20200220-0000277', '2020-02-20', '00100401', '0', '0'),
('20200220-0000278', '2020-02-20', '00100402', '2660', '0'),
('20200220-0000279', '2020-02-20', '00100403', '31', '0'),
('20200220-0000280', '2020-02-20', '00100404', '0', '0'),
('20200220-0000281', '2020-02-20', '00100405', '653', '0'),
('20200220-0000282', '2020-02-20', '00100406', '200', '0'),
('20200220-0000283', '2020-02-20', '00100407', '600', '0'),
('20200220-0000284', '2020-02-20', '00100408', '60', '0'),
('20200220-0000285', '2020-02-20', '00100409', '0', '0'),
('20200220-0000286', '2020-02-20', '00100410', '0', '0'),
('20200220-0000287', '2020-02-20', '00100411', '0', '0'),
('20200220-0000288', '2020-02-20', '00100412', '500', '0'),
('20200220-0000289', '2020-02-20', '00100413', '676', '0'),
('20200220-0000290', '2020-02-20', '00100414', '0', '0'),
('20200220-0000291', '2020-02-20', '00100415', '120', '0'),
('20200220-0000292', '2020-02-20', '00100416', '0', '0'),
('20200220-0000293', '2020-02-20', '00100417', '0', '0'),
('20200220-0000294', '2020-02-20', '00100418', '600', '0'),
('20200220-0000295', '2020-02-20', '00100419', '97', '0'),
('20200220-0000296', '2020-02-20', '00100420', '100', '0'),
('20200220-0000297', '2020-02-20', '00100421', '0', '0'),
('20200220-0000298', '2020-02-20', '00100422', '0', '0'),
('20200220-0000299', '2020-02-20', '00100423', '60', '0'),
('20200220-0000300', '2020-02-20', '00100424', '1100', '0'),
('20200220-0000301', '2020-02-20', '00100425', '152', '0'),
('20200220-0000302', '2020-02-20', '00100426', '100', '0'),
('20200220-0000303', '2020-02-20', '00100427', '1966', '0'),
('20200220-0000304', '2020-02-20', '00100428', '100', '0'),
('20200220-0000305', '2020-02-20', '00100429', '60', '0'),
('20200220-0000306', '2020-02-20', '00100430', '0', '0'),
('20200220-0000307', '2020-02-20', '00100431', '40', '0'),
('20200220-0000308', '2020-02-20', '00100432', '98', '0'),
('20200220-0000309', '2020-02-20', '00100433', '40', '0'),
('20200220-0000310', '2020-02-20', '00100434', '0', '0'),
('20200220-0000311', '2020-02-20', '00100435', '0', '0'),
('20200220-0000312', '2020-02-20', '00100436', '0', '0'),
('20200220-0000313', '2020-02-20', '00100437', '0', '0'),
('20200220-0000314', '2020-02-20', '00100438', '0', '0'),
('20200220-0000315', '2020-02-20', '00100439', '0', '0'),
('20200220-0000316', '2020-02-20', '00100440', '0', '0'),
('20200220-0000317', '2020-02-20', '00100441', '50', '0'),
('20200220-0000318', '2020-02-20', '00100442', '465', '0'),
('20200220-0000319', '2020-02-20', '00100443', '200', '0'),
('20200220-0000320', '2020-02-20', '00100444', '50', '0'),
('20200220-0000321', '2020-02-20', '00100445', '455', '0'),
('20200220-0000322', '2020-02-20', '00100446', '0', '0'),
('20200220-0000323', '2020-02-20', '00100447', '0', '0'),
('20200220-0000324', '2020-02-20', '00100448', '283', '0'),
('20200220-0000325', '2020-02-20', '00100449', '300', '0'),
('20200220-0000326', '2020-02-20', '00100450', '120', '0'),
('20200220-0000327', '2020-02-20', '00100451', '0', '0'),
('20200220-0000328', '2020-02-20', '00100452', '0', '0'),
('20200220-0000329', '2020-02-20', '00100453', '50', '0'),
('20200220-0000330', '2020-02-20', '00100454', '924', '0'),
('20200220-0000331', '2020-02-20', '00100455', '50', '0'),
('20200220-0000332', '2020-02-20', '00100456', '50', '0'),
('20200220-0000333', '2020-02-20', '00100457', '100', '0'),
('20200220-0000334', '2020-02-20', '00100458', '0', '0'),
('20200220-0000335', '2020-02-20', '00100459', '0', '0'),
('20200220-0000336', '2020-02-20', '00100460', '200', '0'),
('20200220-0000337', '2020-02-20', '00100461', '0', '0'),
('20200220-0000338', '2020-02-20', '00100462', '0', '0'),
('20200220-0000339', '2020-02-20', '00100463', '100', '0'),
('20200220-0000340', '2020-02-20', '00100464', '0', '0'),
('20200220-0000341', '2020-02-20', '00100465', '200', '0'),
('20200220-0000342', '2020-02-20', '00100466', '1140', '0'),
('20200220-0000343', '2020-02-20', '00100467', '600', '0'),
('20200220-0000344', '2020-02-20', '00100468', '100', '0'),
('20200220-0000345', '2020-02-20', '00100469', '100', '0'),
('20200220-0000346', '2020-02-20', '00100470', '100', '0'),
('20200220-0000347', '2020-02-20', '00100471', '300', '0'),
('20200220-0000348', '2020-02-20', '00100472', '0', '0'),
('20200220-0000349', '2020-02-20', '00100473', '722', '0'),
('20200220-0000350', '2020-02-20', '00100474', '0', '0'),
('20200220-0000351', '2020-02-20', '00100475', '100', '0'),
('20200220-0000352', '2020-02-20', '00100476', '1104', '0'),
('20200220-0000353', '2020-02-20', '00100477', '100', '0'),
('20200220-0000354', '2020-02-20', '00100478', '349', '0'),
('20200220-0000355', '2020-02-20', '00100479', '0', '0'),
('20200220-0000356', '2020-02-20', '00100480', '0', '0'),
('20200220-0000357', '2020-02-20', '00100481', '100', '0'),
('20200220-0000358', '2020-02-20', '00100482', '523', '0'),
('20200220-0000359', '2020-02-20', '00100483', '0', '0'),
('20200220-0000360', '2020-02-20', '00100484', '0', '0'),
('20200220-0000361', '2020-02-20', '00100485', '0', '0'),
('20200220-0000362', '2020-02-20', '00100486', '0', '0'),
('20200220-0000363', '2020-02-20', '00100487', '0', '0'),
('20200220-0000364', '2020-02-20', '00100488', '0', '0'),
('20200220-0000365', '2020-02-20', '00100490', '0', '0'),
('20200220-0000366', '2020-02-20', '00100491', '0', '0'),
('20200220-0000367', '2020-02-20', '00100492', '20', '0'),
('20200220-0000368', '2020-02-20', '00100493', '20', '0'),
('20200220-0000369', '2020-02-20', '00100494', '100', '0'),
('20200220-0000370', '2020-02-20', '00100495', '6', '0'),
('20200220-0000371', '2020-02-20', '00100496', '0', '0'),
('20200220-0000372', '2020-02-20', '00100497', '370', '0'),
('20200220-0000373', '2020-02-20', '00100498', '7', '0'),
('20200220-0000374', '2020-02-20', '00100499', '0', '0'),
('20200220-0000375', '2020-02-20', '00100500', '1', '0'),
('20200220-0000376', '2020-02-20', '00100501', '0', '0'),
('20200220-0000377', '2020-02-20', '00100502', '0', '0'),
('20200220-0000378', '2020-02-20', '00100503', '0', '0'),
('20200220-0000379', '2020-02-20', '00100504', '20', '0'),
('20200220-0000380', '2020-02-20', '00100505', '40', '0'),
('20200220-0000381', '2020-02-20', '00100506', '0', '0'),
('20200220-0000382', '2020-02-20', '00100507', '10', '0'),
('20200220-0000383', '2020-02-20', '00100508', '560', '0'),
('20200220-0000384', '2020-02-20', '00100509', '100', '0'),
('20200220-0000385', '2020-02-20', '00100510', '400', '0'),
('20200220-0000386', '2020-02-20', '00100511', '1', '0'),
('20200220-0000387', '2020-02-20', '00100512', '1', '0'),
('20200220-0000388', '2020-02-20', '00100513', '1', '0'),
('20200220-0000389', '2020-02-20', '00100514', '1', '0'),
('20200220-0000390', '2020-02-20', '00100515', '2', '0'),
('20200220-0000391', '2020-02-20', '00100516', '10', '0'),
('20200220-0000392', '2020-02-20', '00100517', '0', '0'),
('20200220-0000393', '2020-02-20', '00100519', '0', '0'),
('20200220-0000394', '2020-02-20', '00100520', '0', '0');

-- --------------------------------------------------------

--
-- Table structure for table `record_stok`
--

CREATE TABLE `record_stok` (
  `id_record` varchar(15) NOT NULL,
  `id_tr_detail` varchar(19) NOT NULL,
  `id_item` varchar(10) NOT NULL,
  `tgl_insert` date NOT NULL,
  `keterangan_record_stok` text NOT NULL,
  `jenis_record_stok` varchar(64) NOT NULL,
  `status_record_stok` varchar(64) NOT NULL,
  `stok_awal_record_stok` int(11) NOT NULL,
  `stok_tr_record_stok_before` int(11) NOT NULL,
  `stok_tr_record_stok` int(11) NOT NULL,
  `stok_akhir_record_stok` int(11) NOT NULL,
  `admin_create_record_stok` varchar(12) NOT NULL,
  `time_up_record_stok` datetime NOT NULL,
  `is_del_record_stok` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `record_stok_opnam`
--

CREATE TABLE `record_stok_opnam` (
  `id_record` varchar(15) NOT NULL,
  `id_tr_detail` varchar(19) NOT NULL,
  `id_item` varchar(10) NOT NULL,
  `tgl_insert` date NOT NULL,
  `keterangan_record_stok` text NOT NULL,
  `jenis_record_stok` varchar(64) NOT NULL,
  `status_record_stok` varchar(64) NOT NULL,
  `stok_awal_record_stok` int(11) NOT NULL,
  `stok_tr_record_stok_before` int(11) NOT NULL,
  `stok_tr_record_stok` int(11) NOT NULL,
  `stok_akhir_record_stok` int(11) NOT NULL,
  `admin_create_record_stok` varchar(12) NOT NULL,
  `time_up_record_stok` datetime NOT NULL,
  `is_del_record_stok` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rekanan`
--

CREATE TABLE `rekanan` (
  `id_rekanan` varchar(13) NOT NULL,
  `nama_rekanan` text NOT NULL,
  `jenis_rekanan` text NOT NULL,
  `email_rekanan` text NOT NULL,
  `tlp_rekanan` varchar(13) NOT NULL,
  `alamat_ktr_rekanan` text NOT NULL,
  `alamat_krm_rekanan` text NOT NULL,
  `website_rekanan` text NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `time_update` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rekanan`
--

INSERT INTO `rekanan` (`id_rekanan`, `nama_rekanan`, `jenis_rekanan`, `email_rekanan`, `tlp_rekanan`, `alamat_ktr_rekanan`, `alamat_krm_rekanan`, `website_rekanan`, `is_delete`, `time_update`, `id_admin`) VALUES
('RKA2020010001', 'AP. ABDI MULIA', 'apotik', 'ABDIMULIA@YAHOO.COM', '8522222222', 'JL. HR. MOHAMMAD KAV 401-403 PUTAT GEDE SURABAYA', 'JL. HR MOHAMMAD KAV 401-403 PUTAT GEDE SURABAYA', 'ABDIMULIA.COM', '0', '2020-01-02 09:47:46', ''),
('RKA2020010002', 'AP. BHUMYAMCA II', 'apotik', 'bHUMMYAMCA@YAHOO.COM', '1111111111111', 'JL. TAMBAKREJO NO. 114 SURABAYA', 'JL. TAMBAKREJO NO. 114 SURABAYA', 'BHUMYAMCA.COM', '0', '2020-01-02 09:53:47', ''),
('RKA2020010003', 'AP. CITRA MANDIRI', 'apotik', 'CITRAMANDIRI@YAHOO.COM', '2222222222222', 'JL. GRIYA KEBRAON TENGAH L-8 SURABAYA', 'JL. GRIYA KEBRAON TENGAH L-8 SURABAYA', 'CITRAMANDIRI.COM', '0', '2020-01-02 09:55:23', ''),
('RKA2020010004', 'AP. CHE CHE ', 'apotik', 'CHECHE@YAHOO.COM', '1256446322566', 'JL. MENGANTI LIDAH WETAN NO. 33 SURABAYA', 'JL. MENGANTI LIDAH WETAN NO. 33 SURABAYA', 'CHECHE.COM', '0', '2020-01-02 09:57:08', ''),
('RKA2020010005', 'AP. CAHAYA INDAH FARMA', 'apotik', 'CAHAYAINDAH24@GMAIL.COM', '56875552668', 'JL. RAYA ARJUNO NO. 2-C SURABAYA', 'JL. RAYA ARJUNO NO. 2-C SURABAYA', 'CAHAYA.COM', '0', '2020-01-02 10:01:05', ''),
('RKA2020010006', 'AP. CRYSTAL', 'apotik', 'CRYSTAL@YAHOO.COM', '8745213452552', 'JL. KLAMPIS JAYA 3 (A-3) SURABAYA', 'JL. KLAMPIS JAYA 3 (A-3) SURABAYA', 'CRYSTAL.COM', '0', '2020-01-02 10:03:31', ''),
('RKA2020010007', 'AP. CONFI D', 'apotik', 'CONFID@YAHOO.COM', '465262233442', 'JL. TERATAI 38 SOOKO MOJOKERTO', 'JL. TERATAI 38 SOOKO MOJOKERTO', 'CONFID.COM', '0', '2020-01-02 10:05:56', ''),
('RKA2020010008', 'AP. GRACIA', 'apotik', 'GRACIA@YAHOO.COM', '5648702335126', 'JL. RUNGKUT LOR RL-II/24 SURABAYA', 'JL. RUNGKUT LOR RL-II/24 SURABAYA', 'GRACIA.COM', '0', '2020-01-02 10:08:14', ''),
('RKA2020010009', 'AP. KENCANA MEDIKA', 'apotik', 'KENCANA@GMAIL.COM', '462531226623', 'JL. KENDANGSARI NO. 78 SURABAYA', 'JL. KENDANGSARI NO. 78 SURABAYA', 'KENCANA.COM', '0', '2020-01-02 10:09:38', ''),
('RKA2020010010', 'AP. LANCAR', 'apotik', 'LANCAR@GMAIL.COM', '6524621345233', 'JL. SIMPANG DARMO PERMAI UTARA NO. 29 SURABAYA', 'JL. SIMPANG DARMO PERMAI UTARA NO. 29 SURABAYA', 'LANCAR.COM', '0', '2020-01-02 10:10:54', ''),
('RKA2020010011', 'AP. ELLY', 'apotik', 'ELLY@YAHOO.COM', '56475061523', 'JL. RUNGKUT KIDUL INDUSTRI NO.52 SURABAYA', 'JL. RUNGKUT KIDUL INDUSTRI NO. 52 SURABAYA', 'ELLY.COM', '0', '2020-01-02 10:20:08', ''),
('RKA2020010012', 'AP. HUTAMA PHARMA', 'apotik', 'HUTAMA@GMAIL.COM', '5648752122552', 'JL. GAJAH MADA NO. 35 SIDOARJO', 'JL. GAJAH MADA NO. 35 SIDOARJO', 'HUTAMA.COM', '0', '2020-01-02 10:21:28', ''),
('RKA2020010013', 'AP. LIBRA ', 'apotik', 'LIBRA@YAHOO.COM', '548725622342', 'JL. ARIEF RAHMAN HAKIM 67 SURABAYA', 'JL. ARIEF RAHMAN HAKIM 67 SURABAYA', 'LIBRA.COM', '0', '2020-01-02 10:25:23', ''),
('RKA2020010014', 'AP. BETHESDA', 'apotik', 'BETHESDA@GMAIL.COM', '4652872211225', 'JL. KAPUAS NO. 49 SURABAYA', 'JL. KAPUAS NO. 49 SURABAYA', 'BETHESDA.COM', '0', '2020-01-02 10:47:20', ''),
('RKA2020010015', 'AP. DHARMAHUSADA', 'apotik', 'DHARMAHUSADA@YAHOO.COM', '8954621312422', 'JL. DHARMAHUSADA NO. 176 SURABAYA', 'JL. DHARMAHUSADA NP. 176 SURABAYA', 'DHARMAHUSADA.COM', '0', '2020-01-02 10:50:33', ''),
('RKA2020010016', 'AP. IBUNDA ', 'pbf', 'IBUNDA@YAHOO.COM', '652435252523', 'JL. KH. MANSYUR NO. 201 SURABAYA', 'JL. KH. MANSYUR NO. 201 SURABAYA', 'IBUNDA.COM', '0', '2020-01-02 11:00:36', ''),
('RKA2020010017', 'AP. LIDAH FARMA', 'apotik', 'LIDAHFARMA@YAHOO.COM', '985622422525', 'JL. MENGANTI LIDAH KULON NO. 33 SURABAYA', 'JL. MENGANTI LIDAH KULON NO. 33 SURABAYA', 'LIDAH.COM', '0', '2020-01-02 11:02:44', ''),
('RKA2020010018', 'AP. PRAPEN INDAH FARMA', 'apotik', 'PRAPENINDAH@YAHOO.COM', '652436525362', 'JL. PRAPEN INDAH II/B-6 SURABAYA', 'JL. PRAPEN INDAH II/B-6 SURABAYA', 'PRAPEN.COM', '0', '2020-01-02 11:04:19', ''),
('RKA2020010019', 'AP. RINDANG FARMA', 'apotik', 'RINDANG@YAHOO.COM', '654525252542', 'JL. MENANGGAL II/5 SURABAYA', 'JL. MENANGGAL II/5 SURABAYA', 'RINDANG.COM', '0', '2020-01-02 11:07:18', ''),
('RKA2020010020', 'AP. GARUDA', 'apotik', 'GARUDA@YAHOO.COM', '56428733415', 'JL. NGAGEL JAYA TENGAN IV/32 SURABAYA', 'JL. NGAGEL JAYA TENGAH IV/32 SURABAYA', 'GARUDA.COM', '0', '2020-01-02 11:08:44', ''),
('RKA2020010021', 'PT. GRAHA MUKTI HUSADA / KL. MATA JAVA', 'kl', '-', '-', 'JL. RAYA DARMO NO. 127 SURABAYA', '-', '-', '0', '2020-02-06 03:58:53', ''),
('RKA2020010022', 'AP. ADAM FARMA', 'apotik', 'ADAM@YAHOO.COM', '412356555503', 'JL. PANJANG JIWO IV/18 SURABAYA', 'JL. PANJANG JIWO IV/18 SURABAYA', 'ADAM.COM', '0', '2020-01-02 11:12:48', ''),
('RKA2020010023', 'AP. DAYA FARMA', 'apotik', 'DAYA@GMAIL.COM', '698751225622', 'JL. PUSPOWARNO NO 29 MADIUN', 'JL. PUSPOWARNO NO. 29 MADIUN', 'DAYA.COM', '0', '2020-01-02 11:14:30', ''),
('RKA2020010024', 'AP. CENDANA MITRA FARMA', 'apotik', 'CENDANA@YAHOO.COM', '652456212522', 'JL. UNTUNG SUROPATI NO 41 BOJONEGORO', 'JL. UNTUNG SUROPATI NO. 41 BOJONEGORO', 'CENDANA.COM', '0', '2020-01-02 11:16:57', ''),
('RKA2020010025', 'CV. SETYA DHARMA', 'pbf', 'SETYA@YAHOO.COM', '365246244526', 'JL. JENDERAL SUDIRMAN NO. 011 TARAKAN', 'JL. JENDERAL SUDIRMAN NO. 011 TARAKAN', 'SETYA.COM', '0', '2020-01-02 11:18:42', ''),
('RKA2020010026', 'KLINIK ZYDAN MEDIKA', 'kl', 'ZYDAN@GMAIL.COM', '56245145785', 'JL. BANDUNG SUWARU NO. 6 BANDUNG TULUNGAGUNG', 'JL. BANDUNG SUARU NO. 6 BANDUNG TULUNGAGUNG', 'ZYDAN.COM', '0', '2020-01-02 11:20:37', ''),
('RKA2020010027', 'KLINIK CITRA HUSADA', 'kl', 'CITRAHUSADA@YAHOO.COM', '231652452524', 'PERUM. CITRA GARDEN CLUSTER MELROSE PLACE KAV A/1-3 SIDOARJO', 'PERUM. CITRA GARDEN CLUSTER MELROSE PLACE KAV A/1-3 SIDOARJO', 'CITRA.COM', '0', '2020-01-02 11:22:39', ''),
('RKA2020010028', 'KLINIK MATA EDC ', 'kl', 'EDC@YAHOO.COM', '4652156257222', 'JL. KALIJATEN NO. 71-73 SEPANJANG SIDOARJO', 'JL. KALIJATEN NO. 71-73 SEPANJANG SIDOARJO', 'EDC.COM', '0', '2020-01-02 11:24:52', ''),
('RKA2020010029', 'KLINIK MATA EDC WARUJAYENG', 'kl', 'EDCWARU@YAHOO.COM', '56756245252', 'JL. A.YANI KEC TANJUNGANOM KAB. NGANJUK', 'JL. A.YANI KEC TANJUNGANOM KAB. NGANJUK', 'EDC.COM', '0', '2020-01-02 11:27:19', ''),
('RKA2020010030', 'abcd', 'apotik', '-@gmal.com', '031123456789', '-', '-', '-', '1', '2020-01-06 12:59:32', ''),
('RKA2020010031', 'PERS. PERKUMPULAN ADI HUSADA', 'rs', '123@GMAIL.COM', '0311234562456', 'JL. UNDAAN WETAN 40-44, SURABAYA', '-', '-', '0', '2020-01-13 11:21:00', ''),
('RKA2020010032', 'AP. AMELIA', 'apotik', '1234@GMAIL.COM', '1234567890', 'JL. PUCANG ANOM TIMUR 16, SURABAYA', '-', '-', '0', '2020-01-13 11:22:15', ''),
('RKA2020010033', 'AP. AGUNG FARMA', 'apotik', '6485@GMAIL.COM', '1234567890', 'JL. PANDEGILING 177i, SURABAYA', '-', '-', '0', '2020-01-13 11:23:02', ''),
('RKA2020010034', 'AP. ALAM RIA FARMA', 'apotik', '645@GMAIL.COM', '1234567890', 'JL. RAYA JEMURSARI 189', '-', '-', '0', '2020-01-13 11:26:44', ''),
('RKA2020010035', 'PT. ALPEN AGUNG RAYA', 'rs', '64@GMAIL.COM', '13456790520', 'JL. SATELIT INDAH II, SURABAYA', '-', '-', '0', '2020-01-13 11:28:09', ''),
('RKA2020010036', 'PT. ALPEN AGUNG RAYA/WARU', 'rs', '46879@GMAIL.COM', '1234567890', 'JL. SATELIT INDAH', '-', '-', '0', '2020-01-13 11:28:57', ''),
('RKA2020010037', 'PT. ALPEN AGUNG RAYA / KENJERAN', 'rs', '634@GMAIL.COM', '1234567890', 'JL. SATELIT INDAH II', '-', '-', '0', '2020-01-13 11:29:41', ''),
('RKA2020010038', 'RS. AL-IRSYAD', 'rs', '4658@GMAIL.CM', '1234567890', 'JL. KH MANSYUR 210', '-', '-', '0', '2020-01-13 11:33:14', ''),
('RKA2020010039', 'PT. AFFINITY HEALTH INDONESIA', 'rs', '+569@GMAIL.COM', '1234567890', 'JL. JATINEGARA TIMUR 85A, JAKARTA TIMUR', '-', '-', '0', '2020-01-13 11:53:19', ''),
('RKA2020010040', 'AP. AGAPE', 'apotik', '654@GMAIL.COM', '1234567889', 'JL. KAWI 20 ', '-', '-', '0', '2020-01-13 11:55:21', ''),
('RKA2020010041', 'AP. BUNGURAN', 'apotik', '564@GMAIL.CO', '1234657890', 'JL. BUNGURAN 29 / II', '-', '-', '0', '2020-01-13 11:59:08', ''),
('RKA2020010042', 'AP. BAHAGIA FARMA', 'apotik', '852@GMAIL.N', '1234567908', 'JL. PENELEH 30, SURABAYA', '-', '-', '0', '2020-01-13 12:00:04', ''),
('RKA2020010043', 'PT. CIPTA NIRMALA', 'rs', '5@R.V', '1234567890', 'JL. KARTINI 260 GRESIK', '-', '-', '0', '2020-02-13 03:00:29', ''),
('RKA2020010044', 'AP. CRYSTAL', 'apotik', '8@E.R', '12345674890', 'JL. KLAMPIS JAYA 3/ A-3', '-', '-', '0', '2020-01-13 12:03:34', ''),
('RKA2020010045', 'AP. DHARMAHUSADA', 'apotik', '7@E.M', '1234567890', 'JL. DHARMAHUSADA 176', '-', '-', '0', '2020-01-13 12:06:23', ''),
('RKA2020010046', 'AP. DUTA FARMA I', 'apotik', '8@D.F', '1234567890', 'JL. DUKUH KUPANG 18-A', '-', '-', '0', '2020-01-13 12:07:18', ''),
('RKA2020010047', 'AP. DUTA FARMA II', 'apotik', '3@E.T', '12345674890', 'JL. ADYTIAWARMAN 80', '-', '-', '0', '2020-01-13 12:08:14', ''),
('RKA2020010048', 'AP. FAJAR', 'apotik', '7@E.F', '1234567890', 'JL. RAYA ARJUNA 34', '-', '-', '0', '2020-01-13 12:21:01', ''),
('RKA2020010049', 'PT. GELORA FAJAR FARMA', 'pbf', '1@E.W', '1234567980', 'JL. MARGOREJO INDAH II / A232', '-', '-', '0', '2020-01-13 12:23:41', ''),
('RKA2020010050', 'PT. GIDION JAYA', 'pbf', '1@W.E', '1234567890', 'JL. SETIA BUDI SK II / 3-7, AMBON', '-', '-', '0', '2020-01-13 12:24:21', ''),
('RKA2020010051', 'AP. GEDEG FARMA', 'apotik', '5@E.Y', '1234567890', 'JL. RAYA GEDEG 17', '-', '-', '0', '2020-01-13 12:24:52', ''),
('RKA2020010052', 'AP. GLORY', 'apotik', '1@R.G', '1234567890', 'RUKO TAMAN GAPURA W-01 / 15-B SURABAYA', '-', '-', '0', '2020-02-13 03:00:50', ''),
('RKA2020010053', 'AP. HARAPAN KITA', 'apotik', '1@W.G', '1234567890', 'JL. ELTARI NO 6, WAINGAPU', '-', '-', '0', '2020-01-13 12:58:31', ''),
('RKA2020010054', 'PT. HUSADA PRAJOGI', 'pbf', '1@E.TR', '1234567890', 'JL. KALIANYAR 62, SURABAYA', '-', '-', '0', '2020-01-13 01:34:15', ''),
('RKA2020010055', 'AP. KARTIKA', 'apotik', '1@R.T', '0123456798', 'JL. DHARMAWANGSA 8 SURABAYA', '-', '-', '0', '2020-02-13 03:01:01', ''),
('RKA2020010056', 'PT. LAB MEDIKA SEJAHTERA', 'pbf', '12@W.R', '1234567890', 'JL. KENCANASARI TIMUR VIII/ H-1', '-', '-', '0', '2020-01-13 01:41:34', ''),
('RKA2020010057', 'AP. KARUNIA FARMA', 'apotik', '1@E.Y', '1234567890', 'JL. MENGANTI KRAMAT 19 SURABAYA', '-', '-', '0', '2020-02-13 03:01:12', ''),
('RKA2020010058', 'AP. MAHKOTA', 'apotik', '74@E.T', '1234567890', 'JL. TENGGILIS MEJOYO KB / 22 SURABAYA', '-', '-', '0', '2020-02-13 03:01:25', ''),
('RKA2020010059', 'AP. MELIANA', 'apotik', '74@T.Y', '1234567890', 'JL. KERTAJAYA 137 SURABAYA', '-', '-', '0', '2020-02-13 03:01:39', ''),
('RKA2020010060', 'AP. MENGGALA', 'apotik', '82@R.Y', '1234567890', 'JL. TIDAR 94 SURABAYA', '-', '-', '0', '2020-02-13 03:01:49', ''),
('RKA2020010061', 'AP. MERDEKA', 'apotik', '-', '-', 'JL. MERDEKA 112, JOMBANG', '-', '-', '0', '2020-01-22 12:07:42', ''),
('RKA2020010062', 'AP. MUSTIKA JAYA 1', 'apotik', '-', '-', 'JL. KH. MANSYUR 142, SURABAYA', '-', '-', '0', '2020-01-22 12:08:34', ''),
('RKA2020010063', 'AP. MUSTIKA JAYA II', 'apotik', '-', '-', 'JL. KH. MANSYUR 246, SURABAYA', '-', '-', '0', '2020-01-22 12:09:13', ''),
('RKA2020010064', 'AP. MUSTIKA JAYA III', 'apotik', '-', '-', 'JL. NYAMPLUNGAN 37, SURABAYA', '-', '-', '0', '2020-01-22 12:09:38', ''),
('RKA2020010065', 'AP. MOJO', 'apotik', '-', '-', 'JL. MOJOKLANGGRU KIDUL NO 84, SURABAYA', '-', '-', '0', '2020-01-22 12:10:06', ''),
('RKA2020010066', 'AP. NUSANTARA', 'apotik', '-', '-', 'JL. UNDAAN KULON 109 SURABAYA', '-', '-', '0', '2020-02-13 03:01:59', ''),
('RKA2020010067', 'AP. OKTA', 'apotik', '-', '-', 'JL. KALIBOKOR SELATAN 104 SURABAYA', '-', '-', '0', '2020-02-13 03:02:10', ''),
('RKA2020010068', 'AP. OKKY', 'apotik', '-', '-', 'JL. TANJUNG SADARI 41, SURABAYA', '-', '-', '0', '2020-01-22 12:11:20', ''),
('RKA2020010069', 'AP. 2 OSCAR', 'apotik', '-', '-', 'JL. DHARMAWANGSA 28', '-', '-', '0', '2020-01-22 12:11:39', ''),
('RKA2020010070', 'AP. PATUT JAYA', 'apotik', '-', '-', 'JL. DUKUH KUPANG TMUR VI/26, SURABAYA', '-', '-', '0', '2020-01-22 12:12:09', ''),
('RKA2020010071', 'PT. PETRO GRAHA MEDIKA', 'rs', '-', '-', 'JL. A. YANI 69, GRESIK', '-', '-', '0', '2020-01-22 12:12:36', ''),
('RKA2020010072', 'AP. PATEN JAYA', 'apotik', '-', '-', 'JL. RUNGKUT MADYA 118, SURABAYA', '-', '-', '1', '2020-02-05 03:22:20', ''),
('RKA2020010073', 'PT. PELINDO HUSADA CITRA', 'rs', '-', '-', 'JL. PRAPAT KURUNG SELATAN 1, SURABAYA', '-', '-', '0', '2020-01-22 12:13:27', ''),
('RKA2020010074', 'PT. PRIMA KARYA HUSADA', 'rs', '-', '-', 'JL. RUNGKUT INDUSTRI NO 1, SURABAYA', '-', '-', '0', '2020-01-22 12:13:50', ''),
('RKA2020010075', 'AP. PAHALA', 'apotik', '-', '-', 'JL. SIMOKALANGAN 33, SURABAYA', '-', '-', '0', '2020-01-22 12:14:46', ''),
('RKA2020010076', 'AP. PAMENANG', 'apotik', '-', '-', 'JL. DAMO PERMAI SELATAN I / 4, SURABAYA', '-', '-', '0', '2020-01-22 01:36:29', ''),
('RKA2020010077', 'AP. PANGESTU', 'apotik', '-', '-', 'JL. GAJAHMADA 79, SIDOARJO', '-', '-', '0', '2020-01-22 01:37:40', ''),
('RKA2020010078', 'AP. POLY FARMA 61', 'apotik', '-', '-', 'JL. WR. SUPRATMAN 61, SURABAYA', '-', '-', '0', '2020-01-22 01:38:15', ''),
('RKA2020010079', 'AP. POLY FARMA 77', 'apotik', '-', '-', 'JL. UNDAAN KULON 77, SURABAYA', '-', '-', '0', '2020-01-22 01:38:36', ''),
('RKA2020010080', 'AP. PRAPEN INDAH', 'apotik', '-', '-', 'JL. PRAPEN INDAH IIB / 6 SURABAYA', '-', '-', '0', '2020-02-13 03:02:28', ''),
('RKA2020010081', 'AP. PRISKA', 'apotik', '-', '-', 'JL. MULYOSARI 151, SURABAYA', '-', '-', '0', '2020-01-22 01:41:10', ''),
('RKA2020010082', 'AP. PANACEA', 'apotik', '-', '-', 'JL. KEDUNGSARI 19A SURABAYA', '-', '-', '0', '2020-02-13 03:02:40', ''),
('RKA2020010083', 'RSUD. IBNU SINA KAB. GRESIK', 'rs', '-', '-', 'JL. DR. WAHIDIN SUDIROHUSODO 243-B, GRESIK', '-', '-', '0', '2020-01-22 01:53:49', ''),
('RKA2020010084', 'AP. SUMBER SEHAT', 'apotik', '-', '-', 'JL. DARMO INDAH TIMURG-64, SURABAYA', '-', '-', '0', '2020-01-22 03:11:23', ''),
('RKA2020010085', 'RSK SUMBER GLAGAH', 'rs', '-', '-', 'PACET, MOJOKERTO', '-', '-', '0', '2020-01-22 03:12:29', ''),
('RKA2020010086', 'RS. LUKAS', 'apotik', '-', '-', 'JL. KH. MOH KHOLIL 36-A, BANGKALAN', '-', '-', '0', '2020-02-14 02:17:19', ''),
('RKA2020010087', 'AP. PATEN JAYA', 'apotik', '-', '-', 'JL. RUNGKUT MADYA 11-B, SURABAYA', '-', '-', '0', '2020-01-22 03:13:26', ''),
('RKA2020010088', 'RS. ISLAM SITI HAJAR SIDOARJO', 'rs', '-', '-', 'JL. RADEN PATAH 70-72, SIDOARJO', '-', '-', '0', '2020-01-22 03:13:57', ''),
('RKA2020010089', 'INSTALASI FARMASI RS. UNIVERSITAS AIRLANGGA', 'rs', '-', '-', 'KAMPUS C MULYOREJO, SURABAYA', '-', '-', '0', '2020-01-22 03:14:31', ''),
('RKA2020010090', 'RS. BHAYANGKARA', 'rs', '-', '-', 'JL. A.YANI 116, SURABAYA', '-', '-', '0', '2020-01-22 03:16:03', ''),
('RKA2020010091', 'AP. RMDC', 'apotik', '-', '-', 'JL. RAYA DARMO PERMAI II/26, SURABAYA', '-', '-', '0', '2020-01-22 03:16:30', ''),
('RKA2020010092', 'RSUD BHAKTI DHARMA HUSADA', 'rs', '-', '-', 'JL. RAYA KENDUNG 115-117, SURABAYA', '-', '-', '0', '2020-01-22 03:16:57', ''),
('RKA2020010093', 'RS. MATA FATMA', 'rs', '-', '-', 'JL. RAYA KALIJATEN 40, SIDOARJO', '-', '-', '0', '2020-01-22 03:17:36', ''),
('RKA2020010094', 'RSUD. DR. MOCH. SOEWANDHIE', 'rs', '-', '-', 'JL. TAMBAK REJO 45-47, SURABAYA', '-', '-', '0', '2020-01-22 03:19:10', ''),
('RKA2020010095', 'RSUD. DR. SOETOMO', 'rs', '-', '-', 'JL. MAYJEND PROF DR. MOESTOPO 6-8, SURABAYA', '-', '-', '0', '2020-01-22 03:19:36', ''),
('RKA2020010096', 'RSUD. DR. SOEGIRI', 'rs', '-', '-', 'JL. KUSUMA BANGSA 7, LAMONGAN', '-', '-', '0', '2020-01-22 03:20:05', ''),
('RKA2020010097', 'RSU. HAJI SURABAYA', 'rs', '-', '-', 'JL. MANYAR KERTOADI 59, SURABAYA', '-', '-', '0', '2020-01-22 03:20:28', ''),
('RKA2020010098', 'RSUD SIDOARJO', 'rs', '-', '-', 'JL. MOJOPAHIT 667, SIDOARJO', '-', '-', '0', '2020-01-22 03:21:09', ''),
('RKA2020010099', 'RS. WIJAYA', 'pbf', '-', '-', 'JL. RAYA MENGANTI 398 SURABAYA', '-', '-', '0', '2020-02-13 03:03:03', ''),
('RKA2020010100', 'RS. MATA UNDAAN', 'rs', '-', '-', 'JL. UNDAAN KULON 19, SURABAYA', '-', '-', '0', '2020-01-22 03:22:21', ''),
('RKA2020010101', 'PT. AFFINITY HEALTH INDONESIA', 'rs', '-', '-', 'JL. JATINEGARA TIMUR 85A, BALI MASTER, JAKARTA', '-', '-', '0', '2020-01-22 03:23:04', ''),
('RKA2020010102', 'RS. SPESIALIS HUSADA UTAMA', 'rs', '-', '-', 'JL. PROF. DR. MOESTOPO 31 SURABAYA', '-', '-', '0', '2020-02-13 03:03:15', ''),
('RKA2020010103', 'RSK. WILLIAM BOOTH', 'rs', '-', '-', 'JL. DIPONEGORO 34 SURABAYA', '-', '-', '0', '2020-02-13 03:03:26', ''),
('RKA2020010104', 'RSK. VINCENTIUS A PAULO', 'rs', '-', '-', 'JL. DIPONEGORO 51', '-', '-', '0', '2020-01-22 03:24:15', ''),
('RKA2020010105', 'RUMAH SAKIT ISLAM I', 'rs', '-', '-', 'JL. ACHMAD YANI 2-4, SURABAYA', '-', '-', '0', '2020-01-22 03:26:18', ''),
('RKA2020010106', 'RUMAH SAKIT ISLAM II', 'rs', '-', '-', 'JL. JEMURSARI 51-57, SURABAYA', '-', '-', '0', '2020-01-22 03:26:59', ''),
('RKA2020010107', 'RS. GRIYA HUSADA', 'rs', '-', '-', 'JL. MAYJEND PAJAITAN 22, MADIUN', '-', '-', '0', '2020-01-22 03:28:03', ''),
('RKA2020010108', 'AP. SABA', 'apotik', '-', '-', 'JL. TENGGILIS BLOK R / 18, SURABAYA', '-', '-', '0', '2020-01-22 03:28:38', ''),
('RKA2020010109', 'AP. SATYA FARMA', 'apotik', '-', '-', 'JL. DIPONEGORO 97, SURABAYA', '-', '-', '0', '2020-01-22 03:29:02', ''),
('RKA2020010110', 'AP. SEHAT PROBOLINGGO', 'apotik', '-', '-', 'JL. PANGLIMA SUDIRMAN 2B, PROBOLINGGO', '-', '-', '0', '2020-01-22 03:29:44', ''),
('RKA2020010111', 'AP. STAR FARMA', 'apotik', '-', '-', 'JL. RUNGKUT MENANGGAL 9A, SURABAYA', '-', '-', '0', '2020-01-22 03:30:08', ''),
('RKA2020010112', 'AP. SUKSES', 'apotik', '-', '-', 'RUKO TAMAN GAPURA E-12, SURABAYA', '-', '-', '0', '2020-01-22 03:31:22', ''),
('RKA2020010113', 'AP. TUNGGADEWI II', 'apotik', '-', '-', 'JL. KSATRIAN 17, SURABAYA', '-', '-', '0', '2020-01-22 03:31:55', ''),
('RKA2020010114', 'AP. TUNGGADEWI I', 'apotik', '-', '-', 'JL. GUNUNGSARI 88, SURABAYA', '-', '-', '0', '2020-01-22 03:32:32', ''),
('RKA2020010115', 'AP. UBAYA', 'apotik', '-', '-', 'JL. KALIWARU I/31, SURABAYA', '-', '-', '0', '2020-01-22 03:32:56', ''),
('RKA2020010116', 'AP. TIRTA FARMA', 'apotik', '-', '-', 'JL. KAHURIPAN 32', '-', '-', '0', '2020-01-22 03:33:16', ''),
('RKA2020010117', 'AP. VALENTINE', 'apotik', '-', '-', 'JL. CILIWUNG 60-A, SURABAYA', '-', '-', '0', '2020-01-22 03:33:39', ''),
('RKA2020010118', 'SURABAYA EYE CLINIC', 'kl', '-', '-', 'JL. JEMURSARI 108, SURABAYA', '-', '-', '0', '2020-01-22 03:34:52', ''),
('RKA2020010119', 'AP. TIARA', 'apotik', '-', '-', 'JL. RAYA DARMO PERMAI I/44, SURABAYA', '-', '-', '0', '2020-01-22 03:35:23', ''),
('RKA2020010120', 'NATIONAL HOSPITAL', 'rs', '-', '-', 'JL. BOULEBARD FAMILY SELATAN KAV I, SURABAYA', '-', '-', '0', '2020-01-22 03:36:07', ''),
('RKA2020010121', 'KLINIK MATA PANDAAN', 'kl', '-', '-', 'JL. BYBOASS PANDAAN NO. 1 LINGKUNGAN KLUNCING PANDAAN', '-', '-', '0', '2020-01-22 03:37:11', ''),
('RKA2020010122', 'RSU. RAHMAN RAHIM', 'rs', '-', '-', 'JL. RAYA SAIMBANG 277 SUKODONO, SIDOARJO', '-', '-', '0', '2020-01-22 03:37:42', ''),
('RKA2020010123', 'AP. KIMIA FARMA 22', 'apotik', '-', '-', 'JL. BRATANG GEDE 100A, SBY', '-', '-', '0', '2020-01-22 03:38:13', ''),
('RKA2020010124', 'AP. KIMIA FARMA 23 ', 'apotik', '-', '-', 'JL. RAYA KANDANGSARI BLK. J/7, SBY', '-', '-', '0', '2020-02-13 02:59:18', ''),
('RKA2020010125', 'AP. KIMIA FARMA 24 ', 'apotik', '-', '-', 'JL. DHARMAWANGSA 24, SURABAYA', '-', '-', '0', '2020-02-13 02:59:48', ''),
('RKA2020010126', 'AP. KIMIA FARMA 25', 'apotik', '-', '-', 'JL. RAYA DARMO 2-4, SBY', '-', '-', '0', '2020-01-22 03:39:36', ''),
('RKA2020010127', 'AP. KIMIA FARMA 26', 'apotik', '-', '-', 'JL. DIPONEGORO 94, SBY', '-', '-', '0', '2020-01-22 03:40:21', ''),
('RKA2020010128', 'AP. KIMIA FARMA 35', 'apotik', '-', '-', 'JL. NGAGEL JAYA SELATAN 109, SBY', '-', '-', '0', '2020-01-22 03:40:43', ''),
('RKA2020010129', 'AP. KIMIA FARMA 45', 'apotik', '-', '-', 'JL. RAYA DARMO 94, SBY', '-', '-', '0', '2020-01-22 03:41:08', ''),
('RKA2020010130', 'AP. KIMIA FARMA 52', 'apotik', '-', '-', 'JL. RAYA DUKUH KUPANG 54, SBY', '-', '-', '0', '2020-01-22 03:41:27', ''),
('RKA2020010131', 'AP. KIMIA FARMA 163', 'apotik', '-', '-', 'JL. JOKOTOLE 5, BANGKALAN', '-', '-', '0', '2020-02-07 09:59:22', ''),
('RKA2020010132', 'AP. KIMIA FARMA 166', 'apotik', '-', '-', 'JL. AHMAD YANI 228, SBY', '-', '-', '0', '2020-01-22 03:42:12', ''),
('RKA2020010133', 'AP. KIMIA FARMA 175', 'apotik', '-', '-', 'JL. KARANGMENJANGAN 9, SBY', '-', '-', '0', '2020-01-22 03:42:33', ''),
('RKA2020010134', 'AP. KIMIA FARMA 243', 'apotik', '-', '-', 'JL. RAYA ARJUNO 151, SBY', '-', '-', '0', '2020-01-22 03:42:52', ''),
('RKA2020010135', 'AP. KIMIA FARMA 274', 'apotik', '-', '-', 'JL. DIPONEGORO 4, SBY', '-', '-', '0', '2020-01-22 03:43:22', ''),
('RKA2020010136', 'AP. KIMIA FARMA 304', 'apotik', '-', '-', 'JL. PERAK TIMUR 166, SBY', '-', '-', '0', '2020-01-22 03:55:01', ''),
('RKA2020010137', 'AP. KIMIA FARMA 407', 'apotik', '-', '-', 'JL. RAYA MULYOSARI 157, SBY', '-', '-', '0', '2020-01-22 03:55:20', ''),
('RKA2020010138', 'AP. KIMIA FARMA 419', 'apotik', '-', '-', 'JL. TRUNOJOYO 75A, SBY', '-', '-', '0', '2020-01-22 03:56:02', ''),
('RKA2020010139', 'AP. KIMIA FARMA 432', 'apotik', '-', '-', 'JL. RAYA MERR 2 PENJARINGAN, SBY', '-', '-', '0', '2020-01-22 03:56:32', ''),
('RKA2020010140', 'AP. KIMIA FARMA 485', 'apotik', '-', '-', 'JL. CILIWUNG 76, SBY', '-', '-', '0', '2020-01-22 03:57:46', ''),
('RKA2020010141', 'AP. KIMIA FARMA 459', 'apotik', '-', '-', 'JL. RAYA MENGANTI RUKO TAMAN PONDOK INDAH A-17,SBY', '-', '-', '0', '2020-01-22 03:58:20', ''),
('RKA2020010142', 'AP. KIMIA FARMA 460', 'apotik', '-', '-', 'JL. RUKO TAMAN GAPURA B-2', '-', '-', '0', '2020-01-22 03:58:39', ''),
('RKA2020010143', 'AP. KIMIA FARMA 511', 'apotik', '-', '-', 'JL. KLAMPIS JAYA A-30, SBY', '-', '-', '0', '2020-01-22 03:59:31', ''),
('RKA2020010144', 'AP. KIMIA FARMA 526', 'apotik', '-', '-', 'JL. RUNGKUT MADYA 97, SBY', '-', '-', '0', '2020-01-22 03:59:58', ''),
('RKA2020010145', 'AP. KIMIA FARMA SUMENEP', 'apotik', '-', '-', 'JL. KH. MANSYUR 42, MADURA', '-', '-', '0', '2020-01-22 04:00:37', ''),
('RKA2020010146', 'AP. KIMIA FARMA MANUKAN', 'apotik', '-', '-', 'JL. MANUKAN TAMA 79, SBY', '-', '-', '0', '2020-01-22 04:00:59', ''),
('RKA2020010147', 'AP. KIMIA FARMA DARMO INDAH', 'apotik', '-', '-', 'JL. DARMO INDAH TIMUR SS-8, SBY', '-', '-', '0', '2020-01-22 04:01:21', ''),
('RKA2020010148', 'AP. KIMIA FARMA LONTAR', 'apotik', '-', '-', 'JL. RAYA LONTAR 111, SBY', '-', '-', '0', '2020-01-22 04:01:59', ''),
('RKA2020010149', 'AP. KIMIA FARMA KALIBOKOR', 'apotik', '-', '-', 'JL. NGAGEL JAYA 1, SBY', '-', '-', '0', '2020-01-22 04:02:17', ''),
('RKA2020010150', 'AP. KIMIA FARMA KETINTANG', 'apotik', '-', '-', 'JL. RAYA KETINTANG 178, SBY', '-', '-', '0', '2020-01-22 04:02:38', ''),
('RKA2020010151', 'AP. KIMIA FARMA SAMPANG', 'apotik', '-', '-', 'JL. RAJAWALI 25, SAMPANG MADURA', '-', '-', '0', '2020-01-22 04:03:01', ''),
('RKA2020010152', 'AP. KIMIA FARMA KENJERAN', 'apotik', '-', '-', 'JL. KENJERAN 211, SBY', '-', '-', '0', '2020-01-22 04:03:21', ''),
('RKA2020010153', 'AP. KIMIA FARMA PECINDILAN', 'apotik', '-', '-', 'JL. PECINDILAN 22, SBY', '-', '-', '0', '2020-01-22 04:03:42', ''),
('RKA2020010154', 'AP. KIMIA FARMA RUNGKUT BARU', 'apotik', '-', '-', 'JL. RUNGKUT BARU 17A, SBY', '-', '-', '0', '2020-01-22 04:04:01', ''),
('RKA2020010155', 'AP. KIMIA FARMA NGINDEN', 'apotik', '-', '-', 'JL. RAYA NGINDEN 3, SBY', '-', '-', '0', '2020-01-22 04:04:17', ''),
('RKA2020010156', 'AP. KIMIA FARMA BANYU URIP', 'apotik', '-', '-', 'JL. RAYA BANYU URIP 70-74, SBY', '-', '-', '0', '2020-01-22 04:04:38', ''),
('RKA2020010157', 'AP. KIMIA FARMA SIWALANKERTO', 'apotik', '-', '-', 'JL. SIWALANKERTO TIMUR 221, SBY', '-', '-', '0', '2020-01-22 04:05:02', ''),
('RKA2020010158', 'AP. KIMIA FARMA KUPANG JAYA', 'apotik', '-', '-', 'JL. KUPANG JAYA B NO 4, SBY', '-', '-', '0', '2020-01-22 04:05:29', ''),
('RKA2020010159', 'AP. KIMIA FARMA 62', 'apotik', '-', '-', 'JL. GAJAHMADA 171, JEMBER', '-', '-', '0', '2020-01-22 04:06:02', ''),
('RKA2020010160', 'AP. KIMIA FARMA 67', 'apotik', '-', '-', 'JL. GAJAHMADA 67, JEMBER', '-', '-', '0', '2020-01-22 04:06:23', ''),
('RKA2020010161', 'AP. KIMIA FARMA 121', 'apotik', '-', '-', 'JL. LETJ. SUPRAPTO 71, JEMBER', '-', '-', '0', '2020-01-22 04:06:52', ''),
('RKA2020010162', 'AP. KIMIA FARMA 241', 'apotik', '-', '-', 'JL. PB SUDIRMAN 46, JEMBER', '-', '-', '0', '2020-01-22 04:07:12', ''),
('RKA2020010163', 'AP. KIMIA FARMA 307', 'apotik', '-', '-', 'JL. A.YANI 72, BANYUWANGI', '-', '-', '0', '2020-01-22 04:07:30', ''),
('RKA2020010164', 'AP. KIMIA FARMA 313', 'apotik', '-', '-', 'JL. JAWA 12, JEMBER', '-', '-', '0', '2020-01-22 04:07:46', ''),
('RKA2020010165', 'AP. KIMIA FARMA 470', 'apotik', '-', '-', 'JL. BASUKI RAHMAD 69, BANYUWANGI', '-', '-', '0', '2020-01-22 04:09:22', ''),
('RKA2020010166', 'AP. KIMIA FARMA 522', 'apotik', '-', '-', 'JL. DR. SUTOMO 213, PROBOLINGGO', '-', '-', '0', '2020-01-22 04:09:41', ''),
('RKA2020010167', 'AP. KIMIA FARMA PANGSUD', 'apotik', '-', '-', 'JL. PB SUDIRMAN 339, PROBOLINGGO', '-', '-', '0', '2020-01-22 04:10:50', ''),
('RKA2020010168', 'AP. KIMIA FARMA GENTENG', 'apotik', '-', '-', 'JL. DR. SUTOMO 07, GENTENG', '-', '-', '0', '2020-01-22 04:11:21', ''),
('RKA2020010169', 'AP. KIMIA FARMA RAMBIPUJI', 'apotik', '-', '-', 'JL. GAJAHMADA 09, RAMBIPUJI', '-', '-', '0', '2020-01-22 04:11:41', ''),
('RKA2020010170', 'AP. KIMIA FARMA ROGOJAMPI', 'apotik', '-', '-', 'JL. RAYA ROGOJAMPI RUKO NIAGA A-3', '-', '-', '0', '2020-01-22 04:12:07', ''),
('RKA2020010171', 'AP. PAHALA KALIJATEN', 'apotik', '-', '-', 'JL. RAYA KALIJATEN 84 TAMAN, SIDOARJO', '-', '-', '0', '2020-01-24 11:57:33', ''),
('RKA2020010172', 'AP. BERLIAN FARMA', 'apotik', '-', '-', 'JL. KUTISARI INDAH UTARA X / 116, SURABAYA', '-', '-', '0', '2020-01-28 02:51:51', ''),
('RKA2020020001', 'RSIA KIRANA', 'rs', '-', '-', 'JL. RAYA NGELOM 87 TAMAN, SIDOARJO', '-', '-', '0', '2020-02-03 02:24:28', ''),
('RKA2020020002', 'AP. METRO FARMA', 'apotik', '-', '-', 'JL. UNDAAN WETAN 60-A, SURABAYA', '-', '-', '0', '2020-02-04 03:20:38', ''),
('RKA2020020003', 'KLINIK MATA DR. SJAMSU', 'kl', '-', '-', 'JL. ARIEF RAHMAN HAKIM 40, SURABAYA', '-', '-', '0', '2020-02-04 04:04:51', ''),
('RKA2020020004', 'KLINIK TRITYA', 'kl', '-', '-', 'JL. BARATAJAYA 59 BLOK A-3, SURABAYA', '-', '-', '0', '2020-02-04 04:14:34', ''),
('RKA2020020005', 'AP. KIMIA FARMA BOJONEGORO', 'apotik', '-', '-', 'JL. DIPONEGORO 77, BOJONEGORO', '-', '-', '0', '2020-02-05 09:18:27', ''),
('RKA2020020006', 'AP. KIMIA FARMA 906 (SUNAN DRAJAT)', 'apotik', '-', '-', 'JL. SUNAN DRAJAT', '-', '-', '0', '2020-02-05 09:30:34', ''),
('RKA2020020007', 'AP. JECONIA', 'apotik', '-', '-', 'JL. RAYA KENDALSARI 41 A RUNGKUT, SURABAYA', '-', '-', '0', '2020-02-05 09:41:59', ''),
('RKA2020020008', 'SARANA SEHAT UTAMA', 'kl', '-', '-', 'JL. SUMATRA 27F GKB, GRESIK', '-', '-', '0', '2020-02-05 09:44:31', ''),
('RKA2020020009', 'KIMIA FARMA 668', 'apotik', '-', '-', 'JL. RAYA MENGANTI 169 GRESIK', '-', '-', '0', '2020-02-05 09:49:19', ''),
('RKA2020020010', 'AP. KIMIA FARMA 731 (USMAN SADAR)', 'apotik', '-', '-', 'JL. USMAN SADAR 98 GRESIK', '-', '-', '0', '2020-02-05 09:52:21', ''),
('RKA2020020011', 'AP. KIMIA FARMA BABAT', 'apotik', '-', '-', 'BABAT, LAMONGAN', '-', '-', '0', '2020-02-05 09:53:57', ''),
('RKA2020020012', 'AP. KIMIA FARMA DRIYOREJO', 'apotik', '-', '-', 'JL. BATU MULIA BLOK 12 E/A-14 B GRESIK', '-', '-', '0', '2020-02-05 09:56:13', ''),
('RKA2020020013', 'AP. KIMIA FARMA SUKOMULYO', 'apotik', '-', '-', 'JL. KALIMANTAN 17 GRESIK', '-', '-', '0', '2020-02-05 10:02:33', ''),
('RKA2020020014', 'AP. KIMIA FARMA 788 (GKB)', 'apotik', '-', '-', 'JL. SUMATRA 29-31 GKB GRESIK', '-', '-', '0', '2020-02-05 10:06:55', ''),
('RKA2020020015', 'AP. KIMIA FARMA 492', 'apotik', '-', '-', 'RUKO GREEN GARDEN REGENCY A2-5 GRESIK', '-', '-', '0', '2020-02-05 10:46:10', ''),
('RKA2020020016', 'AP. KIMIA FARMA 321', 'apotik', '-', '-', 'JL. LAMONGREJO 125 LAMONGAN', '-', '-', '0', '2020-02-05 10:50:02', ''),
('RKA2020020017', 'AP. KIMIA FARMA BLIMBING', 'apotik', '-', '-', 'JL. RAYA DANDELS 117 BLIMBING LAMONGAN', '-', '-', '0', '2020-02-05 11:01:23', ''),
('RKA2020020018', 'RS. GOTONG ROYONG', 'rs', '-', '-', 'JL. MEDOKAN SEMAMPIR INDAH 97 SURABAYA', '-', '-', '0', '2020-02-05 11:04:06', ''),
('RKA2020020019', 'KLINIK MATA AYU SIWI', 'kl', '-', '-', 'JL. VETERAN I NO 16, NGANJUK', '-', '-', '0', '2020-02-05 11:24:23', ''),
('RKA2020020020', 'AP. TRIJAYA ABADI', 'apotik', '-', '-', 'JL. BARATAJAYA 19/48-A SURABAYA', '-', '-', '0', '2020-02-05 11:36:44', ''),
('RKA2020020021', 'SAMPLE', 'ln', '-', '-', 'BLESSINDO', '-', '-', '0', '2020-02-05 01:43:34', ''),
('RKA2020020022', 'AP. KIMIA FARMA 261', 'apotik', '-', '-', 'JL. MOJOPAHIT 68 SIDOARJO', '-', '-', '0', '2020-02-05 02:07:41', ''),
('RKA2020020023', 'AP. KIMIA FARMA 604', 'apotik', '-', '-', 'JL. WAHID HASYIM 172-A JOMBANG', '-', '-', '0', '2020-02-05 02:12:54', ''),
('RKA2020020024', 'AP. KIMIA FARMA BANGIL', 'apotik', '-', '-', 'JL. A.YANI 21 BANGIL', '-', '-', '0', '2020-02-05 02:15:54', ''),
('RKA2020020025', 'AP. KIMIA FARMA KH.M.KHOLIL', 'apotik', '-', '-', 'JL. KH. MUHAMMAD CHOLIL 102-B BANGKALAN', '-', '-', '0', '2020-02-05 02:20:39', ''),
('RKA2020020026', 'AP. KIMIA FARMA GEDANGAN 2', 'apotik', '-', '-', 'GEDANGAN - SIDOARJO', '-', '-', '0', '2020-02-05 02:22:37', ''),
('RKA2020020027', 'AP. KIMIA FARMA NGANJUK', 'apotik', '-', '-', 'JL. ABDUL RAHMAN SALEH 105 NGANJUK', '-', '-', '0', '2020-02-05 02:25:19', ''),
('RKA2020020028', 'AP. GABRIEL', 'apotik', '-', '-', 'JL. KAPAS KRAMPUNG 134 SURABAYA', '-', '-', '0', '2020-02-05 03:11:24', ''),
('RKA2020020029', 'AP. KARYA SUKSES', 'apotik', '-', '-', 'JL. KLAMPIS HARAPAN IV / 12 -A', '-', '-', '0', '2020-02-05 03:12:32', ''),
('RKA2020020030', 'AP. CNC', 'apotik', '-', '-', 'JL. PACUAN KUDA 27 SURABAYA', '-', '-', '0', '2020-02-05 03:15:31', ''),
('RKA2020020031', 'KLINIK UTAMA RAWAT JALAN DR.HARYO EYE CARE', 'kl', '-', '-', 'JL. RUNGKUT ASRI UTARA XIII / 10 SURABAYA', '-', '-', '0', '2020-02-05 03:19:45', ''),
('RKA2020020032', 'AP. KIMIA FARMA 164', 'apotik', '-', '-', 'JL. KARTINI 278 GRESIK', '-', '-', '0', '2020-02-05 03:20:49', ''),
('RKA2020020033', 'KLINIK PHC KEBRAON', 'kl', '-', '-', 'JL. KEBRAON SELATAN FA 37-38 SURABAYA', '-', '-', '0', '2020-02-06 12:28:14', ''),
('RKA2020020034', 'RUMAH SAKIT ISLAM JOMBANG', 'rs', '-', '-', 'JL.BRIGJEN KRETARTO N0.22A JOMBANG.', '-', '-', '0', '2020-02-06 01:13:30', ''),
('RKA2020020035', 'KLINIK EDC ROYAL MOJOSARI', 'kl', '-', '-', 'RUKO ROYAL BLOK A 6-7 JL. AIRLANGGA MOJOSARI', '-', '-', '0', '2020-02-06 02:06:21', ''),
('RKA2020020036', 'RS. UMUM AL-ISLAM HM MAWARDI', 'rs', '-', '-', 'JL. KYAI MOJO 12A KRIAN', '-', '-', '0', '2020-02-06 02:13:11', ''),
('RKA2020020037', 'KLINIK PHC TANJUNG PERAK', 'kl', '-', '-', 'JL. PERAK BARAT 239 A SURABAYA', '-', '-', '0', '2020-02-06 02:27:24', ''),
('RKA2020020038', 'AP. MATA KITA', 'apotik', '-', '-', 'PERUM MOJOROTO INDAH E-9 KEDIRI', '-', '-', '0', '2020-02-07 10:49:56', ''),
('RKA2020020039', 'AP. KUSUMA', 'apotik', '-', '-', 'JL. KUSUMA BANGSA 49 SURABAYA', '-', '-', '0', '2020-02-07 02:29:52', ''),
('RKA2020020040', 'AP. CENDANA MITRA FARMA', 'apotik', '-', '-', 'JL. UNTUNG SUROPATI 41 BOJONEGORO', '-', '-', '1', '2020-02-10 12:15:50', ''),
('RKA2020020041', 'PT. LAKSA NUSANTARA FARMA', 'pbf', '-', '-', 'JL. MULAWARMAN RT.24 NO 12 TARAKAN', '-', '-', '0', '2020-02-10 12:32:25', ''),
('RKA2020020042', 'AP. PHARMACARE', 'apotik', '-', '-', 'JL. MAYJEND SOENGKONO 91D SURABAYA', '-', '-', '0', '2020-02-10 12:39:38', ''),
('RKA2020020043', 'YAY. RUMAH SAKIT ISLAM GARAM', 'rs', '-', '-', 'JL. RAYA NO 1 KALIANGET BABAT, SUMENEP', '-', '-', '0', '2020-02-10 01:41:03', ''),
('RKA2020020044', 'AP. KASIH SEHAT', 'apotik', '-', '-', 'JL. SIMOGUNUNG BARAT TOL 2 NO 53A SBY', '-', '-', '0', '2020-02-10 01:45:41', ''),
('RKA2020020045', 'RSUD. DR R. KOESMA', 'rs', '-', '-', 'JL. DR. WAHDIN SUDIROHUSODO 800, TUBAN', '-', '-', '0', '2020-02-10 02:21:58', ''),
('RKA2020020046', 'AP. KIMIA FARMA 643', 'apotik', '-', '-', 'JL. RAYA MANYAR 55 GRESIK', '-', '-', '0', '2020-02-11 10:53:47', ''),
('RKA2020020047', 'AP. MEGA SURYA', 'apotik', '-', '-', 'JL. KALIBUTUH NO. 48 SURABAYA', '-', '-', '0', '2020-02-11 12:47:21', ''),
('RKA2020020048', 'KLINIK UTAMA R. JALAN KASIH KARUNIA', 'kl', '-', '-', 'JL. RAYA SATELIT SELATAN AS/1 SURABAYA', '-', '-', '0', '2020-02-11 12:58:41', ''),
('RKA2020020049', 'KLINIK SEMEN GRESIK', 'kl', '-', '-', 'DS. BOGOREJO, KEC. MARAKURAK, TUBAN', '-', '-', '0', '2020-02-11 01:56:36', ''),
('RKA2020020050', 'AP. INTI FARMA', 'apotik', '-', '-', 'JL. DHARMAHUSADA 124 SURABAYA', '-', '-', '0', '2020-02-11 02:21:49', ''),
('RKA2020020051', 'AP. KIMIA FARMA 119', 'apotik', '-', '-', 'DELTASARI AL/3 SIDOARJO', '-', '-', '0', '2020-02-11 03:43:53', ''),
('RKA2020020052', 'AP. KIMIA FARMA 603', 'apotik', '-', '-', 'JL. A. YANI 119 SIDOARJO', '-', '-', '0', '2020-02-11 03:44:27', ''),
('RKA2020020053', 'AP. KIMIA FARMA 165', 'apotik', '-', '-', 'JL. WAHIDIN SELATAN 69 PASURUAN', '-', '-', '0', '2020-02-11 03:52:28', ''),
('RKA2020020054', 'AP. KIMIA FARMA PANDAAN', 'apotik', '-', '-', 'JL. URIP SUMOHARJO 500 PANDAAN', '-', '-', '0', '2020-02-12 12:42:12', ''),
('RKA2020020055', 'AP. PUCANG', 'apotik', '-', '-', 'JL. PUCANG ANOM TIMUR 34 SURABAYA', '-', '-', '0', '2020-02-12 12:41:52', ''),
('RKA2020020056', 'AP. KIMIA FARMA 25 (PHC)', 'apotik', '-', '-', 'JL. RAYA DARMO 2-4 SURABAYA', '-', '-', '0', '2020-02-12 12:41:31', ''),
('RKA2020020057', 'AP. SELARAS', 'apotik', '-', '-', 'JL. NGINDEN SEMOLO 71 SURABAYA', '-', '-', '0', '2020-02-13 01:46:10', ''),
('RKA2020020058', 'AP. SURYASARI', 'apotik', '-', '-', 'JL. NGAGEL JAYA UTARA 104 SURABAYA', '-', '-', '0', '2020-02-13 01:46:58', ''),
('RKA2020020059', 'RSAD BRAWIJAYA', 'rs', '-', '-', 'JL. KESATRIAN NO. 17 SURABAYA', '-', '-', '0', '2020-02-13 02:37:44', ''),
('RKA2020020060', 'RS. PARU SURABAYA', 'rs', '-', '-', 'JL. KARANG TEMBOK NO. 39 SURABAYA', '-', '-', '0', '2020-02-13 02:38:29', ''),
('RKA2020020061', 'AP. KIMIA FARMA NGANJUK', 'apotik', '-', '-', 'JL. ABDULRAHMAN SALEH NO 105 NGANJUK', '-', '-', '0', '2020-02-13 02:55:15', ''),
('RKA2020020062', 'PT. GARUDA INTI FARMA', 'pbf', '-', '-', 'JL. SIMOLAWANG BARU I NO. 20 SURABAYA', '-', '-', '0', '2020-02-13 03:09:18', ''),
('RKA2020020063', 'AP. KIMIA FARMA DHARMAHUSADA', 'apotik', '-', '-', 'JL. DHARMAHUSADA SURABAYA', '-', '-', '0', '2020-02-13 03:12:42', ''),
('RKA2020020064', 'RS. MATA MASYARAKAT JAWA TIMUR', 'rs', '-', '-', 'JL. GAYUNG KEBONSARI TIMUR NO. 49 SURABAYA', '-', '-', '0', '2020-02-17 10:09:36', '');

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE `sales` (
  `id_sales` varchar(10) NOT NULL,
  `nama_sales` varchar(64) NOT NULL,
  `jk_sales` enum('0','1') NOT NULL,
  `alamat_sales` text NOT NULL,
  `tlp_sales` varchar(13) NOT NULL,
  `nik_sales` varchar(16) NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `time_update` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sales`
--

INSERT INTO `sales` (`id_sales`, `nama_sales`, `jk_sales`, `alamat_sales`, `tlp_sales`, `nik_sales`, `is_delete`, `time_update`, `id_admin`) VALUES
('SL20190001', 'Yance', '0', 'Surabaya', '081252377196', '-', '0', '2019-12-31 10:57:23', ''),
('SL20190002', 'Hari', '0', 'Surabaya', '082337692015', '-', '0', '2019-12-31 10:57:59', ''),
('SL20190003', 'Jefri', '0', 'Surabaya', '0895379573939', '-', '0', '2019-12-31 10:58:39', ''),
('SL20190004', 'tika', '1', 'mlg', '081331677575', '-', '1', '2019-12-31 11:04:03', ''),
('SL20200001', 'BLESSINDO', '0', 'WONOKITRI', '03145673150', '-', '0', '2020-02-12 01:18:06', '');

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `id_setting` int(11) NOT NULL,
  `jenis_setting` varchar(64) NOT NULL,
  `keterangan_setting` text NOT NULL,
  `is_del_setting` enum('0','1') NOT NULL,
  `admin_create_setting` varchar(12) NOT NULL,
  `time_update_setting` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`id_setting`, `jenis_setting`, `keterangan_setting`, `is_del_setting`, `admin_create_setting`, `time_update_setting`) VALUES
(1, 'npwp', '02.054.205.6-618.000', '0', 'AD2019110001', '2019-12-19 09:39:20'),
(2, 'siup', 'HK.07.01/V463/14', '0', 'AD2019110001', '2019-12-19 09:54:29');

-- --------------------------------------------------------

--
-- Table structure for table `suplier`
--

CREATE TABLE `suplier` (
  `id_suplier` varchar(13) NOT NULL,
  `nama_suplier` text NOT NULL,
  `jenis_suplier` text NOT NULL,
  `email_suplier` text NOT NULL,
  `tlp_suplier` varchar(13) NOT NULL,
  `alamat_ktr_suplier` text NOT NULL,
  `alamat_krm_suplier` text NOT NULL,
  `website` text NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `time_update` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suplier`
--

INSERT INTO `suplier` (`id_suplier`, `nama_suplier`, `jenis_suplier`, `email_suplier`, `tlp_suplier`, `alamat_ktr_suplier`, `alamat_krm_suplier`, `website`, `is_delete`, `time_update`, `id_admin`) VALUES
('SUP2019120001', 'dimas', 'pabrik', 'dimas@gmail.com', '08123111111', 'malang', 'malang', 'malang.com', '1', '2019-12-31 10:43:02', ''),
('SUP2019120002', 'PT. Tiga A', 'pabrik', 'tiga@gmail.com', '081331677575', 'malang', 'malang', 'tiga.com', '0', '2020-01-02 10:54:53', '');

-- --------------------------------------------------------

--
-- Table structure for table `tr_detail`
--

CREATE TABLE `tr_detail` (
  `id_tr_detail` varchar(19) NOT NULL,
  `id_tr_header` varchar(15) NOT NULL,
  `id_item` varchar(10) NOT NULL,
  `harga_satuan_tr_detail` varchar(32) NOT NULL,
  `jml_item_tr_detail` varchar(8) NOT NULL,
  `harga_total_tr_detail` varchar(32) NOT NULL,
  `disc_item_tr_detail` varchar(8) NOT NULL,
  `harga_total_fix_tr_detail` varchar(32) NOT NULL,
  `admin_create_tr_detail` varchar(12) NOT NULL,
  `time_up_tr_detail` datetime NOT NULL,
  `is_del_tr_detail` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_detail`
--

INSERT INTO `tr_detail` (`id_tr_detail`, `id_tr_header`, `id_item`, `harga_satuan_tr_detail`, `jml_item_tr_detail`, `harga_total_tr_detail`, `disc_item_tr_detail`, `harga_total_fix_tr_detail`, `admin_create_tr_detail`, `time_up_tr_detail`, `is_del_tr_detail`) VALUES
('TRH2020-0000001-001', 'TRH2020-0000001', '00100002', '11250', '10', '112500', '0', '112500', '', '2020-02-19 05:39:44', '0'),
('TRH2020-0000001-002', 'TRH2020-0000001', '00100004', '15125', '20', '302500', '0', '302500', '', '2020-02-19 05:39:44', '0'),
('TRH2020-0000002-001', 'TRH2020-0000002', '00100002', '11250', '10', '112500', '0', '112500', '', '2020-02-19 05:45:00', '0'),
('TRH2020-0000002-002', 'TRH2020-0000002', '00100004', '15125', '10', '151250', '0', '151250', '', '2020-02-19 05:45:00', '0'),
('TRH2020-0000003-001', 'TRH2020-0000003', '00100002', '11250', '30', '337500', '1', '334125', '', '2020-02-19 05:51:01', '0'),
('TRH2020-0000003-002', 'TRH2020-0000003', '00100004', '15125', '30', '453750', '0', '453750', '', '2020-02-19 05:51:01', '0'),
('TRH2020-0000004-001', 'TRH2020-0000004', '00100002', '11250', '50', '562500', '0', '562500', '', '2020-02-19 05:53:15', '0'),
('TRH2020-0000004-002', 'TRH2020-0000004', '00100004', '15125', '40', '605000', '0', '605000', '', '2020-02-19 05:53:15', '0'),
('TRH2020-0000005-001', 'TRH2020-0000005', '00100002', '11250', '20', '225000', '0', '225000', '', '2020-02-19 06:29:35', '0'),
('TRH2020-0000005-002', 'TRH2020-0000005', '00100004', '15125', '50', '756250', '0', '756250', '', '2020-02-19 06:29:35', '0'),
('TRH2020-0000005-003', 'TRH2020-0000005', '00100122', '37091', '10', '370910', '0', '370910', '', '2020-02-19 06:29:35', '0'),
('TRH2020-0000005-004', 'TRH2020-0000005', '00100126', '40182', '20', '803640', '0', '803640', '', '2020-02-19 06:29:35', '0');

-- --------------------------------------------------------

--
-- Table structure for table `tr_header`
--

CREATE TABLE `tr_header` (
  `id_tr_header` varchar(15) NOT NULL,
  `id_customer` varchar(13) NOT NULL,
  `id_sales` varchar(10) NOT NULL,
  `tgl_transaksi_tr_header` date NOT NULL,
  `cara_pembayaran_tr_header` enum('0','1') NOT NULL,
  `tempo_tr_header` date NOT NULL,
  `status_hutang` enum('0','1','2') NOT NULL,
  `total_pembayaran_tr_header` varchar(32) NOT NULL,
  `disc_all_tr_header` varchar(8) NOT NULL,
  `total_pembayaran_disc_tr_header` varchar(32) NOT NULL,
  `ppn_tr_header` varchar(8) NOT NULL,
  `total_pembayaran_pnn_tr_header` varchar(32) NOT NULL,
  `admin_create_tr_header` varchar(12) NOT NULL,
  `time_up_tr_header` datetime NOT NULL,
  `is_del_tr_header` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_header`
--

INSERT INTO `tr_header` (`id_tr_header`, `id_customer`, `id_sales`, `tgl_transaksi_tr_header`, `cara_pembayaran_tr_header`, `tempo_tr_header`, `status_hutang`, `total_pembayaran_tr_header`, `disc_all_tr_header`, `total_pembayaran_disc_tr_header`, `ppn_tr_header`, `total_pembayaran_pnn_tr_header`, `admin_create_tr_header`, `time_up_tr_header`, `is_del_tr_header`) VALUES
('TRH2020-0000001', 'RKA2020010001', 'SL20190001', '2020-02-19', '0', '2020-03-30', '0', '415000', '0', '415000', '10', '456500', '', '2020-02-19 05:39:44', '0'),
('TRH2020-0000002', 'RKA2020010001', 'SL20190001', '2020-02-19', '0', '2020-03-30', '0', '263750', '0', '263750', '10', '290125', '', '2020-02-19 05:45:00', '0'),
('TRH2020-0000003', 'RKA2020010001', 'SL20190001', '2020-02-19', '0', '2020-03-30', '0', '787875', '0', '787875', '10', '866662.5', '', '2020-02-19 05:51:01', '0'),
('TRH2020-0000004', 'RKA2020010001', 'SL20190001', '2020-02-19', '0', '2020-03-30', '0', '1167500', '10', '1050750', '10', '1155825', '', '2020-02-19 05:53:15', '0'),
('TRH2020-0000005', 'RKA2020010001', 'SL20190001', '2020-02-19', '0', '2020-03-30', '0', '2155800', '0', '2155800', '10', '2371380', '', '2020-02-19 06:29:35', '0');

-- --------------------------------------------------------

--
-- Table structure for table `tr_pb_detail`
--

CREATE TABLE `tr_pb_detail` (
  `id_tr_detail` varchar(19) NOT NULL,
  `id_tr_header` varchar(15) NOT NULL,
  `id_item` varchar(10) NOT NULL,
  `harga_satuan_tr_detail` varchar(32) NOT NULL,
  `jml_item_tr_detail` varchar(8) NOT NULL,
  `harga_total_tr_detail` varchar(32) NOT NULL,
  `disc_item_tr_detail` varchar(8) NOT NULL,
  `harga_total_fix_tr_detail` varchar(32) NOT NULL,
  `admin_create_tr_detail` varchar(12) NOT NULL,
  `time_up_tr_detail` datetime NOT NULL,
  `is_del_tr_detail` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_pb_detail`
--

INSERT INTO `tr_pb_detail` (`id_tr_detail`, `id_tr_header`, `id_item`, `harga_satuan_tr_detail`, `jml_item_tr_detail`, `harga_total_tr_detail`, `disc_item_tr_detail`, `harga_total_fix_tr_detail`, `admin_create_tr_detail`, `time_up_tr_detail`, `is_del_tr_detail`) VALUES
('TBH2020-0000001-001', 'TBH2020-0000001', '00100002', '11250', '1000', '11250000', '0', '11250000', 'AD2019110001', '2020-02-19 05:38:28', '0'),
('TBH2020-0000001-002', 'TBH2020-0000001', '00100004', '15125', '1000', '15125000', '0', '15125000', 'AD2019110001', '2020-02-19 05:38:28', '0'),
('TBH2020-0000002-001', 'TBH2020-0000002', '00100002', '11250', '1000', '11250000', '0', '11250000', 'AD2019110001', '2020-02-19 05:50:15', '0'),
('TBH2020-0000002-002', 'TBH2020-0000002', '00100004', '15125', '1000', '15125000', '0', '15125000', 'AD2019110001', '2020-02-19 05:50:15', '0'),
('TBH2020-0000003-001', 'TBH2020-0000003', '00100002', '11250', '2000', '22500000', '0', '22500000', 'AD2019110001', '2020-02-19 05:52:07', '0'),
('TBH2020-0000003-002', 'TBH2020-0000003', '00100004', '15125', '2000', '30250000', '0', '30250000', 'AD2019110001', '2020-02-19 05:52:07', '0'),
('TBH2020-0000004-001', 'TBH2020-0000004', '00100002', '11250', '1000', '11250000', '0', '11250000', 'AD2019110001', '2020-02-19 06:25:48', '0'),
('TBH2020-0000004-002', 'TBH2020-0000004', '00100004', '15125', '1000', '15125000', '0', '15125000', 'AD2019110001', '2020-02-19 06:25:48', '0'),
('TBH2020-0000004-003', 'TBH2020-0000004', '00100122', '37091', '1000', '37091000', '0', '37091000', 'AD2019110001', '2020-02-19 06:25:48', '0'),
('TBH2020-0000004-004', 'TBH2020-0000004', '00100126', '40182', '1000', '40182000', '0', '40182000', 'AD2019110001', '2020-02-19 06:25:48', '0');

-- --------------------------------------------------------

--
-- Table structure for table `tr_pb_header`
--

CREATE TABLE `tr_pb_header` (
  `id_tr_header` varchar(15) NOT NULL,
  `id_suplier` varchar(13) NOT NULL,
  `tgl_transaksi_tr_header` date NOT NULL,
  `cara_pembayaran_tr_header` enum('0','1') NOT NULL,
  `tempo_tr_header` date NOT NULL,
  `status_hutang` enum('0','1') NOT NULL,
  `total_pembayaran_tr_header` varchar(32) NOT NULL,
  `disc_all_tr_header` varchar(8) NOT NULL,
  `total_pembayaran_disc_tr_header` varchar(32) NOT NULL,
  `ppn_tr_header` varchar(8) NOT NULL,
  `total_pembayaran_pnn_tr_header` varchar(32) NOT NULL,
  `admin_create_tr_header` varchar(12) NOT NULL,
  `time_up_tr_header` datetime NOT NULL,
  `is_del_tr_header` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_pb_header`
--

INSERT INTO `tr_pb_header` (`id_tr_header`, `id_suplier`, `tgl_transaksi_tr_header`, `cara_pembayaran_tr_header`, `tempo_tr_header`, `status_hutang`, `total_pembayaran_tr_header`, `disc_all_tr_header`, `total_pembayaran_disc_tr_header`, `ppn_tr_header`, `total_pembayaran_pnn_tr_header`, `admin_create_tr_header`, `time_up_tr_header`, `is_del_tr_header`) VALUES
('TBH2020-0000001', 'SUP2019120002', '2020-02-19', '0', '2020-03-30', '0', '26375000', '10', '23737500', '10', '26111250', 'AD2019110001', '2020-02-19 05:38:28', '0'),
('TBH2020-0000002', 'SUP2019120002', '2020-02-19', '0', '2020-03-30', '0', '26375000', '0', '26375000', '10', '29012500', 'AD2019110001', '2020-02-19 05:50:15', '0'),
('TBH2020-0000003', 'SUP2019120002', '2020-02-19', '0', '2020-03-30', '0', '52750000', '0', '52750000', '10', '58025000', 'AD2019110001', '2020-02-19 05:52:07', '0'),
('TBH2020-0000004', 'SUP2019120002', '2020-02-19', '0', '2020-03-30', '0', '103648000', '0', '103648000', '10', '114012800', 'AD2019110001', '2020-02-19 06:25:48', '0');

-- --------------------------------------------------------

--
-- Table structure for table `tr_rt_pb_detail`
--

CREATE TABLE `tr_rt_pb_detail` (
  `id_tr_detail` varchar(19) NOT NULL,
  `id_tr_header` varchar(15) NOT NULL,
  `id_item` varchar(10) NOT NULL,
  `kode_produksi_tr_detail` text NOT NULL,
  `tgl_kadaluarsa_tr_detail` date NOT NULL,
  `harga_satuan_tr_detail` varchar(32) NOT NULL,
  `jml_item_tr_detail` varchar(8) NOT NULL,
  `harga_total_tr_detail` varchar(32) NOT NULL,
  `status_rt_tr_detail` enum('0','1') NOT NULL,
  `admin_create_tr_detail` varchar(12) NOT NULL,
  `time_up_tr_detail` datetime NOT NULL,
  `is_del_tr_detail` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_rt_pb_detail`
--

INSERT INTO `tr_rt_pb_detail` (`id_tr_detail`, `id_tr_header`, `id_item`, `kode_produksi_tr_detail`, `tgl_kadaluarsa_tr_detail`, `harga_satuan_tr_detail`, `jml_item_tr_detail`, `harga_total_tr_detail`, `status_rt_tr_detail`, `admin_create_tr_detail`, `time_up_tr_detail`, `is_del_tr_detail`) VALUES
('RBM2020-0000002-001', 'RBM2020-0000002', '00100002', '9A50916', '2022-02-09', '11250', '200', '2250000', '0', 'AD2019110001', '2020-02-20 11:28:45', '0'),
('RBM2020-0000002-002', 'RBM2020-0000002', '00100122', '8M0206', '2021-02-28', '37091', '300', '11127300', '0', 'AD2019110001', '2020-02-20 11:28:45', '0');

-- --------------------------------------------------------

--
-- Table structure for table `tr_rt_pb_detail_p`
--

CREATE TABLE `tr_rt_pb_detail_p` (
  `id_tr_detail` varchar(19) NOT NULL,
  `id_tr_header_p` varchar(15) NOT NULL,
  `id_item` varchar(10) NOT NULL,
  `kode_produksi_tr_detail` text NOT NULL,
  `tgl_kadaluarsa_tr_detail` date NOT NULL,
  `harga_satuan_tr_detail` varchar(32) NOT NULL,
  `jml_item_tr_detail` varchar(8) NOT NULL,
  `harga_total_tr_detail` varchar(32) NOT NULL,
  `admin_create_tr_detail` varchar(12) NOT NULL,
  `time_up_tr_detail` datetime NOT NULL,
  `is_del_tr_detail` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_rt_pb_detail_p`
--

INSERT INTO `tr_rt_pb_detail_p` (`id_tr_detail`, `id_tr_header_p`, `id_item`, `kode_produksi_tr_detail`, `tgl_kadaluarsa_tr_detail`, `harga_satuan_tr_detail`, `jml_item_tr_detail`, `harga_total_tr_detail`, `admin_create_tr_detail`, `time_up_tr_detail`, `is_del_tr_detail`) VALUES
('RBP2020-0000002-001', 'RBP2020-0000002', '00100004', '7CC1005', '2020-10-28', '15125', '200', '3025000', 'AD2019110001', '2020-02-20 11:28:45', '0'),
('RBP2020-0000002-002', 'RBP2020-0000002', '00100133', '9L1001', '2022-10-28', '17386', '100', '1738600', 'AD2019110001', '2020-02-20 11:28:45', '0');

-- --------------------------------------------------------

--
-- Table structure for table `tr_rt_pb_header`
--

CREATE TABLE `tr_rt_pb_header` (
  `id_tr_header` varchar(15) NOT NULL,
  `id_tr_header_p` varchar(15) NOT NULL,
  `status_retur` enum('0','1') NOT NULL,
  `selisih` int(11) NOT NULL,
  `id_suplier` varchar(13) NOT NULL,
  `tgl_transaksi_tr_header` date NOT NULL,
  `total_pembayaran_tr_header` varchar(32) NOT NULL,
  `admin_create_tr_header` varchar(12) NOT NULL,
  `time_up_tr_header` datetime NOT NULL,
  `is_del_tr_header` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_rt_pb_header`
--

INSERT INTO `tr_rt_pb_header` (`id_tr_header`, `id_tr_header_p`, `status_retur`, `selisih`, `id_suplier`, `tgl_transaksi_tr_header`, `total_pembayaran_tr_header`, `admin_create_tr_header`, `time_up_tr_header`, `is_del_tr_header`) VALUES
('RBM2020-0000002', 'RBP2020-0000002', '0', -8613700, 'SUP2019120002', '2020-02-20', '13377300', 'AD2019110001', '2020-02-20 11:28:45', '0');

-- --------------------------------------------------------

--
-- Table structure for table `tr_rt_pb_header_p`
--

CREATE TABLE `tr_rt_pb_header_p` (
  `id_tr_header_p` varchar(15) NOT NULL,
  `id_suplier` varchar(13) NOT NULL,
  `tgl_transaksi_tr_header` date NOT NULL,
  `total_pembayaran_tr_header` varchar(32) NOT NULL,
  `admin_create_tr_header` varchar(12) NOT NULL,
  `time_up_tr_header` datetime NOT NULL,
  `is_del_tr_header` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_rt_pb_header_p`
--

INSERT INTO `tr_rt_pb_header_p` (`id_tr_header_p`, `id_suplier`, `tgl_transaksi_tr_header`, `total_pembayaran_tr_header`, `admin_create_tr_header`, `time_up_tr_header`, `is_del_tr_header`) VALUES
('RBP2020-0000002', 'SUP2019120002', '2020-02-20', '4763600', 'AD2019110001', '2020-02-20 11:28:45', '0');

-- --------------------------------------------------------

--
-- Table structure for table `tr_rt_pj_detail`
--

CREATE TABLE `tr_rt_pj_detail` (
  `id_tr_detail` varchar(19) NOT NULL,
  `id_tr_header` varchar(15) NOT NULL,
  `id_item` varchar(10) NOT NULL,
  `kode_produksi_tr_detail` text NOT NULL,
  `tgl_kadaluarsa_tr_detail` date NOT NULL,
  `harga_satuan_tr_detail` varchar(32) NOT NULL,
  `jml_item_tr_detail` varchar(8) NOT NULL,
  `harga_total_tr_detail` varchar(32) NOT NULL,
  `status_rt_tr_detail` enum('0','1') NOT NULL,
  `admin_create_tr_detail` varchar(12) NOT NULL,
  `time_up_tr_detail` datetime NOT NULL,
  `is_del_tr_detail` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_rt_pj_detail`
--

INSERT INTO `tr_rt_pj_detail` (`id_tr_detail`, `id_tr_header`, `id_item`, `kode_produksi_tr_detail`, `tgl_kadaluarsa_tr_detail`, `harga_satuan_tr_detail`, `jml_item_tr_detail`, `harga_total_tr_detail`, `status_rt_tr_detail`, `admin_create_tr_detail`, `time_up_tr_detail`, `is_del_tr_detail`) VALUES
('RPM2020-0000001-001', 'RPM2020-0000001', '00100002', '9A50916', '2022-02-09', '11250', '50', '562500', '0', 'AD2019110001', '2020-02-20 06:09:40', '0'),
('RPM2020-0000001-002', 'RPM2020-0000001', '00100004', '7CC1005', '2020-10-28', '15125', '50', '756250', '0', 'AD2019110001', '2020-02-20 06:09:40', '0'),
('RPM2020-0000001-003', 'RPM2020-0000001', '00100122', '8M0206', '2021-02-28', '37091', '20', '741820', '0', 'AD2019110001', '2020-02-20 06:09:40', '0'),
('RPM2020-0000002-001', 'RPM2020-0000002', '00100002', '9A50916', '2022-02-09', '11250', '50', '562500', '0', 'AD2019110001', '2020-02-20 06:10:12', '0'),
('RPM2020-0000002-002', 'RPM2020-0000002', '00100004', '7CC1005', '2020-10-28', '15125', '50', '756250', '0', 'AD2019110001', '2020-02-20 06:10:12', '0'),
('RPM2020-0000002-003', 'RPM2020-0000002', '00100122', '8M0206', '2021-02-28', '37091', '100', '3709100', '1', 'AD2019110001', '2020-02-20 06:10:12', '0');

-- --------------------------------------------------------

--
-- Table structure for table `tr_rt_pj_detail_p`
--

CREATE TABLE `tr_rt_pj_detail_p` (
  `id_tr_detail` varchar(19) NOT NULL,
  `id_tr_header_p` varchar(15) NOT NULL,
  `id_item` varchar(10) NOT NULL,
  `kode_produksi_tr_detail` text NOT NULL,
  `tgl_kadaluarsa_tr_detail` date NOT NULL,
  `harga_satuan_tr_detail` varchar(32) NOT NULL,
  `jml_item_tr_detail` varchar(8) NOT NULL,
  `harga_total_tr_detail` varchar(32) NOT NULL,
  `admin_create_tr_detail` varchar(12) NOT NULL,
  `time_up_tr_detail` datetime NOT NULL,
  `is_del_tr_detail` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_rt_pj_detail_p`
--

INSERT INTO `tr_rt_pj_detail_p` (`id_tr_detail`, `id_tr_header_p`, `id_item`, `kode_produksi_tr_detail`, `tgl_kadaluarsa_tr_detail`, `harga_satuan_tr_detail`, `jml_item_tr_detail`, `harga_total_tr_detail`, `admin_create_tr_detail`, `time_up_tr_detail`, `is_del_tr_detail`) VALUES
('RPP2020-0000001-001', 'RPP2020-0000001', '00100126', '9N60524', '2022-05-28', '40182', '50', '2009100', 'AD2019110001', '2020-02-20 06:09:40', '0'),
('RPP2020-0000002-001', 'RPP2020-0000002', '00100122', '8M0206', '2021-02-28', '37091', '200', '7418200', 'AD2019110001', '2020-02-20 06:10:12', '0');

-- --------------------------------------------------------

--
-- Table structure for table `tr_rt_pj_header`
--

CREATE TABLE `tr_rt_pj_header` (
  `id_tr_header` varchar(15) NOT NULL,
  `id_tr_header_p` varchar(15) NOT NULL,
  `selisih` varchar(64) NOT NULL,
  `id_customer` varchar(13) NOT NULL,
  `tgl_transaksi_tr_header` date NOT NULL,
  `total_pembayaran_tr_header` varchar(32) NOT NULL,
  `admin_create_tr_header` varchar(12) NOT NULL,
  `time_up_tr_header` datetime NOT NULL,
  `is_del_tr_header` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_rt_pj_header`
--

INSERT INTO `tr_rt_pj_header` (`id_tr_header`, `id_tr_header_p`, `selisih`, `id_customer`, `tgl_transaksi_tr_header`, `total_pembayaran_tr_header`, `admin_create_tr_header`, `time_up_tr_header`, `is_del_tr_header`) VALUES
('RPM2020-0000001', 'RPP2020-0000001', '-51470', 'RKA2020010001', '2020-02-19', '2060570', 'AD2019110001', '2020-02-20 06:09:40', '0'),
('RPM2020-0000002', 'RPP2020-0000002', '2390350', 'RKA2020010001', '2020-02-20', '5027850', 'AD2019110001', '2020-02-20 06:10:12', '0');

-- --------------------------------------------------------

--
-- Table structure for table `tr_rt_pj_header_p`
--

CREATE TABLE `tr_rt_pj_header_p` (
  `id_tr_header_p` varchar(15) NOT NULL,
  `id_customer` varchar(13) NOT NULL,
  `tgl_transaksi_tr_header` date NOT NULL,
  `total_pembayaran_tr_header` varchar(32) NOT NULL,
  `admin_create_tr_header` varchar(12) NOT NULL,
  `time_up_tr_header` datetime NOT NULL,
  `is_del_tr_header` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_rt_pj_header_p`
--

INSERT INTO `tr_rt_pj_header_p` (`id_tr_header_p`, `id_customer`, `tgl_transaksi_tr_header`, `total_pembayaran_tr_header`, `admin_create_tr_header`, `time_up_tr_header`, `is_del_tr_header`) VALUES
('RPP2020-0000001', 'RKA2020010001', '2020-02-19', '2009100', 'AD2019110001', '2020-02-20 06:09:40', '0'),
('RPP2020-0000002', 'RKA2020010001', '2020-02-20', '7418200', 'AD2019110001', '2020-02-20 06:10:12', '0');

-- --------------------------------------------------------

--
-- Table structure for table `tr_sm_detail`
--

CREATE TABLE `tr_sm_detail` (
  `id_tr_detail` varchar(19) NOT NULL,
  `id_tr_header` varchar(15) NOT NULL,
  `id_item` varchar(10) NOT NULL,
  `kode_produksi_tr_detail` text NOT NULL,
  `tgl_kadaluarsa_tr_detail` date NOT NULL,
  `harga_satuan_tr_detail` varchar(32) NOT NULL,
  `jml_item_tr_detail` varchar(8) NOT NULL,
  `harga_total_tr_detail` varchar(32) NOT NULL,
  `admin_create_tr_detail` varchar(12) NOT NULL,
  `time_up_tr_detail` datetime NOT NULL,
  `is_del_tr_detail` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_sm_detail`
--

INSERT INTO `tr_sm_detail` (`id_tr_detail`, `id_tr_header`, `id_item`, `kode_produksi_tr_detail`, `tgl_kadaluarsa_tr_detail`, `harga_satuan_tr_detail`, `jml_item_tr_detail`, `harga_total_tr_detail`, `admin_create_tr_detail`, `time_up_tr_detail`, `is_del_tr_detail`) VALUES
('TSH2020-0000001-001', 'TSH2020-0000001', '00100002', '9A50916', '2022-02-09', '11250', '10', '112500', '', '2020-02-19 05:54:51', '0'),
('TSH2020-0000001-002', 'TSH2020-0000001', '00100004', '7CC1005', '2020-10-28', '15125', '10', '151250', '', '2020-02-19 05:54:51', '0'),
('TSH2020-0000002-001', 'TSH2020-0000002', '00100002', '9A50916', '2022-02-09', '11250', '20', '225000', '', '2020-02-19 05:55:36', '0'),
('TSH2020-0000002-002', 'TSH2020-0000002', '00100004', '7CC1005', '2020-10-28', '15125', '20', '302500', '', '2020-02-19 05:55:36', '0'),
('TSH2020-0000003-002', 'TSH2020-0000003', '00100002', '9A50916', '2022-02-09', '11250', '20', '225000', '', '2020-02-19 05:59:55', '0'),
('TSH2020-0000003-003', 'TSH2020-0000003', '00100004', '7CC1005', '2020-10-28', '15125', '20', '302500', '', '2020-02-19 05:59:55', '0'),
('TSH2020-0000004-001', 'TSH2020-0000004', '00100002', '9A50916', '2022-02-09', '11250', '50', '562500', '', '2020-02-19 06:00:57', '0'),
('TSH2020-0000004-002', 'TSH2020-0000004', '00100004', '7CC1005', '2020-10-28', '15125', '50', '756250', '', '2020-02-19 06:00:57', '0'),
('TSH2020-0000005-001', 'TSH2020-0000005', '00100002', '9A50916', '2022-02-09', '11250', '20', '225000', '', '2020-02-19 06:18:34', '0'),
('TSH2020-0000005-002', 'TSH2020-0000005', '00100004', '7CC1005', '2020-10-28', '15125', '20', '302500', '', '2020-02-19 06:18:34', '0'),
('TSH2020-0000006-001', 'TSH2020-0000006', '00100002', '9A50916', '2022-02-09', '11250', '10', '112500', '', '2020-02-19 06:31:45', '0'),
('TSH2020-0000006-002', 'TSH2020-0000006', '00100004', '7CC1005', '2020-10-28', '15125', '50', '756250', '', '2020-02-19 06:31:45', '0'),
('TSH2020-0000006-003', 'TSH2020-0000006', '00100122', '8M0206', '2021-02-28', '37091', '10', '370910', '', '2020-02-19 06:31:45', '0'),
('TSH2020-0000006-004', 'TSH2020-0000006', '00100126', '9N60524', '2022-05-28', '40182', '30', '1205460', '', '2020-02-19 06:31:45', '0');

-- --------------------------------------------------------

--
-- Table structure for table `tr_sm_header`
--

CREATE TABLE `tr_sm_header` (
  `id_tr_header` varchar(15) NOT NULL,
  `id_tr_sm_retur_header` varchar(15) NOT NULL,
  `selisih_sm` varchar(64) NOT NULL,
  `id_customer` varchar(13) NOT NULL,
  `atas_nama` text NOT NULL,
  `tgl_transaksi_tr_header` date NOT NULL,
  `total_pembayaran_tr_header` varchar(32) NOT NULL,
  `status_ganti_tr_header` enum('0','1','2') NOT NULL,
  `admin_create_tr_header` varchar(12) NOT NULL,
  `time_up_tr_header` datetime NOT NULL,
  `is_del_tr_header` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_sm_header`
--

INSERT INTO `tr_sm_header` (`id_tr_header`, `id_tr_sm_retur_header`, `selisih_sm`, `id_customer`, `atas_nama`, `tgl_transaksi_tr_header`, `total_pembayaran_tr_header`, `status_ganti_tr_header`, `admin_create_tr_header`, `time_up_tr_header`, `is_del_tr_header`) VALUES
('TSH2020-0000001', '0', '0', 'RKA2020010001', 'dimas', '2020-02-19', '263750', '0', '', '2020-02-19 05:54:51', '0'),
('TSH2020-0000002', '0', '0', 'RKA2020010001', 'donal', '2020-02-19', '527500', '0', '', '2020-02-19 05:55:36', '0'),
('TSH2020-0000003', 'RSP2020-0000003', '341800', 'RKA2020010001', 'fanda', '2020-02-19', '527500', '1', '', '2020-02-19 05:59:55', '0'),
('TSH2020-0000004', 'RSP2020-0000002', '-791250', 'RKA2020010001', 'fiak', '2020-02-19', '1318750', '1', '', '2020-02-19 06:00:57', '0'),
('TSH2020-0000005', 'RSP2020-0000001', '-5920', 'RKA2020010001', 'fanda', '2020-02-19', '527500', '1', '', '2020-02-19 06:18:34', '0'),
('TSH2020-0000006', 'RSP2020-0000004', '4359380', 'RKA2020010001', 'lakmi', '2020-02-19', '2445120', '1', '', '2020-02-19 06:31:45', '0');

-- --------------------------------------------------------

--
-- Table structure for table `tr_sm_rt_detail_p`
--

CREATE TABLE `tr_sm_rt_detail_p` (
  `id_tr_detail` varchar(19) NOT NULL,
  `id_tr_header_p` varchar(15) NOT NULL,
  `id_item` varchar(10) NOT NULL,
  `kode_produksi_tr_detail` text NOT NULL,
  `tgl_kadaluarsa_tr_detail` date NOT NULL,
  `harga_satuan_tr_detail` varchar(32) NOT NULL,
  `jml_item_tr_detail` varchar(8) NOT NULL,
  `harga_total_tr_detail` varchar(32) NOT NULL,
  `admin_create_tr_detail` varchar(12) NOT NULL,
  `time_up_tr_detail` datetime NOT NULL,
  `is_del_tr_detail` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_sm_rt_detail_p`
--

INSERT INTO `tr_sm_rt_detail_p` (`id_tr_detail`, `id_tr_header_p`, `id_item`, `kode_produksi_tr_detail`, `tgl_kadaluarsa_tr_detail`, `harga_satuan_tr_detail`, `jml_item_tr_detail`, `harga_total_tr_detail`, `admin_create_tr_detail`, `time_up_tr_detail`, `is_del_tr_detail`) VALUES
('RSP2020-0000001-001', 'RSP2020-0000001', '00100133', '9L1001', '2022-10-28', '17386', '30', '521580', '', '2020-02-19 06:20:12', '0'),
('RSP2020-0000002-001', 'RSP2020-0000002', '00100002', '9A50916', '2022-02-09', '11250', '20', '225000', '', '2020-02-19 06:20:50', '0'),
('RSP2020-0000002-002', 'RSP2020-0000002', '00100004', '7CC1005', '2020-10-28', '15125', '20', '302500', '', '2020-02-19 06:20:50', '0'),
('RSP2020-0000003-001', 'RSP2020-0000003', '00100133', '9L1001', '2022-10-28', '17386', '50', '869300', '', '2020-02-19 06:22:21', '0'),
('RSP2020-0000004-001', 'RSP2020-0000004', '00100002', '9A50916', '2022-02-09', '11250', '30', '337500', '', '2020-02-19 06:38:14', '0'),
('RSP2020-0000004-002', 'RSP2020-0000004', '00100004', '7CC1005', '2020-10-28', '15125', '100', '1512500', '', '2020-02-19 06:38:14', '0'),
('RSP2020-0000004-003', 'RSP2020-0000004', '00100127', '9PO60125', '2022-05-28', '49545', '100', '4954500', '', '2020-02-19 06:38:14', '0');

-- --------------------------------------------------------

--
-- Table structure for table `tr_sm_rt_header_p`
--

CREATE TABLE `tr_sm_rt_header_p` (
  `id_tr_header_p` varchar(15) NOT NULL,
  `tgl_transaksi_tr_header` date NOT NULL,
  `total_pembayaran_tr_header` varchar(32) NOT NULL,
  `admin_create_tr_header` varchar(12) NOT NULL,
  `time_up_tr_header` datetime NOT NULL,
  `is_del_tr_header` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_sm_rt_header_p`
--

INSERT INTO `tr_sm_rt_header_p` (`id_tr_header_p`, `tgl_transaksi_tr_header`, `total_pembayaran_tr_header`, `admin_create_tr_header`, `time_up_tr_header`, `is_del_tr_header`) VALUES
('RSP2020-0000001', '2020-02-19', '521580', '', '2020-02-19 06:20:12', '0'),
('RSP2020-0000002', '2020-02-19', '527500', '', '2020-02-19 06:20:50', '0'),
('RSP2020-0000003', '2020-02-19', '869300', '', '2020-02-19 06:22:21', '0'),
('RSP2020-0000004', '2020-02-19', '6804500', '', '2020-02-19 06:38:14', '0');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `brand`
--
ALTER TABLE `brand`
  ADD PRIMARY KEY (`id_brand`);

--
-- Indexes for table `item`
--
ALTER TABLE `item`
  ADD PRIMARY KEY (`id_item`);

--
-- Indexes for table `record_item`
--
ALTER TABLE `record_item`
  ADD PRIMARY KEY (`id_record_item`);

--
-- Indexes for table `record_stok`
--
ALTER TABLE `record_stok`
  ADD PRIMARY KEY (`id_record`);

--
-- Indexes for table `record_stok_opnam`
--
ALTER TABLE `record_stok_opnam`
  ADD PRIMARY KEY (`id_record`);

--
-- Indexes for table `rekanan`
--
ALTER TABLE `rekanan`
  ADD PRIMARY KEY (`id_rekanan`);

--
-- Indexes for table `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`id_sales`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id_setting`);

--
-- Indexes for table `suplier`
--
ALTER TABLE `suplier`
  ADD PRIMARY KEY (`id_suplier`);

--
-- Indexes for table `tr_detail`
--
ALTER TABLE `tr_detail`
  ADD PRIMARY KEY (`id_tr_detail`);

--
-- Indexes for table `tr_header`
--
ALTER TABLE `tr_header`
  ADD PRIMARY KEY (`id_tr_header`);

--
-- Indexes for table `tr_pb_detail`
--
ALTER TABLE `tr_pb_detail`
  ADD PRIMARY KEY (`id_tr_detail`);

--
-- Indexes for table `tr_pb_header`
--
ALTER TABLE `tr_pb_header`
  ADD PRIMARY KEY (`id_tr_header`);

--
-- Indexes for table `tr_rt_pb_detail`
--
ALTER TABLE `tr_rt_pb_detail`
  ADD PRIMARY KEY (`id_tr_detail`);

--
-- Indexes for table `tr_rt_pb_detail_p`
--
ALTER TABLE `tr_rt_pb_detail_p`
  ADD PRIMARY KEY (`id_tr_detail`);

--
-- Indexes for table `tr_rt_pb_header`
--
ALTER TABLE `tr_rt_pb_header`
  ADD PRIMARY KEY (`id_tr_header`);

--
-- Indexes for table `tr_rt_pb_header_p`
--
ALTER TABLE `tr_rt_pb_header_p`
  ADD PRIMARY KEY (`id_tr_header_p`);

--
-- Indexes for table `tr_rt_pj_detail`
--
ALTER TABLE `tr_rt_pj_detail`
  ADD PRIMARY KEY (`id_tr_detail`);

--
-- Indexes for table `tr_rt_pj_detail_p`
--
ALTER TABLE `tr_rt_pj_detail_p`
  ADD PRIMARY KEY (`id_tr_detail`);

--
-- Indexes for table `tr_rt_pj_header`
--
ALTER TABLE `tr_rt_pj_header`
  ADD PRIMARY KEY (`id_tr_header`);

--
-- Indexes for table `tr_rt_pj_header_p`
--
ALTER TABLE `tr_rt_pj_header_p`
  ADD PRIMARY KEY (`id_tr_header_p`);

--
-- Indexes for table `tr_sm_detail`
--
ALTER TABLE `tr_sm_detail`
  ADD PRIMARY KEY (`id_tr_detail`);

--
-- Indexes for table `tr_sm_header`
--
ALTER TABLE `tr_sm_header`
  ADD PRIMARY KEY (`id_tr_header`);

--
-- Indexes for table `tr_sm_rt_detail_p`
--
ALTER TABLE `tr_sm_rt_detail_p`
  ADD PRIMARY KEY (`id_tr_detail`);

--
-- Indexes for table `tr_sm_rt_header_p`
--
ALTER TABLE `tr_sm_rt_header_p`
  ADD PRIMARY KEY (`id_tr_header_p`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `brand`
--
ALTER TABLE `brand`
  MODIFY `id_brand` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `id_setting` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
